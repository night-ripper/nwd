import os
import subprocess

class project:
	static_libraries = {}
	
	main_env = None
	source_dir = os.path.join("src", "main", "cpp")
	test_source_dir = os.path.join("src", "test", "cpp")
	target_dir = None
	test_dir = None
	separator = "."
	
	def init(main_env):
		project.main_env = main_env
		
		# Put all libraries in a link group in case of cyclic dependencies
		project.main_env["_LIBFLAGS"] = "-Wl,--start-group " + main_env["_LIBFLAGS"] + " -Wl,--end-group"
		
		# Determine the target directory based on the compiler architecture output
		project.target_dir = os.path.join("target", subprocess.check_output(
			[project.main_env["CXX"], "-dumpmachine"], encoding = "utf-8",
		)[:-1])
		
		project.main_env.AppendUnique(CPPPATH = [project.source_dir])
		
		project.test_dir = os.path.join(project.target_dir, "tests")
		
		# scons clean command
		project.main_env.Clean(".", project.target_dir)
		
		# The default decider always reads file content, this one saves the
		# computation if the timestamp has not changed
		project.main_env.Decider("content-timestamp")
		
		project.main_env.Default(os.path.join(project.target_dir, "executables"))
		
		project.main_env.Alias("test", os.path.join(project.test_dir, "results"))
	
	def _object_file(source_dir, target_dir, env, source):
		object_name = os.path.relpath(source, source_dir)
		object_name = object_name[:object_name.rindex(".")].replace(os.path.sep, project.separator) + project.main_env["OBJSUFFIX"]
		return env.Object(os.path.join(target_dir, "object_files", object_name), source)
	
	def object_file(env, source):
		return project._object_file(project.source_dir, project.target_dir, env, source)
	
	def static_library(static_library_name, *sources, dependencies = [], **kwargs):
		target_dir = project.target_dir
		env = project.main_env.Clone()
		env.MergeFlags(kwargs)
		if len(dependencies) > 0:
			env.MergeFlags(" ".join(["!" + project.main_env["PKG_CONFIG"], "--cflags", "--libs", *dependencies]))
		objects = [project.object_file(env, os.path.join(project.source_dir, source_name)) for source_name in sources]
		project.static_libraries[static_library_name] = env
		return env.StaticLibrary(os.path.join(target_dir, "static_libraries", static_library_name.replace(os.path.sep, project.separator)), objects)
	
	def _executable(source_dir, target_dir, executable_name, source_name, **kwargs):
		env = project.main_env.Clone()
		for linked in kwargs.pop("link_with", []):
			env.AppendUnique(LIBS = linked)
			if linked in project.static_libraries:
				env.AppendUnique(LIBPATH = os.path.join(project.target_dir, "static_libraries"))
				env.AppendUnique(LIBS = project.static_libraries[linked]["LIBS"])
		dependencies = kwargs.pop("dependencies", [])
		if len(dependencies) > 0:
			env.MergeFlags(" ".join(["!" + project.main_env["PKG_CONFIG"], "--cflags", "--libs", *dependencies]))
		env.MergeFlags(kwargs)
		return env.Program(os.path.join(target_dir, "executables", executable_name + (project.main_env["PROGSUFFIX"] or ".bin")),
			kwargs.pop("object_files", []) + project._object_file(source_dir, target_dir, env, os.path.join(source_dir, source_name)),
		)
		
	def executable(executable_name, source_name, **kwargs):
		return project._executable(project.source_dir, project.target_dir, executable_name, source_name, **kwargs)
	
	def test_executable(source_name, **kwargs):
		test_main_object = project.object_file(project.main_env,
			os.path.join(project.source_dir, "testing", "testing.cpp")
		)
		executable_name = source_name[:source_name.rindex(".")].replace(os.path.sep, project.separator)
		result = project._executable(project.test_source_dir, project.test_dir, executable_name, source_name, object_files = test_main_object, **kwargs)
		test_result = os.path.join(project.test_dir, "results", executable_name.replace(os.path.sep, project.separator)) + ".result"
		project.main_env.Command(test_result, result, project.main_env.Action([
			os.path.join(".", "${SOURCE}"),
			"touch ${TARGET}",
		], None))
		return result
