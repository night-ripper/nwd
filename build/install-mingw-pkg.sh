#!/bin/bash

set -e

# installdir="/usr/x86_64-w64-mingw32/sys-root/mingw"
download_dir="."

function get {
    curl -LO "https://mirror.msys2.org/mingw/mingw64/${1}"
    tar -C "${download_dir}" --exclude .PKGINFO --exclude .INSTALL --exclude .MTREE --exclude .BUILDINFO -xf ${1}
}

get "mingw-w64-x86_64-glfw-3.3.7-1-any.pkg.tar.zst"
get "mingw-w64-x86_64-libvorbis-1.3.7-1-any.pkg.tar.zst"
get "mingw-w64-x86_64-libsodium-1.0.18-2-any.pkg.tar.zst"
get "mingw-w64-x86_64-libogg-1.3.5-1-any.pkg.tar.zst"
get "mingw-w64-x86_64-libevent-2.1.12-5-any.pkg.tar.zst"

mv 'mingw64' 'mingw'

for file in mingw/lib/pkgconfig/*.pc; do
    sed -i 's/\/mingw64/\//' "${file}"
done

cp -r -t '/usr/x86_64-w64-mingw32/sys-root/' 'mingw'
