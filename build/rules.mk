MAKEFLAGS += -r

, := ,

define Dependency_file
$(addprefix $(build_dir)/dependencies/,$(addsuffix .mk,$(subst /,.,$(basename $(1)))))
endef

define Object_file
$(addprefix $(build_dir)/object_files/,$(addsuffix .o,$(subst /,.,$(basename $(1)))))
endef

define Static_library
$(addprefix $(build_dir)/static_libraries/,$(addprefix lib~,$(addsuffix .a,$(basename $(1)))))
endef

link_static_libraries = -L$(build_dir)/static_libraries

define Executable_file
$(addprefix $(build_dir)/executables/,$(addsuffix .bin,$(subst /,.,$(basename $(1)))))
endef

define Test_executable_file
$(addprefix $(build_dir)/test_executables/,$(addsuffix .bin,$(subst /,.,$(basename $(1)))))
endef

define Test_result_file
$(addprefix $(build_dir)/test_results/,$(addsuffix .result,$(subst /,.,$(basename $(1)))))
endef

define Variable_rule # target_file, string_value
$(1): force
	@mkdir -p $$(dir $$@)
	@echo '$(2)' | cmp -s - $$@ || echo '$(2)' > $$@
endef

define Dependency_file_rule # source_file, cxxflags...
$(call Dependency_file,$(1)): $(source_dir)/$(1) $(build_dir)/compile_flags
	@mkdir -p $$(dir $$@)
	$$(CXX) $$(CXXFLAGS) $(2) -MM -MG -MMD -MP -MF $$@ -MT $(call Object_file,$(1)) -o /dev/null $$<
endef

define _Object_file_rule # source_file, cxxflags...
$(call Dependency_file_rule,$(1),$(2))
$(call Object_file,$(1)): $(call Dependency_file,$(1))
	@mkdir -p $$(dir $$@)
	$$(CXX) $$(CXXFLAGS) $(2) -MMD -MP -MF $$< -MT $$@ -c -o $$@ $(addprefix $(source_dir)/,$(1))
endef

define Object_file_rule # source_file, cxxflags...
$(call _Object_file_rule,$(addprefix main/cpp/,$(1)),$(2))
endef

define Static_library_rule # static_library_name, source_files..., dependencies..., libs...
$(foreach source_file,$(2),
$(call Object_file_rule,$(source_file),$(if $(3),$(shell pkg-config --cflags $(3))),$(4)))
$(call Static_library,$(1)): $(call Object_file,$(addprefix main/cpp/,$(2)))
	@mkdir -p $$(dir $$@)
	$(AR) rcs $$@ $$^
$(1)~link_flags = -l~$(1) $(if $(3),$(shell pkg-config --libs $(3))) $(4)
endef

define Executable_file_rule # executable, source_files..., static_libraries...
$(foreach source_file,$(2),
$(call Object_file_rule,$(source_file)))
$(call Executable_file,$(1)): $(call Object_file,$(addprefix main/cpp/,$(2))) $(call Static_library,$(3)) $(build_dir)/link_flags
	@mkdir -p $$(dir $$@)
	$$(CXX) $$(LDFLAGS) $(if $(3),$(link_static_libraries)) $$(filter-out -l%,$(foreach e,$(addsuffix ~link_flags,$(3)),$$($(e)))) -o $$@ $(call Object_file,$(addprefix main/cpp/,$(2))) -Wl,--start-group $$(filter -l%,$(foreach e,$(addsuffix ~link_flags,$(3)),$$($(e)))) -Wl,--end-group
executables += $(call Executable_file,$(1))
endef

define Test_executable_file_rule # source_file, static_libraries...
$(call _Object_file_rule,$(addprefix test/cpp/,$(1)))
$(call Test_executable_file,$(1)): $(call Object_file,$(addprefix test/cpp/,$(1))) $(call Static_library,testing) $(call Static_library,$(2)) $(build_dir)/link_flags
	@mkdir -p $$(dir $$@)
	$$(CXX) $$(LDFLAGS) $(if $(2),$(link_static_libraries)) $$(filter-out -l%,$(foreach e,$(addsuffix ~link_flags,$(2)),$$($(e)))) -o $$@ $$< $(call Static_library,testing) -Wl,--start-group $$(filter -l%,$(foreach e,$(addsuffix ~link_flags,$(2)),$$($(e)))) -Wl,--end-group
test_executables += $(call Test_executable_file,$(1))
$(call Test_result_file,$(1)): $(call Test_executable_file,$(1))
	@mkdir -p $$(dir $$@)
	@./$$<
	@touch $$@
test_results += $(call Test_result_file,$(1))
endef
