import os

import SCons

_mingw = os.environ.get("MINGW") is not None
_clang = os.environ.get("CXX") == "clang++"

cxx_flags = [
	"-std=c++2b",
	"-fdiagnostics-color",
	
	"-pthread",
	
	"-ffast-math",
	"-fpie",
	
	"-fexceptions",
]

cxx_warnings = [
	"-Wall",
	"-Wextra",
	"-Wpedantic",
	"-Wdeprecated",
	"" if _clang else "-Wduplicated-cond",
	"-Wcast-align",
	"-Wcast-qual",
	"-Wmisleading-indentation",
	"-Wconversion",
	
	"-fdiagnostics-show-template-tree",
	"" if _clang else "-fconcepts-diagnostics-depth=3",
]

cxx_flags_debug = [
	"-O0",
	"-g",
	"-D_GLIBCXX_DEBUG",
	"-D_GLIBCXX_ASSERTIONS",
	"-fasynchronous-unwind-tables",
	"-fstack-clash-protection",
	"-fcf-protection",
	
	"" if _clang else "-fsanitize-address-use-after-scope",
]

cxx_flags_release = [
	"-O2",
	"-D_FORTIFY_SOURCE=2",
	"-D_GLIBCXX_ASSERTIONS",
	"-ftree-vectorize",
	
	"-flto",
	"-fuse-linker-plugin",
	"-ffat-lto-objects",
]

# TODO
_env_flags = {
	"CC": "gcc",
	"CXX": "g++",
	"AR": "ar",
	"RANLIB": "ranlib",
	"STRIP": "strip",
	"PKG_CONFIG": "pkg-config",
}

if _mingw:
	for key, value in _env_flags.items():
		_env_flags[key] = "x86_64-w64-mingw32-" + value
	_env_flags["PROGSUFFIX"] = ".exe",

project.init(SCons.Environment.Environment(
	ENV = os.environ.copy(),
	CC = os.environ.get("CC", "gcc"),
	CXX = os.environ.get("CXX", "g++"),
	PKG_CONFIG = os.environ.get("PKG_CONFIG", "pkg-config"),
	CXXFLAGS = os.environ.get("CXXFLAGS", cxx_flags + cxx_warnings + cxx_flags_debug),
	LIBS = [],
	LIBPATH = [],
	LINKFLAGS = os.environ.get("LDFLAGS", [
		"-fuse-ld=mold",
		"-pthread",
		"" if _clang else "-fsanitize=address,undefined",
		"-fpie",
		"-Wl,-z,defs",
		"-Wl,-z,now",
		"-Wl,-z,relro",
	]),
))

if not _mingw and not _clang:
	project.main_env["CXXFLAGS"] += ["-fsanitize=" + ",".join([
		# "thread",
		"address",
		"undefined",
		
		"shift",
		"shift-exponent",
		"shift-base",
		"integer-divide-by-zero",
		"unreachable",
		"vla-bound",
		"null",
		"return",
		"signed-integer-overflow",
		"bounds",
		"bounds-strict",
		"alignment",
		"object-size",
		"float-divide-by-zero",
		"float-cast-overflow",
		"nonnull-attribute",
		"returns-nonnull-attribute",
		"bool",
		"enum",
		"vptr",
		"pointer-overflow",
		"builtin",
	])]

################################################################################

project.static_library("~nwd~audio",
	os.path.join("nwd", "audio", "openal.cpp"),
	os.path.join("nwd", "audio", "vorbis.cpp"),
	os.path.join("nwd", "audio", "wav.cpp"),
	dependencies = ["openal", "vorbisfile"],
)

project.static_library("~nwd~crypto",
	os.path.join("nwd", "crypto", "hash.cpp"),
	os.path.join("nwd", "crypto", "keypair.cpp"),
	os.path.join("nwd", "crypto", "shared_key.cpp"),
	dependencies = ["libsodium"],
)

project.static_library("~nwd~databases",
	os.path.join("nwd", "databases", "sqlite.cpp"),
	dependencies = ["sqlite3"],
)

project.static_library("~nwd~geometry",
	os.path.join("nwd", "geometry", "camera.cpp"),
	os.path.join("nwd", "geometry", "common.cpp"),
	os.path.join("nwd", "geometry", "visibility_frustum.cpp"),
)

project.static_library("~nwd~graphics",
	os.path.join("nwd", "graphics", "opengl", "buffers.cpp"),
	os.path.join("nwd", "graphics", "opengl", "program.cpp"),
	os.path.join("nwd", "graphics", "opengl", "textures.cpp"),
	os.path.join("nwd", "graphics", "opengl", "type_enum.cpp"),
	os.path.join("nwd", "graphics", "freetype.cpp"),
	os.path.join("nwd", "graphics", "glfw.cpp"),
	os.path.join("nwd", "graphics", "glfw_constants.cpp"),
	os.path.join("nwd", "graphics", "png.cpp"),
	os.path.join("nwd", "graphics", "wavefront_obj.cpp"),
	dependencies = ["epoxy", "freetype2", "glfw3", "libpng"],
)

project.static_library("~nwd~networking",
	os.path.join("nwd", "networking", "socket.cpp"),
	os.path.join("nwd", "networking", "event.cpp"),
	os.path.join("nwd", "networking", "messages", "from_client.cpp"),
	os.path.join("nwd", "networking", "messages", "from_server.cpp"),
	dependencies = ["libevent", "libevent_pthreads", "sqlite3"],
)

project.static_library("~nwd~platform",
	os.path.join("nwd", "platform", "shared_object.cpp"),
	LIBS = ["dl"],
)

################################################################################

project.executable("server", os.path.join("nwd", "server", "main.cpp"),
	link_with = ["~nwd~databases", "~nwd~crypto", "~nwd~networking", "~nwd~audio"],
)

project.executable("client", os.path.join("nwd", "client", "main.cpp"),
	link_with = ["~nwd~databases", "~nwd~crypto", "~nwd~networking"],
)

project.executable("gui", os.path.join("nwd", "client", "gui.cpp"),
	link_with = ["~nwd~graphics"],
)

project.executable("capture", os.path.join("nwd", "capture.cpp"),
	link_with = ["~nwd~audio", "~nwd~graphics"],
)

# project.executable("udp_forwarder", os.path.join("nwd", "networking", "udp_forwarder.cpp"),
# 	link_with = ["~nwd~networking"],
# )
# 
# project.executable("udp_sender", os.path.join("nwd", "networking", "udp_sender.cpp"),
# 	link_with = ["~nwd~networking"],
# )
# 
# project.executable("udp_printer", os.path.join("nwd", "networking", "udp_printer.cpp"),
# 	link_with = ["~nwd~networking"],
# )

project.executable("qt", os.path.join("nwd", "application", "qt.cpp"),
	dependencies = ["Qt5Widgets"],
	link_with = ["~nwd~audio", "~nwd~graphics"],
)

################################################################################

project.test_executable(os.path.join("nwd", "audio", "openal.cpp"),
	link_with = ["~nwd~audio"],
)

project.test_executable(os.path.join("nwd", "audio", "vorbis.cpp"),
	link_with = ["~nwd~audio"],
)

project.test_executable(os.path.join("nwd", "audio", "wav.cpp"),
	link_with = ["~nwd~audio"],
)

project.test_executable(os.path.join("nwd", "containers", "aa", "map.cpp"))
project.test_executable(os.path.join("nwd", "containers", "aa", "set.cpp"))
project.test_executable(os.path.join("nwd", "containers", "byte_queue.cpp"))
project.test_executable(os.path.join("nwd", "containers", "repository.cpp"))
project.test_executable(os.path.join("nwd", "containers", "vector_queue.cpp"))

project.test_executable(os.path.join("nwd", "databases", "sqlite.cpp"),
	link_with = ["~nwd~databases"],
)

project.test_executable(os.path.join("nwd", "geometry", "gridtree.cpp"))
project.test_executable(os.path.join("nwd", "geometry", "vector3d.cpp"))

project.test_executable(os.path.join("nwd", "graphics", "normalize.cpp"))

project.test_executable(os.path.join("nwd", "graphics", "program.cpp"),
	link_with = ["~nwd~graphics"],
)

project.test_executable(os.path.join("nwd", "graphics", "wavefront_obj.cpp"),
	link_with = ["~nwd~graphics"],
)

project.test_executable(os.path.join("nwd", "math", "fixed_cyclic.cpp"))
project.test_executable(os.path.join("nwd", "math", "fixed_point.cpp"))
project.test_executable(os.path.join("nwd", "math", "vector.cpp"))

# project.test_executable(os.path.join("nwd", "networking", "event.cpp"),
# 	link_with = ["~nwd~networking"],
# )

project.test_executable(os.path.join("nwd", "utility", "byte_array.cpp"))
project.test_executable(os.path.join("nwd", "utility", "executor.cpp"))
project.test_executable(os.path.join("nwd", "utility", "functor.cpp"))
project.test_executable(os.path.join("nwd", "utility", "invoker.cpp"))
project.test_executable(os.path.join("nwd", "utility", "integer_cast.cpp"))
project.test_executable(os.path.join("nwd", "utility", "parallel.cpp"))
project.test_executable(os.path.join("nwd", "utility", "serializer_thread.cpp"))
