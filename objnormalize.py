#!/usr/bin/env python3

import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--zup", action = "store_true")
parser.add_argument("input")
args = parser.parse_args()

output = str()

minv = [sys.float_info.max for i in range(3)]
maxv = [sys.float_info.min for i in range(3)]

coords = list()

with open(args.input) as f:
	for line in f.readlines():
		if False: pass
		elif line.startswith("v "):
			values = line.split()
			for i in range(1, len(values)):
				coords.append(float(values[i]))
				minv[i - 1] = min(minv[i - 1], coords[-1])
				maxv[i - 1] = max(maxv[i - 1], coords[-1])
		elif line.startswith("vt "):
			values = line.split()
			for i in range(1, len(values)):
				values[i] = str.format("{:.6f}", min(1., max(0., float(values[i]))))
			output += " ".join(values) + "\n"
		elif line.startswith("vn "):
			values = line.split()
			if args.zup:
				values[1], values[2], values[3] = [values[i + 1] for i in [2, 0, 1]]
			for i in range(1, len(values)):
				values[i] = str.format("{:.4f}", min(1., max(-1., float(values[i]))))
			output += " ".join(values) + "\n"
		else:
			output += line

divisor = max(abs(max(maxv)), abs(min(minv)))

split = output.find("vt ")
print(output[:split], end = "")

for i in range(0, len(coords), 3):
	if args.zup:
		coords[i], coords[i + 1], coords[i + 2] = [coords[i + j] for j in [2, 0, 1]]
		
	print("v ", end = "")
	for j in range(3):
		coords[i + j] /= divisor
		print(str.format("{:.6f}", coords[i + j]), end = "")
		if j == 2:
			print(end = "\n")
		else:
			print(end = " ")

print(output[split:], end = "")
