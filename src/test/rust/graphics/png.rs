use nwd::graphics::png::*;

#[test]
fn test_png()
{
	for i in [32, 64, 128, 256]
	{
		let filename = format!("src/main/resources/icons/main-logo-{}.png", i);
		
		{
			let image = read_bottom_up(&mut std::fs::File::open(&filename).unwrap());
			assert_eq!(i, image.width());
			assert_eq!(i, image.height());
			assert_eq!((i * i * 4) as usize, image.as_raw().len());
		}
		{
			let image = read_top_down(&mut std::fs::File::open(&filename).unwrap());
			assert_eq!(i, image.width());
			assert_eq!(i, image.height());
			assert_eq!((i * i * 4) as usize, image.as_raw().len());
		}
	}
}
