use nwd::audio;
use std::io::Read;
use std::io::Seek;

#[test]
fn creation()
{
	assert!(audio::wav::from_file(audio::Format::mono_8, std::fs::File::open("src/test/resources/audio/synth_mono_8.wav").unwrap()).is_ok());
	assert!(audio::wav::from_file(audio::Format::stereo_8, std::fs::File::open("src/test/resources/audio/synth_stereo_8.wav").unwrap()).is_ok());
}

#[test]
fn read_data_chunk()
{
	let file = std::fs::File::open("src/test/resources/audio/synth_mono.wav").unwrap();
	let mut stream = audio::wav::from_file(audio::Format::mono_16, file).unwrap();
	assert_eq!(47104, std::io::copy(&mut stream, &mut std::io::sink()).unwrap());
}

#[test]
fn read_data()
{
	for format in [audio::Format::mono_8, audio::Format::mono_16, audio::Format::stereo_8, audio::Format::stereo_16]
	{
		for name in ["mono", "stereo"]
		{
			for source_bit_depth in ["8", "16", "24", "32"]
			{
				let file = std::fs::File::open(format!("src/test/resources/audio/synth_{}_{}.wav", name, source_bit_depth)).unwrap();
				let mut stream = audio::wav::from_file(format, file).unwrap();
				
				let expected_size = 23552 * format.byte_depth() as usize * format.channels() as usize;
				
				assert_eq!(expected_size, stream.seek(std::io::SeekFrom::End(0)).unwrap() as usize);
				assert_eq!(expected_size, stream.seek(std::io::SeekFrom::Current(0)).unwrap() as usize);
				assert_eq!(0, stream.read(&mut [0_u8]).unwrap());
				assert_eq!(expected_size, stream.seek(std::io::SeekFrom::Current(0)).unwrap() as usize);
				assert_eq!(expected_size, stream.seek(std::io::SeekFrom::Start(expected_size as u64)).unwrap() as usize);
				
				stream.rewind().unwrap();
				let mut data = Vec::<u8>::with_capacity(expected_size);
				stream.read_to_end(&mut data).unwrap();
				assert_eq!(expected_size, data.len());
				assert_eq!(0, stream.read(&mut [0_u8]).unwrap());
				
				assert_eq!(1, stream.seek(std::io::SeekFrom::Start(1)).unwrap());
				assert_eq!(1, stream.seek(std::io::SeekFrom::Current(0)).unwrap());
				assert_eq!(2, stream.seek(std::io::SeekFrom::Current(1)).unwrap());
				
				assert_eq!(0, stream.read(&mut []).unwrap());
				assert_eq!(2, stream.seek(std::io::SeekFrom::Current(0)).unwrap());
				
				assert_eq!(1, stream.read(&mut [0_u8]).unwrap());
				assert_eq!(3, stream.seek(std::io::SeekFrom::Current(0)).unwrap());
				
				assert_eq!(1001, stream.seek(std::io::SeekFrom::Start(1001)).unwrap());
				assert_eq!(1001, stream.seek(std::io::SeekFrom::Current(0)).unwrap());
				assert_eq!(1002, stream.seek(std::io::SeekFrom::Current(1)).unwrap());
			}
		}
	}
}
