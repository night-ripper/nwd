use nwd::geometry::coord_t;
use nwd::geometry::d3::*;

#[test]
fn test_overlaps_sphere_vector()
{
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.contains(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)))
	);
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.contains(Vector::from([0.7, 0.7, 0.0].map(coord_t::round_from)))
	);
	assert!(Sphere::new(Vector::from([-2.0, -2.0, -2.0].map(coord_t::round_from)), coord_t::round_from(2.0))
		.contains(Vector::from([-1.0, -1.0, -1.0].map(coord_t::round_from)))
	);
	
	assert!(! Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.contains(Vector::from([1.0, 1.0, 1.0].map(coord_t::round_from)))
	);
	assert!(! Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.contains(Vector::from([1.1, 0.0, 0.0].map(coord_t::round_from)))
	);
	assert!(! Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.contains(Vector::from([0.7, 0.7, 0.2].map(coord_t::round_from)))
	);
	assert!(! Sphere::new(Vector::from([-2.0, -2.0, -2.0].map(coord_t::round_from)), coord_t::round_from(2.0))
		.contains(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)))
	);
}

#[test]
fn test_overlaps_sphere_line_segment()
{
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Line_segment::from_point_vector(
			Vector::from([0.0, 0.0, 1.0].map(coord_t::round_from)),
			Vector::from([0.0, 1.0, 0.0].map(coord_t::round_from)),
		))
	);
	
	assert!(! Sphere::new(Vector::from([2.0, -1.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Line_segment::from_point_vector(
			Vector::from([0.0, 0.0, 1.0].map(coord_t::round_from)),
			Vector::from([0.0, 1.0, 0.0].map(coord_t::round_from)),
		))
	);
}

#[test]
fn test_overlaps_sphere_triangle()
{
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Triangle::from_points(
			Vector::from([0.0, 0.0, 1.0].map(coord_t::round_from)),
			Vector::from([0.0, 1.0, 0.0].map(coord_t::round_from)),
			Vector::from([1.0, 0.0, 0.0].map(coord_t::round_from)),
		))
	);
	
	assert!(! Sphere::new(Vector::from([-1.1, 0.0, 1.1].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Triangle::from_points(
			Vector::from([0.0, 0.0, 1.0].map(coord_t::round_from)),
			Vector::from([0.0, 1.0, 0.0].map(coord_t::round_from)),
			Vector::from([1.0, 0.0, 0.0].map(coord_t::round_from)),
		))
	);
}

#[test]
fn test_overlaps_sphere_sphere()
{
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Sphere::new(Vector::from([1.0, 1.0, 1.0].map(coord_t::round_from)), coord_t::round_from(1.0)))
	);
	
	assert!(! Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Sphere::new(Vector::from([2.0, 2.0, 2.0].map(coord_t::round_from)), coord_t::round_from(1.0)))
	);
}

#[test]
fn test_overlaps_sphere_cylinder()
{
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Cylinder::new(Vector::from([1.0, 1.0, 1.0].map(coord_t::round_from)), coord_t::round_from(1.0), coord_t::round_from(1.0)))
	);
	
	assert!(! Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Cylinder::new(Vector::from([2.0, 2.0, 2.0].map(coord_t::round_from)), coord_t::round_from(1.0), coord_t::round_from(1.0)))
	);
}

#[test]
fn test_overlaps_sphere_bounding_box()
{
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Bounding_box::new(
			Vector::from([0.5, 0.5, 0.5].map(coord_t::round_from)),
			Vector::from([1.0, 1.0, 1.0].map(coord_t::round_from)),
		))
	);
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Bounding_box::new(
			Vector::from([-2.0, -2.0, -2.0].map(coord_t::round_from)),
			Vector::from([2.0, 2.0, 2.0].map(coord_t::round_from)),
		))
	);
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(10.0))
		.overlaps(Bounding_box::new(
			Vector::from([-2.0, -2.0, -2.0].map(coord_t::round_from)),
			Vector::from([2.0, 2.0, 2.0].map(coord_t::round_from)),
		))
	);
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Bounding_box::new(
			Vector::from([-2.0, -2.0, -2.0].map(coord_t::round_from)),
			Vector::from([-0.9, 2.0, 2.0].map(coord_t::round_from)),
		))
	);
	assert!(Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Bounding_box::new(
			Vector::from([-2.0, -2.0, -2.0].map(coord_t::round_from)),
			Vector::from([-0.9, 0.0, 2.0].map(coord_t::round_from)),
		))
	);
	assert!(! Sphere::new(Vector::from([0.0, 0.0, 0.0].map(coord_t::round_from)), coord_t::round_from(1.0))
		.overlaps(Bounding_box::new(
			Vector::from([0.9, 0.9, 0.9].map(coord_t::round_from)),
			Vector::from([2.0, 2.0, 2.0].map(coord_t::round_from)),
		))
	);
}
