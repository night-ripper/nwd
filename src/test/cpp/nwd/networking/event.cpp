#include <iostream>
#include <string>
#include <fstream>

#include <nwd/networking/event.hpp>

#include <testing/testing.hpp>

using namespace nwd::networking;

/*
struct Empty_event_loop : Event_loop
{
	using Event_loop::Event_loop;
	
	void handle(UDP_datagram&) override
	{
	}
};

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("construction", []
{
	auto socket = Unique_socket_udp::create();
	socket.get().set_nonblocking();
	socket.get().bind(Endpoint(Address::localhost, 0));
	auto evloop = Empty_event_loop(socket.get());
	assert_true(evloop.socket().endpoint().port().value() > 0);
	auto sh_loop = std::static_pointer_cast<Event_loop>(std::make_shared<Empty_event_loop>(socket.get()));
}),

Test_case("handling", []
{
	auto socket = Unique_socket_udp::create();
	socket.get().set_nonblocking();
	socket.get().bind(Endpoint(Address::localhost, 0));
	auto evloop = Empty_event_loop(socket.get());
}),
};
*/
