#include <memory>
#include <iostream>

#include <nwd/containers/vector_queue.hpp>

#include <testing/testing.hpp>

using Vq = nwd::containers::Vector_queue<std::unique_ptr<int>>;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("static", []
{
	static_assert(std::random_access_iterator<Vq::iterator>);
}),

Test_case("simple", []
{
	auto vq = Vq();
	assert_eq(vq.size(), 0);
	assert_true(vq.empty());
	assert_true(vq.begin() == vq.end());
	assert_true(vq.begin() == vq.begin());
}),

Test_case("simple_insert", []
{
	auto vq = Vq();
	vq.emplace_back(std::make_unique<int>(1));
	assert_true(not vq.empty());
	assert_eq(1, vq.size());
	assert_eq(1, *vq.front());
	assert_eq(1, **vq.begin());
	
	assert_true(vq.begin() < ++vq.begin());
	assert_true(vq.begin() == vq.begin());
	assert_true(++vq.begin() == ++vq.begin());
	
	vq.pop_front();
	assert_eq(0, vq.size());
	assert_true(vq.empty());
	assert_true(vq.begin() == vq.end());
}),

Test_case("simple_insert_multiple", []
{
	auto vq = Vq();
	
	for (int i = 0; i != 300; ++i)
	{
		vq.emplace_back(std::make_unique<int>(i));
		assert_true(not vq.empty());
		assert_eq(1, vq.size());
		assert_eq(i, *vq.front());
		assert_eq(i, **vq.begin());
		assert_eq(i, *vq.back());
		
		vq.pop_front();
		assert_eq(0, vq.size());
		assert_true(vq.empty());
		assert_true(vq.begin() == vq.end());
	}
}),

Test_case("push_back_pop_front", []
{
	constexpr int max_size = 300;
	
	auto vq = Vq();
	
	for (int i = 0; i != max_size; ++i)
	{
		assert_eq(i, vq.size());
		vq.emplace_back(std::make_unique<int>(i));
		assert_eq(i + 1, vq.size());
		assert_eq(0, *vq.front());
		assert_eq(i, *vq.back());
	}
	
	for (int i = 0; i != max_size; ++i)
	{
		assert_eq(max_size - i, vq.size());
		assert_eq(i, **vq.begin());
		assert_eq(i, *vq.front());
		assert_eq(max_size - 1, *vq.back());
		vq.pop_front();
		assert_eq(max_size - i - 1, vq.size());
	}
	
	assert_eq(0, vq.size());
	assert_true(vq.empty());
	assert_true(vq.begin() == vq.end());
}),

Test_case("push_back_pop_back", []
{
	constexpr int max_size = 300;
	
	auto vq = Vq();
	
	for (int i = 0; i != max_size; ++i)
	{
		assert_eq(vq.size(), i);
		vq.emplace_back(std::make_unique<int>(i));
		assert_eq(i + 1, vq.size());
		assert_eq(0, *vq.front());
		assert_eq(i, *vq.back());
	}
	
	for (int i = 0; i != max_size; ++i)
	{
		assert_eq(max_size - i, vq.size());
		assert_eq(0, **vq.begin());
		assert_eq(0, *vq.front());
		assert_eq(max_size - i - 1, *vq.back());
		vq.pop_back();
		assert_eq(max_size - i - 1, vq.size());
	}
	
	assert_eq(0, vq.size());
	assert_true(vq.empty());
	assert_true(vq.begin() == vq.end());
}),

Test_case("repeated", []
{
	auto vq = Vq();
	
	for (int i = 300; i != -1; --i)
	{
		for (int j = 0; j != i; ++j)
		{
			if (i % 2 == 0)
			{
				vq.emplace_back(std::make_unique<int>(i));
			}
			else
			{
				vq.pop_front();
			}
		}
	}
	
	assert_eq(150, vq.size());
	assert_false(vq.empty());
	assert_true(vq.begin() != vq.end());
}),
};
