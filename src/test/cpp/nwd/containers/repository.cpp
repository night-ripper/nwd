#include <iostream>

#include <vector>
#include <algorithm>
#include <ranges>
#include <random>

#include <memory>

#include <nwd/containers/repository.hpp>

#include <testing/testing.hpp>

using namespace nwd::containers;

std::mt19937 gen(7175);

// TODO
std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("static", []
{
	using Rp = Repository<int>;
	static_assert(std::forward_iterator<Rp::iterator>);
}),

Test_case("main", []
{
	auto idxs = std::vector<std::ptrdiff_t>();
	 
	Repository<std::unique_ptr<int>, std::ptrdiff_t, std::unique_ptr<int>> rp;
	
	for (int i = 0; i != 100; ++i)
	{
		idxs.push_back(rp.insert(std::make_unique<int>()));
	}
	
	*rp.header() = std::make_unique<int>();
	
	std::ranges::shuffle(idxs, gen);
	
	constexpr int limit = 45;
	
	for (int i = 0; i != limit; ++i)
	{
		++**rp.header();
		rp.erase(idxs.back());
		idxs.pop_back();
	}
	
	for (int i = 0; i != limit; ++i)
	{
		idxs.push_back(rp.insert(std::make_unique<int>()));
	}
	
	std::ranges::shuffle(idxs, gen);
	
	for (int i = 0; i != limit; ++i)
	{
		rp.erase(idxs.back());
		idxs.pop_back();
	}
	
	for (int i = 0; i != limit; ++i)
	{
		idxs.push_back(rp.insert(std::make_unique<int>(99)));
	}
}),
};
