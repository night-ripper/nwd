#include <memory>
#include <iostream>
#include <initializer_list>

#include <nwd/containers/byte_queue.hpp>

#include <testing/testing.hpp>

using Bq = nwd::containers::Byte_queue;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("simple", []
{
	auto bq = Bq();
	++bq.begin();
	bq.sputn(std::initializer_list<std::byte> {std::byte(0xff)});
	assert_eq(1, bq.size());
	assert_eq(0xff, std::to_integer<int>(bq.front()));
	assert_eq(0xff, std::to_integer<int>(bq.back()));
	
	for (int i = 0; i != 10; ++i)
	{
		auto barray = std::array<std::byte, 100>();
		
		for (std::size_t j = 0; j != std::size(barray); ++j)
		{
			barray[j] = std::byte(j);
		}
		
		bq.sputn(barray);
		
		assert_eq(99, std::to_integer<int>(bq.back()));
	}
	
	auto it = bq.begin();
	assert_true(it != bq.end());
	assert_eq(0xff, std::to_integer<int>(*it));
	
	++it;
	std::size_t expected = 0;
	while (it != bq.end())
	{
		assert_eq(expected, std::to_integer<std::size_t>(*it));
		++expected;
		expected %= 100;
		++it;
	}
}),
};
