#include <iostream>
#include <fstream>
#include <memory>
#include <random>

#include <nwd/containers/aa/set.hpp>
#include <nwd/containers/aa/multiset.hpp>

#include <testing/testing.hpp>

using namespace nwd::containers;

struct Intptr_cmp
{
	constexpr bool operator()(const std::unique_ptr<int>& lhs, const std::unique_ptr<int>& rhs) const noexcept
	{
		return *lhs < *rhs;
	}
};

using Set = aa::Set<std::unique_ptr<int>, Intptr_cmp>;
using Iset = aa::Set<int>;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("static", []
{
	static_assert(std::is_default_constructible_v<Set>);
	static_assert(std::is_nothrow_default_constructible_v<Set::iterator>);
	static_assert(std::is_nothrow_default_constructible_v<Set::const_iterator>);
}),

Test_case("insert case #1", []
{
	auto set = aa::Multiset<int>();
	aa::privates::validate(set);
	set.insert(0);
	aa::privates::validate(set);
	set.insert(1);
	aa::privates::validate(set);
	set.insert(5);
	aa::privates::validate(set);
	set.insert(6);
	aa::privates::validate(set);
	set.insert(9);
	aa::privates::validate(set);
	set.insert(6);
	aa::privates::validate(set);
}),

Test_case("erase case #2", []
{
	auto set = aa::Set<int>();
	aa::privates::validate(set);
	set.insert(7);
	aa::privates::validate(set);
	set.insert(8);
	aa::privates::validate(set);
	set.insert(11);
	aa::privates::validate(set);
	set.insert(12);
	aa::privates::validate(set);
	set.insert(9);
	aa::privates::validate(set);
	set.insert(10);
	aa::privates::validate(set);
	set.insert(14);
	aa::privates::validate(set);
	set.erase(set.begin());
	aa::privates::validate(set);
}),

Test_case("insert", []
{
	auto set = Set();
	
	for (int i = 0; i != 500; ++i)
	{
		auto result = std::make_unique<int>(i);
		aa::privates::validate(set);
		set.insert(std::move(result));
		aa::privates::validate(set);
	}
	
	for (int i = 0; auto& pi : set)
	{
		assert_eq(i, *pi);
		++i;
	}
	
	assert_eq(500, set.size());
	assert_false(set.begin() == set.end());
	
	{
		int i = 0;
		for (auto it = set.begin(); it != set.end(); ++it, ++i)
		{
			assert_eq(i, **it);
		}
		assert_eq(500, i);
	}
}),

Test_case("erase", []
{
	auto set = Set();
	
	for (int i = 0; i != 500; ++i)
	{
		aa::privates::validate(set);
		set.insert(std::make_unique<int>(i));
		aa::privates::validate(set);
	}
	
	auto it = set.begin();
	++it;
	
	for (int i = 1; i != 500; ++i)
	{
		assert_false(it == set.end());
		aa::privates::validate(set);
		it = set.erase(it);
		aa::privates::validate(set);
	}
	
	assert_true(it == set.end());
	assert_eq(1, set.size());
	
	set.erase(set.begin());
	aa::privates::validate(set);
	
	assert_true(set.empty());
	assert_true(set.begin() == set.end());
}),

Test_case("insert / erase", []
{
	auto set = Iset();
	
	auto rd = std::mt19937(1337);
	
	for (int i = 0; i != 1000; ++i)
	{
		auto random_number = int(rd());
		
		aa::privates::validate(set);
		
		if (random_number % 2 == 0 and not set.empty())
		{
			auto new_begin = set.erase(set.begin());
			assert_true(set.begin() == new_begin);
		}
		else
		{
			set.insert(random_number);
		}
		
		aa::privates::validate(set);
	}
	
	while (true)
	{
		if (auto it = set.begin(); it != set.end())
		{
			set.erase(set.begin());
			aa::privates::validate(set);
		}
		else
		{
			break;
		}
	}
	
	assert_true(set.empty());
}),
};
