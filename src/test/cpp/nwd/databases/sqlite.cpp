#include <iostream>
#include <string>
#include <fstream>

#include <nwd/databases/sqlite.hpp>

#include <testing/testing.hpp>

using namespace nwd::databases;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("construction", []
{
	auto db = SQLite::create_from_memory();
}),

Test_case("create_table", []
{
	auto db = SQLite::create_from_memory();
	
	assert_eq(db.prepare_single("create table test(name text not null primary key, age int)").step(), SQLite::Result::step_done);
}),

Test_case("single_statement", []
{
	auto db = SQLite::create_from_memory();
	
	assert_eq(db.prepare_single("create table test(name text not null primary key, age int)").step(), SQLite::Result::step_done);
	assert_eq(db.prepare_single("select * from test").step(),  SQLite::Result::step_done);
	assert_eq(db.prepare_single("insert into test values('John', 45)").step(),  SQLite::Result::step_done);
	
	{
		auto stmt = db.prepare_single("select * from test");
		assert_eq(stmt.step(),  SQLite::Result::row_ready);
		assert_eq(std::string_view(stmt.column_text(0)),  "John");
		assert_eq(stmt.column_int32(1),  45);
		assert_eq(stmt.step(),  SQLite::Result::step_done);
	}
	
	assert_eq(db.prepare_single("delete from test").step(),  SQLite::Result::step_done);
	assert_eq(db.prepare_single("select * from test").step(),  SQLite::Result::step_done);
}),

Test_case("execute", []
{
	auto db = SQLite::create_from_memory();
	
	db.execute(R"""(
		create table test(name text not null primary key, age int);
		create unique index test_index on test(name);
		select * from test;
		delete from test;
		select * from test;
	)""");
}),
};
