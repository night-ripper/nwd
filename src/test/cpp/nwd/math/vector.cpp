#include <iostream>

#include <nwd/math/vector.hpp>

#include <testing/testing.hpp>

using namespace nwd::math;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("structured_binding", []
{
	auto [a, b, c, d] = Vec<float, 4>(1.f, 2.f, 3.f, 4.f);
	assert_eq(1.f, a);
	assert_eq(2.f, b);
	assert_eq(3.f, c);
	assert_eq(4.f, d);
}),

Test_case("swap", []
{
	auto v1 = Vec3f(1, 2, 3);
	auto v2 = Vec3f(4, 5, 6);
	
	std::swap(v1, v2);
	
	assert_eq(4.f, v1[0]);
	assert_eq(5.f, v1[1]);
	assert_eq(6.f, v1[2]);
	
	assert_eq(1.f, v2[0]);
	assert_eq(2.f, v2[1]);
	assert_eq(3.f, v2[2]);
}),

Test_case("solve_matrix", []
{
	auto matrix = std::array {
		Vec3f(4, -2, 1),
		Vec3f(-3, 1, -1),
		Vec3f(1, -3, 2),
		Vec3f(-8, -4, 2),
	};
	
	auto [x, y, z, div] = solve(matrix);
	
	assert_eq(6.f, div);
	x /= div;
	y /= div;
	z /= div;
	
	assert_eq(matrix[3][0], x * matrix[0][0] + y * matrix[1][0] + z * matrix[2][0]);
	assert_eq(matrix[3][1], x * matrix[0][1] + y * matrix[1][1] + z * matrix[2][1]);
	assert_eq(matrix[3][2], x * matrix[0][2] + y * matrix[1][2] + z * matrix[2][2]);
}),
};
