#include <cstdint>
#include <numbers>

#include <type_traits>

#include <iostream>
#include <functional>

#include <nwd/math/fixed_cyclic.hpp>
#include <nwd/geometry/units.hpp>

#include <testing/testing.hpp>

using namespace nwd::math;

struct Two_pi
{
	// std::pow(2, std::log2(2 * std::numbers::pi_v<long double>) - std::numeric_limits<std::uint32_t>::digits)
	constexpr static long double value = 1.46291807926715968188249154e-09L;
};

using Angle = Fixed_cyclic<std::uint32_t, Two_pi>;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("basic", []
{
	auto a = Angle(5.5);
	a += Angle(1.f);
	Assert_floating_similar(0.00001f)(float(a), 0.216814f);
}),
};
