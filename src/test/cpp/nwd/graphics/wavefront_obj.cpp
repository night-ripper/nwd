#include <iostream>
#include <string>
#include <fstream>

#include <nwd/graphics/wavefront_obj.hpp>

#include <testing/testing.hpp>

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("basic", []
{
	auto obj = nwd::graphics::Wavefront_obj(std::ifstream("src/test/resources/rendering/barrel.obj"));
	
	assert_eq(std::ssize(obj.vertices_), 464);
	assert_eq(std::ssize(obj.vertex_textures_), 524);
	assert_eq(std::ssize(obj.vertex_normals_), 218);
	assert_eq(std::ssize(obj.indices_), 1836);
	assert_eq(obj.num_vertices_, 1468);
	
	{
		auto assert_similar = Assert_floating_similar(0.00007f);
		
		assert_similar(obj.vertices_.front().x,  0.000000f);
		assert_similar(obj.vertices_.front().y,  0.392438f);
		assert_similar(obj.vertices_.front().z,  0.877277f);
		
		assert_similar(obj.vertices_.back().x,  0.325056f);
		assert_similar(obj.vertices_.back().y,  -0.117787f);
		assert_similar(obj.vertices_.back().z,  0.037194f);
		
		assert_similar(obj.vertex_textures_.front().u,  0.239000f);
		assert_similar(obj.vertex_textures_.front().v,  0.466800f);
		
		assert_similar(obj.vertex_textures_.back().u,  0.383100f);
		assert_similar(obj.vertex_textures_.back().v,  0.054700f);
		
		assert_similar(obj.vertex_normals_.front().x,  0.f);
		assert_similar(obj.vertex_normals_.front().y,  0.f);
		assert_similar(obj.vertex_normals_.front().z,  1.f);
		
		assert_similar(obj.vertex_normals_.back().x,  0.8788f);
		assert_similar(obj.vertex_normals_.back().y,  0.4113f);
		assert_similar(obj.vertex_normals_.back().z,  -0.2417f);
	}
	
	assert_eq(obj.indices_.front().v, 245);
	assert_eq(obj.indices_.front().vt,  1);
	assert_eq(obj.indices_.front().vn,  1);
	
	assert_eq(obj.indices_[1836 - 2].v,  403);
	assert_eq(obj.indices_[1836 - 2].vt,  464);
	assert_eq(obj.indices_[1836 - 2].vn,  205);
	
	assert_eq(obj.indices_[1836 - 1].v,  0);
	assert_eq(obj.indices_[1836 - 1].vt,  0);
	assert_eq(obj.indices_[1836 - 1].vn,  0);
	
	for (auto [v, vt, vn] : obj.indices_)
	{
		assert_true(v >= 0);
		assert_true(v <= 464);
		
		assert_true(vt >= 0);
		assert_true(vt <= 524);
		
		assert_true(vn >= 0);
		assert_true(vn <= 218);
	}
}),
};
