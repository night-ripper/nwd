#include <iostream>
#include <string>
#include <vector>

#include <nwd/audio/vorbis.hpp>

#include <testing/testing.hpp>

using namespace nwd::audio;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("creation", []
{
	vorbis::create_stream(Format::mono_8, "src/test/resources/audio/synth_mono.ogg");
	vorbis::create_stream(Format::stereo_8, "src/test/resources/audio/synth_stereo.ogg");
}),

Test_case("read_data", []
{
	for (Format format : {Format::mono_8, Format::mono_16, Format::stereo_8, Format::stereo_16})
	{
		for (std::string name : {"mono", "stereo"})
		{
			auto stream = vorbis::create_stream(format, "src/test/resources/audio/synth_" + name + ".ogg");
			std::ptrdiff_t expected_size = 23552 * format.byte_depth() * format.channels();
			assert_eq(expected_size, stream->in_avail());
			
			auto data = std::vector<char>();
			std::copy(std::istreambuf_iterator(stream.get()), {}, std::back_inserter(data));
			assert_eq(expected_size, std::ssize(data));
			assert_eq(-1, stream->in_avail());
			
			stream->pubseekoff(0, std::ios_base::beg);
			assert_eq(expected_size, stream->in_avail());
			
			{
				std::ptrdiff_t size = expected_size;
				
				while (stream->sbumpc() != Streaming_sound::traits_type::eof())
				{
					--size;
				}
				
				assert_eq(0, size);
				assert_eq(-1, stream->in_avail());
			}
		}
	}
}),
};
