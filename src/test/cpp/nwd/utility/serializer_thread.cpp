#include <type_traits>

#include <iostream>
#include <random>
#include <functional>

#include <nwd/utility/serializer_thread.hpp>

#include <testing/testing.hpp>

using nwd::utility::Serializer_thread;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("static", []
{
	auto wr = Serializer_thread();
	static_assert(noexcept(wr([]() noexcept -> void {})));
	static_assert(not noexcept(wr([]() -> void {})));
}),

Test_case("thread_pool", []
{
	auto wr = Serializer_thread();
	
	auto pi = new unsigned int(0);
	auto ppi = new unsigned int*(pi);
	auto times = reinterpret_cast<std::uintptr_t>(ppi) % 100;
	unsigned int addition = 0;
	
	{
		auto thrs = std::vector<std::jthread>(50);
		
		for (std::uintptr_t i = 0; i != times; ++i)
		{
			auto added = std::random_device()() % 10'000;
			**ppi += added;
			addition += added;
		}
		
		++addition;
		++**ppi;
		
		for (auto& thr : thrs)
		{
			thr = std::jthread([&]
			{
				for (int i = 0; i != 1000; ++i)
				{
					wr([&] {++**ppi;});
				}
			});
		}
	}
	
	assert_eq(*pi, 50000 + addition);
	delete pi;
	delete ppi;
}),

Test_case("exception", []
{
	auto wr = Serializer_thread();
	
	{
		bool result = false;
		
		try
		{
			wr([]() -> void
			{
				throw 0;
			});
		}
		catch (int)
		{
			result = true;
		}
		
		assert_true(result);
	}
	
	{
		auto result = std::atomic_flag(false);
		
		wr([&]() -> void
		{
			result.test_and_set(std::memory_order::release);
		});
		
		assert_true(result.test_and_set(std::memory_order::acquire));
	}
	
	{
		bool result = false;
		
		try
		{
			wr([]() -> void
			{
				throw 0.f;
			});
		}
		catch (float)
		{
			result = true;
		}
		
		assert_true(result);
	}
}),
};
