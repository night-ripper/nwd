#include <cstdint>

#include <type_traits>

#include <iostream>
#include <functional>

#include <nwd/utility/integer_cast.hpp>

#include <testing/testing.hpp>

using namespace nwd::utility;

template<typename Target_type, typename Source_type>
static void test_signedness()
{
	testing::assert_eq(integer_cast<Target_type>(std::numeric_limits<Source_type>::max()),
		std::numeric_limits<Target_type>::max()
	);
	
	testing::assert_eq(integer_cast<Target_type, Source_type>(std::numeric_limits<Source_type>::max() - static_cast<Source_type>(1)),
		std::numeric_limits<Target_type>::max() - static_cast<Target_type>(1)
	);
	
	testing::assert_eq(integer_cast<Target_type, Source_type>(std::numeric_limits<Source_type>::max() - static_cast<Source_type>(2)),
		std::numeric_limits<Target_type>::max() - static_cast<Target_type>(2)
	);
	
	testing::assert_eq(integer_cast<Target_type, Source_type>(std::numeric_limits<Source_type>::min() + static_cast<Source_type>(2)),
		std::numeric_limits<Target_type>::min() + static_cast<Target_type>(2)
	);
	
	testing::assert_eq(integer_cast<Target_type, Source_type>(std::numeric_limits<Source_type>::min() + static_cast<Source_type>(1)),
		std::numeric_limits<Target_type>::min() + static_cast<Target_type>(1)
	);
	
	testing::assert_eq(integer_cast<Target_type>(std::numeric_limits<Source_type>::min()),
		std::numeric_limits<Target_type>::min()
	);
}

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("signedness", []
{
	test_signedness<signed char, unsigned char>();
	test_signedness<unsigned char, signed char>();
	
	test_signedness<signed short, unsigned short>();
	test_signedness<unsigned short, signed short>();
	
	test_signedness<signed int, unsigned int>();
	test_signedness<unsigned int, signed int>();
	
	test_signedness<signed long, unsigned long>();
	test_signedness<unsigned long, signed long>();
	
	test_signedness<signed long long, unsigned long long>();
	test_signedness<unsigned long long, signed long long>();
}),

Test_case("expansion", []
{
	assert_eq(integer_cast<std::uint32_t>(std::uint8_t(0)), std::uint32_t(0));
	
	assert_eq(integer_cast<std::uint16_t>(std::uint8_t(1)), std::uint16_t(256));
	assert_eq(integer_cast<std::uint32_t>(std::uint16_t(1)), std::uint32_t(256 * 256));
	assert_eq(integer_cast<std::uint32_t>(std::uint8_t(1)), std::uint32_t(256 * 256 * 256));
	
	assert_eq(integer_cast<std::uint16_t>(std::uint8_t(255)), std::uint16_t(65535 - 255));
}),
};
