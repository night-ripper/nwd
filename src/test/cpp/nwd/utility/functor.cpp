#include <type_traits>

#include <iostream>
#include <functional>

#include <nwd/utility/invoker.hpp>
#include <nwd/utility/functor.hpp>

#include <testing/testing.hpp>

using namespace nwd::utility;

int add(int lhs, int rhs)
{
	return lhs + rhs;
}

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("functor / invoker", []
{
	assert_eq(20, Invoker<int(int, int)>(Functor<add>())(10, 10));
}),
};
