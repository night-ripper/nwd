#include <nwd/utility/byte_array.hpp>

#include <testing/testing.hpp>

using namespace nwd::utility;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("simple", []
{
	auto barray = Byte_array<4>::of(std::byte(1));
	int i = 0;
	for (auto b : barray)
	{
		++i;
		assert_eq(1, std::to_integer<int>(b));
	}
	
	assert_eq(4, i);
	
	assert_eq(0x01010101u, std::uint32_t(barray));
}),

Test_case("increment", []
{
	auto barray = Byte_array<4>::of(std::byte(0xff));
	
	for (auto b : barray)
	{
		assert_eq(0xff, std::to_integer<int>(b));
	}
	
	++barray;
	for (auto b : barray)
	{
		assert_eq(0, std::to_integer<int>(b));
	}
	
	++barray;
	assert_eq(0, std::to_integer<int>(barray[0]));
	assert_eq(0, std::to_integer<int>(barray[1]));
	assert_eq(0, std::to_integer<int>(barray[2]));
	assert_eq(1, std::to_integer<int>(barray[3]));
	
	++barray;
	assert_eq(0, std::to_integer<int>(barray[0]));
	assert_eq(0, std::to_integer<int>(barray[1]));
	assert_eq(0, std::to_integer<int>(barray[2]));
	assert_eq(2, std::to_integer<int>(barray[3]));
	
	for (int i = 2; i != 0x100; ++i)
	{
		++barray;
	}
	
	assert_eq(0, std::to_integer<int>(barray[0]));
	assert_eq(0, std::to_integer<int>(barray[1]));
	assert_eq(1, std::to_integer<int>(barray[2]));
	assert_eq(0, std::to_integer<int>(barray[3]));
	
	++barray;
	assert_eq(0, std::to_integer<int>(barray[0]));
	assert_eq(0, std::to_integer<int>(barray[1]));
	assert_eq(1, std::to_integer<int>(barray[2]));
	assert_eq(1, std::to_integer<int>(barray[3]));
}),

Test_case("decrement", []
{
	auto barray = Byte_array<4>();
	barray[2] = std::byte(1);
	barray[3] = std::byte(1);
	
	--barray;
	assert_eq(0, std::to_integer<int>(barray[0]));
	assert_eq(0, std::to_integer<int>(barray[1]));
	assert_eq(1, std::to_integer<int>(barray[2]));
	assert_eq(0, std::to_integer<int>(barray[3]));
	
	--barray;
	assert_eq(0, std::to_integer<int>(barray[0]));
	assert_eq(0, std::to_integer<int>(barray[1]));
	assert_eq(0, std::to_integer<int>(barray[2]));
	assert_eq(0xff, std::to_integer<int>(barray[3]));
	
	for (int i = 0xff; i != 0; --i)
	{
		--barray;
	}
	
	for (auto b : barray)
	{
		assert_eq(0, std::to_integer<int>(b));
	}
	
	--barray;
	assert_eq(0xff, std::to_integer<int>(barray[0]));
	assert_eq(0xff, std::to_integer<int>(barray[1]));
	assert_eq(0xff, std::to_integer<int>(barray[2]));
	assert_eq(0xff, std::to_integer<int>(barray[3]));
	
	--barray;
	assert_eq(0xff, std::to_integer<int>(barray[0]));
	assert_eq(0xff, std::to_integer<int>(barray[1]));
	assert_eq(0xff, std::to_integer<int>(barray[2]));
	assert_eq(0xfe, std::to_integer<int>(barray[3]));
}),
};
