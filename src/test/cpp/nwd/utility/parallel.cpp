#include <atomic>
#include <functional>
#include <list>
#include <vector>
#include <thread>
#include <iostream>
#include <ranges>

#include <nwd/utility/iterators.hpp>
#include <nwd/utility/parallel.hpp>

#include <testing/testing.hpp>

using namespace nwd::utility;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("forward_range", []
{
	auto lst = std::list<std::move_only_function<void() const noexcept>>();
	
	auto result = std::atomic<int>(0);
	auto real_result = int(0);
	
	for (int i = 0; i != 200; ++i)
	{
		lst.emplace_back([&, i]() noexcept -> void
		{
			result.fetch_add(i, std::memory_order::acq_rel);
		});
		real_result += i;
	}
	
	parallel::for_each(lst, [](auto&& f) noexcept -> void {f();});
	
	assert_eq(real_result, result.load(std::memory_order::acquire));
	
	result.store(0, std::memory_order::release);
	
	for (int i = 0; auto& f : lst)
	{
		f = [&, i]() noexcept -> void
		{
			result.fetch_add(i, std::memory_order::relaxed);
		};
		
		++i;
	}
	
	parallel::for_each(lst, [m = std::mutex()](auto&& f) mutable noexcept -> void {auto lg = std::lock_guard(m); f();});
	
	assert_eq(real_result, result.load(std::memory_order::relaxed));
}),

Test_case("random_access_range", []
{
	auto lst = std::vector<std::move_only_function<void() const noexcept>>();
	
	auto result = std::atomic<int>(0);
	auto real_result = int(0);
	
	for (int i = 0; i != 200; ++i)
	{
		lst.emplace_back([&, i]() noexcept -> void
		{
			result.fetch_add(i, std::memory_order::acq_rel);
		});
		real_result += i;
	}
	
	parallel::for_each(lst, [](auto&& f) noexcept -> void {f();});
	
	assert_eq(real_result, result.load(std::memory_order::acquire));
	
	result.store(0, std::memory_order::release);
	
	for (int i = 0; auto& f : lst)
	{
		f = [&, i]() noexcept -> void
		{
			result.fetch_add(i, std::memory_order::relaxed);
		};
		
		++i;
	}
	
	parallel::for_each(lst, [m = std::mutex()](auto&& f) mutable noexcept -> void {auto lg = std::lock_guard(m); f();});
	
	assert_eq(real_result, result.load(std::memory_order::relaxed));
}),

Test_case("temporary_value", []
{
	struct It
	{
		using difference_type = std::ptrdiff_t;
		using value_type = std::unique_ptr<int>;
		using reference = void;
		using pointer = void;
		
		int i_ = 0;
		
		bool operator==(It rhs) const noexcept
		{
			return i_ == rhs.i_;
		}
		
		std::unique_ptr<int> operator*() const
		{
			return std::make_unique<int>(i_);
		}
		
		It& operator++() noexcept
		{
			++i_;
			return *this;
		}
		
		It operator++(int) noexcept {return operators::post_increment(*this);}
	};
	
	static_assert(std::input_iterator<It>);
	
	auto sum = std::atomic<int>(0);
	
	parallel::for_each(std::ranges::subrange(It(), It(200)), [&](auto ptr) noexcept -> void
	{
		sum.fetch_add(*ptr, std::memory_order::acq_rel);
	});
	
	assert_eq(19900, sum.load(std::memory_order::acquire));
}),
};
