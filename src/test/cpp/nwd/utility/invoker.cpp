#include <type_traits>

#include <iostream>
#include <functional>

#include <nwd/utility/invoker.hpp>

#include <testing/testing.hpp>

/// NOTE when using noncapturing lambdas, they decay into function pointers

using nwd::utility::Invoker;

std::initializer_list<testing::Test_case> testing::test_cases
{
Test_case("const_invoke", []
{
	const auto lambda = [&]() -> bool {return true;};
	Invoker<bool()> inv = lambda;
	assert_true(inv());
}),

Test_case("no_const_invoke", []
{
	auto lambda = [&]() mutable -> bool {return true;};
	Invoker<bool()> inv = lambda;
	assert_true(inv());
}),

Test_case("convertibility", []
{
	const auto const_mutable = [&]() mutable -> bool {return true;};
	static_assert(not std::is_convertible_v<decltype(const_mutable), Invoker<bool()>>);
	
	auto no_const_ref = [&](int&) -> void {};
	static_assert(not std::is_convertible_v<decltype(no_const_ref), Invoker<void(const int&)>>);
	
	static_assert(std::is_convertible_v<Invoker<int(int) const>, Invoker<int(int)>>);
	static_assert(std::is_convertible_v<Invoker<int(int) noexcept>, Invoker<int(int)>>);
	static_assert(std::is_convertible_v<Invoker<int(int) const noexcept>, Invoker<int(int)>>);
	static_assert(std::is_convertible_v<Invoker<int(int) const noexcept>, Invoker<int(int) const>>);
	static_assert(std::is_convertible_v<Invoker<int(int) const noexcept>, Invoker<int(int) noexcept>>);
	
	static_assert(not std::is_convertible_v<Invoker<int(int)>, Invoker<int(int) const>>);
	static_assert(not std::is_convertible_v<Invoker<int(int)>, Invoker<int(int) noexcept>>);
	static_assert(not std::is_convertible_v<Invoker<int(int)>, Invoker<int(int) const noexcept>>);
	static_assert(not std::is_convertible_v<Invoker<int(int) const>, Invoker<int(int) noexcept>>);
	static_assert(not std::is_convertible_v<Invoker<int(int) noexcept>, Invoker<int(int) const>>);
}),

Test_case("pass_by_ref_no_copy", []
{
	struct Nocopy
	{
		Nocopy() = default;
		Nocopy(const Nocopy&) = delete;
		Nocopy(Nocopy&&) = delete;
		int value = 0;
	}
	nocopy;
	
	Invoker<Nocopy&(Nocopy&)>([](Nocopy& value) -> Nocopy&
	{
		return value;
	})(nocopy).value = 1;
	
	assert_eq(nocopy.value, 1);
}),

Test_case("simple_supplier", []
{
	auto supplier = [](Invoker<void(const int&)> visitor) -> void
	{
		for (int i = 0; i != 100; ++i)
		{
			visitor(i);
		}
	};
	
	supplier([i = 0](const int& value) mutable -> void
	{
		assert_eq(i, value);
		++i;
	});
}),

Test_case("throw_exception", []
{
	do
	{
		try
		{
			Invoker<void()>([&] {throw std::runtime_error("");})();
		}
		catch (std::runtime_error&)
		{
			break;
		}
		
		assert_true(false);
	}
	while (false);
	
	assert_true(true);
}),
};
