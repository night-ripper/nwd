fn main()
{
	let entry = ash::Entry::linked();
	
	let app_info = ash::vk::ApplicationInfo
	{
		api_version: ash::vk::make_api_version(0, 1, 0, 0),
		.. Default::default()
	};
	
	let create_info = ash::vk::InstanceCreateInfo
	{
		p_application_info: &app_info,
		.. Default::default()
	};
	
	let _instance = unsafe {entry.create_instance(&create_info, None).unwrap()};
}
