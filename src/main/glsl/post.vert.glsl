#version 450

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texture_coordinates;
layout(location = 2) in ivec2 texture_indices;
layout(location = 3) in vec4 color;

layout(location = 0) out vec2 vs_texture_coordinates;
layout(location = 1) out ivec2 vs_texture_indices;
layout(location = 2) out vec4 vs_color;

void main()
{
	gl_Position = vec4(position.x, position.y, 0.0, 1.0);
	vs_texture_coordinates = texture_coordinates;
	vs_texture_indices = texture_indices;
	vs_color = color;
}
