#version 450
#extension GL_ARB_bindless_texture : require

layout(location = 0) in vec2 texture_coordinates;

layout(location = 0) uniform float gamma;
layout(location = 1) uniform float exposure;
layout(location = 2, bindless_sampler) uniform sampler2D screen_texture;

layout(location = 0) out vec4 final_color;

void main()
{
	vec3 hdr_color = texture(screen_texture, texture_coordinates).rgb;
	
	/// Exposure tone mapping
	vec3 mapped = vec3(1) - exp(-hdr_color * exposure);
	
	/// Gamma correction 
	mapped = pow(mapped, vec3(gamma));
	
	final_color = vec4(mapped, 1);
}
