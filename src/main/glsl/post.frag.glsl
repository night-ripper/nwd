#version 450

layout(location = 0) in vec2 texture_coordinates;
layout(location = 1) in flat ivec2 texture_indices;
layout(location = 2) in vec4 color;

layout(location = 0) out vec4 final_color;

layout(binding = 0) uniform sampler2DArray screen_textures[16];

void main()
{
	final_color = texture(screen_textures[texture_indices.x],
		vec3(texture_coordinates, texture_indices.y)
	);
	
	final_color *= color;
}
