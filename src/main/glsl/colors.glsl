vec3 hsv(vec3 rgb)
{
	float value = max(rgb.r, max(rgb.g, rgb.b));
	float minimum = min(rgb.r, min(rgb.g, rgb.b));
	float chroma = value - minimum;
	float lightness = mix(value, minimum, .5);
	
	float hue = 0;
	
	if (chroma == 0);
	else if (value == rgb.r) {hue = (1. / 6) * (0 + (rgb.g - rgb.b) / chroma);}
	else if (value == rgb.g) {hue = (1. / 6) * (2 + (rgb.b - rgb.r) / chroma);}
	else if (value == rgb.b) {hue = (1. / 6) * (4 + (rgb.r - rgb.g) / chroma);}
	
	hue = mod(hue, 1);
	float saturation = (value == 0 ? 0 : chroma / value);
	
	return vec3(hue, saturation, value);
}

vec3 rgb(vec3 hsv)
{
	float chroma = hsv[1] * hsv[2];
	
	float hmod = hsv[0] * 6;
	float x = chroma * (1 - abs(mod(hmod, 2) - 1));
	vec3 result = vec3(0);
	
	if (false);
	else if (0 <= hmod && hmod <= 1) {result = vec3(chroma, x, 0);}
	else if (1 < hmod && hmod <= 2) {result = vec3(x, chroma, 0);}
	else if (2 < hmod && hmod <= 3) {result = vec3(0, chroma, x);}
	else if (3 < hmod && hmod <= 4) {result = vec3(0, x, chroma);}
	else if (4 < hmod && hmod <= 5) {result = vec3(x, 0, chroma);}
	else if (5 < hmod && hmod <= 6) {result = vec3(chroma, 0, x);}
	
	float m = hsv.z - chroma;
	
	return result + vec3(m);
}
