#version 450
#extension GL_ARB_bindless_texture : require
#extension GL_ARB_gpu_shader5 : require

layout(location = 0) in vec2 in_texture_coordinate;
layout(location = 1) in flat sampler2D in_texture_handle;

out vec4 out_color;

void main()
{
	// out_color = vec4(1.0f, 0.5f, 0.2f, 1.0f);
	out_color = texture(in_texture_handle, in_texture_coordinate);
}
