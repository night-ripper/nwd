pub mod d3;

pub type coord_t = crate::math::fixed_point::Fixed_point<14>;
pub type angle_t = crate::math::cyclic_angle::Cyclic_angle;

pub const pi: angle_t = angle_t::from_underlying(u32::MAX / 2 + 1);
