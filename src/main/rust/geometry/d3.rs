mod bounding_box;
mod vector;
mod cylinder;
mod line_segment;
mod sphere;
mod triangle;

mod gridtree;

////////////////////////////////////////////////////////////////////////////////

pub use bounding_box::Bounding_box;
pub use bounding_box::Box_of;

pub use cylinder::Cylinder;

pub use line_segment::Line_iterator;
pub use line_segment::Line_segment;
pub use line_segment::lines;

pub use sphere::Sphere;

pub use triangle::Triangle_iterator;
pub use triangle::Triangle;
pub use triangle::triangles;

pub use vector::Vector;

pub use gridtree::Gridtree;

////////////////////////////////////////////////////////////////////////////////

pub trait Move_by
{
	fn move_by(&mut self, vector: Vector);
}

pub trait Contains<Inner>
{
	fn contains(self, inner: Inner) -> bool;
}

pub trait Overlaps<Rhs>
{
	fn overlaps(self, rhs: Rhs) -> bool;
}
