use crate::geometry::coord_t;
use crate::geometry::d3::*;

#[derive(Clone, Copy, Eq)]
pub struct Vector
{
	coordinates: [coord_t; 3],
}

impl std::fmt::Display for Vector
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "Vector3D [{}, {}, {}]", self[0], self[1], self[2])
	}
}

impl From<[coord_t; 3]> for Vector
{
	fn from(coordinates: [coord_t; 3]) -> Self {Self {coordinates}}
}

impl Vector
{
	pub fn of_value(value: coord_t) -> Self
	{
		return Self::from([value; 3]);
	}
	
	pub fn new() -> Self
	{
		return Self::of_value(coord_t::from_underlying(0));
	}
	
	pub fn length(&self) -> f32
	{
		let mut values = self.coordinates.map(f32::from).map(f32::abs);
		let max = values[0].max(values[1].max(values[2]));
		
		if max != 0.0
		{
			values[0] *= ((values[0] / max).powi(2) + (values[1] / max).powi(2) + (values[2] / max).powi(2)).sqrt();
		}
		
		return values[0];
	}
	
	pub fn distance(self, rhs: Self) -> f32
	{
		return (rhs - self).length();
	}
	
	pub fn length_xy(self) -> f32
	{
		return f32::from(self.coordinates[0]).hypot(self.coordinates[1].into());
	}
	
	pub fn distance_xy(self, rhs: Self) -> f32
	{
		return (rhs - self).length_xy();
	}
	
	pub fn round_from(coordinates: [f32; 3]) -> Vector
	{
		Vector::from(coordinates.map(coord_t::round_from))
	}
	
	pub fn to_array(self) -> [coord_t; 3]
	{
		self.coordinates
	}
	
	pub fn into_array<Type>(self) -> [Type; 3] where Type: From<coord_t>
	{
		self.coordinates.map(Type::from)
	}
	
	pub fn cross(self, rhs: Self) -> Vector
	{
		let lhs = self.to_array().map(coord_t::underlying_value).map(i64::from);
		let rhs = rhs.to_array().map(coord_t::underlying_value).map(i64::from);
		
		Vector::from([
			lhs[1].saturating_mul(rhs[2]).saturating_sub(lhs[2].saturating_mul(rhs[1])),
			lhs[2].saturating_mul(rhs[0]).saturating_sub(lhs[0].saturating_mul(rhs[2])),
			lhs[0].saturating_mul(rhs[1]).saturating_sub(lhs[1].saturating_mul(rhs[0])),
		].map(|v| coord_t::from_underlying(v.clamp(i32::MIN as i64, i32::MAX as i64) as i32)))
	}
	
	pub fn dot(self, rhs: Self) -> coord_t
	{
		coord_t::from_underlying((0 .. 3).fold(0_i64, |acc, e|
			acc.saturating_add((self[e].underlying_value() as i64).saturating_mul(rhs[e].underlying_value() as i64))
		).clamp(i32::MIN as i64, i32::MAX as i64) as i32)
	}
}

impl std::ops::Index<usize> for Vector
{
	type Output = coord_t;
	
	fn index(&self, index: usize) -> &Self::Output {&self.coordinates[index]}
}

impl std::ops::IndexMut<usize> for Vector
{
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {&mut self.coordinates[index]}
}

////////////////////////////////////////////////////////////////////////////////

impl std::cmp::PartialEq for Vector
{
	fn eq(&self, rhs: &Self) -> bool
	{
		self.coordinates == rhs.coordinates
	}
}

impl std::ops::Neg for Vector
{
	type Output = Vector;
	
	fn neg(mut self) -> Self::Output
	{
		for c in &mut self.coordinates
		{
			*c = -*c;
		}
		
		return self;
	}
}

impl std::ops::AddAssign for Vector
{
	fn add_assign(&mut self, rhs: Self)
	{
		for i in 0 .. self.coordinates.len()
		{
			self.coordinates[i] += rhs.coordinates[i];
		}
	}
}

impl std::ops::Add<Vector> for Vector
{
	type Output = Self;
	
	fn add(mut self, rhs: Self) -> Self::Output
	{
		self += rhs;
		return self;
	}
}

impl std::ops::SubAssign for Vector
{
	fn sub_assign(&mut self, rhs: Self)
	{
		for i in 0 .. self.coordinates.len()
		{
			self.coordinates[i] -= rhs.coordinates[i];
		}
	}
}

impl std::ops::Sub<Vector> for Vector
{
	type Output = Self;
	
	fn sub(mut self, rhs: Self) -> Self::Output
	{
		self -= rhs;
		return self;
	}
}

/*
impl std::ops::MulAssign<f32> for Vector3D
{
	fn mul_assign(&mut self, rhs: f32)
	{
		for i in 0 .. self.coordinates.len()
		{
			self.coordinates[i] *= rhs;
		}
	}
}

impl std::ops::Mul<f32> for Vector3D
{
	type Output = Self;
	
	fn mul(mut self, rhs: f32) -> Self::Output
	{
		self *= rhs;
		return self;
	}
}

impl std::ops::DivAssign<f32> for Vector3D
{
	fn div_assign(&mut self, rhs: f32)
	{
		for i in 0 .. self.coordinates.len()
		{
			self.coordinates[i] /= rhs;
		}
	}
}

impl std::ops::Div<f32> for Vector3D
{
	type Output = Self;
	
	fn div(mut self, rhs: f32) -> Self::Output
	{
		self /= rhs;
		return self;
	}
}
*/

////////////////////////////////////////////////////////////////////////////////

impl crate::math::Midpoint for Vector
{
	fn midpoint(mut self, rhs: Self) -> Self
	{
		for i in 0 .. self.coordinates.len()
		{
			self.coordinates[i] = self.coordinates[i].midpoint(rhs.coordinates[i]);
		}
		
		return self
	}
}

impl Move_by for Vector
{
	fn move_by(&mut self, vector: self::Vector)
	{
		*self += vector;
	}
}

impl Box_of for Vector
{
	fn box_of(self) -> Bounding_box
	{
		Bounding_box::from(self)
	}
}

impl Contains<Vector> for Vector
{
	fn contains(self, _inner: Vector) -> bool {false}
}

impl Overlaps<Vector> for Vector
{
	fn overlaps(self, _rhs: Vector) -> bool {false}
}

impl Contains<Line_segment> for Vector
{
	fn contains(self, _inner: Line_segment) -> bool {false}
}

impl Overlaps<Line_segment> for Vector
{
	fn overlaps(self, _rhs: Line_segment) -> bool {false}
}

impl Contains<Triangle> for Vector
{
	fn contains(self, _inner: Triangle) -> bool {false}
}

impl Overlaps<Triangle> for Vector
{
	fn overlaps(self, rhs: Triangle) -> bool {rhs.contains(self)}
}

impl Contains<Sphere> for Vector
{
	fn contains(self, _inner: Sphere) -> bool {false}
}

impl Overlaps<Sphere> for Vector
{
	fn overlaps(self, rhs: Sphere) -> bool {rhs.contains(self)}
}

impl Contains<Cylinder> for Vector
{
	fn contains(self, _inner: Cylinder) -> bool {false}
}

impl Overlaps<Cylinder> for Vector
{
	fn overlaps(self, rhs: Cylinder) -> bool {rhs.contains(self)}
}

impl Overlaps<Bounding_box> for Vector
{
	fn overlaps(self, rhs: Bounding_box) -> bool {rhs.contains(self)}
}
