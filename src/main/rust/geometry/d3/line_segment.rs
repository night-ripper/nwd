use crate::geometry::coord_t;
use crate::geometry::d3::*;

#[derive(Clone, Copy)]
pub struct Line_segment
{
	point: Vector,
	vector: Vector,
}

impl Line_segment
{
	pub fn from_point_vector(point: Vector, vector: Vector) -> Self {Self {point, vector}}
	pub fn from_begin_end(begin: Vector, end: Vector) -> Self {Self::from_point_vector(begin, end - begin)}
	
	pub fn point(&self) -> Vector {self.point}
	pub fn vector(&self) -> Vector {self.vector}
	
	pub fn begin(&self) -> Vector {self.point}
	pub fn end(&self) -> Vector {self.point + self.vector}
}

impl Move_by for Line_segment
{
	fn move_by(&mut self, vector: Vector)
	{
		self.point.move_by(vector);
	}
}

impl Box_of for Line_segment
{
	fn box_of(self) -> Bounding_box
	{
		Bounding_box::from(self.begin()) + self.end()
	}
}

impl Contains<Vector> for Line_segment
{
	fn contains(self, _inner: Vector) -> bool {false}
}

impl Overlaps<Vector> for Line_segment
{
	fn overlaps(self, _rhs: Vector) -> bool {false}
}

impl Contains<Line_segment> for Line_segment
{
	fn contains(self, _inner: Line_segment) -> bool {false}
}

impl Overlaps<Line_segment> for Line_segment
{
	fn overlaps(self, _rhs: Line_segment) -> bool {false}
}

impl Contains<Triangle> for Line_segment
{
	fn contains(self, _inner: Triangle) -> bool {false}
}

impl Overlaps<Triangle> for Line_segment
{
	fn overlaps(self, rhs: Triangle) -> bool
	{
		// t = -(a * x_0 + b * y_0 + c * z_0) / (a * x_t + b * y_t + c * z_t)
		
		let [t, v0, v1, div] = crate::math::utility::solve([
			(-self.vector()).into_array::<f32>(),
			rhs.vectors()[0].into_array::<f32>(),
			rhs.vectors()[1].into_array::<f32>(),
			(self.point() - rhs.point()).into_array::<f32>(),
		]);
		
		if t < 0.0 || v0 < 0.0 || v1 < 0.0
		{
			return false;
		}
		
		if div < t || div < v0 + v1
		{
			return false;
		}
		
		return true;
	}
}

impl Contains<Sphere> for Line_segment
{
	fn contains(self, _inner: Sphere) -> bool {false}
}

impl Overlaps<Sphere> for Line_segment
{
	fn overlaps(mut self, rhs: Sphere) -> bool
	{
		/*
		if rhs.contains(self.point())
		{
			return true;
		}
		*/
		
		self.move_by(-rhs.center());
		
		let a = crate::math::utility::dot_sqr_array(&self.vector().into_array::<f32>());
		let b = 2.0 * crate::math::utility::dot_array(&self.point().into_array::<f32>(), &self.vector().into_array::<f32>());
		let c = crate::math::utility::dot_sqr_array(&self.point().into_array::<f32>()) - f32::from(rhs.radius()).powi(2);
		let discriminant = b.powi(2) - 4.0 * a * c;
		
		if discriminant >= 0.0
		{
			let sqrt_dis = discriminant.sqrt();
			let t_1 = -b - sqrt_dis;
			let t_2 = -b + sqrt_dis;
			
			return (0.0 <= t_1 && t_1 <= 2.0 * a) || (0.0 <= t_2 && t_2 <= 2.0 * a);
		}
		
		return false;
	}
}

impl Contains<Cylinder> for Line_segment
{
	fn contains(self, _inner: Cylinder) -> bool {false}
}

impl Overlaps<Cylinder> for Line_segment
{
	fn overlaps(mut self, rhs: Cylinder) -> bool
	{
		if rhs.contains(self.point())
		{
			return true;
		}
		
		self.move_by(-rhs.center());
		
		let z_0 = f32::from(self.begin()[2]);
		let z_v = f32::from(self.vector()[2]);
		
		if ! (self.vector()[0] == coord_t::from_underlying(0) && self.vector()[1] == coord_t::from_underlying(0))
		{
			let line_xy = Line_segment::from_point_vector(
				Vector::from([self.begin()[0], self.begin()[1], coord_t::from_underlying(0)]),
				Vector::from([self.vector()[0], self.vector()[1], coord_t::from_underlying(0)]),
			);
			
			let a = crate::math::utility::dot_sqr_array(&line_xy.vector().into_array::<f32>());
			let b = 2.0 * crate::math::utility::dot_array(&line_xy.point().into_array::<f32>(), &line_xy.vector().into_array::<f32>());
			let c = crate::math::utility::dot_sqr_array(&line_xy.point().into_array::<f32>()) - f32::from(rhs.radius()).powi(2);
			let discriminant = b.powi(2) - 4.0 * a * c;
			
			if discriminant >= 0.0
			{
				let sqrt_dis = discriminant.sqrt();
				
				for t in [-b - sqrt_dis, -b + sqrt_dis]
				{
					// We are supposed to divide t by (2 * a) to make t a line parameter
					// of range [0, 1]
					// Instead we multiply everything else by 2 * a.
					let z = z_0 * 2.0 * a + t * z_v;
					
					if f32::from(-rhs.height()) * 2.0 * a <= z && z <= f32::from(rhs.height()) * 2.0 * a && 0.0 <= t && t <= 2.0 * a
					{
						return true;
					}
				}
			}
		}
		
		if self.vector()[2] != coord_t::from_underlying(0)
		{
			let z_v = f32::from(self.vector()[2]);
			let t_numerator = f32::from(rhs.height() + self.begin()[2]);
			
			for t in [t_numerator / -z_v, t_numerator / z_v]
			{
				if 0.0 <= t && t <= 1.0
				{
					let value = f32::hypot(f32::from(self.begin()[0]) + t * f32::from(self.vector()[0]),
						f32::from(self.begin()[1]) + t * f32::from(self.vector()[1])
					);
					
					if value <= f32::from(rhs.radius())
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
}

impl Overlaps<Bounding_box> for Line_segment
{
	fn overlaps(self, mut rhs: Bounding_box) -> bool
	{
		rhs.move_by(-self.point());
		
		// For all three axis-aligned views
		// Keep the order so that d0 is always a different dimension
		let dimensions = [[0, 1], [1, 2], [2, 0]];
		for [d0, d1] in dimensions
		{
			let crds = [
				[rhs.min()[d0], rhs.max()[d0]],
				[rhs.min()[d1], rhs.max()[d1]],
			];
			
			let v = [self.vector()[d0], self.vector()[d1]];
			
			if crds[0][0] <= coord_t::from_underlying(0) && coord_t::from_underlying(0) <= crds[0][1]
				&& crds[1][0] <= coord_t::from_underlying(0) && coord_t::from_underlying(0) <= crds[1][1]
			{
				continue;
			}
			
			// Check only d0, the outer loop will check for all coordinates
			if coord_t::from_underlying(0) < crds[0][0] && v[0] < crds[0][0]
			{
				return false;
			}
			
			if crds[0][1] < coord_t::from_underlying(0) && crds[0][1] < v[0]
			{
				return false;
			}
			
			let mut cross_negative = false;
			let mut cross_zero = false;
			let mut cross_positive = false;
			
			for crd0 in crds[0]
			{
				for crd1 in crds[1]
				{
					let result = f32::from(crd0) * f32::from(v[1]) - f32::from(crd1) * f32::from(v[0]);
					
					if result < 0.0 {cross_negative = true;}
					if result == 0.0 {cross_zero = true;}
					if result > 0.0 {cross_positive = true;}
				}
			}
			
			if ! cross_zero && ! (cross_negative && cross_positive)
			{
				return false;
			}
		}
		
		return true;
	}
}

#[test]
fn test_line_triangle()
{
	assert_eq!(true, Line_segment::from_begin_end(
		Vector::of_value(coord_t::round_from(0.0)), Vector::of_value(coord_t::round_from(1.0))).overlaps(
		Triangle::from_points(
			Vector::round_from([0.5, 0.0, 0.0]),
			Vector::round_from([0.0, 0.5, 0.0]),
			Vector::round_from([0.0, 0.0, 1.0]),
		)
	));
	
	assert_eq!(true, Line_segment::from_begin_end(
		Vector::round_from([0.0, 0.0, 0.5]),
		Vector::round_from([1.0, 0.0, 0.5])).overlaps(
		Triangle::from_points(
			Vector::round_from([0.5, 0.0, 0.0]),
			Vector::round_from([0.5, 0.0, 1.0]),
			Vector::round_from([1.0, 1.0, 0.5]),
		)
	));
	
	assert_eq!(false, Line_segment::from_begin_end(
		Vector::of_value(coord_t::round_from(0.0)),
		Vector::of_value(coord_t::round_from(-1.0))).overlaps(
		Triangle::from_points(
			Vector::round_from([0.5, 0.0, 0.0]),
			Vector::round_from([0.0, 0.5, 0.0]),
			Vector::round_from([0.0, 0.0, 1.0]),
		)
	));
}

pub struct Line_iterator<Vector_iterator>
{
	begin: Option<Vector>,
	last: Option<Vector>,
	it: Vector_iterator,
	single_line: bool,
}

pub fn lines<Vector_iterator>(mut it: Vector_iterator) -> Line_iterator<Vector_iterator>
where Vector_iterator: std::iter::Iterator, <Vector_iterator as Iterator>::Item: Into<Vector>
{
	Line_iterator {begin: None, last: it.next().map(Into::into), it, single_line: false}
}

impl<Vector_iterator> std::iter::Iterator for Line_iterator<Vector_iterator>
where Vector_iterator: std::iter::Iterator, <Vector_iterator as Iterator>::Item: Into<Vector>
{
	type Item = Line_segment;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		if let Some(last) = self.last
		{
			if let Some(next) = self.it.next()
			{
				let next = next.into();
				let result = Line_segment::from_begin_end(last, next);
				
				if self.begin.is_none()
				{
					self.begin = self.last;
					self.single_line = true;
				}
				else if self.single_line
				{
					self.single_line = false;
				}
				
				self.last = Some(next);
				
				return Some(result);
			}
			else if ! self.single_line
			{
				if let Some(begin) = self.begin
				{
					let result = Line_segment::from_begin_end(last, begin);
					self.last = None;
					return Some(result);
				}
			}
		}
		
		return None;
	}
}

#[test]
fn test_lines()
{
	{
		let empty: [Vector; 0] = [];
		for _ in lines(empty.iter().cloned())
		{
			assert!(false);
		}
	}
	{
		let single: [Vector; 1] = [Vector::new()];
		for _ in lines(single.iter().cloned())
		{
			assert!(false);
		}
	}
	{
		let double: [Vector; 2] = [
			[0,0,0].map(|v| coord_t::round_from(v as f32)).into(),
			[1,1,1].map(|v| coord_t::round_from(v as f32)).into(),
		];
		assert_eq!(1, lines(double.iter().cloned()).count());
	}
	{
		let triple: [Vector; 3] = [
			[0,0,0].map(|v| coord_t::round_from(v as f32)).into(),
			[1,1,1].map(|v| coord_t::round_from(v as f32)).into(),
			[2,2,2].map(|v| coord_t::round_from(v as f32)).into(),
		];
		assert_eq!(3, lines(triple.iter().cloned()).count());
	}
	{
		let quadruple: [Vector; 4] = [
			[0,0,0].map(|v| coord_t::round_from(v as f32)).into(),
			[1,1,1].map(|v| coord_t::round_from(v as f32)).into(),
			[2,2,2].map(|v| coord_t::round_from(v as f32)).into(),
			[3,3,3].map(|v| coord_t::round_from(v as f32)).into(),
		];
		assert_eq!(4, lines(quadruple.iter().cloned()).count());
	}
}
