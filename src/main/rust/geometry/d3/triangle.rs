use crate::geometry::coord_t;
use crate::geometry::d3::*;

#[derive(Clone, Copy)]
pub struct Triangle
{
	point: Vector,
	v1: Vector,
	v2: Vector,
}

impl Triangle
{
	pub fn from_vectors(point: Vector, v1: Vector, v2: Vector) -> Self {Self {point, v1, v2}}
	pub fn from_points(p1: Vector, p2: Vector, p3: Vector) -> Self {Self::from_vectors(p1, p2 - p1, p3 - p1)}
	
	pub fn point(&self) -> Vector {self.point}
	pub fn vectors(&self) -> [Vector; 2] {[self.v1, self.v2]}
	pub fn points(&self) -> [Vector; 3] {[self.point, self.point + self.v1, self.point + self.v2]}
	
	pub fn normal(&self) -> Vector
	{
		crate::math::utility::cross(self.vectors()[0], self.vectors()[1])
	}
}

impl Move_by for Triangle
{
	fn move_by(&mut self, vector: Vector)
	{
		self.point.move_by(vector);
	}
}

impl Box_of for Triangle
{
	fn box_of(self) -> Bounding_box
	{
		Bounding_box::from(self.point) + (self.point + self.v1) + (self.point + self.v2)
	}
}

impl Contains<Vector> for Triangle
{
	fn contains(self, _inner: Vector) -> bool {false}
}

impl Overlaps<Vector> for Triangle
{
	fn overlaps(self, _rhs: Vector) -> bool {false}
}

impl Contains<Line_segment> for Triangle
{
	fn contains(self, _inner: Line_segment) -> bool {false}
}

impl Overlaps<Line_segment> for Triangle
{
	fn overlaps(self, rhs: Line_segment) -> bool {rhs.overlaps(self)}
}

impl Contains<Triangle> for Triangle
{
	fn contains(self, _inner: Triangle) -> bool {false}
}

impl Overlaps<Triangle> for Triangle
{
	fn overlaps(self, rhs: Triangle) -> bool
	{
		for ll in lines(self.points().iter().copied())
		{
			if ll.overlaps(rhs)
			{
				return true;
			}
		}
		
		for rl in lines(rhs.points().iter().copied())
		{
			if self.overlaps(rl)
			{
				return true;
			}
		}
		
		return false;
	}
}

impl Contains<Sphere> for Triangle
{
	fn contains(self, _inner: Sphere) -> bool {false}
}

impl Overlaps<Sphere> for Triangle
{
	fn overlaps(self, rhs: Sphere) -> bool
	{
		if rhs.contains(self.point())
		{
			return true;
		}
		
		for line in lines(self.points().iter().copied())
		{
			if line.overlaps(rhs)
			{
				return true;
			}
		}
		
		let normal = crate::math::utility::cross(self.vectors()[0], self.vectors()[1]);
		
		let [t, v0, v1, div] = crate::math::utility::solve([
			normal.into_array::<f32>(),
			self.vectors()[0].into_array::<f32>(),
			self.vectors()[1].into_array::<f32>(),
			(rhs.center() - self.point()).into_array::<f32>(),
		]);
		
		if t.abs() > f32::from(rhs.radius()) * f32::from(normal.length())
		{
			return false;
		}
		
		if v0 < 0.0 || v1 < 0.0
		{
			return false;
		}
		
		if div < v0 + v1
		{
			return false;
		}
		
		return true;
	}
}

impl Contains<Cylinder> for Triangle
{
	fn contains(self, _inner: Cylinder) -> bool {false}
}

impl Overlaps<Cylinder> for Triangle
{
	fn overlaps(self, rhs: Cylinder) -> bool
	{
		for line in lines(self.points().iter().copied())
		{
			if line.overlaps(rhs)
			{
				return true;
			}
		}
		
		if self.overlaps(Line_segment::from_point_vector(
			rhs.center() - Vector::from([coord_t::from_underlying(0), coord_t::from_underlying(0), rhs.height()]),
			Vector::from([coord_t::from_underlying(0), coord_t::from_underlying(0), rhs.height() + rhs.height()])))
		{
			return true;
		}
		
		let mut normal = crate::math::utility::cross(self.vectors()[0], self.vectors()[1]);
		normal[2] = coord_t::from_underlying(0);
		
		for h in [-rhs.height(), rhs.height()]
		{
			let [t, v0, v1, div] = crate::math::utility::solve([
				normal.into_array::<f32>(),
				self.vectors()[0].into_array::<f32>(),
				self.vectors()[1].into_array::<f32>(),
				(rhs.center() + Vector::from([coord_t::from_underlying(0), coord_t::from_underlying(0), h]) - self.point()).into_array::<f32>(),
			]);
			
			if t.abs() > f32::from(rhs.radius()) * f32::from(normal.length())
			{
				continue;
			}
			
			if v0 < 0.0 || v1 < 0.0
			{
				continue;
			}
			
			if div < v0 + v1
			{
				continue;
			}
			
			return true;
		}
		
		return false;
	}
}

impl Overlaps<Bounding_box> for Triangle
{
	fn overlaps(self, rhs: Bounding_box) -> bool
	{
		let ps = self.points();
		
		for [i0, i1] in [[0, 1], [1, 2], [2, 0]]
		{
			let line = Line_segment::from_begin_end(ps[i0], ps[i1]);
			
			if line.overlaps(rhs)
			{
				return true;
			}
		}
		
		let min = rhs.min();
		let max = rhs.max();
		
		if self.overlaps(Line_segment::from_begin_end(min, max))
		{
			return true;
		}
		
		if self.overlaps(Line_segment::from_begin_end(
			Vector::from([max[0], min[1], min[2]]), Vector::from([min[0], max[1], max[2]])))
		{
			return true;
		}
		
		if self.overlaps(Line_segment::from_begin_end(
			Vector::from([min[0], min[1], max[2]]), Vector::from([max[0], max[1], min[2]])))
		{
			return true;
		}
		
		if self.overlaps(Line_segment::from_begin_end(
			Vector::from([max[0], min[1], max[2]]), Vector::from([min[0], max[1], min[2]])))
		{
			return true;
		}
		
		return false;
	}
}

pub struct Triangle_iterator<Vector_iterator>
{
	shuffle: bool,
	pprev: Option<Vector>,
	prev: Option<Vector>,
	it: Vector_iterator,
}

pub fn triangles<Vector_iterator>(mut it: Vector_iterator) -> Triangle_iterator<Vector_iterator>
where Vector_iterator: std::iter::Iterator, <Vector_iterator as Iterator>::Item: Into<Vector>
{
	let pprev = it.next().map(Into::into);
	let prev = it.next().map(Into::into);
	Triangle_iterator {shuffle: false, pprev, prev, it}
}

impl<Vector_iterator> std::iter::Iterator for Triangle_iterator<Vector_iterator>
where Vector_iterator: std::iter::Iterator, <Vector_iterator as Iterator>::Item: Into<Vector>
{
	type Item = Triangle;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		match (self.pprev, self.prev, self.it.next().map(Into::into))
		{
			(Some(first), Some(second), Some(third)) =>
			{
				let result;
				
				if ! self.shuffle
				{
					result = Triangle::from_points(first, second, third);
				}
				else
				{
					result = Triangle::from_points(first, third, second);
				}
				
				self.shuffle = ! self.shuffle;
				
				self.pprev = Some(second);
				self.prev = Some(third);
				
				Some(result)
			},
			_ => None,
		}
	}
}
