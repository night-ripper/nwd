use crate::geometry::coord_t;
use crate::geometry::d3::*;

#[derive(Clone, Copy)]
pub struct Cylinder
{
	center: Vector,
	radius: coord_t,
	height: coord_t,
}

impl Cylinder
{
	pub fn new(center: Vector, radius: coord_t, height: coord_t) -> Self {Self {center, radius, height}}
	pub fn center(&self) -> Vector {self.center}
	pub fn radius(&self) -> coord_t {self.radius}
	pub fn height(&self) -> coord_t {self.height}
}

impl Move_by for Cylinder
{
	fn move_by(&mut self, vector: Vector)
	{
		self.center.move_by(vector);
	}
}

impl Box_of for Cylinder
{
	fn box_of(self) -> Bounding_box
	{
		Bounding_box::new(
			[self.center[0] - self.radius, self.center[1] - self.radius, self.center[2] - self.height].into(),
			[self.center[0] + self.radius, self.center[1] + self.radius, self.center[2] + self.height].into(),
		)
	}
}

impl Contains<Vector> for Cylinder
{
	fn contains(self, mut inner: Vector) -> bool
	{
		inner.move_by(-self.center());
		
		if inner.length_xy() > f32::from(self.radius())
		{
			return false;
		}
		
		if inner[2] < -self.height()
			|| inner[2] > self.height()
		{
			return false;
		}
		
		return true;
	}
}

impl Overlaps<Vector> for Cylinder
{
	fn overlaps(self, rhs: Vector) -> bool {self.contains(rhs)}
}

impl Contains<Line_segment> for Cylinder
{
	fn contains(self, inner: Line_segment) -> bool
	{
		self.contains(inner.begin()) && self.contains(inner.end())
	}
}

impl Overlaps<Line_segment> for Cylinder
{
	fn overlaps(self, rhs: Line_segment) -> bool {rhs.overlaps(self)}
}

impl Contains<Triangle> for Cylinder
{
	fn contains(self, inner: Triangle) -> bool
	{
		inner.points().iter().all(|p| self.contains(*p))
	}
}

impl Overlaps<Triangle> for Cylinder
{
	fn overlaps(self, rhs: Triangle) -> bool {rhs.overlaps(self)}
}

impl Contains<Sphere> for Cylinder
{
	fn contains(self, mut inner: Sphere) -> bool
	{
		inner.move_by(-self.center());
		
		if inner.center().length_xy() > f32::from(self.radius() - inner.radius())
		{
			return false;
		}
		
		if inner.center()[2] - inner.radius() < -self.height()
			|| inner.center()[2] + inner.radius() > self.height()
		{
			return false;
		}
		
		return true;
	}
}

impl Overlaps<Sphere> for Cylinder
{
	fn overlaps(self, rhs: Sphere) -> bool {rhs.overlaps(self)}
}

impl Contains<Cylinder> for Cylinder
{
	fn contains(self, mut inner: Cylinder) -> bool
	{
		inner.move_by(-self.center());
		
		if inner.center().length_xy() > (self.radius() - inner.radius()).into()
		{
			return false;
		}
		
		if inner.center()[2] - inner.height() < -self.height()
			|| inner.center()[2] + inner.height() > self.height()
		{
			return false;
		}
		
		return true;
	}
}

impl Overlaps<Cylinder> for Cylinder
{
	fn overlaps(self, rhs: Cylinder) -> bool
	{
		if self.center().distance_xy(rhs.center()) > (self.radius() + rhs.radius()).into()
		{
			return false;
		}
		
		if self.center()[2] - self.height() > rhs.center()[2] + rhs.height()
			|| rhs.center()[2] - rhs.height() > self.center()[2] + self.height()
		{
			return false;
		}
		
		return true;
	}
}

impl Overlaps<Bounding_box> for Cylinder
{
	fn overlaps(self, mut rhs: Bounding_box) -> bool
	{
		if rhs.contains(self.center())
		{
			return true;
		}
		
		rhs.move_by(-self.center());
		
		let r = f32::from(self.radius());
		
		let box_inside_xy = || -> bool {rhs.center().length_xy() <= r};
		let cylinder_inside_xy = || -> bool
		{
			return f32::from(rhs.min()[0]) <= -r && r <= f32::from(rhs.max()[0])
				&& f32::from(rhs.min()[1]) <= -r && r <= f32::from(rhs.max()[1])
			;
		};
		
		let values = [rhs.min()[0], rhs.min()[1], rhs.max()[0], rhs.max()[1]];
		let abs_r = r.abs();
		
		// Check of any of the top-down view quadratic equations has at least one solution
		// Need to check for all 4
		if ! cylinder_inside_xy() && ! box_inside_xy()
			&& ! values.iter().any(|v| f32::from(*v).abs() <= abs_r)
		{
			return false;
		}
		
		// Side view for vertical intersections in x and y dimensions
		for d in [0, 1]
		{
			if rhs.max()[d] < -self.radius() || self.radius() < rhs.min()[d]
			{
				return false;
			}
		}
		
		// Side view for horizontal intersections from any direction since height is the same
		if rhs.max()[2] < -self.height() || self.height() < rhs.min()[2]
		{
			return false;
		}
		
		return true;
	}
}
