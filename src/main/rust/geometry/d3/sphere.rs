use crate::geometry::coord_t;
use crate::geometry::d3::*;

#[derive(Clone, Copy)]
pub struct Sphere
{
	center: Vector,
	radius: coord_t,
}

impl Sphere
{
	pub fn new(center: Vector, radius: coord_t) -> Self {Self {center, radius}}
	pub fn center(&self) -> Vector {self.center}
	pub fn radius(&self) -> coord_t {self.radius}
}

impl Move_by for Sphere
{
	fn move_by(&mut self, vector: Vector)
	{
		self.center.move_by(vector);
	}
}

impl Box_of for Sphere
{
	fn box_of(self) -> Bounding_box
	{
		Bounding_box::new(
			[self.center[0] - self.radius, self.center[1] - self.radius, self.center[2] - self.radius].into(),
			[self.center[0] + self.radius, self.center[1] + self.radius, self.center[2] + self.radius].into(),
		)
	}
}

impl Contains<Vector> for Sphere
{
	fn contains(self, inner: Vector) -> bool
	{
		self.center.distance(inner) <= self.radius().into()
	}
}

impl Overlaps<Vector> for Sphere
{
	fn overlaps(self, rhs: Vector) -> bool
	{
		self.contains(rhs)
	}
}

impl Contains<Line_segment> for Sphere
{
	fn contains(self, inner: Line_segment) -> bool
	{
		self.contains(inner.begin()) && self.contains(inner.end())
	}
}

impl Overlaps<Line_segment> for Sphere
{
	fn overlaps(self, rhs: Line_segment) -> bool {rhs.overlaps(self)}
}

impl Contains<Triangle> for Sphere
{
	fn contains(self, inner: Triangle) -> bool
	{
		inner.points().iter().all(|p| self.contains(*p))
	}
}

impl Overlaps<Triangle> for Sphere
{
	fn overlaps(self, rhs: Triangle) -> bool {rhs.overlaps(self)}
}

impl Contains<Sphere> for Sphere
{
	fn contains(self, inner: Sphere) -> bool
	{
		self.center.distance(inner.center()) <= (self.radius() - inner.radius()).into()
	}
}

impl Overlaps<Sphere> for Sphere
{
	fn overlaps(self, rhs: Sphere) -> bool
	{
		self.center.distance(rhs.center()) <= (self.radius() + rhs.radius()).into()
	}
}

impl Contains<Cylinder> for Sphere
{
	fn contains(self, inner: Cylinder) -> bool
	{
		self.contains(Sphere::new(inner.center(),
			coord_t::ceil_from(f32::hypot(f32::from(inner.radius()), f32::from(inner.height())))
		))
	}
}

impl Overlaps<Cylinder> for Sphere
{
	fn overlaps(self, mut rhs: Cylinder) -> bool
	{
		if self.contains(rhs) || rhs.contains(self)
		{
			return true;
		}
		
		rhs.move_by(-self.center());
		
		let dist_xy = rhs.center().length_xy();
		
		if dist_xy > f32::from(self.radius()) + f32::from(rhs.radius())
		{
			return false;
		}
		
		for h in [-rhs.height(), rhs.height()]
		{
			let z = f32::from(rhs.center()[2] + h);
			
			if z.abs() <= f32::from(self.radius())
				&& (f32::from(self.radius()).powi(2) - z.powi(2)).sqrt() + f32::from(rhs.radius()) >= dist_xy
			{
				return true;
			}
		}
		
		return false;
	}
}

impl Overlaps<Bounding_box> for Sphere
{
	fn overlaps(self, mut rhs: Bounding_box) -> bool
	{
		if rhs.contains(self.center())
		{
			return true;
		}
		
		rhs.move_by(-self.center());
		
		let r_sqr = f32::from(self.radius()).powi(2);
		
		for [d0, d1] in [[0, 1], [0, 2], [1, 2]]
		{
			let vertices = [rhs.min()[d0], rhs.max()[d0], rhs.min()[d1], rhs.max()[d1]]
				.map(|v| {let f = f32::from(v); f.powi(2).copysign(f)})
			;
			let min0 = vertices[0].abs().min(vertices[1].abs());
			let min1 = vertices[2].abs().min(vertices[3].abs());
			
			if (vertices[1] < -(r_sqr - min1) || r_sqr - min1 < vertices[0])
				&& (vertices[3] < -(r_sqr - min0) || r_sqr - min0 < vertices[2])
			{
				return false;
			}
		}
		
		return true;
	}
}
