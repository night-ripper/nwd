use crate::utility::option;

pub mod ffi;

macro_rules! call_checked
{
	($function: expr) =>
	{
		unsafe
		{
			match $function
			{
				ffi::VkResult::VK_SUCCESS => Ok(()),
				err => Err(err),
			}
		}
	};
}

pub type Result<Type> = std::result::Result<Type, ffi::VkResult>;

pub fn enumerate_instance_extension_properties() -> Vec::<ffi::VkExtensionProperties>
{
	unsafe
	{
		let mut count = 0;
		ffi::vkEnumerateInstanceExtensionProperties(std::ptr::null(), &mut count, std::ptr::null_mut());
		let mut properties = Vec::<ffi::VkExtensionProperties>::with_capacity(count as usize);
		ffi::vkEnumerateInstanceExtensionProperties(std::ptr::null(), &mut count, properties.as_mut_ptr());
		properties.set_len(count as usize);
		return properties;
	}
}

pub struct Instance
{
	instance: ffi::VkInstance,
	callbacks: Option<ffi::VkAllocationCallbacks>,
}

impl Drop for Instance
{
	fn drop(&mut self)
	{
		unsafe {ffi::vkDestroyInstance(self.instance, option::as_ptr(&self.callbacks))};
	}
}

impl Instance
{
	pub fn create(create_info: &ffi::VkInstanceCreateInfo, allocator: Option<ffi::VkAllocationCallbacks>) -> Result<Self>
	{
		let mut instance = ffi::VkInstance(std::ptr::null_mut());
		call_checked!(ffi::vkCreateInstance(create_info, option::as_ptr(&allocator), &mut instance))?;
		Ok(Self {instance, callbacks: allocator})
	}
}
