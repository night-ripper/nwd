type FT_Encoding = u32;

const fn get_encoding_from_chars(chars: [u8; 4]) -> FT_Encoding
{
	0
	| (chars[0] as FT_Encoding) << 24
	| (chars[1] as FT_Encoding) << 16
	| (chars[2] as FT_Encoding) << 8
	| (chars[3] as FT_Encoding) << 0
}

const FT_ENCODING_UNICODE: FT_Encoding = get_encoding_from_chars([b'u', b'n', b'i', b'c']);

#[repr(C)]
struct FT_Generic
{
	data: *mut std::ffi::c_void,
	finalizer: Option<extern "C" fn(*mut std::ffi::c_void)>,
}

type FT_Pos = std::ffi::c_long;

#[repr(C)]
struct FT_BBox
{
	xMin: FT_Pos,
	yMin: FT_Pos,
    xMax: FT_Pos,
	yMax: FT_Pos,
}

#[repr(C)]
struct FT_ListRec
{
	head: *mut std::ffi::c_void,
	tail: *mut std::ffi::c_void,
}

#[repr(C)]
struct FT_Vector
{
	x: FT_Pos,
	y: FT_Pos,
}

#[repr(C)]
struct  FT_Glyph_Metrics
{
	width: FT_Pos,
	height: FT_Pos,
	
	horiBearingX: FT_Pos,
	horiBearingY: FT_Pos,
	horiAdvance: FT_Pos,
	
	vertBearingX: FT_Pos,
	vertBearingY: FT_Pos,
	vertAdvance: FT_Pos,
}

#[repr(C)]
struct FT_Bitmap
{
	rows: std::ffi::c_uint,
	width: std::ffi::c_uint,
	pitch: std::ffi::c_int,
	buffer: std::ptr::NonNull<std::ffi::c_uchar>,
	num_grays: std::ffi::c_ushort,
	pixel_mode: std::ffi::c_uchar,
	palette_mode: std::ffi::c_uchar,
	palette: *mut std::ffi::c_void,
}

#[repr(C)]
struct FT_Outline
{
	n_contours: std::ffi::c_short,
	n_points: std::ffi::c_short,
	
	points: *mut FT_Vector,
	tags: *mut std::ffi::c_char,
	contours: *mut std::ffi::c_short,
	
	flags: std::ffi::c_int,
}

#[repr(C)]
struct FT_GlyphSlotRec
{
	library: *mut std::ffi::c_void,
	face: *mut std::ffi::c_void,
	next: *mut std::ffi::c_void,
	glyph_index: std::ffi::c_uint,
	generic: FT_Generic,
	
	metrics: FT_Glyph_Metrics,
	linearHoriAdvance: std::ffi::c_long,
	linearVertAdvance: std::ffi::c_long,
	advance: FT_Vector,
	
	format: std::ffi::c_uint,
	
	bitmap: FT_Bitmap,
	bitmap_left: std::ffi::c_int,
	bitmap_top: std::ffi::c_int,
	
	outline: FT_Outline,
	
	num_subglyphs: std::ffi::c_uint,
	subglyphs: *mut std::ffi::c_void,
	
	control_data: *mut std::ffi::c_void,
	control_len: std::ffi::c_long,
	
	lsb_delta: FT_Pos,
	rsb_delta: FT_Pos,
	
	other: *mut std::ffi::c_void,
	
	internal: *mut std::ffi::c_void,
}

#[repr(C)]
struct FT_CharMapRec
{
	face: *mut FT_FaceRec,
	encoding: FT_Encoding,
	platform_id: std::ffi::c_ushort,
	encoding_id: std::ffi::c_ushort,
}

#[repr(C)]
struct FT_FaceRec
{
	num_faces: std::ffi::c_long,
	face_index: std::ffi::c_long,
	
	face_flags: std::ffi::c_long,
	style_flags: std::ffi::c_long,
	
	num_glyphs: std::ffi::c_long,
	
	family_name: *mut std::ffi::c_char,
	style_name: *mut std::ffi::c_char,
	
	num_fixed_sizes: std::ffi::c_int,
	available_sizes: *mut std::ffi::c_void, // FT_Bitmap_Size*
	
	num_charmaps: std::ffi::c_int,
	charmaps: *mut std::ffi::c_void, // FT_CharMap*
	
	generic: FT_Generic,

	bbox: FT_BBox,
	
	units_per_EM: std::ffi::c_ushort,
	ascender: std::ffi::c_short,
	descender: std::ffi::c_short,
	height: std::ffi::c_short,
	
	max_advance_width: std::ffi::c_short,
	max_advance_height: std::ffi::c_short,

	underline_position: std::ffi::c_short,
	underline_thickness: std::ffi::c_short,
	
	glyph: std::ptr::NonNull<FT_GlyphSlotRec>,
	size: *mut std::ffi::c_void, // FT_Size
	charmap: *mut FT_CharMapRec,
	
	driver: *mut std::ffi::c_void, // FT_Driver
	memory: *mut std::ffi::c_void, // FT_Memory
	stream: *mut std::ffi::c_void, // FT_Stream
	
	sizes_list: FT_ListRec,
	
	autohint: FT_Generic,
	extensions: *mut std::ffi::c_void,

	internal: *mut std::ffi::c_void, // FT_Face_Internal
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Freetype_error
{
	unknown {code: u32},
	error {code: u32, message: &'static str},
}

impl std::fmt::Display for Freetype_error
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "Freetype error: ")?;
		
		match self
		{
			Self::unknown {code} => write!(f, "code {}: <unknown error>", code),
			Self::error {code, message} => write!(f, "code {}: {}", code, message),
		}
	}
}

impl std::error::Error for Freetype_error
{
}

fn check_error(return_code: i32) -> Result<(), Freetype_error>
{
	let code = return_code as u32;
	match code
	{
		// generic errors
		0x00 => Ok(()),
		0x01 => Err(Freetype_error::error {code, message: "cannot open resource"}),
		0x02 => Err(Freetype_error::error {code, message: "unknown file format"}),
		0x03 => Err(Freetype_error::error {code, message: "broken file"}),
		0x04 => Err(Freetype_error::error {code, message: "invalid FreeType version"}),
		0x05 => Err(Freetype_error::error {code, message: "module version is too low"}),
		0x06 => Err(Freetype_error::error {code, message: "invalid argument"}),
		0x07 => Err(Freetype_error::error {code, message: "unimplemented feature"}),
		0x08 => Err(Freetype_error::error {code, message: "broken table"}),
		0x09 => Err(Freetype_error::error {code, message: "broken offset within table"}),
		0x0A => Err(Freetype_error::error {code, message: "array allocation size too large"}),
		0x0B => Err(Freetype_error::error {code, message: "missing module"}),
		0x0C => Err(Freetype_error::error {code, message: "missing property"}),
		// glyph/character errors
		0x10 => Err(Freetype_error::error {code, message: "invalid glyph index"}),
		0x11 => Err(Freetype_error::error {code, message: "invalid character code"}),
		0x12 => Err(Freetype_error::error {code, message: "unsupported glyph image format"}),
		0x13 => Err(Freetype_error::error {code, message: "cannot render this glyph format"}),
		0x14 => Err(Freetype_error::error {code, message: "invalid outline"}),
		0x15 => Err(Freetype_error::error {code, message: "invalid composite glyph"}),
		0x16 => Err(Freetype_error::error {code, message: "too many hints"}),
		0x17 => Err(Freetype_error::error {code, message: "invalid pixel size"}),
		0x18 => Err(Freetype_error::error {code, message: "invalid SVG document"}),
		// handle errors
		0x20 => Err(Freetype_error::error {code, message: "invalid object handle"}),
		0x21 => Err(Freetype_error::error {code, message: "invalid library handle"}),
		0x22 => Err(Freetype_error::error {code, message: "invalid module handle"}),
		0x23 => Err(Freetype_error::error {code, message: "invalid face handle"}),
		0x24 => Err(Freetype_error::error {code, message: "invalid size handle"}),
		0x25 => Err(Freetype_error::error {code, message: "invalid glyph slot handle"}),
		0x26 => Err(Freetype_error::error {code, message: "invalid charmap handle"}),
		0x27 => Err(Freetype_error::error {code, message: "invalid cache manager handle"}),
		0x28 => Err(Freetype_error::error {code, message: "invalid stream handle"}),
		// driver errors
		0x30 => Err(Freetype_error::error {code, message: "too many modules"}),
		0x31 => Err(Freetype_error::error {code, message: "too many extensions"}),
		// memory errors
		0x40 => Err(Freetype_error::error {code, message: "out of memory"}),
		0x41 => Err(Freetype_error::error {code, message: "unlisted object"}),
		// stream errors
		0x51 => Err(Freetype_error::error {code, message: "cannot open stream"}),
		0x52 => Err(Freetype_error::error {code, message: "invalid stream seek"}),
		0x53 => Err(Freetype_error::error {code, message: "invalid stream skip"}),
		0x54 => Err(Freetype_error::error {code, message: "invalid stream read"}),
		0x55 => Err(Freetype_error::error {code, message: "invalid stream operation"}),
		0x56 => Err(Freetype_error::error {code, message: "invalid frame operation"}),
		0x57 => Err(Freetype_error::error {code, message: "nested frame access"}),
		0x58 => Err(Freetype_error::error {code, message: "invalid frame read"}),
		// raster errors
		0x60 => Err(Freetype_error::error {code, message: "raster uninitialized"}),
		0x61 => Err(Freetype_error::error {code, message: "raster corrupted"}),
		0x62 => Err(Freetype_error::error {code, message: "raster overflow"}),
		0x63 => Err(Freetype_error::error {code, message: "negative height while rastering"}),
		// cache errors
		0x70 => Err(Freetype_error::error {code, message: "too many registered caches"}),
		// TrueType and SFNT errors
		0x80 => Err(Freetype_error::error {code, message: "invalid opcode"}),
		0x81 => Err(Freetype_error::error {code, message: "too few arguments"}),
		0x82 => Err(Freetype_error::error {code, message: "stack overflow"}),
		0x83 => Err(Freetype_error::error {code, message: "code overflow"}),
		0x84 => Err(Freetype_error::error {code, message: "bad argument"}),
		0x85 => Err(Freetype_error::error {code, message: "division by zero"}),
		0x86 => Err(Freetype_error::error {code, message: "invalid reference"}),
		0x87 => Err(Freetype_error::error {code, message: "found debug opcode"}),
		0x88 => Err(Freetype_error::error {code, message: "found ENDF opcode in execution stream"}),
		0x89 => Err(Freetype_error::error {code, message: "nested DEFS"}),
		0x8A => Err(Freetype_error::error {code, message: "invalid code range"}),
		0x8B => Err(Freetype_error::error {code, message: "execution context too long"}),
		0x8C => Err(Freetype_error::error {code, message: "too many function definitions"}),
		0x8D => Err(Freetype_error::error {code, message: "too many instruction definitions"}),
		0x8E => Err(Freetype_error::error {code, message: "SFNT font table missing"}),
		0x8F => Err(Freetype_error::error {code, message: "horizontal header (hhea) table missing"}),
		0x90 => Err(Freetype_error::error {code, message: "locations (loca) table missing"}),
		0x91 => Err(Freetype_error::error {code, message: "name table missing"}),
		0x92 => Err(Freetype_error::error {code, message: "character map (cmap) table missing"}),
		0x93 => Err(Freetype_error::error {code, message: "horizontal metrics (hmtx) table missing"}),
		0x94 => Err(Freetype_error::error {code, message: "PostScript (post) table missing"}),
		0x95 => Err(Freetype_error::error {code, message: "invalid horizontal metrics"}),
		0x96 => Err(Freetype_error::error {code, message: "invalid character map (cmap) format"}),
		0x97 => Err(Freetype_error::error {code, message: "invalid ppem value"}),
		0x98 => Err(Freetype_error::error {code, message: "invalid vertical metrics"}),
		0x99 => Err(Freetype_error::error {code, message: "could not find context"}),
		0x9A => Err(Freetype_error::error {code, message: "invalid PostScript (post) table format"}),
		0x9B => Err(Freetype_error::error {code, message: "invalid PostScript (post) table"}),
		0x9C => Err(Freetype_error::error {code, message: "found FDEF or IDEF opcode in glyph bytecode"}),
		0x9D => Err(Freetype_error::error {code, message: "missing bitmap in strike"}),
		0x9E => Err(Freetype_error::error {code, message: "SVG hooks have not been set"}),
		// CFF, CID, and Type 1 errors
		0xA0 => Err(Freetype_error::error {code, message: "opcode syntax error"}),
		0xA1 => Err(Freetype_error::error {code, message: "argument stack underflow"}),
		0xA2 => Err(Freetype_error::error {code, message: "ignore"}),
		0xA3 => Err(Freetype_error::error {code, message: "no Unicode glyph name found"}),
		0xA4 => Err(Freetype_error::error {code, message: "glyph too big for hinting"}),
		// BDF errors
		0xB0 => Err(Freetype_error::error {code, message: "`STARTFONT' field missing"}),
		0xB1 => Err(Freetype_error::error {code, message: "`FONT' field missing"}),
		0xB2 => Err(Freetype_error::error {code, message: "`SIZE' field missing"}),
		0xB3 => Err(Freetype_error::error {code, message: "`FONTBOUNDINGBOX' field missing"}),
		0xB4 => Err(Freetype_error::error {code, message: "`CHARS' field missing"}),
		0xB5 => Err(Freetype_error::error {code, message: "`STARTCHAR' field missing"}),
		0xB6 => Err(Freetype_error::error {code, message: "`ENCODING' field missing"}),
		0xB7 => Err(Freetype_error::error {code, message: "`BBX' field missing"}),
		0xB8 => Err(Freetype_error::error {code, message: "`BBX' too big"}),
		0xB9 => Err(Freetype_error::error {code, message: "font header corrupted or missing fields"}),
		0xBA => Err(Freetype_error::error {code, message: "font glyphs corrupted or missing fields"}),
		code => Err(Freetype_error::unknown {code}),
	}
}

#[link(name = "freetype")]
extern "C"
{
	fn FT_Done_FreeType(library: *mut std::ffi::c_void) -> std::ffi::c_int;
	fn FT_Init_FreeType(library: *mut *mut std::ffi::c_void) -> std::ffi::c_int;
	
	fn FT_Done_Face(face: *mut FT_FaceRec) -> std::ffi::c_int;
	fn FT_New_Face(library: *mut std::ffi::c_void, filepath: *const std::ffi::c_char, face_index: std::ffi::c_long, face: *mut *mut FT_FaceRec) -> std::ffi::c_int;
	
	fn FT_Select_Charmap(face: *mut FT_FaceRec, encoding: FT_Encoding) -> std::ffi::c_int;
	fn FT_Set_Pixel_Sizes(face: *mut FT_FaceRec, pixel_width: std::ffi::c_uint, pixel_height: std::ffi::c_uint) -> std::ffi::c_int;
	fn FT_Load_Char(face: *mut FT_FaceRec, char_code: std::ffi::c_ulong, load_flags: i32) -> std::ffi::c_int;
}

const FT_LOAD_DEFAULT: i32 = 0;
const FT_LOAD_NO_SCALE: i32 = 1 << 0;
const FT_LOAD_NO_HINTING: i32 = 1 << 1;
const FT_LOAD_RENDER: i32 = 1 << 2;
const FT_LOAD_NO_BITMAP: i32 = 1 << 3;
const FT_LOAD_VERTICAL_LAYOUT: i32 = 1 << 4;
const FT_LOAD_FORCE_AUTOHINT: i32 = 1 << 5;
const FT_LOAD_CROP_BITMAP: i32 =  1 << 6;
const FT_LOAD_PEDANTIC: i32 =  1 << 7;
const FT_LOAD_IGNORE_GLOBAL_ADVANCE_WIDTH: i32 =  1 << 9;
const FT_LOAD_NO_RECURSE: i32 =  1 << 10;
const FT_LOAD_IGNORE_TRANSFORM: i32 =  1 << 11;
const FT_LOAD_MONOCHROME: i32 =  1 << 12;
const FT_LOAD_LINEAR_DESIGN: i32 =  1 << 13;
const FT_LOAD_SBITS_ONLY: i32 =  1 << 14;
const FT_LOAD_NO_AUTOHINT: i32 =  1 << 15;
const FT_LOAD_COLOR: i32 =  1 << 20;
const FT_LOAD_COMPUTE_METRICS: i32 =  1 << 21;
const FT_LOAD_BITMAP_METRICS_ONLY: i32 =  1 << 22;

pub struct Glyph<'t>
{
	pub width: u32,
	pub height: u32,
	pub bearing_x: i32,
	pub bearing_y: i32,
	pub advance_x_64: i64,
	pub bitmap: &'t[u8],
}

pub struct Face
{
	face: std::ptr::NonNull<FT_FaceRec>,
}

impl Drop for Face
{
	fn drop(&mut self)
	{
		if let Err(error) = check_error(unsafe {FT_Done_Face(self.face.as_ptr())})
		{
			panic!("Freetype error: FT_Done_Face returned an error: {}", error);
		}
	}
}

impl Face
{
	pub fn set_pixel_sizes(&mut self, width: u32, height: u32)
	{
		check_error(unsafe {FT_Set_Pixel_Sizes(self.face.as_ptr(), width, height)}).unwrap()
	}
	
	pub fn load_char<'t>(&'t mut self, character: char) -> Glyph<'t>
	{
		check_error(unsafe {FT_Load_Char(self.face.as_ptr(), character as std::ffi::c_ulong, FT_LOAD_RENDER)}).unwrap();
		
		let glyph = unsafe {self.face.as_ref().glyph.as_ref()};
		
		let width = glyph.bitmap.width;
		let height = glyph.bitmap.rows;
		
		Glyph
		{
			width,
			height,
			bearing_x: glyph.bitmap_left,
			bearing_y: glyph.bitmap_top,
			advance_x_64: glyph.advance.x as i64,
			bitmap: unsafe {std::slice::from_raw_parts(glyph.bitmap.buffer.as_ptr(), width as usize * height as usize)},
		}
	}
}

pub struct Library
{
	library: std::ptr::NonNull<std::ffi::c_void>,
}

impl Drop for Library
{
	fn drop(&mut self)
	{
		if let Err(error) = check_error(unsafe {FT_Done_FreeType(self.library.as_ptr())})
		{
			panic!("Freetype error: FT_Done_FreeType returned an error {}", error);
		}
	}
}

impl Library
{
	pub fn new() -> Self
	{
		let mut library = std::ptr::null_mut();
		check_error(unsafe {FT_Init_FreeType(&mut library)}).unwrap();
		let library = std::ptr::NonNull::new(library).unwrap();
		Self {library}
	}
	
	pub fn load_face(&mut self, filepath: &std::ffi::CStr) -> Face
	{
		let mut face = std::ptr::null_mut();
		check_error(unsafe {FT_New_Face(self.library.as_ptr(), filepath.as_ptr(), 0, &mut face)}).unwrap();
		let face = std::ptr::NonNull::new(face).unwrap();
		check_error(unsafe {FT_Select_Charmap(face.as_ptr(), FT_ENCODING_UNICODE)}).unwrap();
		Face {face}
	}
}
