use crate::graphics::opengl::types::*;
use crate::graphics::opengl::constants;

#[link(name = "GL")]
extern "C"
{
	fn glDeleteBuffers(n: GLsizei, buffers: *const GLuint);
	fn glCreateBuffers(n: GLsizei, buffers: *mut GLuint);
	fn glNamedBufferData(buffer: GLuint, size: GLsizeiptr, data: *const std::ffi::c_void, usage: GLenum);
	
	fn glDeleteVertexArrays(n: GLsizei, buffers: *const GLuint);
	fn glCreateVertexArrays(n: GLsizei, buffers: *mut GLuint);
	
	fn glEnableVertexArrayAttrib(vaobj: GLuint, index: GLuint);
	fn glVertexArrayAttribBinding(vaobj: GLuint, attribindex: GLuint, bindingindex: GLuint);
	
	fn glVertexArrayAttribFormat(vaobj: GLuint, attribindex: GLuint, size: GLint,
		typee: GLenum, normalized: GLboolean, relativeoffset: GLuint
	);
	fn glVertexArrayAttribIFormat(vaobj: GLuint, attribindex: GLuint, size: GLint,
		typee: GLenum, relativeoffset: GLuint
	);
	fn glVertexArrayAttribLFormat(vaobj: GLuint, attribindex: GLuint, size: GLint,
		typee: GLenum, relativeoffset: GLuint
	);
	
	fn glVertexArrayVertexBuffer(vaobj: GLuint, bindingindex: GLuint, buffer: GLuint, offset: GLintptr, stride: GLsizei);
	fn glVertexArrayElementBuffer(vaobj: GLuint, buffer: GLuint);
	
	fn glBindVertexArray(array: GLuint);
	fn glBindBufferBase(target: GLenum, index: GLuint, buffer: GLuint);
}

pub struct Buffer
{
	object: u32,
}

impl Drop for Buffer
{
	fn drop(&mut self)
	{
		unsafe {glDeleteBuffers(1, &mut self.object)};
	}
}

fn create_buffer() -> u32
{
	let mut object = 0;
	unsafe {glCreateBuffers(1, &mut object)};
	return object;
}

impl Buffer
{
	pub fn create() -> Self
	{
		Self {object: create_buffer()}
	}
	
	pub fn data(&self, data: &[u8])
	{
		unsafe {glNamedBufferData(self.object, data.len() as GLsizeiptr, data.as_ptr() as *const std::ffi::c_void, constants::GL_DYNAMIC_DRAW)};
	}
}

pub struct Vertex_array
{
	object: u32,
}

impl Drop for Vertex_array
{
	fn drop(&mut self)
	{
		unsafe {glDeleteVertexArrays(1, &mut self.object)};
	}
}

impl Vertex_array
{
	pub fn create() -> Self
	{
		let mut object = 0;
		unsafe {glCreateVertexArrays(1, &mut object)};
		return Self {object};
	}
	
	pub fn format_bind_data_buffer(&self, attribute_index: u32, size: i32, typee: u32,
		normalize: bool, relative_offset: u32, buffer: &Buffer, offset: isize, stride: i32)
	{
		unsafe
		{
			glEnableVertexArrayAttrib(self.object, attribute_index);
			glVertexArrayAttribBinding(self.object, attribute_index, attribute_index);
			glVertexArrayAttribFormat(self.object, attribute_index, size, typee, normalize as GLboolean, relative_offset);
			glVertexArrayVertexBuffer(self.object, attribute_index, buffer.object, offset, stride);
		}
	}
	
	pub fn format_i_bind_data_buffer(&self, attribute_index: u32, size: i32, typee: u32,
		relative_offset: u32, buffer: &Buffer, offset: isize, stride: i32)
	{
		unsafe
		{
			glEnableVertexArrayAttrib(self.object, attribute_index);
			glVertexArrayAttribBinding(self.object, attribute_index, attribute_index);
			glVertexArrayAttribIFormat(self.object, attribute_index, size, typee, relative_offset);
			glVertexArrayVertexBuffer(self.object, attribute_index, buffer.object, offset, stride);
		}
	}
	
	pub fn format_l_bind_data_buffer(&self, attribute_index: u32, size: i32, typee: u32,
		relative_offset: u32, buffer: &Buffer, offset: isize, stride: i32)
	{
		unsafe
		{
			glEnableVertexArrayAttrib(self.object, attribute_index);
			glVertexArrayAttribBinding(self.object, attribute_index, attribute_index);
			glVertexArrayAttribLFormat(self.object, attribute_index, size, typee, relative_offset);
			glVertexArrayVertexBuffer(self.object, attribute_index, buffer.object, offset, stride);
		}
	}
	
	pub fn bind_element_buffer(&self, buffer: &Buffer)
	{
		unsafe {glVertexArrayElementBuffer(self.object, buffer.object)};
	}

	pub fn bind(&self)
	{
		unsafe {glBindVertexArray(self.object)};
	}
}

pub struct Shader_storage
{
	object: u32,
}

impl Drop for Shader_storage
{
	fn drop(&mut self)
	{
		unsafe {glDeleteBuffers(1, &mut self.object)};
	}
}

impl Shader_storage
{
	pub fn create() -> Self
	{
		Self {object: create_buffer()}
	}
	
	pub fn bind(&self, binding: u32)
	{
		unsafe {glBindBufferBase(constants::GL_SHADER_STORAGE_BUFFER, binding, self.object)};
	}
}
