#[derive(Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize)]
pub enum Message_content
{
	_0,
	heartbeat,
	initial_accept,
	number {i: i32},
}
