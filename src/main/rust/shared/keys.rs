#[derive(Default)]
pub struct Session_keys
{
	pub receive_key: crate::crypto::shared_key::Shared_key,
	pub send_key: crate::crypto::shared_key::Shared_key,
	pub receive_nonce: crate::crypto::shared_key::Nonce,
	pub send_nonce: crate::crypto::shared_key::Nonce,
}

pub enum Session_keys_error<'t>
{
	no_terminating_0,
	invalid_version_string,
	version_mismatch {client_version: &'t str},
	wrong_data_length,
}

impl Session_keys
{
	pub fn try_new(data: &[u8]) -> Result<Self, Session_keys_error>
	{
		let Some(pos) = data.iter().position(|&c| c == '\0' as u8) else
		{
			return Err(Session_keys_error::no_terminating_0);
		};
		
		let Ok(version) = std::str::from_utf8(&data[.. pos]) else
		{
			return Err(Session_keys_error::invalid_version_string);
		};
		
		if version != crate::VERSION
		{
			return Err(Session_keys_error::version_mismatch {client_version: &version});
		}
		
		let Some(mut data_slice) = data.get(pos + 1 .. pos + 1
			+ 2 * std::mem::size_of::<crate::crypto::shared_key::Shared_key>()
			+ 2 * std::mem::size_of::<crate::crypto::shared_key::Nonce>()) else
		{
			return Err(Session_keys_error::wrong_data_length);
		};
		
		let mut result: Self = Default::default();
		
		result.receive_key.copy_from_slice(&data_slice[.. std::mem::size_of::<crate::crypto::shared_key::Shared_key>()]);
		data_slice = &data_slice[std::mem::size_of::<crate::crypto::shared_key::Shared_key>() ..];
		
		result.send_key.copy_from_slice(&data_slice[.. std::mem::size_of::<crate::crypto::shared_key::Shared_key>()]);
		data_slice = &data_slice[std::mem::size_of::<crate::crypto::shared_key::Shared_key>() ..];
		
		result.receive_nonce.copy_from_slice(&data_slice[.. std::mem::size_of::<crate::crypto::shared_key::Nonce>()]);
		data_slice = &data_slice[std::mem::size_of::<crate::crypto::shared_key::Nonce>() ..];
		
		result.send_nonce.copy_from_slice(&data_slice[.. std::mem::size_of::<crate::crypto::shared_key::Nonce>()]);
		
		Ok(result)
	}
}

pub fn read_key(stream: &mut impl std::io::Read) -> std::io::Result<[u8; 32]>
{
	let mut buf = [0];
	
	while {stream.read(&mut buf)?; buf[0] != '\n' as u8}
	{
	}
	
	let mut text = [0; 64];
	let mut result = [0_u8; 32];
	stream.read_exact(&mut text)?;
	
	let mut decoder = crate::utility::hex::Decoder::new(text.iter().copied());
	
	// TODO next_chunk
	for b in &mut result
	{
		*b = decoder.next().unwrap();
	}
	
	Ok(result)
}
