pub mod math;
pub mod geometry;
pub mod containers;
pub mod utility;
pub mod audio;
pub mod crypto;
pub mod graphics;
pub mod shared;

pub const VERSION: &str = "0.0.1";
