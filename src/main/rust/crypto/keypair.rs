#[link(name = "sodium")]
extern "C"
{
	fn crypto_box_keypair(pk: *mut std::ffi::c_uchar, sk: *mut std::ffi::c_uchar) -> std::ffi::c_int;
	fn crypto_box_seal(c: *mut std::ffi::c_uchar, m: *const std::ffi::c_uchar,
		mlen: std::ffi::c_ulonglong, pk: *const std::ffi::c_uchar
	) -> std::ffi::c_int;
	fn crypto_box_seal_open(m: *mut std::ffi::c_uchar, c: *const std::ffi::c_uchar,
		clen: std::ffi::c_ulonglong, pk: *const std::ffi::c_uchar, sk: *const std::ffi::c_uchar
	) -> std::ffi::c_int;
}

pub type Public_key = [u8; 32];
pub type Private_key = [u8; 32];
pub struct Keypair
{
	pub public: Public_key,
	pub private: Private_key,
}
pub const seal_length: usize = 48;

pub fn new() -> Keypair
{
	let mut public = [0_u8; 32];
	let mut private = [0_u8; 32];
	unsafe {crypto_box_keypair(public.as_mut_ptr(), private.as_mut_ptr())};
	return Keypair {public, private};
}

/// `result` must be at least the length of the `message` plus `seal_length`
pub fn encrypt<'t>(message: &[u8], result: &'t mut [u8], public_key: &Public_key) -> &'t mut [u8]
{
	unsafe {crypto_box_seal(result.as_mut_ptr(), message.as_ptr(),
		message.len() as std::ffi::c_ulonglong, public_key.as_ptr()
	)};
	return &mut result[.. seal_length + message.len()];
}

/// `result` must be at least the length of the `message` minus `seal_length`
pub fn decrypt<'t>(message: &[u8], result: &'t mut [u8], public_key: &Public_key, private_key: &Private_key) -> Option<&'t mut [u8]>
{
	if unsafe {crypto_box_seal_open(result.as_mut_ptr(), message.as_ptr(),
		message.len() as std::ffi::c_ulonglong, public_key.as_ptr(), private_key.as_ptr())} == 0
	{
		return Some(&mut result[.. message.len() - seal_length]);
	}
	
	return None;
}

#[test]
fn test_keypair()
{
	let key = new();
	const len: usize = 15;
	let message = [9_u8; len];
	let mut encrypted = [0_u8; seal_length + len];
	assert_eq!(seal_length + len, encrypt(&message, &mut encrypted, &key.public).len());
	let mut message = [0_u8; len];
	assert_eq!(len, decrypt(&encrypted, &mut message, &key.public, &key.private).unwrap().len());
	assert_eq!([9_u8; len], message);
}
