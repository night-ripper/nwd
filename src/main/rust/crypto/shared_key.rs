#[link(name = "sodium")]
extern "C"
{
	fn crypto_secretbox_keygen(k: *mut std::ffi::c_uchar);
	fn crypto_secretbox_easy(c: *mut std::ffi::c_uchar, m: *const std::ffi::c_uchar,
		mlen: std::ffi::c_ulonglong, n: *const std::ffi::c_uchar, k: *const std::ffi::c_uchar
	) -> std::ffi::c_int;
	fn crypto_secretbox_open_easy(m: *mut std::ffi::c_uchar, c: *const std::ffi::c_uchar,
		clen: std::ffi::c_ulonglong, n: *const std::ffi::c_uchar, k: *const std::ffi::c_uchar
	) -> std::ffi::c_int;
}

pub type Nonce = [u8; 24];
pub type Shared_key = [u8; 32];
pub const mac_length: usize = 16;

pub fn new() -> Shared_key
{
	let mut result: Shared_key = [0_u8; 32];
	unsafe {crypto_secretbox_keygen(result.as_mut_ptr())};
	return result;
}

pub fn encrypt<'t>(message: &[u8], result: &'t mut [u8], nonce: &Nonce, key: &Shared_key) -> &'t mut [u8]
{
	unsafe {crypto_secretbox_easy(result.as_mut_ptr(), message.as_ptr(),
		message.len() as std::ffi::c_ulonglong, nonce.as_ptr(), key.as_ptr()
	)};
	return &mut result[.. mac_length + message.len()];
}

/// `message` contains `mac_length` of empty space and then the message
pub fn encrypt_in_place<'t>(message: &'t mut [u8], nonce: &Nonce, key: &Shared_key) -> &'t mut [u8]
{
	assert!(message.len() >= mac_length);
	unsafe {crypto_secretbox_easy(message.as_mut_ptr(), message.as_ptr().offset(mac_length as isize),
		(message.len() - mac_length) as std::ffi::c_ulonglong, nonce.as_ptr(), key.as_ptr()
	)};
	return message;
}

pub fn decrypt<'t>(message: &[u8], result: &'t mut [u8], nonce: &Nonce, key: &Shared_key) -> Option<&'t mut [u8]>
{
	if unsafe {crypto_secretbox_open_easy(result.as_mut_ptr(), message.as_ptr(),
		message.len() as std::ffi::c_ulonglong, nonce.as_ptr(), key.as_ptr())} == 0
	{
		return Some(&mut result[.. message.len() - mac_length]);
	}
	
	return None;
}

pub fn decrypt_in_place<'t>(message: &'t mut [u8], nonce: &Nonce, key: &Shared_key) -> Option<&'t mut [u8]>
{
	if unsafe {crypto_secretbox_open_easy(message.as_mut_ptr(), message.as_ptr(),
		message.len() as std::ffi::c_ulonglong, nonce.as_ptr(), key.as_ptr())} == 0
	{
		let length = message.len() - mac_length;
		return Some(&mut message[.. length]);
	}
	
	return None;
}

#[test]
fn test_shared_key()
{
	let key = new();
	let message = [9_u8; 15];
	let mut encrypted = [0_u8; mac_length + 15];
	assert_eq!(mac_length + 15, encrypt(&message, &mut encrypted, &[0_u8; 24], &key).len());
	let mut message = [0_u8; 15];
	assert_eq!(15, decrypt(&encrypted, &mut message, &[0_u8; 24], &key).unwrap().len());
	assert_eq!([9_u8; 15], message);
}
