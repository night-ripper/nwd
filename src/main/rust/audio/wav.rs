use std::io::Read;
use rand::SeedableRng;
use rand::RngCore;
use crate::audio;

struct Endian_functions
{
	read_u16_fn: fn([u8; 2]) -> u16,
	read_u32_fn: fn([u8; 4]) -> u32,
}

impl Endian_functions
{
	fn read_u16(&self, buf: &[u8]) -> u16
	{
		(self.read_u16_fn)(buf[.. 2].try_into().unwrap())
	}
	
	fn read_u32(&self, buf: &[u8]) -> u32
	{
		(self.read_u32_fn)(buf[.. 4].try_into().unwrap())
	}
}

const big_endian_functions: Endian_functions = Endian_functions
{
	read_u16_fn: u16::from_be_bytes,
	read_u32_fn: u32::from_be_bytes,
};

const little_endian_functions: Endian_functions = Endian_functions
{
	read_u16_fn: u16::from_le_bytes,
	read_u32_fn: u32::from_le_bytes,
};

pub struct Streaming_wav
{
	sound_info: audio::Sound_info,
	source_format: audio::Format,
	source_endianness: crate::utility::endian::Endianness,
	source_data_chunk_begin: usize,
	source_data_size: usize,
	reader: std::ptr::NonNull<dyn std::io::Read>,
	seeker: std::ptr::NonNull<dyn std::io::Seek>,
	source: Box<dyn std::any::Any>,
	buffer: audio::Streaming_buffer
}

impl Streaming_wav
{
	fn new(format: audio::Format, mut source: impl std::io::Read + std::io::Seek + 'static) -> std::io::Result<Self>
	{
		const header_size: usize = 36;
		let mut buf = [0_u8; header_size];
		
		if source.read_exact(&mut buf).is_err()
		{
			return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Wav error: incomplete header"))
		}
		
		let mut header_position = 0;
		let endian_functions: Endian_functions;
		let source_endianness: crate::utility::endian::Endianness;
		
		if &buf[header_position ..][.. 4] == b"RIFF"
		{
			source_endianness = crate::utility::endian::Endianness::little;
			endian_functions = little_endian_functions;
		}
		else if &buf[header_position ..][.. 4] == b"RIFX"
		{
			source_endianness = crate::utility::endian::Endianness::big;
			endian_functions = big_endian_functions;
		}
		else
		{
			return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Wav error: wrong id"))
		}
		
		header_position += 4;
		
		let _chunk_size = endian_functions.read_u32(&buf[header_position ..]);
		header_position += 4;
		
		if &buf[header_position ..][.. 4] != b"WAVE"
		{
			return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Wav error: wrong WAVE string"))
		}
		
		header_position += 4;
		
		if &buf[header_position ..][.. 4] != b"fmt "
		{
			return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Wav error: wrong fmt string"))
		}
		
		header_position += 4;
		
		let _subchunk_size_ = endian_functions.read_u32(&buf[header_position ..]);
		header_position += 4;
		
		let fmt = endian_functions.read_u16(&buf[header_position ..]);
		header_position += 2;
		
		if fmt != 1
		{
			return Err(std::io::Error::new(std::io::ErrorKind::Unsupported, "Wav error: contains compressed data"))
		}
		
		let channels = endian_functions.read_u16(&buf[header_position ..]);
		header_position += 2;
		
		let sample_rate = endian_functions.read_u32(&buf[header_position ..]);
		header_position += 4;
		
		let _byte_rate = endian_functions.read_u32(&buf[header_position ..]);
		header_position += 4;
		
		let _block_align = endian_functions.read_u16(&buf[header_position ..]);
		header_position += 2;
		
		let bit_depth = endian_functions.read_u16(&buf[header_position ..]);
		// header_position += 2;
		
		let source_data_size: usize;
		
		if let Ok(size) = Self::read_until_data_chunk(&mut source, endian_functions)
		{
			source_data_size = size;
		}
		else
		{
			return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Wav error: missing data chunk"))
		}
		
		let source_data_chunk_begin = source.stream_position()? as usize;
		
		let mut source = Box::new(source);
		
		Ok(Self
		{
			sound_info: audio::Sound_info::new(format, sample_rate as i32),
			source_format: audio::Format::new(channels as i16, bit_depth as i16),
			source_endianness,
			source_data_chunk_begin,
			source_data_size,
			reader: std::ptr::NonNull::from(source.as_mut()),
			seeker: std::ptr::NonNull::from(source.as_mut()),
			source,
			buffer: audio::Streaming_buffer::new(),
		})
	}
	
	fn read_until_data_chunk(source: &mut impl std::io::Read, endian_functions: Endian_functions) -> std::io::Result<usize>
	{
		const chunk_size: usize = 8;
		let mut chunk_data = [0_u8; chunk_size];
		let mut result: usize;
		
		loop
		{
			if source.read_exact(&mut chunk_data).is_err()
			{
				return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, "Wav error: incomplete chunk"))
			}
			
			result = endian_functions.read_u32(&chunk_data[4 ..]) as usize;
			
			if &chunk_data[.. 4] == b"data"
			{
				return Ok(result);
			}
			
			std::io::copy(&mut source.take(result as u64), &mut std::io::sink());
		}
	}
}

impl std::ops::Deref for Streaming_wav
{
	type Target = audio::Sound_info;
	
	fn deref(&self) -> &Self::Target
	{
		&self.sound_info
	}
}

impl audio::Streaming_sound for Streaming_wav
{
}

impl std::io::Read for Streaming_wav
{
	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize>
	{
		audio::buf_read(self, buf)
	}
}

impl std::io::BufRead for Streaming_wav
{
	fn consume(&mut self, bytes: usize)
	{
		self.buffer.consume(bytes);
	}
	
	fn fill_buf(&mut self) -> std::io::Result<&[u8]>
	{
		if self.buffer.begin >= self.buffer.end
		{
			let mut max_data_length = self.buffer.data.len();
			
			if self.byte_depth() > self.source_format.byte_depth()
			{
				max_data_length /= self.byte_depth() as usize;
				max_data_length *= self.source_format.byte_depth() as usize;
			}
			
			if self.channels() > self.source_format.channels()
			{
				max_data_length /= self.channels() as usize;
				max_data_length *= self.source_format.channels() as usize;
			}
			
			let want_to_read = max_data_length;
			let mut data_length = unsafe {self.reader.as_mut().read(&mut self.buffer.data[.. want_to_read])}?;
			
			if data_length == 0
			{
				return Ok(&[]);
			}
			
			let mut source_byte_depth = self.source_format.byte_depth() as usize;
			
			////////////////////////////////////////////////////////////////////////
			// Reduce 16 / 24 / 32-bit signed data of source endianness to 16-bit
			// signed data of native endianness
			
			if source_byte_depth >= 2 || (source_byte_depth > 2 && self.source_endianness != crate::utility::endian::Endianness::native)
			{
				let prefix = if self.source_endianness == crate::utility::endian::Endianness::little {source_byte_depth - 2} else {0};
				
				for i in (0 .. data_length / source_byte_depth * source_byte_depth).step_by(source_byte_depth)
				{
					let mut value = i16::from_ne_bytes(self.buffer.data[i + prefix ..][.. 2].try_into().unwrap());
					
					if self.source_endianness != crate::utility::endian::Endianness::native
					{
						value = value.swap_bytes();
					}
					
					self.buffer.data[i / source_byte_depth * 2 ..][.. 2].copy_from_slice(&value.to_ne_bytes());
				}
				
				data_length = data_length * 2 / source_byte_depth as usize;
				source_byte_depth = 2;
			}
			
			////////////////////////////////////////////////////////////////////////
			
			if self.source_format.channels() == 1 && self.channels() == 2
			{
				data_length *= 2;
				audio::channels_function_1_2(&mut self.buffer.data[.. data_length], source_byte_depth);
			}
			else if self.source_format.channels() == 2 && self.channels() == 1
			{
				audio::channels_function_2_1(&mut self.buffer.data[.. data_length], source_byte_depth);
				data_length /= 2;
			}
			
			////////////////////////////////////////////////////////////////////////
			
			if source_byte_depth == 1 && self.byte_depth() == 2
			{
				for i in (0 .. data_length).rev()
				{
					let mut result: u16 = self.buffer.data[i] as u16;
					result <<= 8;
					result ^= 1 << 15;
					self.buffer.data[i * 2 ..][.. 2].copy_from_slice(&result.to_ne_bytes());
				}
				
				data_length *= 2;
			}
			else if source_byte_depth == 2 && self.byte_depth() == 1
			{
				data_length /= 2;
				
				let mut rng = rand::rngs::SmallRng::from_rng(&mut rand::thread_rng()).unwrap();
				
				for i in 0 .. data_length
				{
					let mut result = u16::from_ne_bytes(self.buffer.data[i * 2 ..][.. 2].try_into().unwrap());
					result ^= 1 << 15;
					let result = result as i16;
					let mut dither = [0, 0];
					rng.fill_bytes(&mut dither);
					for b in dither
					{
						result.saturating_add(b as i16 / 2);
					}
					self.buffer.data[i] = result.to_be_bytes()[0];
				}
			}
			
			self.buffer.end = data_length as u16;
		}
		
		Ok(&self.buffer.data[self.buffer.begin as usize .. self.buffer.end as usize])
	}
}

impl std::io::Seek for Streaming_wav
{
	fn seek(&mut self, pos: std::io::SeekFrom) -> std::io::Result<u64>
	{
		let in_coeff = (self.source_format.channels() * self.source_format.byte_depth()) as u64;
		let out_coeff = (self.channels() * self.byte_depth()) as u64;
		
		let seekable = unsafe {self.seeker.as_mut()};
		let current_pos = (seekable.seek(std::io::SeekFrom::Current(0))? - self.source_data_chunk_begin as u64)
			* out_coeff / in_coeff + self.buffer.begin as u64 - self.buffer.end as u64
		;
		
		let abs_pos = match pos
		{
			std::io::SeekFrom::Start(s) => s,
			std::io::SeekFrom::Current(c) => (current_pos as i64 + c) as u64,
			std::io::SeekFrom::End(e) => ((self.source_data_size as u64 * out_coeff / in_coeff) as i64 + e) as u64,
		};
		
		let diff = abs_pos as i64 - current_pos as i64;
		
		if 0 <= self.buffer.begin as i64 + diff
			&& self.buffer.begin as i64 + diff <= self.buffer.end as i64
		{
			self.buffer.begin = (self.buffer.begin as i64 + diff) as u16;
		}
		else
		{
			let in_offset = abs_pos * in_coeff / out_coeff;
			seekable.seek(std::io::SeekFrom::Start(self.source_data_chunk_begin as u64 + in_offset))?;
			self.buffer.begin = (abs_pos - in_offset * out_coeff / in_coeff) as u16;
			self.buffer.end = 0;
		}
		
		Ok(abs_pos)
	}
}

pub fn from_file(format: audio::Format, source: impl std::io::Read + std::io::Seek + 'static)
-> std::io::Result<Streaming_wav>
{
	Streaming_wav::new(format, source)
}
