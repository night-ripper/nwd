pub mod cyclic_angle;
pub mod fixed_point;
pub mod utility;

pub trait Midpoint
{
	fn midpoint(self, rhs: Self) -> Self;
}
