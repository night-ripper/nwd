pub mod keys;
pub mod session;
pub mod client;
pub mod server;

use bincode::Options;

pub fn initialize_bincode()
{
	bincode::DefaultOptions::new()
		.with_little_endian()
		.with_limit(1024)
		.with_fixint_encoding()
		.allow_trailing_bytes()
	;
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Message<Content>
{
	pub last_received: crate::shared::session::Sequence,
	pub received_bitset: [u8; 4],
	pub content: Content,
}
