#[derive(Clone, Copy, Eq, Debug)]
pub struct Fixed_point<const Point_position: i32>
{
	value: i32,
}

impl<const Point_position: i32> Fixed_point<Point_position>
{
	pub const MIN: Self = Self::from_underlying(i32::MIN);
	pub const MAX: Self = Self::from_underlying(i32::MAX);
	pub const EPSILON: Self = Self::from_underlying(1);
	
	const point_position: i32 = Point_position;
	const one_value: i32 = if Point_position == 31 {i32::MAX} else {1 << Point_position};
	
	pub const fn abs(self) -> Self
	{
		Self {value: self.value.abs()}
	}
	
	pub const fn underlying_value(self) -> i32
	{
		self.value
	}
	
	pub const fn from_underlying(value: i32) -> Self
	{
		Self {value}
	}
	
	pub fn round_from(value: f32) -> Self
	{
		Self::from_underlying((value * Self::one_value as f32).round() as i32)
	}
	
	pub fn ceil_from(value: f32) -> Self
	{
		Self::from_underlying((value * Self::one_value as f32).ceil() as i32)
	}
}

impl<const Point_position: i32> From<Fixed_point<Point_position>> for f64
{
	fn from(this: Fixed_point<Point_position>) -> f64
	{
		let multiplier = 1 as f64 / Fixed_point::<Point_position>::one_value as f64;
		return multiplier * this.value as f64;
	}
}

impl<const Point_position: i32> From<Fixed_point<Point_position>> for f32
{
	fn from(this: Fixed_point<Point_position>) -> f32
	{
		f64::from(this) as f32
	}
}

////////////////////////////////////////////////////////////////////////////////

impl<const Point_position: i32> std::cmp::Ord for Fixed_point<Point_position>
{
	fn cmp(&self, rhs: &Self) -> std::cmp::Ordering
	{
		self.value.cmp(&rhs.value)
	}
}

impl<const Point_position: i32> std::cmp::PartialOrd for Fixed_point<Point_position>
{
	fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering>
	{
		Some(self.cmp(rhs))
	}
}

impl<const Point_position: i32> std::cmp::PartialEq for Fixed_point<Point_position>
{
	fn eq(&self, rhs: &Self) -> bool
	{
		self.cmp(rhs) == std::cmp::Ordering::Equal
	}
}

impl<const Point_position: i32> std::ops::Neg for Fixed_point<Point_position>
{
	type Output = Self;
	
	fn neg(mut self) -> Self::Output
	{
		self.value = -self.value;
		return self;
	}
}

impl<const Point_position: i32> std::ops::AddAssign for Fixed_point<Point_position>
{
	fn add_assign(&mut self, rhs: Self)
	{
		self.value += rhs.value;
	}
}

impl<const Point_position: i32> std::ops::Add<Fixed_point<Point_position>> for Fixed_point<Point_position>
{
	type Output = Self;
	
	fn add(mut self, rhs: Self) -> Self::Output
	{
		self += rhs;
		return self;
	}
}

impl<const Point_position: i32> std::ops::SubAssign for Fixed_point<Point_position>
{
	fn sub_assign(&mut self, rhs: Self)
	{
		self.value -= rhs.value;
	}
}

impl<const Point_position: i32> std::ops::Sub<Fixed_point<Point_position>> for Fixed_point<Point_position>
{
	type Output = Self;
	
	fn sub(mut self, rhs: Self) -> Self::Output
	{
		self -= rhs;
		return self;
	}
}

impl<const Point_position: i32> std::ops::Mul for Fixed_point<Point_position>
{
	type Output = Self;
	
	fn mul(self, rhs: Fixed_point<Point_position>) -> Self::Output
	{
		let mut result = self.value as i64 * rhs.value as i64;
		result /= 1 << Point_position;
		return Self::from_underlying(result as i32);
	}
}

#[test]
fn test_fixed_mul()
{
	type Fp = Fixed_point<10>;
	assert_eq!(Fp::from_underlying(2252), Fp::round_from(1.1) * Fp::round_from(2.0));
}
/*
impl<const Point_position: i32> std::ops::Mul<f32> for Fixed_point<Point_position>
{
	type Output = Self;
	
	fn mul(self, rhs: f32) -> Self::Output
	{
		return Self::round_from(self.value as f32 * rhs);
	}
}

impl<const Point_position: i32> std::ops::MulAssign<f32> for Fixed_point<Point_position>
{
	fn mul_assign(&mut self, rhs: f32)
	{
		return *self = *self * rhs;
	}
}

impl<const Point_position: i32> std::ops::Div<f32> for Fixed_point<Point_position>
{
	type Output = Self;
	
	fn div(self, rhs: f32) -> Self::Output
	{
		return Self::round_from(self.value as f32 / rhs);
	}
}

impl<const Point_position: i32> std::ops::DivAssign<f32> for Fixed_point<Point_position>
{
	fn div_assign(&mut self, rhs: f32)
	{
		return *self = *self / rhs;
	}
}
*/

impl<const Point_position: i32> std::fmt::Display for Fixed_point<Point_position>
{
	// TODO
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
	{
		write!(f, "{}", f64::from(*self))
	}
}

////////////////////////////////////////////////////////////////////////////////

impl<const Point_position: i32> crate::math::Midpoint for Fixed_point<Point_position>
{
	fn midpoint(mut self, rhs: Self) -> Self
	{
		let (diff, overflow) = rhs.value.overflowing_sub(self.value);
		
		if overflow
		{
			self.value += rhs.value;
		}
		else
		{
			self.value += diff / 2;
		}
		
		return self;
	}
}
