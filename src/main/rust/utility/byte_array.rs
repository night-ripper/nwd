pub trait Byte_array
{
	fn increment(&mut self);
	fn decrement(&mut self);
}

impl<const Size: usize> Byte_array for [u8; Size]
{
	fn increment(&mut self)
	{
		for v in &mut self.iter_mut().rev()
		{
			let overflow;
			(*v, overflow) = v.overflowing_add(1);
			
			if ! overflow
			{
				break;
			}
		}
	}
	
	fn decrement(&mut self)
	{
		for v in &mut self.iter_mut().rev()
		{
			let overflow;
			(*v, overflow) = v.overflowing_sub(1);
			
			if ! overflow
			{
				break;
			}
		}
	}
}

#[test]
fn test_increment()
{
	let mut arr = [0_u8, 0];
	
	for i in 0 ..= 255
	{
		assert_eq!([0, i], arr);
		arr.increment();
	}
	
	assert_eq!([1, 0], arr);
	
	arr = [255, 255];
	arr.increment();
	assert_eq!([0, 0], arr);
}

#[test]
fn test_decrement()
{
	let mut arr = [0_u8, 0];
	arr.decrement();
	
	for i in (0 ..= 255).rev()
	{
		assert_eq!([255, i], arr);
		arr.decrement();
	}
	
	assert_eq!([254, 255], arr);
}
