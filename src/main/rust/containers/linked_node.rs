pub struct Linked_node<Type>
{
	lsib: usize,
	rsib: usize,
	value: Type,
}

impl<Type> From<Type> for Linked_node<Type>
{
	fn from(value: Type) -> Self
	{
		Self
		{
			lsib: usize::MAX,
			rsib: usize::MAX,
			value,
		}
	}
}

impl<Type> AsRef<Type> for Linked_node<Type>
{
	fn as_ref(&self) -> &Type {&self.value}
}

impl<Type> AsMut<Type> for Linked_node<Type>
{
	fn as_mut(&mut self) -> &mut Type {&mut self.value}
}

impl<Type> Linked_node<Type>
{
	pub fn value(self) -> Type {self.value}
	pub fn next(&self) -> usize {self.rsib}
}

pub fn unlink<Nodes, Type>(nodes: &mut Nodes, index: usize) -> [usize; 2]
where
	Nodes: ?Sized + std::ops::IndexMut<usize, Output = Linked_node<Type>>
{
	let prev = nodes[index].lsib;
	let next = nodes[index].rsib;
	
	if prev != usize::MAX
	{
		nodes[prev].rsib = next;
	}
	
	if next != usize::MAX
	{
		nodes[next].lsib = prev;
	}
	
	return [prev, next];
}

pub fn link_back<Nodes, Type>(nodes: &mut Nodes, index: usize, last: usize)
where
	Nodes: ?Sized + std::ops::IndexMut<usize, Output = Linked_node<Type>>
{
	nodes[index].lsib = last;
	
	if last != usize::MAX
	{
		nodes[last].rsib = index;
	}
}
