use crate::containers::aa;

pub struct Set_entry<Type>(Type);

impl<Type> aa::node::Entry for Set_entry<Type>
{
	type Key = Type;
	fn key(&self) -> &Self::Key {&self.0}
}

pub type Set<Type> = aa::tree::Tree<Set_entry<Type>>;

impl<Type> Set<Type>
{
	pub fn contains<Key>(&self, key: &Key) -> bool
	where
		Type: std::borrow::Borrow<Key> + std::cmp::Ord,
		Key: ?Sized + std::cmp::Ord,
	{
		aa::node::find(self.repository.values(), self.root, key).0 != usize::MAX
	}
	
	pub fn get<Key>(&self, key: &Key) -> Option<&Type>
	where
		Type: std::borrow::Borrow<Key> + std::cmp::Ord,
		Key: ?Sized + std::cmp::Ord,
	{
		let index = aa::node::find(self.repository.values(), self.root, key).0;
		
		if index != usize::MAX
		{
			return Some(&self.repository[index].as_ref().0);
		}
		
		return None;
	}
	
	pub fn insert(&mut self, value: Type) -> bool
	where
		Type: std::cmp::Ord
	{
		self.try_insert(Set_entry {0: value}, |v| v.is_none())
	}
	
	pub fn replace(&mut self, value: Type) -> Option<Type>
	where
		Type: std::cmp::Ord
	{
		self.try_insert(Set_entry {0: value}, |v| v.map(|v| v.0))
	}
	
	pub fn remove<Key>(&mut self, value: &Key) -> bool
	where
		Type: std::borrow::Borrow<Key> + std::cmp::Ord,
		Key: ?Sized + std::cmp::Ord,
	{
		let index = aa::node::find(self.repository.values(), self.root, value).0;
		
		if index != usize::MAX
		{
			self.remove_at(index);
			return true;
		}
		
		return false;
	}
	
	pub fn iter<'t>(&'t self) -> aa::node::Iterator<&'t [aa::node::Node<Set_entry<Type>>]>
	{
		aa::node::Iterator::<&'t [aa::node::Node<Set_entry<Type>>]>
		{
			first: self.first,
			last: self.last,
			bounds: [self.first, self.last],
			nodes: self.repository.values(),
		}
	}
}

impl<'t, Type> std::iter::Iterator for aa::node::Iterator<&'t [aa::node::Node<Set_entry<Type>>]>
{
	type Item = &'t Type;
	
	fn next(&mut self) -> Option<Self::Item>
	{
		match aa::node::iter_impl!(self, 0)
		{
			usize::MAX => None,
			i => Some(&self.nodes[i].as_ref().0),
		}
	}
}

impl<'t, Type> std::iter::DoubleEndedIterator for aa::node::Iterator<&'t [aa::node::Node<Set_entry<Type>>]>
{
	fn next_back(&mut self) -> Option<Self::Item>
	{
		match aa::node::iter_impl!(self, 1)
		{
			usize::MAX => None,
			i => Some(&self.nodes[i].as_ref().0),
		}
	}
}

#[test]
fn test_aa_set_0()
{
	let set = Set::<i32>::new();
	
	assert_eq!(0, set.len());
	
	assert_eq!(0, set.iter().count());
	assert_eq!(None, set.iter().next());
	assert_eq!(None, set.iter().rev().next());
}

#[test]
fn test_aa_set_1()
{
	let mut set = Set::<i32>::new();
	set.insert(7);
	set.insert(8);
	set.insert(11);
	set.insert(12);
	set.insert(9);
	set.insert(10);
	set.insert(14);
	set.insert(14);
	
	assert_eq!(7, set.len());
	
	assert_eq!(false, set.contains(&6));
	assert_eq!(true, set.contains(&7));
	assert_eq!(true, set.contains(&8));
	assert_eq!(true, set.contains(&9));
	assert_eq!(true, set.contains(&10));
	assert_eq!(true, set.contains(&11));
	assert_eq!(true, set.contains(&12));
	assert_eq!(false, set.contains(&13));
	assert_eq!(true, set.contains(&14));
	assert_eq!(false, set.contains(&15));
	
	assert_eq!(7, set.iter().count());
	
	{
		let mut it = set.iter();
		assert_eq!(7, *it.next().unwrap());
		assert_eq!(8, *it.next().unwrap());
		assert_eq!(9, *it.next().unwrap());
		assert_eq!(10, *it.next().unwrap());
		assert_eq!(11, *it.next().unwrap());
		assert_eq!(12, *it.next().unwrap());
		assert_eq!(14, *it.next().unwrap());
		assert_eq!(None, it.next());
	}
	
	assert_eq!(7, set.iter().rev().count());
	
	{
		let mut rit = set.iter().rev();
		assert_eq!(14, *rit.next().unwrap());
		assert_eq!(12, *rit.next().unwrap());
		assert_eq!(11, *rit.next().unwrap());
		assert_eq!(10, *rit.next().unwrap());
		assert_eq!(9, *rit.next().unwrap());
		assert_eq!(8, *rit.next().unwrap());
		assert_eq!(7, *rit.next().unwrap());
		assert_eq!(None, rit.next());
	}
}
