#pragma once

#include <cstddef>
#include <cstdint>

#include <limits>
#include <span>
#include <stdexcept>
#include <streambuf>

namespace nwd::audio
{
struct Format
{
	constexpr Format() noexcept = default;
	
	constexpr Format(std::int16_t channels, std::int16_t bit_depth) noexcept
		:
		channels_(channels),
		bit_depth_(bit_depth)
	{
	}
	
	constexpr std::int16_t channels() const noexcept {return channels_;}
	constexpr std::int16_t bit_depth() const noexcept {return bit_depth_;}
	constexpr std::int16_t byte_depth() const noexcept {return bit_depth_ / std::numeric_limits<unsigned char>::digits;}
	constexpr std::ptrdiff_t sample_size_bytes() const noexcept {return channels_ * byte_depth();}
	
	static const Format mono_8;
	static const Format mono_16;
	static const Format stereo_8;
	static const Format stereo_16;
	
protected:
	std::int16_t channels_;
	std::int16_t bit_depth_;
};

constexpr const Format Format::mono_8 = {1, 8};
constexpr const Format Format::mono_16 = {1, 16};
constexpr const Format Format::stereo_8 = {2, 8};
constexpr const Format Format::stereo_16 = {2, 16};

struct Sound_info : Format
{
	constexpr Sound_info() noexcept = default;
	
	constexpr Sound_info(Format format, std::int32_t frequency) noexcept
		:
		Format(format),
		frequency_(frequency)
	{
	}
	
	constexpr std::int32_t frequency() const noexcept {return frequency_;}
	
protected:
	std::int32_t frequency_;
};

struct Sound_view : Sound_info
{
	constexpr Sound_view() noexcept = default;
	
	constexpr Sound_view(Sound_info info, std::span<std::byte> view) noexcept
		:
		Sound_info(info),
		view_(view)
	{
	}
	
	constexpr Sound_view(Sound_info info) noexcept
		:
		Sound_view(info, {})
	{
	}
	
	constexpr std::span<std::byte> view() noexcept {return view_;}
	constexpr std::span<const std::byte> view() const noexcept {return view_;}
	
protected:
	std::span<std::byte> view_;
};

struct Streaming_sound : Sound_info, std::streambuf
{
	Streaming_sound(Sound_info info) noexcept
		:
		Sound_info(info)
	{
	}
};
} // namespace nwd::audio
