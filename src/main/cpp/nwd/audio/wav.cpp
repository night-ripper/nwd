#include <cstring>
#include <cstdint>

#include <algorithm>
#include <bit>
#include <fstream>
#include <istream>
#include <limits>
#include <span>
#include <utility>

#include <nwd/audio/wav.hpp>
#include <nwd/audio/privates/common.hpp>

#include <nwd/utility/endian.hpp>
#include <nwd/utility/integer_cast.hpp>
#include <nwd/utility/fileio.hpp>

namespace nwd::audio::wav
{
namespace
{
struct Wav_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

/// http://soundfile.sapp.org/doc/WaveFormat/
struct Header
{
	struct Endian_functions
	{
		std::uint16_t(*uint16_)(const std::byte*) noexcept = nullptr;
		std::uint32_t(*uint32_)(const std::byte*) noexcept = nullptr;
	};
	
	constexpr static Endian_functions little_endian_functions = Endian_functions
	{
		.uint16_ = +[](const std::byte* bytes) noexcept -> std::uint16_t {return utility::endian::little_uint16_t(bytes).value();},
		.uint32_ = +[](const std::byte* bytes) noexcept -> std::uint32_t {return utility::endian::little_uint32_t(bytes).value();},
	};
	
	constexpr static Endian_functions big_endian_functions = Endian_functions
	{
		.uint16_ = +[](const std::byte* bytes) noexcept -> std::uint16_t {return utility::endian::big_uint16_t(bytes).value();},
		.uint32_ = +[](const std::byte* bytes) noexcept -> std::uint32_t {return utility::endian::big_uint32_t(bytes).value();},
	};
	
	char magic_[4];
	std::uint32_t chunk_size_;
	char wave_name_[4];
	char fmt_[4];
	std::uint32_t subchunk_size_;
	std::uint16_t format_;
	std::uint16_t channels_;
	std::uint32_t sample_rate_;
	std::uint32_t byte_rate_;
	std::uint16_t block_align_;
	std::uint16_t bit_depth_;
	
	// chunks ...
	
	std::uint32_t data_size_;
	
	static std::uint32_t read_until_data_chunk(std::streambuf& streambuf, Endian_functions endian_functions)
	{
		constexpr static std::size_t chunk_size = 8;
		std::byte chunk_data[chunk_size];
		std::uint32_t result = 0;
		
		while (true)
		{
			if (auto length = streambuf.sgetn(reinterpret_cast<char*>(chunk_data), chunk_size); length != chunk_size)
			{
				throw Wav_error(std::string("Could not read WAV chunk: expected ")
					+ std::to_string(chunk_size) + ", got " + std::to_string(length)
				);
			}
			
			result = endian_functions.uint32_(chunk_data + 4);
			
			if (std::strncmp(reinterpret_cast<char*>(chunk_data), "data", 4) == 0)
			{
				return result;
			}
			
			if (auto position = streambuf.pubseekoff(result, std::ios_base::cur, std::ios_base::in); position == -1)
			{
				throw Wav_error(std::string("Could not read WAV chunk data"));
			}
		}
	}
	
	Header(std::streambuf& streambuf)
	{
		constexpr static std::size_t header_size = 36;
		std::byte header_data[header_size];
		std::size_t header_position = 0;
		
		if (auto length = streambuf.sgetn(reinterpret_cast<char*>(header_data), header_size); length != header_size)
		{
			throw Wav_error(std::string("Could not read the full WAV header: expected ")
				+ std::to_string(header_size) + ", got " + std::to_string(length)
			);
		}
		
		auto endian_functions = Endian_functions();
		
		std::memcpy(magic_, header_data + header_position, 4);
		header_position += 4;
		if (std::strncmp(magic_, "RIFF", 4) == 0)
		{
			endian_functions = little_endian_functions;
			
		}
		else if (std::strncmp(magic_, "RIFX", 4) == 0)
		{
			endian_functions = big_endian_functions;
		}
		else
		{
			throw Wav_error("Wav file does not contain \"RIFF\" or \"RIFX\" string");
		}
		
		chunk_size_ = endian_functions.uint32_(header_data + header_position);
		header_position += 4;
		
		std::memcpy(wave_name_, header_data + header_position, 4);
		header_position += 4;
		if (std::strncmp(wave_name_, "WAVE", 4) != 0)
		{
			throw Wav_error("Wav file does not contain \"WAVE\" string");
		}
		
		std::memcpy(fmt_, header_data + header_position, 4);
		header_position += 4;
		if (std::strncmp(fmt_, "fmt", 3) != 0)
		{
			throw Wav_error("Wav file does not contain \"fmt\" string");
		}
		
		subchunk_size_ = endian_functions.uint32_(header_data + header_position);
		header_position += 4;
		
		format_ = endian_functions.uint16_(header_data + header_position);
		header_position += 2;
		if (format_ != 1)
		{
			throw Wav_error("Wav contains compressed data");
		}
		
		channels_ = endian_functions.uint16_(header_data + header_position);
		header_position += 2;
		
		sample_rate_ = endian_functions.uint32_(header_data + header_position);
		header_position += 4;
		
		byte_rate_ = endian_functions.uint32_(header_data + header_position);
		header_position += 4;
		
		block_align_ = endian_functions.uint16_(header_data + header_position);
		header_position += 2;
		
		bit_depth_ = endian_functions.uint16_(header_data + header_position);
		header_position += 2;
		
		data_size_ = read_until_data_chunk(streambuf, endian_functions);
	}
};
} // namespace

namespace
{
struct Streaming_wav : privates::Common_streaming_sound
{
	//! Reduces 16 / 24 / 32-bit signed data of source endianness to 16-bit
	//! signed data of native endianness
	template<std::size_t Source_size, std::endian Source_endianness>
	static void reducing_function(std::span<std::byte> bytes) noexcept
	{
		for (std::size_t i = 0; i != bytes.size() / Source_size * Source_size; i += Source_size)
		{
			auto value = std::int16_t();
			constexpr std::size_t prefix = (Source_endianness == std::endian::little) ? (Source_size - 2) : 0;
			std::memcpy(&value, bytes.data() + i + prefix, 2);
			if constexpr (Source_endianness != std::endian::native)
			{
				value = std::byteswap(value);
			}
			std::memcpy(bytes.data() + i / Source_size * 2, &value, 2);
		}
	}
	
	constexpr static void(*reducing_function_table[])(std::span<std::byte> bytes) noexcept
	{
		&reducing_function<2, std::endian::little>,
		&reducing_function<3, std::endian::little>,
		&reducing_function<4, std::endian::little>,
		
		&reducing_function<2, std::endian::big>,
		&reducing_function<3, std::endian::big>,
		&reducing_function<4, std::endian::big>,
	};
	
	template<typename Source_type, typename Target_type>
	static void integer_function(std::span<const std::byte> source, std::byte* target) noexcept
	{
		for (std::size_t i = 0; i != source.size() / sizeof(Source_type); ++i)
		{
			utility::reinterpret_aligned_cast<Target_type>(target)[i] = utility::integer_cast<Target_type>(
				utility::reinterpret_aligned_cast<const Source_type>(source.data())[i]
			);
		}
	}
	
	constexpr static void(*integer_function_table[])(std::span<const std::byte> source, std::byte* target) noexcept
	{
		&integer_function<std::uint8_t, std::int16_t>,
		&integer_function<std::int16_t, std::uint8_t>,
	};
	
	constexpr static std::ptrdiff_t buffer_size = 2520;
	
	Streaming_wav(Format format, std::streambuf& streambuf, Header header)
		:
		Common_streaming_sound(Sound_info(format, static_cast<std::int32_t>(header.sample_rate_))),
		streambuf_(&streambuf),
		source_format_(static_cast<std::int16_t>(header.channels_), static_cast<std::int16_t>(header.bit_depth_)),
		data_begin_(streambuf_->pubseekoff(0, std::ios_base::cur, std::ios_base::in)),
		bytes_remaining_(header.data_size_),
		bytes_total_(bytes_remaining_)
	{
		if (header.magic_[3] == 'F')
		{
			source_endianness_ = std::endian::little;
		}
		else if (header.magic_[3] == 'X')
		{
			source_endianness_ = std::endian::big;
		}
		else
		{
			throw Wav_error("Source file has unknown endianness");
		}
	}
	
	int sync() final override
	{
		return streambuf_->pubsync();
	}
	
	pos_type seekoff(off_type offset, std::ios_base::seekdir direction, std::ios_base::openmode) final override
	{
		if (offset == 0 and direction == std::ios_base::beg)
		{
			streambuf_->pubseekoff(data_begin_, std::ios_base::beg, std::ios_base::in);
			bytes_remaining_ = bytes_total_;
			setg(nullptr, nullptr, nullptr);
			return 0;
		}
		
		return -1;
	}
	
	std::streamsize showmanyc() final override
	{
		if (bytes_remaining_ == 0)
		{
			return -1;
		}
		
		return bytes_remaining_ / source_format_.channels() / source_format_.byte_depth() * channels() * byte_depth();
	}
	
	int_type underflow() final override
	{
		if (bytes_remaining_ == 0)
		{
			return traits_type::eof();
		}
		
		std::streamsize max_data_length = buffer_size;
		
		if (byte_depth() > source_format_.byte_depth())
		{
			max_data_length /= byte_depth();
			max_data_length *= source_format_.byte_depth();
		}
		
		if (channels() > source_format_.channels())
		{
			max_data_length /= channels();
			max_data_length *= source_format_.channels();
		}
		
		const std::streamsize want_to_read = std::min(bytes_remaining_, max_data_length);
		
		std::byte* const buffer_end = buffer_.data() + buffer_size;
		std::byte* data_begin = buffer_end - max_data_length;
		
		std::streamsize data_length = streambuf_->sgetn(reinterpret_cast<char_type*>(data_begin), want_to_read);
		bytes_remaining_ -= data_length;
		
		if (data_length != want_to_read)
		{
			throw Wav_error("Read " + std::to_string(data_length) + " bytes, but  " + std::to_string(want_to_read) + " requested");
		}
		
		auto source_byte_depth = source_format_.byte_depth();
		
		if (source_format_.byte_depth() == 1 or (source_format_.byte_depth() == 2 and source_endianness_ == std::endian::native))
		{
		}
		else if (auto index = std::size_t(3 * (source_endianness_ == std::endian::big) + source_format_.byte_depth() - 2);
			index < std::size(reducing_function_table))
		{
			reducing_function_table[index](std::span(data_begin, std::size_t(data_length)));
			data_length = data_length * 2 / source_byte_depth;
			source_byte_depth = 2;
		}
		else
		{
			std::unreachable();
		}
		
		////////////////////////////////////////////////////////////////////////
		
		if (source_format_.channels() == channels())
		{
		}
		else if (auto index = std::size_t(((source_format_.channels() - 1) * 2) + (source_byte_depth - 1));
			index < std::size(privates::channels_function_table))
		{
			auto new_data_begin = buffer_end - (buffer_end - data_begin) * std::max(1, channels() / source_format_.channels());
			privates::channels_function_table[index](std::span(data_begin, std::size_t(data_length)), new_data_begin);
			data_begin = new_data_begin;
			data_length = data_length * channels() / source_format_.channels();
		}
		else
		{
			std::unreachable();
		}
		
		////////////////////////////////////////////////////////////////////////
		
		if (source_byte_depth == byte_depth())
		{
		}
		else if (auto index = std::size_t(source_byte_depth - 1);
			index < std::size(integer_function_table))
		{
			auto new_data_begin = buffer_end - (buffer_end - data_begin) * std::max(1, byte_depth() / source_byte_depth);
			integer_function_table[index](std::span(data_begin, std::size_t(data_length)), new_data_begin);
			data_begin = new_data_begin;
			data_length = data_length * byte_depth() / source_byte_depth;
		}
		else
		{
			std::unreachable();
		}
		
		////////////////////////////////////////////////////////////////////////
		
		auto result_data = reinterpret_cast<char_type*>(data_begin);
		setg(result_data, result_data, result_data + data_length);
		
		return static_cast<unsigned char>(*result_data);
	}
	
private:
	std::streambuf* streambuf_;
	Format source_format_;
	std::endian source_endianness_;
	std::ptrdiff_t data_begin_;
	std::ptrdiff_t bytes_remaining_;
	std::ptrdiff_t bytes_total_;
	alignas(16) std::array<std::byte, buffer_size> buffer_;
};

struct Streaming_wav_file final : std::filebuf, Streaming_wav
{
	Streaming_wav_file(Format format, std::filebuf&& filebuf, Header header)
		:
		std::filebuf(std::move(filebuf)),
		Streaming_wav(format, static_cast<std::filebuf&>(*this), header)
	{
	}
};
} // namespace

std::unique_ptr<Streaming_sound> create_stream(Format format, std::streambuf& streambuf)
{
	auto header = Header(streambuf);
	
	if (header.bit_depth_ % 8 != 0)
	{
		Wav_error("Bit depth of " + std::to_string(header.bit_depth_) + " not divisible by 8");
	}
	
	if (auto byte_depth = header.bit_depth_ / 8; byte_depth <= 0 or 4 < byte_depth)
	{
		throw Wav_error("Bit depth of " + std::to_string(header.bit_depth_) + " is not supported");
	}
	
	return std::make_unique<Streaming_wav>(format, streambuf, header);
}

std::unique_ptr<Streaming_sound> create_stream(Format format, const std::filesystem::path& path)
{
	auto filebuf = utility::fileio::read_binary(path);
	auto header = Header(filebuf);
	
	if (header.bit_depth_ % 8 != 0)
	{
		throw Wav_error("Bit depth of " + std::to_string(header.bit_depth_) + " not divisible by 8");
	}
	
	if (auto byte_depth = header.bit_depth_ / 8; byte_depth <= 0 or 4 < byte_depth)
	{
		throw Wav_error("Bit depth of " + std::to_string(header.bit_depth_) + " is not supported");
	}
	
	return std::make_unique<Streaming_wav_file>(format, std::move(filebuf), header);
}
} // namespace nwd::audio::wav
