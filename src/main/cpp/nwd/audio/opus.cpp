// #include <opus/opusfile.h>

#include <nwd/audio/opus.hpp>

namespace nwd::audio::opus
{
struct Libopus_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

// NOTE https://opus-codec.org/docs/opus_api-1.3.1/group__opus__decoder.html
// NOTE https://www.opus-codec.org/docs/html_api/group__opusencoder.html
} // namespace nwd::audio::opus
