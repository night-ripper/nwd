#pragma once

#include <random>

namespace nwd::shared::globals
{
inline thread_local std::random_device random_device;
} // namespace nwd::shared::globals
