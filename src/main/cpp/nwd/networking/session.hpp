#pragma once

#include <iostream>
#include <syncstream>
#include <algorithm>
#include <mutex>

#include <nwd/containers/aa/map.hpp>
#include <nwd/containers/byte_queue.hpp>
#include <nwd/crypto/shared_key.hpp>
#include <nwd/networking/common.hpp>
#include <nwd/networking/socket.hpp>
#include <nwd/networking/messages/from_client.hpp>
#include <nwd/utility/endian.hpp>
#include <nwd/utility/to_chars.hpp>

namespace nwd::networking
{
struct Session
{
	using sequence_type = std::uint32_t;
	
	struct Client_inconsistent : std::exception
	{
		using std::exception::exception;
	};
	
	virtual ~Session() = default;
	
	Session(networking::Endpoint remote_endpoint, networking::Socket_udp send_socket) noexcept
		:
		remote_endpoint_(remote_endpoint),
		send_socket_(send_socket)
	{
	}
	
	enum struct Receive_result
	{
		ok,
		message_too_short,
		already_received,
		wrong_encryption,
	};
	
	void log_buf(auto&&... args)
	{
		(std::osyncstream(std::cout) << ... << args) << "\n";
	}
	
	Receive_result receive(std::span<std::byte> data)
	{
		if (data.size() < int(sizeof(sequence_type)))
		{
			return Receive_result::message_too_short;
		}
		
		auto lg = std::lock_guard(mutex_);
		
		const auto sequence = utility::endian::big_t<sequence_type>(data.data()).value();
		const auto expected_sequence = nonce_sequence(receive_nonce_).value();
		
		log_buf("Heap size: ", lengths_.size(), ", expecting: ", expected_sequence, ", received: ", sequence);
		
		// message already received
		if (Sequence_compare()(sequence, expected_sequence))
		{
			log_buf("Discarding ", sequence, " because it has already been received (from past)");
			return Receive_result::already_received;
		}
		
		auto nonce = receive_nonce_;
		
		if (sequence < expected_sequence)
		{
			std::fill(nonce.end() - sizeof(sequence_type), nonce.end(), std::byte(0xff));
			++nonce;
		}
		
		utility::endian::big_t<sequence_type>(sequence).store(nonce.end() - sizeof(sequence_type));
		
		if (auto opt_message = crypto::decrypt(data.subspan(sizeof(sequence_type)), data.data(), nonce, receive_key_); 
			opt_message.has_value())
		{
			data = *opt_message;
		}
		else
		{
			// message is ඞ
			log_buf("Discarding ", sequence, " because it was not properly encrypted");
			return Receive_result::wrong_encryption;
		}
		
		auto position = std::ptrdiff_t(0);
		bool present = false;
		
		auto it = lengths_.begin();
		
		if (sequence == expected_sequence)
		{
			++receive_nonce_;
		}
		else
		{
			for (bool increment_nonce = true; it != lengths_.end(); ++it)
			{
				log_buf(it->sequence_, ", ", nonce_sequence(receive_nonce_).value());
				if (increment_nonce and it->sequence_ == nonce_sequence(receive_nonce_).value())
				{
					++receive_nonce_;
				}
				else
				{
					increment_nonce = false;
				}
				
				if (sequence - expected_sequence == it->sequence_ - expected_sequence)
				{
					present = true;
				}
				
				if (sequence - expected_sequence <= it->sequence_ - expected_sequence)
				{
					break;
				}
				
				position += it->length_;
			}
		}
		
		for (auto jt = it; jt != lengths_.end(); ++jt)
		{
			if (jt->sequence_ != nonce_sequence(receive_nonce_).value())
			{
				break;
			}
			
			++receive_nonce_;
		}
		
		if (present)
		{
			// message already received
			log_buf("Discarding ", sequence, " because it has already been received (from future)");
			return Receive_result::already_received;
		}
		
		if (data.size() > 0 and data[0] == std::byte(networking::messages::Shared::stream_message))
		{
			lengths_.emplace(it, sequence, data.size() - 1, true);
			data_stream_.insert(data_stream_.begin() + position, data.begin() + 1, data.end());
		}
		else
		{
			lengths_.emplace(it, sequence, data.size(), false);
			data_stream_.insert(data_stream_.begin() + position, data.begin(), data.end());
		}
		
		return Receive_result::ok;
	}
	
	virtual void handle_out_of_order(sequence_type sequence, std::span<std::byte> message) = 0;
	
	[[nodiscard]]
	virtual std::ptrdiff_t handle_in_order(std::span<std::byte> message) = 0;
	
	std::lock_guard<std::mutex> get_lock()
	{
		return std::lock_guard(mutex_);
	}
	
	void handle(const std::lock_guard<std::mutex>& lg)
	{
		do_handle(lg);
	}
	
	virtual void do_handle(const std::lock_guard<std::mutex>&)
	{
		log_buf("handle");
		
		auto in_order_length = std::ptrdiff_t(0);
		
		for (auto i = std::ssize(lengths_) - 1; i != -1; --i)
		{
			auto& entry = lengths_[std::size_t(i)];
			
			if (not entry.in_order_)
			{
				handle_out_of_order(entry.sequence_, std::span(
					data_stream_.end() - entry.length_ - in_order_length, data_stream_.end() - in_order_length)
				);
				lengths_.erase(lengths_.begin() + i);
				data_stream_.erase(data_stream_.end() - entry.length_ - in_order_length, data_stream_.end() - in_order_length);
			}
			else
			{
				in_order_length += entry.length_;
			}
		}
		
		if (lengths_.size() > 0)
		{
			std::ptrdiff_t to_remove = handle_in_order(data_stream_);
			
			data_stream_.erase(data_stream_.begin(), data_stream_.begin() + to_remove);
			
			if (to_remove != std::ssize(data_stream_))
			{
				lengths_.erase(lengths_.begin(), lengths_.end() - 1);
				lengths_.front().length_ = std::int32_t(std::ssize(data_stream_) - to_remove)
					& (std::numeric_limits<std::int32_t>::max() >> 1)
				;
			}
			else
			{
				lengths_.clear();
			}
		}
	}
	
	virtual void send_immediately(std::span<const std::byte> data, const std::lock_guard<std::mutex>&)
	{
		auto message = std::array<std::byte, config::max_udp_length>();
		auto* begin = message.data();
		begin = nonce_sequence(send_nonce_).store(begin);
		crypto::encrypt(data, begin, send_nonce_, send_key_);
		++send_nonce_;
		send_socket_.send(remote_endpoint_, message);
	}
	
	const networking::Endpoint& remote_endpoint() const noexcept
	{
		return remote_endpoint_;
	}
	
	const networking::Socket_udp& send_socket() const noexcept
	{
		return send_socket_;
	}
	
protected:
	struct Sequence_compare
	{
		bool operator()(sequence_type lhs, sequence_type rhs) const noexcept
		{
			return sequence_type(std::numeric_limits<sequence_type>::max() / 2) < sequence_type(lhs - rhs);
		}
	};
	
	static utility::endian::big_t<sequence_type> nonce_sequence(const crypto::Nonce& nonce) noexcept
	{
		return utility::endian::big_t<sequence_type>(nonce.data() + crypto::Nonce::length - sizeof(sequence_type));
	}
	
	struct Length_entry
	{
		sequence_type sequence_;
		bool in_order_ : 1;
		std::int32_t length_ : 31;
	};
	
private:
	virtual Receive_result do_receive(std::span<std::byte> data) = 0;
	
protected:
	std::mutex mutex_;
	const networking::Endpoint remote_endpoint_;
	const networking::Socket_udp send_socket_;
	crypto::Shared_key receive_key_;
	crypto::Shared_key send_key_;
	crypto::Nonce receive_nonce_;
	crypto::Nonce send_nonce_;
	std::vector<Length_entry> lengths_;
	std::vector<std::byte> data_stream_;
};
} // namespace nwd::networking
