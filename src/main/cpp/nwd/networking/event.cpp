#include <cstring>

#include <iostream>
#include <memory>
#include <bitset>
#include <thread>
#include <semaphore>
#include <chrono>
#include <random>

#include <event2/event.h>
#include <event2/thread.h>

#include <unistd.h>

#include <nwd/networking/event.hpp>
#include <nwd/utility/atomic_lock.hpp>
#include <nwd/containers/static_vector.hpp>
#include <nwd/containers/vector_queue.hpp>

namespace nwd::networking
{
namespace
{
struct Libevent_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

void event_log_callback(int severity, const char* message)
{
	switch (severity)
	{
	case EVENT_LOG_DEBUG:
		std::clog << "libevent debug: " << message << "\n";
		break;
	case EVENT_LOG_MSG:
		std::clog << "libevent message: " << message << "\n";
		break;
	case EVENT_LOG_WARN:
		std::clog << "libevent warning: " << message << "\n";
		break;
	case EVENT_LOG_ERR:
		throw Libevent_error(message);
	}
}

static bool static_initialization = []() -> bool
{
	event_set_log_callback(&event_log_callback);
	event_enable_debug_mode();
	
#ifdef __unix__
	if (evthread_use_pthreads())
	{
		throw Libevent_error("evthread_use_pthreads: failed");
	}
#else
	if (evthread_use_windows_threads())
	{
		throw Libevent_error("evthread_use_windows_threads: failed");
	}
#endif
	
	if (std::atexit(libevent_global_shutdown))
	{
		throw Libevent_error("std::atexit(libevent_global_shutdown): failed");
	}
	
	return true;
}();
} // namespace

void deleters::event_base(void* object) noexcept
{
	event_base_free(static_cast<::event_base*>(object));
}

Event_base Event_base::create()
{
	if (auto* value = event_base_new(); value != nullptr)
	{
		auto result = Event_base();
		result.reset(value);
		return result;
	}
	
	throw Libevent_error("Could not initialize a new event_base");
}

void deleters::event(void* object) noexcept
{
	event_free(static_cast<::event*>(object));
}

Event_loop::Event_loop(Socket_udp socket)
	:
	socket_(socket),
	event_base_(Event_base::create())
{
	event_.reset(event_new(static_cast<::event_base*>(event_base_.get()), socket_.get(), EV_READ | EV_PERSIST,
	[](evutil_socket_t file_descriptor, [[maybe_unused]] short events, void* void_arg) -> void
	{
		static_cast<Event_loop*>(void_arg)->handle(file_descriptor);
	}, this));
	
	event_add(static_cast<::event*>(event_.get()), nullptr);
	loop_thread_ = std::jthread(event_base_loop, static_cast<::event_base*>(event_base_.get()), 0);
}
} // namespace nwd::networking
