#include <iostream>
#include <chrono>

#include <event2/event.h>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <atomic>

#include <nwd/networking/events/event.hpp>

#include <nwd/networking/common/nethost.hpp>
#include <nwd/networking/common/receiver.hpp>

using namespace nwd::networking;
using namespace nwd::networking::events;

int main()
{
#if 0
	int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	evutil_make_socket_nonblocking(fd);
	sockaddr_in socket_address;
	socket_address.sin_family = AF_INET;
	socket_address.sin_port = htons(25565);
	socket_address.sin_addr = {ipv4_of(127, 0, 0, 1)};
		
	bind(fd, reinterpret_cast<sockaddr*>(&socket_address), sizeof(socket_address));
	char dataz[1];
	
	sockaddr_in source;
	socklen_t source_len = sizeof(source);
	
	std::cout << recvfrom(fd, nullptr, 0, MSG_PEEK, reinterpret_cast<sockaddr*>(&source), &source_len) << "\n";
	std::cout << evutil_socket_geterror(fd) << "\n";
	std::cout << EAGAIN << "\n";
	std::cout << EWOULDBLOCK << "\n";
#else
	std::atomic<std::ptrdiff_t> asda = 0;
	
	Event_loop lp(ntohl(ipv4_of(127, 0, 0, 1)), 25566);
	Receiver receiver;
	
	auto f = [&](UDP_data& data) -> void
	{
		if (data.address_ != ntohl(ipv4_of(127, 0, 0, 1)))
		{
			throw;
			return;
		}
		
		if (data.port_ != 25565)
		{
			throw;
			return;
		}
		
		receiver.receive(data, [&](UDP_data& data) -> void
		{
			++asda;
			std::cout.write(reinterpret_cast<const char*>(data.content()), data.size_);
		});
	};
	
	lp.handle(f);
	
	auto now = std::chrono::steady_clock::now();
	
	for (int i = 0; i != 7000; ++i)
	{
		lp.send({htonl(ipv4_of(127, 0, 0, 1)), 25565, std::uint16_t(i), "xddd, roflmao\n"});
	}
	
	auto rtt = std::chrono::steady_clock::now() - now;
	
	std::cout << "it took ages: " << std::chrono::duration_cast<std::chrono::microseconds>(rtt).count() << "\n";
	
	std::cin.get();
	
	std::cout << "here: " << asda << "\n";
#endif
}
