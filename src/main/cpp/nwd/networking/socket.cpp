#include <stdexcept>

#include <event2/event.h>

#include <nwd/networking/socket.hpp>
#include <nwd/utility/integer_cast.hpp>

namespace nwd::networking
{
namespace
{
struct Socket_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

Socket_value_type make_socket(int domain, int type, int protocol)
{
	Socket_value_type result = socket(domain, type, protocol);
	
	if (result < 0)
	{
		throw Socket_error("Could not open a socket");
	}
	
	return result;
}

void bind_common(Socket_value_type socket, const sockaddr* address, socklen_t length)
{
	if (auto error = bind(socket, address, length); error == -1)
	{
		throw Socket_error("Socket bind failed: " + std::string(evutil_socket_error_to_string(evutil_socket_geterror(socket))));
	}
}
} // namespace

static_assert(std::is_same_v<evutil_socket_t, Socket_value_type>);

void deleters::socket(Socket object) noexcept
{
	evutil_closesocket(object.get());
}

void Socket::set_nonblocking() const
{
	if (auto error = evutil_make_socket_nonblocking(this->get()); error != 0)
	{
		throw Socket_error("evutil_make_socket_nonblocking failed");
	}
}

void Socket_udp::bind(Endpoint endpoint) const
{
	auto socket_address = sockaddr_in();
	socket_address.sin_family = AF_INET;
	std::memcpy(&socket_address.sin_port, endpoint.port_.data(), Port::length);
	std::memcpy(&socket_address.sin_addr, endpoint.address_.data(), Address::length);
	
	bind_common(this->get(), reinterpret_cast<const sockaddr*>(&socket_address), sizeof(socket_address));
}

Endpoint Socket_udp::endpoint() const
{
	auto socket_address = sockaddr_in();
	socket_address.sin_family = AF_INET;
	socklen_t source_len = sizeof(socket_address);
	
	if (getsockname(this->get(), reinterpret_cast<sockaddr*>(&socket_address), &source_len) == -1)
	{
		throw Socket_error("Socket getsockname failed: " + std::string(evutil_socket_error_to_string(evutil_socket_geterror(this->get()))));
	}
	
	auto result = Endpoint();
	std::memcpy(result.port_.data(), &socket_address.sin_port, Port::length);
	std::memcpy(result.address_.data(), &socket_address.sin_addr, Address::length);
	return result;
}

std::ptrdiff_t Socket_udp::send(Endpoint endpoint, std::span<const std::byte> data) const
{
	auto send_address = sockaddr_in();
	send_address.sin_family = AF_INET;
	std::memcpy(&send_address.sin_port, endpoint.port_.data(), Port::length);
	std::memcpy(&send_address.sin_addr, endpoint.address_.data(), Address::length);
	
	auto result = sendto(this->get(), reinterpret_cast<const char*>(data.data()),
		data.size(), 0, reinterpret_cast<sockaddr*>(&send_address), sizeof(send_address)
	);
	
	if (result == -1)
	{
		throw Socket_error("Socket send failed: " + std::string(evutil_socket_error_to_string(evutil_socket_geterror(this->get()))));
	}
	
	return result;
}

static std::tuple<Endpoint, std::ptrdiff_t> recv_helper(Socket_value_type socket, std::span<std::byte> result, int flags)
{
	auto socket_address = sockaddr_in();
	socklen_t source_len = sizeof(socket_address);
	auto size = recvfrom(socket, result.data(), result.size(), flags, reinterpret_cast<sockaddr*>(&socket_address), &source_len);
	
	auto endpoint = Endpoint();
	std::memcpy(endpoint.port_.data(), &socket_address.sin_port, Port::length);
	std::memcpy(endpoint.address_.data(), &socket_address.sin_addr, Address::length);
	
	return std::tuple(endpoint, size);
}

static Socket_error socket_receive_error()
{
	return Socket_error("Socket receive failed: " + std::string(evutil_socket_error_to_string(evutil_socket_geterror(socket))));
}

std::tuple<Endpoint, std::ptrdiff_t> Socket_udp::peek(std::span<std::byte> result) const
{
	auto [endpoint, size] = recv_helper(this->get(), result, MSG_TRUNC | MSG_PEEK);
	
	if (size == -1)
	{
		throw socket_receive_error();
	}
	
	return std::tuple(endpoint, size);
}

std::tuple<Endpoint, std::ptrdiff_t> Socket_udp::receive(std::span<std::byte> result) const
{
	auto [endpoint, size] = recv_helper(this->get(), result, MSG_TRUNC);
	
	if (size == -1)
	{
		throw socket_receive_error();
	}
	
	return std::tuple(endpoint, size);
}

std::tuple<Endpoint, std::ptrdiff_t> Socket_udp::try_receive(std::span<std::byte> result) const
{
	auto [endpoint, size] = recv_helper(this->get(), result, MSG_TRUNC | MSG_DONTWAIT);
	
	if (size == -1)
	{
		auto error = evutil_socket_geterror(this->get());
		
		if (error != EAGAIN and error != EWOULDBLOCK)
		{
			throw socket_receive_error();
		}
	}
	
	return std::tuple(endpoint, size);
}

Unique_socket_udp Unique_socket_udp::create()
{
	return Unique_socket_udp(make_socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP));
}
} // namespace nwd::networking
