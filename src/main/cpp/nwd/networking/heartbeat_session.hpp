#pragma once

#include <cstddef>
#include <cstdint>

#include <thread>
#include <chrono>

#include <nwd/shared/messages/common.hpp>
#include <nwd/networking/session.hpp>
#include <nwd/utility/executor.hpp>
#include <nwd/utility/endian.hpp>
#include <nwd/utility/integer_cast.hpp>
#include <nwd/utility/iterators.hpp>
#include <nwd/containers/aa/set.hpp>

namespace nwd::networking
{
struct Heartbeat_session : virtual Session
{
private:
	static void set_bit(std::ptrdiff_t index, std::span<std::byte> bytes) noexcept
	{
		bytes[utility::checked_cast(index / 8)] |= (std::byte(1) << (index % 8));
	}
	
	static bool get_bit(std::ptrdiff_t index, std::span<const std::byte> bytes) noexcept
	{
		return (bytes[utility::checked_cast(index / 8)] & (std::byte(1) << (index % 8))) != std::byte(0);
	}
	
	struct Bitset_iterator
	{
		using difference_type = std::ptrdiff_t;
		using value_type = std::int32_t;
		
		constexpr Bitset_iterator() noexcept = default;
		
		constexpr Bitset_iterator(std::span<const std::byte> bytes, std::int32_t end) noexcept
			:
			index_(-1),
			end_(end),
			bytes_(bytes)
		{
			++*this;
		}
		
		constexpr bool operator==(std::default_sentinel_t) const noexcept
		{
			return index_ == end_;
		}
		
		constexpr value_type operator*() const noexcept
		{
			return index_;
		}
		
		constexpr Bitset_iterator& operator++() noexcept
		{
			++index_;
			
			while (*this != std::default_sentinel)
			{
				if (((~bytes_[static_cast<std::size_t>(index_ / 8)]) & (std::byte(1) << (index_ % 8))) != std::byte(0))
				{
					break;
				}
				
				++index_;
			}
			
			return *this;
		}
		
		constexpr Bitset_iterator operator++(int) noexcept {return utility::operators::post_increment(*this);}
		
	protected:
		std::int32_t index_ = 0;
		std::int32_t end_ = 0;
		std::span<const std::byte> bytes_;
	};
	
	std::ranges::subrange<Bitset_iterator, std::default_sentinel_t>
	bitset_indices(std::span<const std::byte> bytes, std::int32_t end) noexcept
	{
		return std::ranges::subrange(Bitset_iterator(bytes, end), std::default_sentinel);
	}
	
public:
	Heartbeat_session()
	{
		send_buffer_.reserve(1024);
	}
	
	void do_handle(const std::lock_guard<std::mutex>& lg) override
	{
		Session::do_handle(lg);
	}
	
	virtual void send_deferred(std::span<const std::byte> data, const std::lock_guard<std::mutex>&)
	{
		send_buffer_.insert(send_buffer_.end(), data.begin(), data.end());
	}
	
	void send_from_buffer(const std::lock_guard<std::mutex>& lg)
	{
		auto i = std::size_t(0);
		
		while (i < send_buffer_.size())
		{
			auto length = std::min<std::size_t>(send_buffer_.size() - i, config::max_udp_length - crypto::Shared_key::mac_length);
			send_immediately(std::span(send_buffer_.data() + i, length), lg);
			i += length;
		}
		
		send_buffer_.clear();
	}
	
protected:
	std::vector<std::byte> send_buffer_;
};
} // namespace nwd::networking
