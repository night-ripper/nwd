#pragma once

#include <cstddef>

#include <bit>
#include <charconv>
#include <memory>
#include <optional>
#include <string>

#include <nwd/utility/byte_array.hpp>
#include <nwd/utility/integer_cast.hpp>
#include <nwd/utility/to_chars.hpp>

namespace nwd::networking
{
struct alignas(4) Address : utility::Byte_array<4>
{
	friend constexpr auto operator<=>(Address lhs, Address rhs) noexcept = default;
	
	Address() = default;
	
	constexpr Address(std::uint8_t v0, std::uint8_t v1, std::uint8_t v2, std::uint8_t v3) noexcept
		:
		utility::Byte_array<4>(std::array {std::byte(v0), std::byte(v1), std::byte(v2), std::byte(v3)})
	{
	}
	
	utility::To_chars_buffer<15> to_chars() const
	{
		auto result = utility::To_chars_buffer<15>();
		result << std::to_integer<std::uint8_t>(data()[0]);
		result << ".";
		result << std::to_integer<std::uint8_t>(data()[1]);
		result << ".";
		result << std::to_integer<std::uint8_t>(data()[2]);
		result << ".";
		result << std::to_integer<std::uint8_t>(data()[3]);
		return result;
	}
	
	const static Address localhost;
};

constexpr Address Address::localhost = Address(127, 0, 0, 1);

struct alignas(2) Port : utility::Byte_array<2>
{
	friend constexpr auto operator<=>(Port lhs, Port rhs) noexcept = default;
	
	Port() = default;
	
	//! @param port in host endianness
	constexpr explicit Port(std::uint16_t port) noexcept
	{
		data()[1] = std::byte(port & std::uint16_t(0xff));
		data()[0] = std::byte((port >> 8) & std::uint16_t(0xff));
	}
	
	constexpr std::uint16_t value() const noexcept
	{
		return std::uint16_t(std::to_integer<std::uint16_t>(data()[0]) << std::uint16_t(8)) + std::to_integer<std::uint16_t>(data()[1]);
	}
};

struct Endpoint
{
	Address address_;
	Port port_;
	
	friend constexpr auto operator<=>(Endpoint lhs, Endpoint rhs) noexcept = default;
	
	Endpoint() = default;
	
	constexpr Endpoint(Address address, Port port) noexcept
		:
		address_(address),
		port_(port)
	{
	}
	
	struct Hasher
	{
		std::size_t operator()(Endpoint value) const noexcept
		{
			std::size_t result = 0;
			std::memcpy(&result, &value, std::min(sizeof(std::size_t), sizeof(Endpoint)));
			return result;
		}
	};
	
	constexpr Address address() const noexcept {return address_;}
	constexpr Port port() const noexcept {return port_;}
};

/*
struct UDP_data
{
	constexpr static std::ptrdiff_t capacity = 500;
	
	std::int16_t size_;
	std::array<std::byte, capacity> data_;
	
	UDP_data() = default;
	
	constexpr std::int16_t size() const noexcept
	{
		return size_;
	}
	
	constexpr void size(std::int16_t value) & noexcept
	{
		size_ = value;
	}
	
	std::byte* data() noexcept {return data_.data();}
	const std::byte* data() const noexcept {return data_.data();}
	
private:
	template<typename Result_type>
	static Result_type static_content(auto&& self) noexcept
	{
		if (self.size_ < 0)
		{
			std::unreachable();
		}
		
		return std::span(self.data(), std::size_t(self.size_));
	}
	
public:
	std::span<std::byte> content() noexcept
	{
		return static_content<std::span<std::byte>>(*this);
	}
	
	std::span<const std::byte> content() const noexcept
	{
		return static_content<std::span<const std::byte>>(*this);
	}
	
	UDP_data& operator<<(std::byte value) noexcept
	{
		data_[utility::checked_cast<std::size_t>(size_)] = value;
		++size_;
		return *this;
	}
	
	UDP_data& operator<<(std::string_view text) noexcept
	{
		std::ranges::copy(text, reinterpret_cast<char*>(data() + size_));
		size_ += utility::checked_cast<std::int16_t>(std::ssize(text));
		return *this;
	}
	
	UDP_data& operator<<(std::span<const std::byte> span) noexcept
	{
		std::ranges::copy(span, data() + size_);
		size_ += utility::checked_cast<std::int16_t>(std::ssize(span));
		return *this;
	}
	
	void skip(std::int16_t value) noexcept
	{
		size_ += value;
	}
	
	void clear() noexcept
	{
		size_ = 0;
	}
};

struct UDP_datagram
{
	[[no_unique_address]] Endpoint endpoint_;
	UDP_data data_;
	
	UDP_datagram() = default;
	
	constexpr UDP_datagram(Endpoint endpoint) noexcept
		:
		endpoint_(endpoint)
	{
		data_.size(0);
	}
	
	constexpr Endpoint endpoint() const noexcept {return endpoint_;}
	constexpr UDP_data& data() noexcept {return data_;}
	constexpr const UDP_data& data() const noexcept {return data_;}
};
*/
} // namespace nwd::networking
