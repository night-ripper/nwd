#include <cstdint>
#include <type_traits>
#include <stdexcept>
#include <ranges>
#include <utility>

#include <nwd/utility/arrays.hpp>
#include <nwd/shared/config.hpp>
#include <nwd/shared/globals/random.hpp>
#include <nwd/utility/random.hpp>
#include <nwd/networking/messages/from_client.hpp>

namespace nwd::networking::messages
{
std::array<std::byte, crypto::keypair::seal_size + From_client_initial::length>
From_client_initial::prepare(const crypto::keypair::Public& public_key)
{
	std::ranges::copy(utility::random_data<crypto::Shared_key::length>(shared::globals::random_device), 
		client_send_key_.begin()
	);
	std::ranges::copy(utility::random_data<crypto::Shared_key::length>(shared::globals::random_device), 
		server_send_key_.begin()
	);
	std::ranges::copy(utility::random_data<crypto::Nonce::length>(shared::globals::random_device), 
		client_send_nonce_.begin()
	);
	std::ranges::copy(utility::random_data<crypto::Nonce::length>(shared::globals::random_device),
		server_send_nonce_.begin()
	);
	
	auto array = utility::arrays::join(config::version, client_send_key_,
		server_send_key_, client_send_nonce_, server_send_nonce_
	);
	
	return crypto::keypair::encrypt(std::span(std::as_const(array)), public_key);
}

auto From_client_initial::receive(std::span<const std::byte> data, const crypto::keypair::Public& public_key,
	const crypto::keypair::Private& private_key) -> Receive_result
{
	auto buffer = std::array<std::byte, From_client_initial::length + 32>();
	
	if (std::size(buffer) + crypto::keypair::seal_size < std::size(data))
	{
		return Receive_result::wrong_size;
	}
	
	auto opt = crypto::keypair::decrypt(data, buffer.data(), public_key, private_key);
	
	if (not opt)
	{
		return Receive_result::wrong_encryption;
	}
	
	// TODO version compatibility
	if (not std::equal(config::version.begin(), config::version.end(), opt->begin()))
	{
		return Receive_result::wrong_version;
	}
	
	auto input = opt->begin() + config::version.size();
	input = std::ranges::copy_n(input, crypto::Shared_key::length, client_send_key_.begin()).in;
	input = std::ranges::copy_n(input, crypto::Shared_key::length, server_send_key_.begin()).in;
	input = std::ranges::copy_n(input, crypto::Nonce::length, client_send_nonce_.begin()).in;
	input = std::ranges::copy_n(input, crypto::Nonce::length, server_send_nonce_.begin()).in;
	
	return Receive_result::ok;
}
} // namespace nwd::networking::messages
