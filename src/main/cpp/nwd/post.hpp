#pragma once

#include "rendering/type-enum.hpp"

#include <array>
#include <limits>

namespace bttf
{
struct Post_data
{
	struct Vertex_coordinates
	{
		using element_type = std::int16_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 2;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> vertex_coordinates;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		
		constexpr Vertex_coordinates() noexcept = default;
		
		constexpr Vertex_coordinates(element_type x, element_type y) noexcept
			:
			vertex_coordinates {x, y}
		{
		}
	};
	
	struct Texture_coordinates
	{
		using element_type = std::int16_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 2;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> texture_coordinates;
		
		constexpr Texture_coordinates() noexcept = default;
		
		constexpr Texture_coordinates(element_type x, element_type y) noexcept
			:
			texture_coordinates {x, y}
		{
		}
	};
	
	struct Texture_indices
	{
		using element_type = std::int16_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 2;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		
		element_type binding;
		element_type array_index;
		
		constexpr Texture_indices() noexcept = default;
		
		constexpr Texture_indices(element_type binding, element_type array_index) noexcept
			:
			binding {binding},
			array_index {array_index}
		{
		}
	};
	
	struct Color
	{
		using element_type = std::int8_t;
		
		constexpr static element_type max = std::numeric_limits<element_type>::max();
		
		constexpr static std::ptrdiff_t elements_per_vertex = 4;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> color {max, max, max, max};
		
		constexpr Color() noexcept = default;
		
		constexpr Color(element_type r, element_type g, element_type b, element_type a) noexcept
			:
			color {r, g, b, a}
		{
		}
	};
	
	Vertex_coordinates vertex_coordinates;
	Texture_coordinates texture_coordinates;
	Texture_indices texture_indices;
	Color color;
};

static_assert(std::is_standard_layout_v<Post_data>);

/// See Post_data::Color
// static_assert(std::is_trivially_constructible_v<Post_data>);

/// Make sure that vector::clear() does nothing
static_assert(std::is_trivially_destructible_v<Post_data>);
} // namespace bttf
