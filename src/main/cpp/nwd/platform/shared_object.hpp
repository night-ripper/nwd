#pragma once

#include <memory>
#include <string_view>
#include <type_traits>

namespace nwd::platform
{
struct Shared_object
{
	Shared_object() = default;
	
	explicit Shared_object(const char* filepath);
	
	template<typename Type>
	std::shared_ptr<Type> symbol(const char* name) const
	{
		return std::reinterpret_pointer_cast<Type>(private_symbol(name, true));
	}
	
	template<typename Type>
	std::shared_ptr<Type> symbol_if(const char* name) const noexcept
	{
		return std::reinterpret_pointer_cast<Type>(private_symbol(name, false));
	}
	
private:
	std::shared_ptr<void> private_symbol(const char* name, bool exception) const;
	
private:
	std::shared_ptr<void> handle_;
};
} // namespace nwd::platform
