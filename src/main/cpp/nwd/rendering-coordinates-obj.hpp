#pragma once

#include "rendering-coordinates.hpp"
#include "rendering/wavefront-obj.hpp"

#include "utility/exceptions.hpp"
#include "utility/normalize.hpp"

#include <cstddef>
#include <cstdint>

#include <algorithm>
#include <iterator>
#include <tuple>
#include <vector>

namespace bttf
{
struct Wavefront_obj_vertex_data
{
	struct Vertex_entry
	{
		Vertex_data::Relative_position relative_position;
		Vertex_data::Texture_coordinates texture_coordinates;
		Vertex_data::Normal_coordinates normal_coordinates;
	};
	
	std::vector<Vertex_entry> vdata_;
	std::vector<Element_index> vindices_;
	
	Wavefront_obj_vertex_data() = default;
	
	Wavefront_obj_vertex_data(const rendering::Wavefront_obj& obj)
	{
		vdata_.reserve(obj.num_vertices_);
		vindices_.reserve(std::ssize(obj.indices_));
		
		for (Element_index::index_type vindices_index = 0; const auto idx : obj.indices_)
		{
			if (idx.face_end())
			{
				vindices_.emplace_back(Element_index::restart);
			}
			else
			{
				vindices_.emplace_back(vindices_index);
				++vindices_index;
				
				const auto vertex = obj.vertices_[idx.v - 1];
				const auto vtex = obj.vertex_textures_[idx.vt - 1];
				const auto vnorm = obj.vertex_normals_[idx.vn - 1];
				
				auto& [rel_pos, tex_coords, normal_coords] = vdata_.emplace_back();
				
				rel_pos = {utility::nrm(vertex.x), utility::nrm(vertex.y), utility::nrm(vertex.z)};
				tex_coords = {utility::nrm(vtex.u), utility::nrm(vtex.v)};
				normal_coords = {utility::nrm(vnorm.x), utility::nrm(vnorm.y), utility::nrm(vnorm.z)};
			}
		}
		
		__glibcxx_assert(std::ssize(vdata_) == std::ptrdiff_t(vdata_.capacity()));
		__glibcxx_assert(std::ssize(vindices_) == std::ptrdiff_t(vindices_.capacity()));
	}
	
	struct Vertex_iterator
	{
		using difference_type = std::ptrdiff_t;
		using value_type = Vertex_data;
		using pointer = void;
		using reference = void;
		using iterator_category = std::random_access_iterator_tag;
		
		const Vertex_entry* index_ = nullptr;
		
		Vertex_iterator& operator+=(difference_type d) {index_ += d; return *this;}
		Vertex_iterator& operator-=(difference_type d) {return *this += -d;}
		friend Vertex_iterator operator+(Vertex_iterator left, difference_type d) {return left += d;}
		friend Vertex_iterator operator-(Vertex_iterator left, difference_type d) {return left -= d;}
		Vertex_iterator& operator++() {return *this += 1;}
		
		friend difference_type operator-(Vertex_iterator left, Vertex_iterator right) {return left.index_ - right.index_;}
		
		value_type operator*() const
		{
			value_type result;
			
			result.relative_position = index_->relative_position;
			result.texture_coordinates = index_->texture_coordinates;
			result.normal_coordinates = index_->normal_coordinates;
			
			return result;
		}
		
		constexpr friend auto operator<=>(const Vertex_iterator&, const Vertex_iterator&) = default;
	};
	
	Vertex_iterator vbegin() const {return {std::data(vdata_)};}
	Vertex_iterator vend() const {return {std::data(vdata_) + std::ssize(vdata_)};}
	
	auto vertices() const
	{
		struct
		{
			Vertex_iterator begin_;
			Vertex_iterator end_;
			
			Vertex_iterator begin() const {return begin_;}
			Vertex_iterator end() const {return end_;}
		}
		result {vbegin(), vend()};
		
		return result;
	}
	
	std::ptrdiff_t size() const
	{
		return std::ssize(vdata_);
	}
	
	std::ptrdiff_t size_indices() const
	{
		return std::ssize(vindices_);
	}
	
	template<typename Out_it>
	void fill_indices(Out_it out_it, std::int64_t last_index)
	{
		std::transform(std::begin(vindices_), std::end(vindices_), out_it,
		[&](Element_index idx) -> Element_index
		{
			if (idx == Element_index::restart)
			{
				return idx;
			}
			else
			{
				return idx + last_index;
			}
		});
	}
};
} // namespace bttf
