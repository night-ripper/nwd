#pragma once

#include <array>
#include <span>

namespace nwd::crypto
{
std::array<std::byte, 32> hash32(std::span<const std::byte> data);
} // namespace nwd::crypto
