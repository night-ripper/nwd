#include <iostream>

#include <sodium/crypto_generichash.h>

#include <nwd/crypto/hash.hpp>

namespace nwd::crypto
{
std::array<std::byte, 32> hash32(std::span<const std::byte> data)
{
	static_assert(crypto_generichash_BYTES == 32);
	
	std::array<std::byte, 32> result;
	
	crypto_generichash(
		reinterpret_cast<unsigned char*>(result.data()), result.size(),
		reinterpret_cast<const unsigned char*>(data.data()), data.size(), nullptr, 0
	);
	
	return result;
}
} // namespace nwd::crypto
