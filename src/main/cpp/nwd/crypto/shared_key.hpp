#pragma once

#include <cstddef>
#include <cstring>

#include <array>
#include <optional>
#include <span>

#include <nwd/utility/byte_array.hpp>

namespace nwd::crypto
{
struct Shared_key : utility::Byte_array<32>
{
	using utility::Byte_array<32>::Byte_array;
	constexpr static std::ptrdiff_t mac_length = 16;
	
	static Shared_key create();
};

struct Nonce : utility::Byte_array<24>
{
	using utility::Byte_array<24>::Byte_array;
};

/**
 * @param message and @param result may overlap
 */
std::span<std::byte> encrypt(std::span<const std::byte> message,
	std::byte* result, const Nonce& nonce, const Shared_key& key
);

inline std::span<std::byte> encrypt_in_place(std::span<std::byte> message,
	const Nonce& nonce, const Shared_key& key)
{
	return encrypt(message, message.data(), nonce, key);
}

template<std::size_t Size>
inline std::array<std::byte, Size + Shared_key::mac_length> encrypt(
	const std::byte* message, const Nonce& nonce, const Shared_key& key)
{
	std::array<std::byte, Size + Shared_key::mac_length> result;
	encrypt(message, result.data(), nonce, key);
	return result;
}

template<std::size_t Size>
inline std::array<std::byte, Size + Shared_key::mac_length> encrypt(
	std::span<const std::byte, Size> message, const Nonce& nonce, const Shared_key& key)
{
	return encrypt<Size>(message.data(), nonce, key);
}

/**
 * @param message and @param result may overlap
 * @return Decrypted message or empty optional if verification failed.
 */
std::optional<std::span<std::byte>> decrypt(std::span<const std::byte> message,
	std::byte* result, const Nonce& nonce, const Shared_key& key
);

inline std::optional<std::span<std::byte>> decrypt_in_place(std::span<std::byte> message,
	const Nonce& nonce, const Shared_key& key)
{
	return decrypt(message, message.data(), nonce, key);
}

template<std::size_t Size>
inline std::optional<std::array<std::byte, Size - Shared_key::mac_length>> decrypt(
	const std::byte* message, const Nonce& nonce, const Shared_key& key)
{
	auto result = std::optional<std::array<std::byte, Size - Shared_key::mac_length>>(std::in_place);
	
	if (not decrypt(message, result->data(), nonce, key).has_value())
	{
		result.reset();
	}
	
	return result;
}

template<std::size_t Size>
inline std::optional<std::array<std::byte, Size - Shared_key::mac_length>> decrypt(
	std::span<const std::byte, Size> message, const Nonce& nonce, const Shared_key& key)
{
	return decrypt(message.data(), nonce, key);
}
} // namespace nwd::crypto
