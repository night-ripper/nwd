#pragma once

#include <cstddef>
#include <cstring>

#include <array>
#include <optional>
#include <span>
#include <utility>

#include <nwd/utility/byte_array.hpp>

namespace nwd::crypto::keypair
{
constexpr static std::ptrdiff_t seal_size = 48;

struct Public : utility::Byte_array<32>
{
	using utility::Byte_array<32>::Byte_array;
};

struct Private : utility::Byte_array<32>
{
	using utility::Byte_array<32>::Byte_array;
};

using keypair_type = std::pair<Public, Private>;

keypair_type create();

/**
 * @param message and @param result may not overlap
 */
std::span<std::byte> encrypt(std::span<const std::byte> message,
	std::byte* result, const Public& public_key
);

template<std::size_t Size>
inline std::array<std::byte, Size + seal_size> encrypt(
	const std::byte* message, const Public& public_key)
{
	auto result = std::array<std::byte, Size + seal_size>();
	encrypt(std::span(message, Size), result.data(), public_key);
	return result;
}

template<std::size_t Size>
inline std::array<std::byte, Size + seal_size> encrypt(
	std::span<const std::byte, Size> message, const Public& public_key)
{
	return encrypt<Size>(message.data(), public_key);
}

/**
 * @param message and @param result may not overlap
 */
std::optional<std::span<std::byte>> decrypt(std::span<const std::byte> message,
	std::byte* result, const Public& public_key, const Private& private_key
);

template<std::size_t Size>
inline std::optional<std::array<std::byte, Size - seal_size>> decrypt(
	const std::byte* message, const Public& public_key, const Private& private_key)
{
	auto result = std::optional<std::array<std::byte, Size - seal_size>>(std::in_place);
	
	if (not decrypt(std::span<const std::byte>(message, Size), result->data(), public_key, private_key).has_value())
	{
		result.reset();
	}
	
	return result;
}

template<std::size_t Size>
inline std::optional<std::array<std::byte, Size - seal_size>> decrypt(
	std::span<const std::byte, Size> message, const Public& public_key, const Private& private_key)
{
	return decrypt<Size>(message.data(), public_key, private_key);
}
} // namespace nwd::crypto::keypair
