#pragma once

#include <nwd/math/vector.hpp>
#include <nwd/math/quaternion.hpp>

namespace nwd::math
{
template<typename Type>
requires(std::is_floating_point_v<Type>)
inline constexpr Vec3<Type> rotate(Vec3<Type> vector, Quaternion q)
{
	auto v = Quaternion(vector[0], vector[1], vector[2], 0);
	v = q * v * -q;
	return Vec3<Type> {v[0], v[1], v[2]};
}
} // namespace nwd::math
