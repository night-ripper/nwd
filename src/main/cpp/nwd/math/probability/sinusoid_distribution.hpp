#pragma once

#include <cmath>
#include <numbers>

#include <random>
#include <numeric>
#include <type_traits>

namespace nwd::math
{
template<typename Result_type>
requires(std::is_floating_point_v<Result_type>)
struct Sinusoid_distribution
{
	using result_type = Result_type;
	
	struct param_type
	{
		using distribution_type = Sinusoid_distribution<result_type>;
		
		constexpr param_type(result_type radius = 1, result_type center = 0) noexcept
			:
			radius_(radius),
			center_(center)
		{
		}
		
		constexpr result_type radius() const noexcept {return radius_;}
		constexpr result_type center() const noexcept {return center_;}
		
		constexpr friend bool operator==(param_type, param_type) noexcept = default;
		
	private:
		result_type radius_;
		result_type center_;
	};
	
	constexpr Sinusoid_distribution(param_type param = param_type()) noexcept
		:
		param_(param)
	{
	}
	
	constexpr static Sinusoid_distribution radius_center(result_type radius, result_type center = 0) noexcept
	{
		return Sinusoid_distribution(param_type(radius, center));
	}
	
	constexpr static Sinusoid_distribution min_max(result_type min, result_type max) noexcept
	{
		auto middle = std::midpoint(min, max);
		return Sinusoid_distribution(param_type(std::min({middle - min, max - middle}), middle));
	}
	
	constexpr Sinusoid_distribution(const Sinusoid_distribution&) noexcept = default;
	constexpr Sinusoid_distribution& operator=(const Sinusoid_distribution&) noexcept = default;
	
	void reset() const noexcept
	{
		return;
	}
	
	param_type param() const noexcept {return param_;}
	void param(param_type param) noexcept {param_ = param;}
	
	template<typename Generator_type>
	result_type operator()(Generator_type&& generator)
	{
		return (*this)(generator, param());
	}
	
	template<typename Generator_type>
	result_type operator()(Generator_type&& generator, param_type param)
	{
		auto value = std::generate_canonical<result_type, 1>(generator) * 2 - 1;
		
		constexpr auto multiplier = result_type(2) / std::numbers::pi_v<result_type>;
		
		value = std::asin(value) * multiplier;
		
		return value * param.radius() + param.center();
	}
	
	constexpr result_type min() const noexcept {return param_.center() - param_.radius();}
	constexpr result_type max() const noexcept {return param_.center() + param_.radius();}
	
	constexpr friend bool operator==(Sinusoid_distribution, Sinusoid_distribution) noexcept = default;
	
private:
	param_type param_;
};
} // namespace nwd::math
