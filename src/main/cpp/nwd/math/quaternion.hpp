#pragma once

#include <cmath>
#include <cstdint>

#include <ostream>

#include <nwd/serialization/common.hpp>
#include <nwd/serialization/fundamental.hpp>

namespace nwd::math
{
struct Quaternion : std::array<float, 4>
{
	template<typename Type, typename L_type, typename R_type>
	constexpr static void multiply(Type& result, const L_type& lc, const R_type& rc) noexcept
	{
		result[0] = lc[3] * rc[0] + lc[0] * rc[3] + lc[1] * rc[2] - lc[2] * rc[1];
		result[1] = lc[3] * rc[1] + lc[1] * rc[3] + lc[2] * rc[0] - lc[0] * rc[2];
		result[2] = lc[3] * rc[2] + lc[2] * rc[3] + lc[0] * rc[1] - lc[1] * rc[0];
		result[3] = lc[3] * rc[3] - lc[0] * rc[0] - lc[1] * rc[1] - lc[2] * rc[2];
	}

	template<typename It, typename Type, typename L_type, typename R_type>
	constexpr static void multiply_typed(Type& result, const L_type& lc, const R_type& rc) noexcept
	{
		result[0] = It(lc[3]) * It(rc[0]) + It(lc[0]) * It(rc[3]) + It(lc[1]) * It(rc[2]) - It(lc[2]) * It(rc[1]);
		result[1] = It(lc[3]) * It(rc[1]) + It(lc[1]) * It(rc[3]) + It(lc[2]) * It(rc[0]) - It(lc[0]) * It(rc[2]);
		result[2] = It(lc[3]) * It(rc[2]) + It(lc[2]) * It(rc[3]) + It(lc[0]) * It(rc[1]) - It(lc[1]) * It(rc[0]);
		result[3] = It(lc[3]) * It(rc[3]) - It(lc[0]) * It(rc[0]) - It(lc[1]) * It(rc[1]) - It(lc[2]) * It(rc[2]);
	}
	
	constexpr Quaternion() noexcept = default;
	
	constexpr Quaternion(value_type x, value_type y, value_type z, value_type w) noexcept
		:
		std::array<value_type, 4> {x, y, z, w}
	{
	}
	
	constexpr static Quaternion from_unit_axis_sin_cos_half(float x, float y, float z, float sin_half, float cos_half)
	{
		x *= sin_half;
		y *= sin_half;
		z *= sin_half;
		return Quaternion(x, y, z, cos_half);
	}
	
	constexpr static Quaternion from_unit_axis_angle(float x, float y, float z, float angle)
	{
		const float half_angle = angle / 2.f;
		return from_unit_axis_sin_cos_half(x, y, z, std::sin(half_angle), std::cos(half_angle));
	}
	
	constexpr friend Quaternion operator*(Quaternion lhs, Quaternion rhs) noexcept
	{
		auto result = Quaternion();
		multiply(result, lhs, rhs);
		return result;
	}
	
	constexpr Quaternion& operator*=(Quaternion rhs) noexcept
	{
		return *this = *this * rhs;
	}
};

constexpr Quaternion operator-(Quaternion q) noexcept
{
	q[0] = -q[0];
	q[1] = -q[1];
	q[2] = -q[2];
	return q;
}

inline std::ostream& operator<<(std::ostream& os, Quaternion q)
{
	return os << "Quaternion {" << q[0] << ", " << q[1] << ", " << q[2] << ", " << q[3] << "}";
}
} // namespace nwd::math

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<math::Quaternion>
= 4 * serialized_size<math::Quaternion::value_type>;

template<> inline constexpr auto& private_serialize<math::Quaternion> =*
[](std::byte* target, math::Quaternion value) -> void
{
	serialize_shift(target, value[0]);
	serialize_shift(target, value[1]);
	serialize_shift(target, value[2]);
	serialize_shift(target, value[3]);
};

template<> inline constexpr auto& private_deserialize<math::Quaternion> =*
[](const std::byte* source, math::Quaternion& result) -> void
{
	deserialize_shift(source, result[0]);
	deserialize_shift(source, result[1]);
	deserialize_shift(source, result[2]);
	deserialize_shift(source, result[3]);
};
} // namespace nwd::serialization
