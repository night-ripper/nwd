#pragma once

#include <cstdint>
#include <cmath>

#include <array>
#include <type_traits>

namespace nwd::math
{
struct Mat4f : std::array<float, 4 * 4>
{
	Mat4f() = default;
	
	constexpr explicit Mat4f(float diagonal_value) noexcept
		:
		std::array<float, 4 * 4> {
			diagonal_value, 0, 0, 0,
			0, diagonal_value, 0, 0,
			0, 0, diagonal_value, 0,
			0, 0, 0, diagonal_value,
		}
	{
	}
	
	constexpr float& operator[](std::int32_t column, std::int32_t row) & noexcept
	{
		return this->data()[std::size_t(column * 4 + row)];
	}
	
	constexpr const float& operator[](std::int32_t column, std::int32_t row) const& noexcept
	{
		return this->data()[std::size_t(column * 4 + row)];
	}
};

inline constexpr Mat4f transpose(Mat4f matrix) noexcept
{
	for (std::int16_t row = 0; row < 4; ++row)
	{
		for (std::int16_t column = row + 1; column < 4; ++column)
		{
			std::swap(matrix[row, column], matrix[column, row]);
		}
	}
	
	return matrix;
}

inline constexpr Mat4f operator*(const Mat4f& lhs, const Mat4f& rhs) noexcept
{
	auto result = Mat4f();
	
	for (std::int16_t i = 0; i < 4; ++i)
	{
		for (std::int16_t j = 0; j < 4; ++j)
		{
			for (std::int16_t k = 0; k < 4; ++k)
			{
				result[i, j] += lhs[k, j] * rhs[i, k];
			}
		}
	}
	
	return result;
}
} // namespace nwd::math
