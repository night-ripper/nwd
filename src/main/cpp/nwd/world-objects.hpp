#pragma once

#include "world.hpp"

#include "audio/openal.hpp"
#include "rendering/context.hpp"
#include "geometry/cylinder3d.hpp"
#include "geometry/sphere3d.hpp"
#include "audio-component.hpp"
#include "rendering-coordinates-obj.hpp"

namespace bttf
{
extern std::pmr::memory_resource& memory_resource();

struct Audio_chunk
{
	audio::openal::Source source_;
	const audio::openal::Buffer* buffer_;
	geometry::Vector3D position_;
	
	~Audio_chunk();
	Audio_chunk(const audio::openal::Buffer* buffer, geometry::Vector3D position);
	Audio_chunk(const audio::openal::Buffer* buffer);
	
	void update_position();
};

struct Audio_object : bttf::World::Object<Audio_object>, Audio_chunk
{
	~Audio_object();
	Audio_object(const audio::openal::Buffer* buffer, geometry::Vector3D position);
	Audio_object(const audio::openal::Buffer* buffer);
};

struct Wall_chunk : bttf::World::Rendering_object
{
	using index_type = bttf::Vertex_data::Texture_indices;
	
	index_type tex_index_;
	geometry::Vector3D begin_;
	float scale_;
	float scale_height_;
	geometry::Quaternion quaternion_;
	
	Wall_chunk(geometry::Vector3D begin, geometry::Vector3D end, index_type tex_index, float height = 1);
	explicit operator geometry::Bounding_box3D() const;
	
private:
	void write_buffers(bttf::Rendering_component& comp);
};

struct Wall : bttf::World::Object<Wall>, Wall_chunk
{
	geometry::R_Tree3D<Object_base>::Handle handle_;
	
	~Wall();
	Wall(geometry::Vector3D begin, geometry::Vector3D end, index_type tex_index, float height = 1);
};

struct Floor_chunk : bttf::World::Rendering_object
{
	using index_type = bttf::Vertex_data::Texture_indices;
	
	index_type tex_index_;
	geometry::Vector3D center_;
	
	Floor_chunk(geometry::Vector3D origin, index_type tex_index);
	explicit operator geometry::Bounding_box3D() const;
	
private:
	void write_buffers(bttf::Rendering_component& comp);
};


struct Floor : bttf::World::Object<Floor>, Floor_chunk
{
	Floor(geometry::Vector3D origin, index_type tex_index);
};

struct Smonk : bttf::World::Object<Smonk>, bttf::World::Rendering_object
{
	using index_type = bttf::Vertex_data::Texture_indices;
	
	index_type tex_index_;
	geometry::Cylinder3D cylinder_;
	geometry::Vector3D center_;
	glsl::vec3 rotation_vector_;
	geometry::angle_t angle_;
	float transparency_ = 0;
	
	~Smonk();
	Smonk(geometry::Cylinder3D cylinder_);
	explicit operator geometry::Bounding_box3D() const;
	
private:
	void write_buffers(bttf::Rendering_component& comp);
};

struct Light_chunk : bttf::World::Rendering_object
{
	geometry::Sphere3D area_;
	float r_, g_, b_;
	float multiplier_ = 1;
	glsl::vec3 direction_ = {};
	float cos_inner_cutoff_ = -1;
	float cos_outer_cutoff_ = -1;
	geometry::angle_t angle_ {};
	glsl::vec3 original_direction_ = {};
	
	Light_chunk(geometry::Sphere3D area, float r, float g, float b);
	
private:
	void write_buffers(bttf::Rendering_component& comp);
};

struct Light : bttf::World::Object<Light>, Light_chunk
{
	Light(geometry::Sphere3D area, float r, float g, float b);
};

struct Smonk_generator : bttf::World::Object<Smonk_generator>
{
	geometry::Cylinder3D cylinder_;
	Light_chunk light_;
	float time_to_generate_;
	float delay_;
	
	Smonk_generator(geometry::Cylinder3D cylinder_, float delay);
};

struct Punch;
struct Key;

struct Creature : bttf::World::Object<Creature>, bttf::World::Rendering_object
{
	using index_type = bttf::Vertex_data::Texture_indices;
	
	geometry::R_Tree3D<Object_base>::Handle handle_;
	
	geometry::Cylinder3D cylinder_;
	float tex_center_x_;
	float tex_center_y_;
	float tex_scale_;
	geometry::Vector3D moving_;
	geometry::Vector3D velocity_;
	index_type tex_index_;
	geometry::angle_t angle_xy_ {};
	Light_chunk light_;
	Light_chunk flashlight_;
	Punch* punch_ = nullptr;
	std::vector<std::shared_ptr<Key>> keys_;
	float health_ = 1;
	
	~Creature();
	Creature(geometry::Cylinder3D collision, float tex_center_x, float tex_center_y, float tex_scale,
		geometry::angle_t angle_xy, index_type tex_index);
	
	void control(rendering::opengl::Context& context, Rendering_component& rcomp);
	
private:
	void time_handle(float seconds);
	void write_buffers(bttf::Rendering_component& comp);
};

struct Crate : bttf::World::Object<Crate>, bttf::World::Rendering_object
{
	using index_type = bttf::Vertex_data::Texture_indices;
	
	geometry::R_Tree3D<Object_base>::Handle handle_;
	float side_length_;
	geometry::Sphere3D sphere_;
	geometry::Quaternion quaternion_;
	index_type tex_index_;
	bttf::Wavefront_obj_vertex_data* model_;
	std::shared_ptr<Key> key_;
	int life_;
	
	~Crate();
	Crate(geometry::Vector3D bottom, float side_length, geometry::angle_t angle, index_type tex_index, bttf::Wavefront_obj_vertex_data* model);
	
	void get_hit();
	
private:
	void write_buffers(bttf::Rendering_component& comp);
};

struct Key : bttf::World::Object<Key>, bttf::World::Rendering_object
{
	float scale_;
	geometry::R_Tree3D<Object_base>::Handle handle_;
	geometry::Cylinder3D collision_;
	geometry::Quaternion quaternion_;
	Light_chunk light_;
	
	~Key();
	Key(geometry::Vector3D origin);
	
private:
	void write_buffers(bttf::Rendering_component& comp);
};

struct Door : Wall
{
	const Key* unlocked_by_ = nullptr;
	bool locked_ = true;
	
	~Door();
	Door(geometry::Vector3D begin, geometry::Vector3D end);
};

struct Aloe : bttf::World::Object<Aloe>, bttf::World::Rendering_object
{
	geometry::Vector3D position_;
	
	Aloe(geometry::Vector3D position);
	
	explicit operator geometry::Bounding_box3D() const;
	
private:
	void write_buffers(bttf::Rendering_component& comp);
};
} // namespace bttf
