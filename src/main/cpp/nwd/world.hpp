#pragma once

#include "geometry/geometry.hpp"
#include "geometry/rtree-old/rtree3d-fwd.hpp"

#include "glue/camera.hpp"

#include "rendering-component.hpp"

#include <functional>
#include <memory>

namespace bttf
{
struct World
{
	struct Object_base
	{
		friend struct World;
		
		template<typename>
		friend struct Object;
		
		template<typename>
		friend struct Lambda_object;
		
		template<typename Type = void>
		using Time_handler = void(*)(Type* super, float seconds);
		
		/// There is no virtual, correct deletion is handled by shared_ptr
		~Object_base();
		
		Object_base(void* super) : Object_base(super, nullptr) {}
		
		World& world()
		{
			return *world_;
		}
		
		void disable();
		
		template<typename Object_type>
		requires(std::is_convertible_v<Object_type&, Object_base&>)
		Object_type& own(std::shared_ptr<Object_type> object)
		{
			return static_cast<Object_type&>(own(std::shared_ptr<Object_base>(std::move(object))));
		}
		
		std::shared_ptr<Object_base> disown(Object_base* object);
		
	private:
		Object_base(void* super, Time_handler<> handler);
		
		const std::shared_ptr<Object_base>& shared_this()
		{
			return shared_this_;
		}
		
		void set_time_handler(Time_handler<> handler);
		Object_base& own(std::shared_ptr<Object_base> object);
		
		/// @p guard is provided externally because objects owned by others
		/// cannot guard themselves by copying shared_this_ inside the function.
		Object_base* time_handle(float seconds, std::shared_ptr<Object_base> guard);
		
		void link(World* world);
		void unlink();
		
	private:
		std::shared_ptr<Object_base> shared_this_;
		
		void* const super_ = nullptr;
		Time_handler<> time_handler_ = nullptr;
		std::pmr::vector<std::shared_ptr<Object_base>> owned_objects_;
		
		Object_base* prev_ = nullptr;
		Object_base* next_ = nullptr;
		
		World* world_ = nullptr;
	};
	
	template<typename Type>
	struct Object : Object_base
	{
		Object(Type* super)
			:
			Object_base(super, nullptr)
		{
		}
		
		template<typename Functor>
		Object(Type* super, Functor)
			:
			Object_base(super, [](void* self, float seconds) -> void
			{
				Functor {}(static_cast<Type*>(self), seconds);
			})
		{
		}
		
		Type& super()
		{
			return *static_cast<Type*>(super_);
		}
		
		std::shared_ptr<Type> shared_this()
		{
			return std::static_pointer_cast<Type>(Object_base::shared_this());
		}
		
		template<typename Functor>
		void set_time_handler(Functor)
		{
			Object_base::set_time_handler([](void* self, float seconds) -> void
			{
				Functor {}(static_cast<Type*>(self), seconds);
			});
		}
	};
	
	struct Rendering_object
	{
		friend struct World;
		
		template<typename Type = void>
		using Draw_handler = void(*)(Type* super, Rendering_component&);
		
	private:
		Rendering_object(void* super, Draw_handler<> handler)
			:
			super_ {super},
			draw_handler_ {handler}
		{
		}
		
		void draw_handle(Rendering_component& comp)
		{
			return draw_handler_(super_, comp);
		}
		
	public:
		template<typename Type, typename Functor>
		Rendering_object(Type* super, Functor)
			:
			Rendering_object(static_cast<void*>(super), Draw_handler<>(
			[](void* self, Rendering_component& rcomp) -> void
			{
				Functor {}(static_cast<Type*>(self), rcomp);
			}))
		{
		}
		
		void* const super_;
		geometry::R_Tree3D<Rendering_object>::Handle handle_;
		Draw_handler<> draw_handler_ = nullptr;
	};
	
	bttf::Camera camera_;
	
	geometry::R_Tree3D<Rendering_object> rtree_rendering_;
	geometry::R_Tree3D<Object_base> rtree_collision_;
	
	Object_base* first_ = nullptr;
	Object_base* last_updating_ = nullptr;
	Object_base* last_updating_previous_ = nullptr;
	
	~World();
	World();
	
	void time_pass(float seconds);
	void draw(Rendering_component&);
	
	Object_base& attach(std::shared_ptr<Object_base>);
	
	template<typename Object_type>
	requires(std::is_convertible_v<Object_type&, Object_base&>)
	Object_type& attach(std::shared_ptr<Object_type> object)
	{
		return static_cast<Object_type&>(attach(std::shared_ptr<Object_base>(std::move(object))));
	}
	
	void attach(Rendering_object&, const geometry::Bounding_box3D&);
	void attach_collision(Object_base&, geometry::R_Tree3D<Object_base>::Handle&,
		const geometry::Bounding_box3D&);
};
} // namespace bttf
