#include "world.hpp"

#include "utility/exceptions.hpp"

#include "geometry/rtree-old/strategies.hpp"

#include <iostream>

namespace bttf
{
World::~World()
{
	for (auto* object = first_; object;)
	{
		auto* next = object->next_;
		object->shared_this_.reset();
		object = next;
	}
}

World::World()
	:
	rtree_rendering_(6, geometry::r_tree::strategies::r_star),
	rtree_collision_(6, geometry::r_tree::strategies::r_star)
{
}

void World::time_pass(float seconds)
{
	for (auto* object = last_updating_; object and object != last_updating_previous_;)
	{
		object = object->time_handle(seconds, object->shared_this_);
	}
	
	last_updating_previous_ = last_updating_;
	
	for (auto* object = last_updating_previous_; object;)
	{
		/// Do a single adress-based swap, this will in the long run cause
		/// all the objects to be sorted and hopefully increase cache performance
		/*
		if (auto* p = object->prev_; p and p < object)
		{
			if (auto* n = object->next_) {n->prev_ = p;}
			if (auto* pp = p->prev_) {pp->next_ = object;}
			
			p->next_ = object->next_;
			object->next_ = p;
			object->prev_ = p->prev_;
			p->prev_ = object;
			
			if (last_updating_ == object) {last_updating_ = p;}
			if (last_updating_previous_ == object) {last_updating_previous_ = p;}
			if (first_ == p) {first_ = object;}
			
			object = p;
		}
		*/
		
		object = object->time_handle(seconds, object->shared_this_);
	}
}

void World::draw(Rendering_component& rcomp)
{
	rtree_rendering_.visit_immediate(camera_.visibility_frustum(),
	[&](Rendering_object& robject) -> void
	{
		robject.draw_handle(rcomp);
	});
	
	rcomp.draw(camera_.position(), camera_.final_matrix());
}

World::Object_base::~Object_base()
{
	/// Just to catch possible errors
	
	prev_ = nullptr;
	next_ = nullptr;
	
	time_handler_ = nullptr;
}

World::Object_base::Object_base(void* super, Time_handler<> handler)
	:
	super_ {super},
	time_handler_ {handler}
{
}

void World::Object_base::set_time_handler(Time_handler<> handler)
{
	if (bool(time_handler_) != bool(handler))
	{
		auto* world = world_;
		
		unlink();
		time_handler_ = handler;
		link(world);
	}
	else
	{
		time_handler_ = handler;
	}
}

/// Keep the order:
/// 1) time handler on this
/// 2) time handler on owned objects
auto World::Object_base::time_handle(float seconds, [[maybe_unused]] std::shared_ptr<Object_base> guard) -> Object_base*
{
	utility::assert_that([&]() -> bool {return guard.get() == this;});
	
	time_handler_(super_, seconds);
	
	auto end = std::end(owned_objects_);
	
	for (auto it = std::begin(owned_objects_); it != end;)
	{
		if (*it)
		{
			(*it)->time_handle(seconds, *it);
		}
		
		/// Yes there is no *else* branch, we check for its lifetime again after
		/// executing the owned object's handler
		if (not *it)
		{
			std::swap(*it, *(end - 1));
			--end;
			
			/// Debug version of libstdc++ throws an error when comparing
			/// iterators after popping
			const bool stop = (it == end);
			
			owned_objects_.pop_back();
			
			if (stop)
			{
				break;
			}
		}
		else
		{
			++it;
		}
	}
	
	return prev_;
}

void World::Object_base::disable()
{
	if (shared_this_)
	{
		unlink();
		shared_this_.reset();
	}
	else
	{
		std::clog << "Calling disable() on a disabled object" << "\n";
	}
}

void World::Object_base::link(World* world)
{
	world_ = world;
	
	if (world_->last_updating_)
	{
		prev_ = world_->last_updating_;
		next_ = world_->last_updating_->next_;
		if (auto* n = world_->last_updating_->next_) {n->prev_ = this;}
		world_->last_updating_->next_ = this;
	}
	else
	{
		if (auto* f = world_->first_) {f->prev_ = this;}
		next_ = world_->first_;
		world_->first_ = this;
	}
	
	if (time_handler_)
	{
		world_->last_updating_ = this;
	}
}

void World::Object_base::unlink()
{
	if (prev_) {prev_->next_ = next_;}
	if (next_) {next_->prev_ = prev_;}
	
	if (world_->first_ == this) {world_->first_ = next_;}
	if (world_->last_updating_ == this) {world_->last_updating_ = prev_;}
	if (world_->last_updating_previous_ == this) {world_->last_updating_previous_ = prev_;}
	
	world_ = nullptr;
}

auto World::attach(std::shared_ptr<Object_base> object) -> Object_base&
{
	auto& result = *object;
	result.shared_this_ = std::move(object);
	result.link(this);
	
	return result;
}

void World::attach(Rendering_object& object, const geometry::Bounding_box3D& box)
{
	rtree_rendering_.attach(&object, &object.handle_, box);
}

void World::attach_collision(Object_base& object,
	geometry::R_Tree3D<Object_base>::Handle& handle,
	const geometry::Bounding_box3D& box)
{
	rtree_collision_.attach(&object, &handle, box);
}

auto World::Object_base::own(std::shared_ptr<Object_base> object) -> Object_base&
{
	owned_objects_.push_back(std::move(object));
	
	return *owned_objects_.back();
}

auto World::Object_base::disown(Object_base* object) -> std::shared_ptr<Object_base>
{
	return std::move(*std::find_if(std::begin(owned_objects_), std::end(owned_objects_),
	[&](const std::shared_ptr<Object_base> owned_object) -> bool
	{
		return owned_object.get() == object;
	}));
}
} // namespace bttf
