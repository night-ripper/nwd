#pragma once

#include <cstddef>
#include <cstring>

#include <array>
#include <type_traits>

namespace nwd::serialization
{
template<typename Type>
requires(not std::is_reference_v<Type>)
extern const std::ptrdiff_t serialized_size;

/*
template<typename Type>
std::byte* serialize(std::byte* target, Type value);

template<typename Type>
Type deserialize(const std::byte* source);
*/

template<typename Type>
extern void(&private_serialize)(std::byte* target, const Type& value);

template<typename Type>
extern void(&private_deserialize)(const std::byte* source, Type& result);

template<typename Type>
requires(not std::is_reference_v<Type>)
std::byte* serialize(std::byte* target, const Type& result)
{
	private_serialize<Type>(target, result);
	return target + serialized_size<Type>;
}

template<typename Type>
Type& deserialize(const std::byte* source, Type& result)
{
	private_deserialize<Type>(source, result);
	return result;
}
template<typename Type>
Type&& deserialize(const std::byte* source, Type&& result)
requires(std::is_rvalue_reference_v<decltype(result)>)
{
	deserialize(source, result);
	return std::move(result);
}

template<typename Type>
Type deserialize(const std::byte* source)
{
	return std::move(deserialize(source, Type()));
}

#if 0
template<typename Type>
struct Serializable
{
};

template<typename Type>
requires(std::is_base_of_v<Serializable<Type>, Type>)
inline constexpr const std::ptrdiff_t serialized_size<Type> = sizeof(Type);

template<typename Type>
requires(std::is_base_of_v<Serializable<Type>, Type>)
inline std::byte* serialize(std::byte* target, Type value)
{
	std::memcpy(target, &value, sizeof(Type));
	return target + sizeof(Type);
}

template<typename Type>
requires(std::is_base_of_v<Serializable<Type>, Type>)
inline Type deserialize(const std::byte* source)
{
	Type result;
	std::memcpy(&result, source, sizeof(Type));
	return result;
}
#endif

/*
template<std::ptrdiff_t Size, typename Type>
inline std::array<std::byte, Size> serialize(Type value)
{
	static_assert(serialized_size<Type> <= Size);
	
	std::array<std::byte, Size> result {};
	serialize(result.data(), value);
	return result;
}

template<typename Type>
inline std::array<std::byte, serialized_size<Type>> serialize(Type value)
{
	return serialize<serialized_size<Type>>(value);
}
*/

template<typename Type>
inline void serialize_shift(std::byte*& target, Type value)
{
	private_serialize<Type>(target, value);
	target += serialized_size<Type>;
}

template<typename Type>
inline void deserialize_shift(const std::byte*& source, Type& result)
{
	private_deserialize<Type>(source, result);
	source += serialized_size<Type>;
}
} // namespace nwd::serialization
