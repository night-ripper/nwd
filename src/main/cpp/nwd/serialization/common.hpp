#pragma once

#include <cstdint>
#include <cstring>

#include <span>
#include <type_traits>

#include <nwd/serialization/fwd.hpp>
#include <nwd/utility/byte_array.hpp>

namespace nwd::serialization
{
template<std::ptrdiff_t Length> inline constexpr std::ptrdiff_t serialized_size<utility::Byte_array<Length>> = Length;

template<std::ptrdiff_t Length>
inline constexpr void serialize(std::byte* target, const utility::Byte_array<Length>& value)
{
	std::memcpy(target, value.data(), Length);
}

template<std::ptrdiff_t Length>
inline constexpr void deserialize(const std::byte* source, utility::Byte_array<Length>& result)
{
	result = utility::Byte_array<Length>(source);
}

template<typename Type>
inline constexpr void serialize(std::byte* target, std::span<const Type> values)
{
	for (const Type& value : values)
	{
		serialize_shift(target, value);
	}
};

template<typename Type>
inline constexpr void deserialize(const std::byte* source, std::span<Type> results)
{
	for (Type& result : results)
	{
		deserialize_shift(source, result);
	}
};
} // namespace nwd::serialization
