#pragma once

#include <cstdint>
#include <cstring>

#include <bit>
#include <type_traits>

#include <nwd/serialization/fwd.hpp>

namespace nwd::serialization
{
namespace fundamental
{
template<typename Type>
requires(std::is_arithmetic_v<Type>)
inline constexpr void serialize_arithmetic(std::byte* target, Type value)
{
	if constexpr (std::endian::native == std::endian::big)
	{
		value = std::byteswap(value);
	}
	
	std::memcpy(target, &value, sizeof(Type));
}

template<typename Type>
requires(std::is_floating_point_v<Type>)
constexpr void serialize_floating(std::byte* target, Type value)
{
	std::memcpy(target, &value, sizeof(Type));
}

template<typename Type>
requires(std::is_arithmetic_v<Type>)
constexpr void deserialize_arithmetic(const std::byte* source, Type& result)
{
	std::memcpy(&result, source, sizeof(Type));
	
	if constexpr (std::endian::native == std::endian::big)
	{
		result = std::byteswap(result);
	}
}

template<typename Type>
requires(std::is_floating_point_v<Type>)
constexpr void deserialize_floating(const std::byte* source, Type& result)
{
	std::memcpy(&result, source, sizeof(Type));
}
} // namespace fundamental

template<> inline constexpr std::ptrdiff_t serialized_size<bool> = sizeof(bool);

template<> inline constexpr std::ptrdiff_t serialized_size<std::int8_t> = sizeof(std::int8_t);
template<> inline constexpr std::ptrdiff_t serialized_size<std::uint8_t> = sizeof(std::uint8_t);

template<> inline constexpr std::ptrdiff_t serialized_size<std::int16_t> = sizeof(std::int16_t);
template<> inline constexpr std::ptrdiff_t serialized_size<std::uint16_t> = sizeof(std::uint16_t);

template<> inline constexpr std::ptrdiff_t serialized_size<std::int32_t> = sizeof(std::int32_t);
template<> inline constexpr std::ptrdiff_t serialized_size<std::uint32_t> = sizeof(std::uint32_t);

template<> inline constexpr std::ptrdiff_t serialized_size<std::int64_t> = sizeof(std::int64_t);
template<> inline constexpr std::ptrdiff_t serialized_size<std::uint64_t> = sizeof(std::uint64_t);

template<> inline constexpr std::ptrdiff_t serialized_size<float> = sizeof(float);
template<> inline constexpr std::ptrdiff_t serialized_size<double> = sizeof(double);
template<> inline constexpr std::ptrdiff_t serialized_size<long double> = sizeof(long double);

//

inline constexpr void serialize(std::byte* target, bool value){fundamental::serialize_arithmetic(target, value);};

inline constexpr void serialize(std::byte* target, std::int8_t value){fundamental::serialize_arithmetic(target, value);};
inline constexpr void serialize(std::byte* target, std::uint8_t value) {fundamental::serialize_arithmetic(target, value);};

inline constexpr void serialize(std::byte* target, std::int16_t value) {fundamental::serialize_arithmetic(target, value);};
inline constexpr void serialize(std::byte* target, std::uint16_t value) {fundamental::serialize_arithmetic(target, value);};

inline constexpr void serialize(std::byte* target, std::int32_t value) {fundamental::serialize_arithmetic(target, value);};
inline constexpr void serialize(std::byte* target, std::uint32_t value) {fundamental::serialize_arithmetic(target, value);};

inline constexpr void serialize(std::byte* target, std::int64_t value) {fundamental::serialize_arithmetic(target, value);};
inline constexpr void serialize(std::byte* target, std::uint64_t value) {fundamental::serialize_arithmetic(target, value);};

inline constexpr void serialize(std::byte* target, float value) {fundamental::serialize_floating(target, value);};
inline constexpr void serialize(std::byte* target, double value) {fundamental::serialize_floating(target, value);};
inline constexpr void serialize(std::byte* target, long double value) {fundamental::serialize_floating(target, value);};

//

inline constexpr void deserialize(const std::byte* source, bool& result) {fundamental::deserialize_arithmetic(source, result);};

inline constexpr void deserialize(const std::byte* source, std::int8_t& result) {fundamental::deserialize_arithmetic(source, result);};
inline constexpr void deserialize(const std::byte* source, std::uint8_t& result) {fundamental::deserialize_arithmetic(source, result);};

inline constexpr void deserialize(const std::byte* source, std::int16_t& result) {fundamental::deserialize_arithmetic(source, result);};
inline constexpr void deserialize(const std::byte* source, std::uint16_t& result) {fundamental::deserialize_arithmetic(source, result);};

inline constexpr void deserialize(const std::byte* source, std::int32_t& result) {fundamental::deserialize_arithmetic(source, result);};
inline constexpr void deserialize(const std::byte* source, std::uint32_t& result) {fundamental::deserialize_arithmetic(source, result);};

inline constexpr void deserialize(const std::byte* source, std::int64_t& result) {fundamental::deserialize_arithmetic(source, result);};
inline constexpr void deserialize(const std::byte* source, std::uint64_t& result) {fundamental::deserialize_arithmetic(source, result);};

inline constexpr void deserialize(const std::byte* source, float& result) {fundamental::deserialize_floating(source, result);};
inline constexpr void deserialize(const std::byte* source, double& result) {fundamental::deserialize_floating(source, result);};
inline constexpr void deserialize(const std::byte* source, long double& result) {fundamental::deserialize_floating(source, result);};
} // namespace nwd::serialization
