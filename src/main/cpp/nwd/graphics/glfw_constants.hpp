#pragma once

#include <nwd/graphics/glfw.hpp>
#include <nwd/utility/enum_base.hpp>

namespace nwd::graphics::glfw
{
namespace action
{
extern const Action press;
extern const Action release;
} // namespace action

namespace mouse
{
extern const Mouse_button button_1;
extern const Mouse_button button_2;
extern const Mouse_button button_3;
extern const Mouse_button button_4;
extern const Mouse_button button_5;
extern const Mouse_button button_6;
extern const Mouse_button button_7;
extern const Mouse_button button_8;
} // namespace mouse

namespace keys
{
extern const Keyboard_key space;
extern const Keyboard_key apostrophe;
extern const Keyboard_key comma;
extern const Keyboard_key minus;
extern const Keyboard_key period;
extern const Keyboard_key slash;
extern const Keyboard_key key_0;
extern const Keyboard_key key_1;
extern const Keyboard_key key_2;
extern const Keyboard_key key_3;
extern const Keyboard_key key_4;
extern const Keyboard_key key_5;
extern const Keyboard_key key_6;
extern const Keyboard_key key_7;
extern const Keyboard_key key_8;
extern const Keyboard_key key_9;
extern const Keyboard_key semicolon;
extern const Keyboard_key equal;
extern const Keyboard_key key_a;
extern const Keyboard_key key_b;
extern const Keyboard_key key_c;
extern const Keyboard_key key_d;
extern const Keyboard_key key_e;
extern const Keyboard_key key_f;
extern const Keyboard_key key_g;
extern const Keyboard_key key_h;
extern const Keyboard_key key_i;
extern const Keyboard_key key_j;
extern const Keyboard_key key_k;
extern const Keyboard_key key_l;
extern const Keyboard_key key_m;
extern const Keyboard_key key_n;
extern const Keyboard_key key_o;
extern const Keyboard_key key_p;
extern const Keyboard_key key_q;
extern const Keyboard_key key_r;
extern const Keyboard_key key_s;
extern const Keyboard_key key_t;
extern const Keyboard_key key_u;
extern const Keyboard_key key_v;
extern const Keyboard_key key_w;
extern const Keyboard_key key_x;
extern const Keyboard_key key_y;
extern const Keyboard_key key_z;
extern const Keyboard_key left_bracket;
extern const Keyboard_key backslash;
extern const Keyboard_key right_bracket;
extern const Keyboard_key grave_accent;
extern const Keyboard_key escape;
extern const Keyboard_key enter;
extern const Keyboard_key tab;
extern const Keyboard_key backspace;
extern const Keyboard_key insert;
extern const Keyboard_key key_delete;
extern const Keyboard_key right;
extern const Keyboard_key left;
extern const Keyboard_key down;
extern const Keyboard_key up;
extern const Keyboard_key page_up;
extern const Keyboard_key page_down;
extern const Keyboard_key home;
extern const Keyboard_key end;
extern const Keyboard_key caps_lock;
extern const Keyboard_key scroll_lock;
extern const Keyboard_key num_lock;
extern const Keyboard_key print_screen;
extern const Keyboard_key pause;
extern const Keyboard_key f1;
extern const Keyboard_key f2;
extern const Keyboard_key f3;
extern const Keyboard_key f4;
extern const Keyboard_key f5;
extern const Keyboard_key f6;
extern const Keyboard_key f7;
extern const Keyboard_key f8;
extern const Keyboard_key f9;
extern const Keyboard_key f10;
extern const Keyboard_key f11;
extern const Keyboard_key f12;
extern const Keyboard_key num_0;
extern const Keyboard_key num_1;
extern const Keyboard_key num_2;
extern const Keyboard_key num_3;
extern const Keyboard_key num_4;
extern const Keyboard_key num_5;
extern const Keyboard_key num_6;
extern const Keyboard_key num_7;
extern const Keyboard_key num_8;
extern const Keyboard_key num_9;
extern const Keyboard_key num_decimal;
extern const Keyboard_key num_divide;
extern const Keyboard_key num_multiply;
extern const Keyboard_key num_subtract;
extern const Keyboard_key num_add;
extern const Keyboard_key num_enter;
extern const Keyboard_key num_equal;
extern const Keyboard_key left_shift;
extern const Keyboard_key left_control;
extern const Keyboard_key left_alt;
extern const Keyboard_key left_super;
extern const Keyboard_key right_shift;
extern const Keyboard_key right_control;
extern const Keyboard_key right_alt;
extern const Keyboard_key right_super;
extern const Keyboard_key menu;
} // namespace keys
} // namespace nwd::graphics::glfw
