#pragma once

#include <cstddef>
#include <cstdint>

#include <iosfwd>
#include <vector>

namespace nwd::graphics
{
struct Wavefront_obj
{
	struct Vertex {float x, y, z;};
	struct Vertex_texture {float u, v;};
	struct Vertex_normal {float x, y, z;};
	struct Indices
	{
		std::int32_t v, vt, vn;
		bool face_end() const {return v == 0;}
	};
	
	std::vector<Vertex> vertices_;
	std::vector<Vertex_texture> vertex_textures_;
	std::vector<Vertex_normal> vertex_normals_;
	std::vector<Indices> indices_;
	std::ptrdiff_t num_vertices_ = 0;
	
	Wavefront_obj() = default;
	
	Wavefront_obj(std::istream& is)
	{
		read(is);
	}
	
	Wavefront_obj(std::istream&& is)
		:
		Wavefront_obj(is)
	{
	}
	
	void read(std::istream& is);
	
	void read(std::istream&& is)
	{
		return read(is);
	}
};
} // namespace nwd::graphics
