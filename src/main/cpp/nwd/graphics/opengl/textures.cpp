#include <cstring>

#include <algorithm>
#include <memory>
#include <stdexcept>

#include <epoxy/gl.h>

#include <nwd/graphics/opengl/textures.hpp>
#include <nwd/utility/integer_cast.hpp>

namespace nwd::graphics::opengl
{
struct Texture_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

const Texture::Format Texture::Format::rgb_u_3_3_2 {0, 3, 3, 2, 0, GL_R3_G3_B2, GL_RGB, GL_UNSIGNED_BYTE_3_3_2};
const Texture::Format Texture::Format::rgb_u_5_6_5 {0, 5, 6, 5, 0, GL_RGB565, GL_RGB, GL_UNSIGNED_SHORT_5_6_5};
const Texture::Format Texture::Format::rgba_u_4_4_4_4 {0, 4, 4, 4, 4, GL_RGBA4, GL_RGBA, GL_UNSIGNED_SHORT_4_4_4_4};
const Texture::Format Texture::Format::rgba_u_5_5_5_1 {0, 5, 5, 5, 1, GL_RGB5_A1, GL_RGBA, GL_UNSIGNED_SHORT_5_5_5_1};
const Texture::Format Texture::Format::rgba_u_8_8_8_8 {0, 8, 8, 8, 8, GL_RGBA8, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8};
const Texture::Format Texture::Format::rgba_u_10_10_10_2 {0, 10, 10, 10, 2, GL_RGB10_A2, GL_RGBA, GL_UNSIGNED_INT_10_10_10_2};

const Texture::Format Texture::Format::r_s_8 {1, 8, 0, 0, 0, GL_R8, GL_RED, GL_BYTE};
const Texture::Format Texture::Format::r_u_8 {0, 8, 0, 0, 0, GL_R8, GL_RED, GL_UNSIGNED_BYTE};
const Texture::Format Texture::Format::rg_s_8 {1, 8, 8, 0, 0, GL_RG8, GL_RG, GL_BYTE};
const Texture::Format Texture::Format::rg_u_8 {0, 8, 8, 0, 0, GL_RG8, GL_RG, GL_UNSIGNED_BYTE};
const Texture::Format Texture::Format::rgb_s_8 {1, 8, 8, 8, 0, GL_RGB8, GL_RGB, GL_BYTE};
const Texture::Format Texture::Format::rgb_u_8 {0, 8, 8, 8, 0, GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE};
const Texture::Format Texture::Format::rgba_s_8 {1, 8, 8, 8, 8, GL_RGBA8, GL_RGBA, GL_BYTE};
const Texture::Format Texture::Format::rgba_u_8 {0, 8, 8, 8, 8, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE};

const Texture::Format Texture::Format::r_s_16 {1, 16, 0, 0, 0, GL_R16, GL_RED, GL_SHORT};
const Texture::Format Texture::Format::r_u_16 {0, 16, 0, 0, 0, GL_R16, GL_RED, GL_UNSIGNED_SHORT};
const Texture::Format Texture::Format::rg_s_16 {1, 16, 16, 0, 0, GL_RG16, GL_RG, GL_SHORT};
const Texture::Format Texture::Format::rg_u_16 {0, 16, 16, 0, 0, GL_RG16, GL_RG, GL_UNSIGNED_SHORT};
const Texture::Format Texture::Format::rgb_s_16 {1, 16, 16, 16, 0, GL_RGB16, GL_RGB, GL_SHORT};
const Texture::Format Texture::Format::rgb_u_16 {0, 16, 16, 16, 0, GL_RGB16, GL_RGB, GL_UNSIGNED_SHORT};
const Texture::Format Texture::Format::rgba_s_16 {1, 16, 16, 16, 16, GL_RGBA16, GL_RGBA, GL_SHORT};
const Texture::Format Texture::Format::rgba_u_16 {0, 16, 16, 16, 16, GL_RGBA16, GL_RGBA, GL_UNSIGNED_SHORT};

const Texture::Format Texture::Format::r_s_32 {1, 32, 0, 0, 0, -1u, GL_RED, GL_INT};
const Texture::Format Texture::Format::r_u_32 {0, 32, 0, 0, 0, -1u, GL_RED, GL_UNSIGNED_INT};
const Texture::Format Texture::Format::rg_s_32 {1, 32, 32, 0, 0, -1u, GL_RG, GL_INT};
const Texture::Format Texture::Format::rg_u_32 {0, 32, 32, 0, 0, -1u, GL_RG, GL_UNSIGNED_INT};
const Texture::Format Texture::Format::rgb_s_32 {1, 32, 32, 32, 0, -1u, GL_RGB, GL_INT};
const Texture::Format Texture::Format::rgb_u_32 {0, 32, 32, 32, 0, -1u, GL_RGB, GL_UNSIGNED_INT};
const Texture::Format Texture::Format::rgba_s_32 {1, 32, 32, 32, 32, -1u, GL_RGBA, GL_INT};
const Texture::Format Texture::Format::rgba_u_32 {0, 32, 32, 32, 32, -1u, GL_RGBA, GL_UNSIGNED_INT};

const Texture::Format Texture::Format::r_f_16 {1, 16, 0, 0, 0, GL_R16F, GL_RED, GL_HALF_FLOAT};
const Texture::Format Texture::Format::rg_f_16 {1, 16, 16, 0, 0, GL_RG16F, GL_RG, GL_HALF_FLOAT};
const Texture::Format Texture::Format::rgb_f_16 {1, 16, 16, 16, 0, GL_RGB16F, GL_RGB, GL_HALF_FLOAT};
const Texture::Format Texture::Format::rgba_f_16 {1, 16, 16, 16, 16, GL_RGBA16F, GL_RGBA, GL_HALF_FLOAT};

const Texture::Format Texture::Format::r_f_32 {1, 32, 0, 0, 0, GL_R32F, GL_RED, GL_FLOAT};
const Texture::Format Texture::Format::rg_f_32 {1, 32, 32, 0, 0, GL_RG32F, GL_RG, GL_FLOAT};
const Texture::Format Texture::Format::rgb_f_32 {1, 32, 32, 32, 0, GL_RGB32F, GL_RGB, GL_FLOAT};
const Texture::Format Texture::Format::rgba_f_32 {1, 32, 32, 32, 32, GL_RGBA32F, GL_RGBA, GL_FLOAT};

const Texture::Format Texture::Format::depth_16 {0, 16, 0, 0, 0, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, -1u};
const Texture::Format Texture::Format::depth_24 {0, 24, 0, 0, 0, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, -1u};
const Texture::Format Texture::Format::depth_32 {0, 32, 0, 0, 0, GL_DEPTH_COMPONENT32, GL_DEPTH_COMPONENT, -1u};
const Texture::Format Texture::Format::depth_24_stencil_8 {0, 24, 8, 0, 0, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, -1u};
const Texture::Format Texture::Format::stencil_8 {0, 8, 0, 0, 0, GL_STENCIL_INDEX8, GL_STENCIL_INDEX, -1u};
const Texture::Format Texture::Format::stencil_16 {0, 16, 0, 0, 0, GL_STENCIL_INDEX16, GL_STENCIL_INDEX, -1u};

////////////////////////////////////////////////////////////////////////////////

const Texture::Parameter Texture::Wrapping::repeat = [](Texture& texture)
{
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_T, GL_REPEAT);
};

const Texture::Parameter Texture::Wrapping::repeat_mirror = [](Texture& texture)
{
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
};

const Texture::Parameter Texture::Wrapping::clamp_to_edge = [](Texture& texture)
{
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
};

const Texture::Parameter Texture::Wrapping::clamp_to_edge_mirror = [](Texture& texture)
{
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_S, GL_MIRROR_CLAMP_TO_EDGE);
	glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_T, GL_MIRROR_CLAMP_TO_EDGE);
};

const Texture::Parameter Texture::Wrapping::clamp_to_border_black = Texture::Wrapping::clamp_to_border(0, 0, 0, 1);

const Texture::Parameter Texture::Wrapping::clamp_to_border_transparent = Texture::Wrapping::clamp_to_border(0, 0, 0, 0);

Texture::Parameter Texture::Wrapping::clamp_to_border(float r, float g, float b, float a) noexcept
{
	const float color[] = {r, g, b, a};
	
	return [color](Texture& texture)
	{
		glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTextureParameteri(texture.get(), GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		glTextureParameterfv(texture.get(), GL_TEXTURE_BORDER_COLOR, color);
	};
}

const Texture::Parameter Texture::Minifying::nearest = [](Texture& texture) -> void
{
	glTextureParameteri(texture.get(), GL_TEXTURE_MIN_FILTER, GL_NEAREST);
};

const Texture::Parameter Texture::Minifying::linear = [](Texture& texture) -> void
{
	glTextureParameteri(texture.get(), GL_TEXTURE_MIN_FILTER, GL_LINEAR);
};

const Texture::Parameter Texture::Magnifying::nearest = [](Texture& texture) -> void
{
	glTextureParameteri(texture.get(), GL_TEXTURE_MAG_FILTER, GL_NEAREST);
};

const Texture::Parameter Texture::Magnifying::linear = [](Texture& texture) -> void
{
	glTextureParameteri(texture.get(), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
};

void deleters::texture(std::uint32_t object) noexcept
{
	glDeleteTextures(1, &object);
}

Texture::Texture(std::uint32_t target, std::int32_t levels)
	:
	levels_(levels)
{
	std::uint32_t object;
	glCreateTextures(target, 1, &object);
	this->reset(object);
}

void Texture::bind(std::int32_t binding)
{
	glBindTextureUnit(binding, this->get());
}

Texture_2D::Texture_2D(std::int32_t width, std::int32_t height, std::int32_t levels)
	:
	Texture(GL_TEXTURE_2D, levels)
{
	width_ = width;
	height_ = height;
}

void Texture_2D::storage(Format internal_format)
{
	glTextureStorage2D(this->get(), levels_, internal_format.internal, width_, height_);
}

void Texture_2D::upload(Format external_format, const void* data)
{
	glTextureSubImage2D(this->get(), 0, 0, 0, width_, height_,
		external_format.external, external_format.type, data
	);
	
	if (levels_ > 1)
	{
		glGenerateTextureMipmap(this->get());
	}
}

Texture_2D_multisample::Texture_2D_multisample(
	std::int32_t width, std::int32_t height, std::int32_t samples, bool fixed_sample_locations)
	:
	Texture(GL_TEXTURE_2D_MULTISAMPLE, samples),
	fixed_sample_locations_(fixed_sample_locations)
{
	width_ = width;
	height_ = height;
}

void Texture_2D_multisample::storage(Format internal_format)
{
	glTextureStorage2DMultisample(this->get(), levels_,
		internal_format.internal, width_, height_, fixed_sample_locations_
	);
}

Texture_2D_array::Texture_2D_array(std::int32_t width, std::int32_t height, std::int32_t levels)
	:
	Texture(GL_TEXTURE_2D_ARRAY, levels)
{
	width_ = width;
	height_ = height;
}

void Texture_2D_array::storage(Format internal_format, std::int32_t layers)
{
	glTextureStorage3D(this->get(), levels_, internal_format.internal,
		width_, height_, layers
	);
	
	layers_ = layers;
}

void Texture_2D_array::upload(std::span<const Image_rgba_u_8> span, std::int32_t layer_begin)
{
	const std::ptrdiff_t image_size = width_ * height_ * 4;
	auto data = std::make_unique<unsigned char[]>(utility::checked_cast(std::ssize(span) * image_size));
	auto begin = data.get();
	
	for (const auto& image : span)
	{
		if (width_ != image.width())
		{
			throw Texture_error(std::string("Texture_2D_array::upload: Different image width, expected ")
				+ std::to_string(width_) + " but was " + std::to_string(image.width())
			);
		}
		
		if (height_ != image.height())
		{
			throw Texture_error(std::string("Texture_2D_array::upload: Different image height, expected ")
				+ std::to_string(height_) + " but was " + std::to_string(image.height())
			);
		}
		
		std::memcpy(begin, image.begin(), utility::checked_cast(image_size));
		begin += image_size;
	}
	
	upload(layer_begin, utility::checked_cast(std::ssize(span)), Format::rgba_u_8, data.get());
}

void Texture_2D_array::upload(std::int32_t layer_begin, std::int32_t amount,
	Format external_format, const void* data)
{
	if (layer_begin + amount > layers_)
	{
		throw Texture_error(std::string("Texture_2D_array::upload: Number of layers exceeds array capacity"));
	}
	
	glTextureSubImage3D(this->get(), 0,
		0, 0, layer_begin,
		width_, height_, amount,
		external_format.external, external_format.type, data
	);
	
	if (levels_ > 1)
	{
		glGenerateTextureMipmap(this->get());
	}
}

////////////////////////////////////////////////////////////////////////////////

void deleters::renderbuffer(std::uint32_t object) noexcept
{
	glDeleteRenderbuffers(1, &object);
}

Renderbuffer::Renderbuffer(std::int32_t width, std::int32_t height, std::int32_t samples)
	:
	width_(width),
	height_(height),
	samples_(samples)
{
	std::uint32_t object;
	glCreateRenderbuffers(1, &object);
	this->reset(object);
}

void Renderbuffer::storage(Texture::Format internal_format)
{
	glNamedRenderbufferStorageMultisample(this->get(),
		samples_, internal_format.internal, width_, height_
	);
}

////////////////////////////////////////////////////////////////////////////////

const Framebuffer::Attachment Framebuffer::Attachment::depth {GL_DEPTH_ATTACHMENT};
const Framebuffer::Attachment Framebuffer::Attachment::stencil {GL_STENCIL_ATTACHMENT};
const Framebuffer::Attachment Framebuffer::Attachment::depth_stencil {GL_DEPTH_STENCIL_ATTACHMENT};
const Framebuffer::Attachment Framebuffer::Attachment::color {GL_COLOR_ATTACHMENT0};

const Framebuffer::Target Framebuffer::Target::draw {GL_DRAW_FRAMEBUFFER};
const Framebuffer::Target Framebuffer::Target::read {GL_READ_FRAMEBUFFER};
const Framebuffer::Target Framebuffer::Target::read_draw {GL_FRAMEBUFFER};

const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::none {GL_NONE};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::front_left {GL_FRONT_LEFT};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::front_right {GL_FRONT_RIGHT};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::back_left {GL_BACK_LEFT};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::back_right {GL_BACK_RIGHT};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::front {GL_FRONT};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::back {GL_BACK};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::left {GL_LEFT};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::right {GL_RIGHT};
const Framebuffer::Draw_buffer Framebuffer::Draw_buffer::front_back {GL_FRONT_AND_BACK};

void deleters::framebuffer(std::uint32_t object) noexcept
{
	glDeleteFramebuffers(1, &object);
}

Framebuffer Framebuffer::create()
{
	std::uint32_t object;
	glCreateFramebuffers(1, &object);
	return Framebuffer(object);
}

void Framebuffer::attach(const Texture& texture, Attachment attachment) const
{
	glNamedFramebufferTexture(this->get(), attachment, texture.get(), 0);
}

void Framebuffer::attach(const Renderbuffer& renderbuffer, Attachment attachment) const
{
	glNamedFramebufferRenderbuffer(this->get(), attachment, GL_RENDERBUFFER, renderbuffer.get());
}

void Framebuffer::draw_buffer(Draw_buffer draw_buffer)
{
	draw_buffer_ = draw_buffer;
	glNamedFramebufferDrawBuffer(this->get(), draw_buffer_);
}

void deleters::bound_framebuffer(utility::Enum_base object) noexcept
{
	glBindFramebuffer(object, 0);
}

Framebuffer::Bound::Bound(const Framebuffer& framebuffer, Target target)
	:
	Unique_resource(target)
{
	glBindFramebuffer(target, framebuffer.get());
}

auto Framebuffer::bind(Target target) const -> Bound
{
	return Bound(*this, target);
}

void deleters::resident_handle(std::uint64_t object) noexcept
{
	glMakeTextureHandleNonResidentARB(object);
}

Texture_handle::Resident::Resident(const Texture_handle& handle)
	:
	Unique_resource(handle.value_)
{
	glMakeTextureHandleResidentARB(get());
}

Texture_handle::Texture_handle(const Texture& texture)
	:
	value_(glGetTextureHandleARB(texture.get()))
{
}

Texture_handle::Texture_handle(const Resident& resident)
	:
	value_(resident.get())
{
}
} // namespace nwd::graphics::opengl
