#pragma once

#include <cstdint>

#include <initializer_list>
#include <iosfwd>
#include <span>
#include <string_view>

#include <nwd/utility/functor.hpp>
#include <nwd/utility/unique_resource.hpp>

namespace nwd::graphics::opengl
{
namespace deleters
{
void shader_base(std::uint32_t object) noexcept;
void program(std::uint32_t object) noexcept;
} // namespace deleters

struct Program;

struct Shader_base : protected utility::Unique_resource<std::uint32_t, utility::Functor<deleters::shader_base>>
{
	Shader_base() = default;
	Shader_base(Shader_base&&) noexcept = default;
	Shader_base& operator=(Shader_base&&) noexcept = default;
	
protected:
	friend Program;
	
	void compile(std::span<std::string_view>);
	void compile(std::initializer_list<std::istream*>);
	void compile(std::istream& is, auto&&... iss) {compile({&is, &iss...});}
	void compile(std::istream&& is, auto&&... iss) {compile(is, iss...);}
};

template<typename>
struct Shader : Shader_base
{
protected:
	Shader() = default;
	Shader(Shader&&) noexcept = default;
	Shader& operator=(Shader&&) noexcept = default;
	
	static Shader create();
	
public:
	Shader(std::string_view view, auto... views) : Shader(create()) {compile({view, views...});}
	Shader(std::istream& is, auto&&... iss) : Shader(create()) {compile(is, iss...);}
	Shader(std::istream&& is, auto&&... iss) : Shader(is, iss...) {}
};

struct Vertex_shader : Shader<Vertex_shader>
{
	using Shader::Shader;
};

struct Geometry_shader : Shader<Geometry_shader>
{
	using Shader::Shader;
};

struct Fragment_shader : Shader<Fragment_shader>
{
	using Shader::Shader;
};

struct Program : protected utility::Unique_resource<std::uint32_t, utility::Functor<deleters::program>>
{
	Program() noexcept = default;
	Program(Program&&) noexcept = default;
	Program& operator=(Program&&) noexcept = default;
	
	static Program create();
	
	Program(std::span<const Shader_base* const> span);
	
	Program(const Shader_base& shader, const auto&... shaders)
		:
		Program(std::span<const Shader_base* const>(std::initializer_list<const Shader_base*>({&shader, &shaders...})))
	{
	}
	
	void use() const;
	
	void uniform(std::int32_t location, auto... values) = delete;
	
	template<int X, int Y, typename Value_type>
	void uniform_matrix(std::int32_t location, const Value_type* matrices,
		std::int32_t count = 1, bool transpose = false)
	= delete;
	
	void uniform_handle(std::int32_t location, std::uint64_t value);
};

template<> void Program::uniform(std::int32_t, float);
template<> void Program::uniform(std::int32_t, float, float);
template<> void Program::uniform(std::int32_t, float, float, float);
template<> void Program::uniform(std::int32_t, float, float, float, float);

template<> void Program::uniform(std::int32_t, std::int32_t);
template<> void Program::uniform(std::int32_t, std::int32_t, std::int32_t);
template<> void Program::uniform(std::int32_t, std::int32_t, std::int32_t, std::int32_t);
template<> void Program::uniform(std::int32_t, std::int32_t, std::int32_t, std::int32_t, std::int32_t);

template<> void Program::uniform(std::int32_t, std::uint32_t);
template<> void Program::uniform(std::int32_t, std::uint32_t, std::uint32_t);
template<> void Program::uniform(std::int32_t, std::uint32_t, std::uint32_t, std::uint32_t);
template<> void Program::uniform(std::int32_t, std::uint32_t, std::uint32_t, std::uint32_t, std::uint32_t);

template<> void Program::uniform_matrix<2, 2>(std::int32_t, const float*, std::int32_t, bool);
template<> void Program::uniform_matrix<2, 3>(std::int32_t, const float*, std::int32_t, bool);
template<> void Program::uniform_matrix<2, 4>(std::int32_t, const float*, std::int32_t, bool);

template<> void Program::uniform_matrix<3, 2>(std::int32_t, const float*, std::int32_t, bool);
template<> void Program::uniform_matrix<3, 3>(std::int32_t, const float*, std::int32_t, bool);
template<> void Program::uniform_matrix<3, 4>(std::int32_t, const float*, std::int32_t, bool);

template<> void Program::uniform_matrix<4, 2>(std::int32_t, const float*, std::int32_t, bool);
template<> void Program::uniform_matrix<4, 3>(std::int32_t, const float*, std::int32_t, bool);
template<> void Program::uniform_matrix<4, 4>(std::int32_t, const float*, std::int32_t, bool);
} // namespace nwd::graphics::opengl
