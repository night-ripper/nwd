#pragma once

#include <cstdint>

#include <span>

#include <nwd/graphics/opengl/type_enum.hpp>

#include <nwd/utility/functor.hpp>
#include <nwd/utility/unique_resource.hpp>

namespace nwd::graphics::opengl
{
namespace deleters
{
void buffer(std::uint32_t object) noexcept;
void vertex_array(std::uint32_t object) noexcept;
} // namespace deleters

struct Buffer : public utility::Unique_resource<std::uint32_t, utility::Functor<deleters::buffer>>
{
	Buffer() noexcept = default;
	Buffer(Buffer&&) noexcept = default;
	Buffer& operator=(Buffer&&) noexcept = default;
	
	static Buffer create();
	
	void data(std::span<const std::byte> data);
};

struct Vertex_array : public utility::Unique_resource<std::uint32_t, utility::Functor<deleters::vertex_array>>
{
	Vertex_array() noexcept = default;
	Vertex_array(Vertex_array&&) noexcept = default;
	Vertex_array& operator=(Vertex_array&&) noexcept = default;
	
	static Vertex_array create();
	
	void format_bind_data_buffer(std::uint32_t attribute_index, std::int32_t size,
		std::uint32_t type, bool normalize, std::uint32_t relative_offset,
		const Buffer& buffer, std::intptr_t offset, std::int32_t stride) const
	;
	void format_i_bind_data_buffer(std::uint32_t attribute_index, std::int32_t size,
		std::uint32_t type, std::uint32_t relative_offset,
		const Buffer& buffer, std::intptr_t offset, std::int32_t stride) const
	;
	void format_l_bind_data_buffer(std::uint32_t attribute_index, std::int32_t size,
		std::uint32_t type, std::uint32_t relative_offset,
		const Buffer& buffer, std::intptr_t offset, std::int32_t stride) const
	;
	
	void bind_element_buffer(const Buffer& buffer) const;
	
	void bind() const;
	
private:
	void bind_data_buffers(std::uint32_t first, std::int32_t count,
		const std::uint32_t* buffers, const std::intptr_t* offsets, const std::int32_t* strides) const
	;
};

struct Shader_storage : public utility::Unique_resource<std::uint32_t, utility::Functor<deleters::buffer>>
{
	Shader_storage() noexcept = default;
	Shader_storage(Shader_storage&&) noexcept = default;
	Shader_storage& operator=(Shader_storage&&) noexcept = default;
	
	explicit Shader_storage(std::uint32_t binding);
	
	void bind() const;
	
protected:
	std::uint32_t binding_ = std::uint32_t(-1);
};
} // namespace nwd::graphics::opengl
