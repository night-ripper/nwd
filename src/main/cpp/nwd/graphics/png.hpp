#pragma once

#include <cstdint>

#include <iosfwd>
#include <memory>

#include <nwd/graphics/image.hpp>

namespace nwd::graphics::png
{
std::byte* read_bottom_up(std::streambuf&& streambuf, std::byte* result);
std::byte* read_top_down(std::streambuf&& streambuf, std::byte* result);

Image_rgba_u_8 read_bottom_up(std::streambuf&& streambuf);
Image_rgba_u_8 read_top_down(std::streambuf&& streambuf);
} // namespace nwd::graphics::png
