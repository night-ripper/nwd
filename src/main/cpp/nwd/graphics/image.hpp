#pragma once

#include <cstddef>
#include <cstdint>

#include <bit>
#include <memory>
#include <vector>
#include <span>
#include <memory_resource>

namespace nwd::graphics
{
struct Image_rgba_u_8
{
	Image_rgba_u_8() = default;
	
	Image_rgba_u_8(std::int32_t width, std::int32_t height) noexcept
		:
		width_(width),
		height_(height)
	{
	}
	
	std::ptrdiff_t width() const {return width_;}
	std::ptrdiff_t height() const {return height_;}
	
	std::ptrdiff_t size_bytes() const {return width() * height() * 4;}
	
	std::shared_ptr<std::byte[]>& data() & {return data_;}
	const std::shared_ptr<std::byte[]>& data() const& {return data_;}
	std::shared_ptr<std::byte[]> data() && {return std::move(data_);}
	
	std::byte* begin() {return data_.get();}
	const std::byte* begin() const {return data_.get();}
	
	std::byte* end() {return begin() + size_bytes();}
	const std::byte* end() const {return begin() + size_bytes();}
	
private:
	std::int32_t width_ = 0;
	std::int32_t height_ = 0;
	std::shared_ptr<std::byte[]> data_;
};
} // namespace nwd::graphics
