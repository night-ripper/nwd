#pragma once

#include <cstddef>
#include <cstdint>

#include <bit>
#include <filesystem>
#include <memory>
#include <string_view>

#include <nwd/utility/functor.hpp>
#include <nwd/graphics/image.hpp>

namespace nwd::graphics
{
namespace deleters
{
void freetype(void* object) noexcept;
void freetype_face(void* object) noexcept;
} // namespace deleters

struct Freetype : protected std::unique_ptr<void, utility::Functor<deleters::freetype>>
{
	using std::unique_ptr<void, utility::Functor<deleters::freetype>>::unique_ptr;
	
	static Freetype create();
	
	struct Face;
	
	struct Character_info
	{
		friend Face;
		
		std::ptrdiff_t width() const noexcept {return width_;}
		std::ptrdiff_t height() const noexcept {return height_;}
		std::ptrdiff_t bearing_x() const noexcept {return bearing_x_;}
		std::ptrdiff_t bearing_y() const noexcept {return bearing_y_;}
		std::ptrdiff_t advance_x() const noexcept {return advance_x_;}
		
	private:
		std::int16_t width_;
		std::int16_t height_;
		std::int16_t bearing_x_;
		std::int16_t bearing_y_;
		std::int16_t advance_x_;
	};
	
	struct Face : protected std::unique_ptr<void, utility::Functor<deleters::freetype_face>>
	{
	protected:
		friend Freetype;
		
		using std::unique_ptr<void, utility::Functor<deleters::freetype_face>>::unique_ptr;
		
	public:
		void set_pixel_sizes(std::int32_t width, std::int32_t height);
		
		Character_info load(char32_t character);
		const std::byte* data() const;
	};
	
	Face load(const char* filepath) const;
};
} // namespace nwd::graphics
