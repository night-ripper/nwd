#include <cstring>

#include <stdexcept>
#include <iostream>
#include <functional>

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#include <nwd/graphics/freetype.hpp>
#include <nwd/utility/integer_cast.hpp>

namespace nwd::graphics
{
struct Freetype_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

void deleters::freetype(void* object) noexcept
{
	if (FT_Done_FreeType(static_cast<FT_Library>(object)))
	{
		std::clog << "Freetype: Could not destroy library" << "\n";
	}
}

void deleters::freetype_face(void* object) noexcept
{
	if (FT_Done_Face(static_cast<FT_Face>(object)))
	{
		std::clog << "Freetype: Error during destruction of Face object" << "\n";
	}
}

Freetype Freetype::create()
{
	FT_Library library;
	
	if (FT_Init_FreeType(&library))
	{
		throw Freetype_error("Could not initialize library");
	}
	
	return Freetype(library);
}

auto Freetype::load(const char* filepath) const -> Face
{
	FT_Face result;
	
	if (FT_New_Face(static_cast<FT_Library>(get()), filepath, 0, &result))
	{
		throw Freetype_error("Failed to load font");
	}
	
	FT_Select_Charmap(result, FT_ENCODING_UNICODE);
	
	if (result->charmap->encoding != FT_ENCODING_UNICODE)
	{
		throw Freetype_error("Font does not contain unicode character map");
	}
	
	return Face(result);
}

void Freetype::Face::set_pixel_sizes(std::int32_t width, std::int32_t height)
{
	if (FT_Set_Pixel_Sizes(static_cast<FT_Face>(get()), std::uint32_t(width), std::uint32_t(height)))
	{
		throw Freetype_error("Failed to set pixel sizes");
	}
}

auto Freetype::Face::load(char32_t character) -> Character_info
{
	auto face = static_cast<FT_Face>(get());
	
	if (FT_Load_Char(face, character, FT_LOAD_RENDER))
	{
		throw Freetype_error("Failed to load glyph");
	}
	
	Character_info result {};
	
	result.width_ = utility::checked_cast(face->glyph->bitmap.width);
	result.height_ = utility::checked_cast(face->glyph->bitmap.rows);
	result.bearing_x_ = utility::checked_cast(face->glyph->bitmap_left);
	result.bearing_y_ = utility::checked_cast(face->glyph->bitmap_top);
	result.advance_x_ = utility::checked_cast(face->glyph->advance.x / 64);
	
	return result;
}

const std::byte* Freetype::Face::data() const
{
	return reinterpret_cast<const std::byte*>(static_cast<FT_Face>(get())->glyph->bitmap.buffer);
}
} // namespace nwd::graphics

#if 0
int main()
{
	using namespace nwd::graphics;
	
	auto ft = Freetype::create();
 	auto fc = ft.load("/usr/share/fonts/dejavu-sans-fonts/DejaVuSansCondensed-Bold.ttf");
	fc.set_pixel_sizes(24, 24);
// 	auto info = fc.load(U'₴');
	auto info = fc.load(U'я');
	
	std::cout << info.height() << "\n";
	std::cout << info.width() << "\n";
	std::cout << info.bearing_y() << "\n";
	
	for (int i = 0; i != info.height(); ++i)
	{
		for (int j = 0; j != info.width(); ++j)
		{
			auto val = std::to_integer<int>(fc.data()[i * info.width() + j]);
			if (val > 150)
			{
				std::cout << '#';
			}
			else if (val > 0)
			{
				std::cout << '*';
			}
			else
			{
				std::cout << ' ';
			}
		}
		std::cout << "\n";
	}
	
	std::cout << sizeof(std::move_only_function<void()>) << "\n";
}
#endif
