#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstring>

#include <algorithm>
#include <iostream>
#include <sstream>

#include <nwd/graphics/wavefront_obj.hpp>

namespace nwd::graphics
{
void Wavefront_obj::read(std::istream& is)
{
	auto line = std::string();
	
	while (is)
	{
		std::getline(is, line);
		auto ss = std::stringstream(std::move(line));
		const auto view = ss.view();
		
		if (false);
		else if (view.starts_with("v "))
		{
			ss.ignore(2);
			
			Vertex value;
			
			ss >> value.x;
			ss >> value.y;
			ss >> value.z;
			
			vertices_.push_back(value);
		}
		else if (view.starts_with("vt "))
		{
			ss.ignore(3);
			
			Vertex_texture value;
			
			ss >> value.u;
			value.u = std::clamp(value.u, 0.f, 1.f);
			ss >> value.v;
			value.v = std::clamp(value.v, 0.f, 1.f);
			
			vertex_textures_.push_back(value);
		}
		else if (view.starts_with("vn "))
		{
			ss.ignore(3);
			
			Vertex_normal value;
			
			ss >> value.x;
			ss >> value.y;
			ss >> value.z;
			
			double inverse = 1. / std::hypot(value.x, value.y, value.z);
			value.x = static_cast<float>(value.x * inverse);
			value.y = static_cast<float>(value.y * inverse);
			value.z = static_cast<float>(value.z * inverse);
			
			vertex_normals_.push_back(value);
		}
		else if (view.starts_with("f "))
		{
			ss.ignore(2);
			std::ptrdiff_t vertices_in_face = 0;
			
			while (not ss.eof())
			{
				Indices single_indices;
				
				ss >> single_indices.v;
				ss.get();
				ss >> single_indices.vt;
				ss.get();
				ss >> single_indices.vn;
				ss >> std::ws;
				
				indices_.push_back(single_indices);
				++vertices_in_face;
			}
			
			// This is purely a fix for Blender's ordering of vertices in faces
			if (vertices_in_face == 4)
			{
				std::swap(*(std::end(indices_) - 2), *(std::end(indices_) - 1));
			}
			
			num_vertices_ += vertices_in_face;
			indices_.push_back({0, 0, 0});
		}
	}
}
} // namespace nwd::graphics
