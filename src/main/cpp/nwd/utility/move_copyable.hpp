#pragma once

#include <utility>

namespace nwd::utility
{
template<typename Type>
struct Move_copyable : Type
{
	using Type::Type;
	
	constexpr Move_copyable(Type&& other) noexcept
		:
		Type(static_cast<Type&&>(other))
	{
	}
	
	constexpr Move_copyable& operator=(Type&& other) noexcept
	{
		static_cast<Type&>(*this) = static_cast<Type&&>(other);
		return *this;
	}
	
	constexpr Move_copyable(Move_copyable&& other) noexcept = default;
	
	constexpr Move_copyable& operator=(Move_copyable&& other) noexcept = default;
	
	constexpr Move_copyable(Move_copyable& other) noexcept
		:
		Move_copyable(std::move(other))
	{
	}
	
	constexpr Move_copyable& operator=(Move_copyable& other) noexcept
	{
		return *this = std::move(other);
	}
};
} // namespace nwd::utility
