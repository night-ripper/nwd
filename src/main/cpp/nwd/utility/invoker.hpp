#pragma once

#include <type_traits>
#include <utility>

namespace nwd::utility
{
namespace privates
{
template<bool Noexcept, bool Const, typename Result_type, typename... Args>
struct Invoker_impl
{
private:
	template<typename Functor>
	constexpr static const Functor& typed(const void* functor) noexcept
	{
		return *static_cast<const Functor*>(functor);
	}
	
	template<typename Arg_result_type, typename... Arg_args>
	constexpr static bool is_maybe_nothrow_invocable_r_v = Noexcept ?
		std::is_nothrow_invocable_r_v<Arg_result_type, Arg_args...> :
		std::is_invocable_r_v<Arg_result_type, Arg_args...>
	;
	
public:
	using result_type = Result_type;
	
	constexpr Invoker_impl() noexcept = default;
	
	template<typename Type>
	constexpr Invoker_impl(const Type* object, result_type(*function)(const Type*, Args...) noexcept(Noexcept)) noexcept
		:
		functor_(object),
		functor_operator_(reinterpret_cast<result_type(*)(const void*, Args...) noexcept(Noexcept)>(function))
	{
	}
	
	template<typename Type>
	constexpr Invoker_impl(Type* object, result_type(*function)(Type*, Args...) noexcept(Noexcept)) noexcept
	requires(not Const)
		:
		Invoker_impl(object, reinterpret_cast<result_type(*)(const Type*, Args...) noexcept(Noexcept)>(function))
	{
	}
	
	//! Version for functors with `operator()(Args...) const`
	template<typename Functor>
	constexpr Invoker_impl(const Functor& functor) noexcept
	requires(is_maybe_nothrow_invocable_r_v<result_type, std::add_const_t<Functor>, Args...>)
		:
		functor_(&functor),
		functor_operator_([](const void* captured_functor, Args... args) noexcept(Noexcept) -> result_type
		{
			return typed<Functor>(captured_functor)(std::forward<Args>(args)...);
		})
	{
	}
	
	//! Version for functors with `operator()(Args...) mutable`
	//! In this case we disallow passing the functor by const reference
	template<typename Functor>
	constexpr Invoker_impl(Functor&& functor) noexcept
	requires(not Const and is_maybe_nothrow_invocable_r_v<result_type, Functor, Args...>
		and not is_maybe_nothrow_invocable_r_v<result_type, std::add_const_t<std::remove_reference_t<Functor>>, Args...>
		and not std::is_const_v<std::remove_reference_t<Functor>>)
		:
		functor_(&functor),
		functor_operator_([](const void* captured_functor, Args... args) noexcept(Noexcept) -> result_type
		{
			return const_cast<Functor&>(typed<std::remove_reference_t<Functor>>(captured_functor))(std::forward<Args>(args)...);
		})
	{
	}
	
	constexpr result_type operator()(Args... args) noexcept(Noexcept)
	requires(not Const)
	{
		return functor_operator_(functor_, std::forward<Args>(args)...);
	}
	
	constexpr result_type operator()(Args... args) const noexcept(Noexcept)
	requires(Const)
	{
		return functor_operator_(functor_, std::forward<Args>(args)...);
	}
	
	constexpr explicit operator bool() const noexcept
	{
		return functor_operator_;
	}
	
protected:
	const void* functor_ = nullptr;
	result_type(*functor_operator_)(const void*, Args...) noexcept(Noexcept) = nullptr;
};
} // namespace privates
////////////////////////////////////////////////////////////////////////////////

//! A simple class that wraps a pointer to data along with a function pointer
//! (accepting the data pointer as the first argument). It is invocable and
//! constructable from any lambda expression or other invocable object.
//! 
//! The class is non-owning so the function object must outlive the Invoker
//! object. This class does not do any memory allocations.
template<typename Signature>
struct Invoker;

template<typename Result_type, typename... Args>
struct Invoker<Result_type(Args...) const noexcept> : privates::Invoker_impl<true, true, Result_type, Args...>
{
	using privates::Invoker_impl<true, true, Result_type, Args...>::Invoker_impl;
};

template<typename Result_type, typename... Args>
struct Invoker<Result_type(Args...) const> : privates::Invoker_impl<false, true, Result_type, Args...>
{
	using privates::Invoker_impl<false, true, Result_type, Args...>::Invoker_impl;
	
	constexpr Invoker(Invoker<Result_type(Args...) const noexcept> other) noexcept
		:
		privates::Invoker_impl<false, true, Result_type, Args...>(other.functor_, other.functor_operator_)
	{
	}
};

template<typename Result_type, typename... Args>
struct Invoker<Result_type(Args...) noexcept> : privates::Invoker_impl<true, false, Result_type, Args...>
{
	using privates::Invoker_impl<true, false, Result_type, Args...>::Invoker_impl;
	
	constexpr Invoker(Invoker<Result_type(Args...) const noexcept> other) noexcept
		:
		privates::Invoker_impl<true, false, Result_type, Args...>(other.functor_, other.functor_operator_)
	{
	}
};

template<typename Result_type, typename... Args>
struct Invoker<Result_type(Args...)> : privates::Invoker_impl<false, false, Result_type, Args...>
{
	using privates::Invoker_impl<false, false, Result_type, Args...>::Invoker_impl;
	
	constexpr Invoker(Invoker<Result_type(Args...) const noexcept> other) noexcept
		:
		privates::Invoker_impl<false, false, Result_type, Args...>(other.functor_, other.functor_operator_)
	{
	}
	
	constexpr Invoker(Invoker<Result_type(Args...) noexcept> other) noexcept
		:
		privates::Invoker_impl<false, false, Result_type, Args...>(other.functor_, other.functor_operator_)
	{
	}
	
	constexpr Invoker(Invoker<Result_type(Args...) const> other) noexcept
		:
		privates::Invoker_impl<false, false, Result_type, Args...>(other.functor_, other.functor_operator_)
	{
	}
};

template<typename Functor, typename Signature = decltype(&Functor::operator())>
Invoker(Functor&&) -> Invoker<Signature>;
} // namespace nwd::utility
