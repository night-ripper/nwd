#pragma once

#include <iterator>
#include <functional>
#include <utility>

namespace nwd::utility
{
template<typename Iterator, typename Compare = std::less<>>
constexpr Iterator shift_left(Iterator begin, Iterator entry, Compare compare = Compare()) noexcept
{
	while (begin != entry)
	{
		using std::swap;
		auto prev = std::prev(entry);
		
		if (compare(*prev, *entry))
		{
			break;
		}
		
		swap(*prev, *entry);
		entry = prev;
	}
	
	return entry;
}

template<typename Iterator, typename Compare = std::less<>>
constexpr Iterator shift_right(Iterator entry, Iterator end, Compare compare = Compare()) noexcept
{
	if (entry != end)
	{
		for (auto next = entry; next = std::next(next), next != end; entry = next)
		{
			using std::swap;
			
			if (compare(*entry, *next))
			{
				break;
			}
			
			swap(*entry, *next);
			entry = next;
		}
	}
	
	return entry;
}
} // namespace nwd::utility
