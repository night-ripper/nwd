#pragma once

#include <cstdint>

#include <type_traits>

namespace nwd::utility
{
template<typename Type>
constexpr Type& unmove(Type&& value) noexcept
{
	return value;
}

template<typename From, typename To>
using forward_const_t = std::conditional_t<std::is_const_v<std::remove_reference_t<From>>, std::add_const_t<To>, To>;

template<typename Type>
inline std::size_t offset_of(auto Type::*Member) noexcept
{
	auto* value = static_cast<const Type*>(nullptr);
	return reinterpret_cast<const unsigned char*>(&(value->*Member)) - reinterpret_cast<const unsigned char*>(value);
}
} // namespace nwd::utility
