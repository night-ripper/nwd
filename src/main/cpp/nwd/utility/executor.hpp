#pragma once

#include <cstddef>
#include <cstdint>

#include <chrono>
#include <functional>
#include <thread>
#include <mutex>

#include <nwd/containers/aa/multimap.hpp>
#include <nwd/utility/move_copyable.hpp>
#include <nwd/utility/parallel.hpp>

namespace nwd::utility
{
struct Executor
{
	using duration = std::chrono::microseconds;
	using time_point = std::chrono::time_point<std::chrono::steady_clock, duration>;
	
	// TODO wait until clang fixes noexcept signature issues
	struct function_type;
	struct function_type : std::move_only_function<void()>
	{
		using std::move_only_function<void()>::move_only_function;
	};
	
	explicit Executor(duration timeout) noexcept
	{
		jthread_ = std::jthread([this, timeout](std::stop_token stop_token) -> void
		{
			auto last_time = std::chrono::time_point_cast<duration>(std::chrono::steady_clock::now());
			
			while (not stop_token.stop_requested())
			{
				last_time = execute_past_deadline() + timeout;
				std::this_thread::sleep_until(last_time);
			}
		});
	}
	
	void submit(time_point time, function_type&& function)
	{
		auto lg = std::lock_guard(lock_);
		queue_.emplace(time, std::move(function));
	}
	
	void submit(duration from_now, function_type&& function)
	{
		submit(std::chrono::time_point_cast<Executor::duration>(
			std::chrono::steady_clock::now() + from_now
		), std::move(function));
	}
	
	void submit_now(function_type&& function)
	{
		submit(duration(0), std::move(function));
	}
	
protected:
	time_point execute_past_deadline() noexcept
	{
		auto new_now = time_point();
		auto now = time_point();
		
		new_now = std::chrono::time_point_cast<duration>(std::chrono::steady_clock::now());
		
		do
		{
			now = new_now;
			
			auto lg = std::lock_guard(lock_);
			
			while (true)
			{
				if (auto it = queue_.begin(); it != queue_.end() and it->first <= now)
				{
					utility::parallel::thread_pool.submit_task(std::move(it->second));
					queue_.erase(it);
				}
				else
				{
					break;
				}
			}
			
			new_now = std::chrono::time_point_cast<duration>(std::chrono::steady_clock::now());
		}
		while (now != new_now);
		
		return now;
	}
	
private:
	containers::aa::Multimap<time_point, function_type, std::less<time_point>, std::int32_t> queue_;
	std::mutex lock_;
	std::jthread jthread_;
};
} // namespace nwd::utility
