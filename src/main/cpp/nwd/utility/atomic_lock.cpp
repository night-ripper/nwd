#include <iostream>

#include <nwd/utility/atomic_lock.hpp>
#include <nwd/utility/semaphore_lock.hpp>

#include <mutex>
#include <thread>
#include <vector>
#include <shared_mutex>

using namespace nwd::utility;

struct Data
{
// 	Atomic_shared_lock_32 lk;
// 	Atomic_unique_lock lk;
	std::mutex lk;
// 	Semaphore_lock lk;
	long long vals[64] {};
};

void f(Data& data, int num)
{
	std::lock_guard lg(data.lk);
	
	for (int i = 0; i < 32; ++i)
	{
		data.vals[i] += data.vals[64 - 1 - i] + num;
	}
}

void g(Data& data, int num)
{
	std::lock_guard lg(data.lk);
	
	for (int i = 0; i < 64; ++i)
	{
		data.vals[i] -= num;
	}
}

long long h(Data& data, int num)
{
// 	std::shared_lock lg(data.lk);
 	std::lock_guard lg(data.lk);
	
	long long sum = 0;
	
	for (int i = 0; i < 64; ++i)
	{
		sum += data.vals[i];
	}
	
	return sum;
}

int main()
{
	Data d;
	
	std::vector<std::jthread> js(64);
	
	for (int j = 0; j < 8; ++j)
	{
		js[j] = std::jthread([&](int n) -> void
		{
			for (int i = 0; i != 1'000'000; ++i)
			{
				f(d, n);
			}
		}, j);
	}
	
	for (int j = 8; j < 16; ++j)
	{
		js[j] = std::jthread([&](int n) -> void
		{
			for (int i = 0; i != 100'000; ++i)
			{
				g(d, n);
			}
		}, j);
	}
	
	for (int j = 16; j < 64; ++j)
	{
		js[j] = std::jthread([&](int n) -> void
		{
			for (int i = 0; i != 100'000; ++i)
			{
				h(d, n);
			}
		}, j);
	}
	
	js.clear();
	
	for (auto& d : d.vals)
	{
		std::cout << d << ", ";
	}
	
	std::cout << "\n";
}
