#pragma once

#include <fstream>
#include <filesystem>
#include <stdexcept>

namespace nwd::utility::fileio
{
inline std::filebuf open(std::filesystem::path path, std::ios_base::openmode mode)
{
	auto filebuf = std::filebuf();
	
	if (filebuf.open(path, mode))
	{
		return filebuf;
	}
	else
	{
		throw std::runtime_error("Unable to open file " + path.string());
	}
}

inline std::filebuf read_binary(std::filesystem::path path)
{
	return open(std::move(path), std::ios::in | std::ios::binary);
}
} // namespace nwd::utility:fileio
