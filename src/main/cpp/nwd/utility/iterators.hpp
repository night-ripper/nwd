#pragma once

#include <compare>
#include <concepts>
#include <iterator>

namespace nwd::utility
{
namespace operators
{
template<typename Iterator_type>
constexpr Iterator_type post_increment(Iterator_type& self)
noexcept(noexcept(++self))
{
	auto result = static_cast<const Iterator_type&>(self);
	++self;
	return result;
}

template<typename Iterator_type>
constexpr Iterator_type post_decrement(Iterator_type& self)
noexcept(noexcept(--self))
{
	auto result = static_cast<const Iterator_type&>(self);
	--self;
	return result;
}
} // namespace operators

template<typename Iterator_type>
struct Forward_iterator
{
	using iterator_category = std::forward_iterator_tag;
	
	constexpr Iterator_type operator++(int)
	noexcept(noexcept(operators::post_increment(static_cast<Iterator_type&>(*this))))
	{
		return operators::post_increment(static_cast<Iterator_type&>(*this));
	}
};

template<typename Iterator_type>
struct Bidirectional_iterator : Forward_iterator<Iterator_type>
{
	using iterator_category = std::bidirectional_iterator_tag;
	
	constexpr Iterator_type operator--(int)
	noexcept(noexcept(operators::post_decrement(static_cast<Iterator_type&>(*this))))
	{
		return operators::post_decrement(static_cast<Iterator_type&>(*this));
	}
};

/*
requires requires(Iterator_type it, Iterator_type jt)
{
	{*it};
	{it += std::declval<typename std::iterator_traits<Iterator_type>::difference_type>()}
		-> std::convertible_to<Iterator_type&>;
	{it - jt} -> std::convertible_to<typename std::iterator_traits<Iterator_type>::difference_type>;
}
*/
template<typename Iterator_type, typename Difference_type>
struct Random_access_iterator : Bidirectional_iterator<Iterator_type>
{
	using iterator_category = std::random_access_iterator_tag;
	using difference_type = Difference_type;
	
	using Bidirectional_iterator<Iterator_type>::operator++;
	using Bidirectional_iterator<Iterator_type>::operator--;
	
	constexpr decltype(auto) operator[](difference_type difference) const
	noexcept(noexcept(*(static_cast<const Iterator_type&>(*this) + difference)))
	{
		return *(static_cast<const Iterator_type&>(*this) + difference);
	}
	
	constexpr Iterator_type& operator++()
	noexcept(noexcept(static_cast<Iterator_type&>(*this) += 1))
	{
		return static_cast<Iterator_type&>(*this) += 1;
	}

	constexpr Iterator_type& operator--()
	noexcept(noexcept(static_cast<Iterator_type&>(*this) += -1))
	{
		return static_cast<Iterator_type&>(*this) += -1;
	}

	constexpr Iterator_type& operator-=(difference_type difference)
	noexcept(noexcept(static_cast<Iterator_type&>(*this) += -difference))
	{
		return static_cast<Iterator_type&>(*this) += -difference;
	}

	constexpr friend Iterator_type operator+(Iterator_type lhs, difference_type difference)
	noexcept(noexcept(lhs += difference))
	{
		return lhs += difference;
	}

	constexpr friend Iterator_type operator+(difference_type difference, Iterator_type rhs)
	noexcept(noexcept(rhs + difference))
	{
		return rhs + difference;
	}

	constexpr friend Iterator_type operator-(Iterator_type lhs, difference_type difference)
	noexcept(noexcept(lhs + (-difference)))
	{
		return lhs + (-difference);
	}
};
} // namespace nwd::utility
