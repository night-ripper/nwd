#pragma once

#include <cstdint>

#include <algorithm>
#include <bit>
#include <limits>
#include <ranges>
#include <span>
#include <type_traits>
#include <utility>

#include <nwd/utility/iterators.hpp>

namespace nwd::utility::bit_indexing
{
template<typename Type>
constexpr int digits = std::numeric_limits<Type>::digits;

struct Index_iterator : utility::Forward_iterator<Index_iterator>
{
	std::uintmax_t value_ = 0;
	
	Index_iterator() noexcept = default;
	
	constexpr Index_iterator(std::uintmax_t value)
		:
		value_(value)
	{
	}
	
	constexpr bool operator==(std::default_sentinel_t) const noexcept
	{
		return value_ == 0;
	}
	
	constexpr std::uintmax_t operator*() const noexcept
	{
		return static_cast<std::uintmax_t>(std::countr_zero(value_));
	}
	
	constexpr Index_iterator& operator++() noexcept
	{
		value_ &= ~(std::uintmax_t(1) << (**this));
		return *this;
	}
	
	using Forward_iterator::operator++;
	
	constexpr Index_iterator begin() noexcept
	{
		return *this;
	}
	
	constexpr static std::default_sentinel_t end() noexcept
	{
		return std::default_sentinel;
	}
};

template<typename Index_type, typename Size_type>
requires(std::is_unsigned_v<Index_type>)
constexpr Size_type level_length(Size_type size)
{
	size += digits<Index_type> - 1;
	size /= digits<Index_type>;
	return size;
}

template<typename Index_type, typename Size_type>
requires(std::is_unsigned_v<Index_type>)
constexpr Size_type index_length(Size_type size)
{
	Size_type result = 0;
	
	do
	{
		size = level_length<Index_type>(size);
		result += size;
	}
	while (size > 1);
	
	return result;
}

template<typename Index_type, typename Size_type>
requires(std::is_unsigned_v<Index_type>)
inline bool contains(Index_type* index_end, Size_type position, Size_type size)
{
	size = level_length<Index_type>(size);
	Index_type* level_begin = index_end - size;
	
	auto modulus = static_cast<std::int16_t>(position % digits<Index_type>);
	position /= digits<Index_type>;
	
	return level_begin[position] & (Index_type(1) << Index_type(modulus));
}

template<typename Index_type, typename Size_type>
requires(std::is_unsigned_v<Index_type>)
inline bool erase(Index_type* index_end, Size_type position, Size_type size)
{
	bool result = false;
	
	do
	{
		size = level_length<Index_type>(size);
		Index_type* level_begin = index_end - size;
		
		auto modulus = static_cast<std::int16_t>(position % digits<Index_type>);
		position /= digits<Index_type>;
		
		if (not (level_begin[position] & (Index_type(1) << Index_type(modulus))))
		{
			break;
		}
		
		result = true;
		
		level_begin[position] &= ~(Index_type(1) << Index_type(modulus));
		index_end = level_begin;
	}
	while (size > 1);
	
	return result;
}

template<typename Index_type, typename Size_type>
requires(std::is_unsigned_v<Index_type>)
[[nodiscard]] inline Size_type push_front(Index_type* index, Size_type size)
{
	if (size <= 0)
	{
		std::unreachable();
	}
	
	Size_type result = std::countr_one(*index);
	
	std::int16_t height = 0;
	Size_type llength = 0;
	
	for (Size_type length = size; length > 1; length = level_length<Index_type>(length))
	{
		++height;
		llength = length;
	}
	
	index += 1;
	
	for (; height > 1; --height)
	{
		result = std::countr_one(index[result]) + result * digits<Index_type>;
		
		index += llength;
		llength = size;
		
		for (std::int16_t h = 2; h != height; ++h)
		{
			llength = level_length<Index_type>(llength);
		}
	}
	
	Size_type position = result;
	
	do
	{
		auto modulus = static_cast<std::int16_t>(position % digits<Index_type>);
		position /= digits<Index_type>;
		size = level_length<Index_type>(size);
		index -= size;
		
		index[position] |= (Index_type(1) << modulus);
	}
	while (index[position] == std::numeric_limits<Index_type>::max() and size > 1);
	
	return result;
}

template<typename Index_type, typename Size_type>
requires(std::is_unsigned_v<Index_type>)
inline void copy(const Index_type* src_index_end, Size_type src_size, Index_type* dst_index_end, Size_type dst_size)
{
	if (dst_size < src_size)
	{
		std::unreachable();
	}
	
	do
	{
		src_size = level_length<Index_type>(src_size);
		src_index_end -= src_size;
		
		dst_size = level_length<Index_type>(dst_size);
		dst_index_end -= dst_size;
		
		std::copy_n(src_index_end, src_size, dst_index_end);
	}
	while (src_size > 1);
	
	if (*src_index_end == std::numeric_limits<Index_type>::max() and dst_size > 1)
	{
		dst_size = level_length<Index_type>(dst_size);
		dst_index_end -= dst_size;
		*dst_index_end = Index_type(1);
	}
}
} // namespace nwd::utility::bit_indexing
