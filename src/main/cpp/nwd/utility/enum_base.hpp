#pragma once

namespace nwd::utility
{
struct Enum_base
{
	constexpr Enum_base(int value) noexcept
		:
		value_(value)
	{
	}
	
	constexpr operator int() const noexcept
	{
		return value_;
	}
	
protected:
	int value_;
};
} // namespace nwd::utility
