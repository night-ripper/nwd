#pragma once

#include <cstddef>
#include <cstring>

#include <array>
#include <algorithm>
#include <random>

namespace nwd::utility
{
template<std::ptrdiff_t Size>
std::array<std::byte, Size> random_data(std::random_device& rd)
{
	auto result = std::array<std::byte, Size>();
	
	auto random_value = rd();
	auto i = std::ptrdiff_t(0);
	while (i < std::ssize(result) - static_cast<std::ptrdiff_t>(sizeof(std::random_device::result_type)))
	{
		std::memcpy(result.data() + i, &random_value, sizeof(std::random_device::result_type));
		random_value = rd();
		i += sizeof(std::random_device::result_type);
	}
	
	std::memcpy(result.data() + i, &random_value, std::size_t(std::ssize(result) - i));
	
	return result;
}
} // namespace nwd::utility
