#pragma once

#include <cstddef>

#include <array>
#include <utility>

namespace nwd::utility::arrays
{
template<typename Type, typename Arg_type, std::size_t Size>
constexpr std::array<Type, Size> cast(std::array<Arg_type, Size>& array) noexcept
{
	return []<std::size_t... I>(auto& array, std::index_sequence<I...>) constexpr noexcept -> std::array<Type, Size>
	{
		return std::array<Type, Size> {Type(array[I])...};
	}(array, std::make_index_sequence<Size>());
}

template<typename Type, typename Arg_type, std::size_t Size>
constexpr std::array<Type, Size> cast(const std::array<Arg_type, Size>& array) noexcept
{
	return []<std::size_t... I>(const auto& array, std::index_sequence<I...>) constexpr noexcept -> std::array<Type, Size>
	{
		return std::array<Type, Size> {Type(array[I])...};
	}(array, std::make_index_sequence<Size>());
}

template<typename Type, typename Arg_type, std::size_t Size>
constexpr std::array<Type, Size> cast(std::array<Arg_type, Size>&& array) noexcept
{
	return []<std::size_t... I>(auto& array, std::index_sequence<I...>) constexpr noexcept -> std::array<Type, Size>
	{
		return std::array<Type, Size> {Type(std::move(array[I]))...};
	}(array, std::make_index_sequence<Size>());
}

template<typename L_type, std::size_t L_size, typename R_type, std::size_t R_size>
constexpr auto join(const std::array<L_type, L_size>& lhs,
	const std::array<R_type, R_size>& rhs, const auto&... args) noexcept
{
	if constexpr (sizeof...(args) == 0)
	{
		return []<std::size_t... I, std::size_t... J>
		(const auto& lhs, const auto& rhs, std::index_sequence<I...>, std::index_sequence<J...>) constexpr noexcept
		{
			return std::array<std::common_type_t<L_type, R_type>, L_size + R_size> {lhs[I]..., rhs[J]...};
		}(lhs, rhs, std::make_index_sequence<L_size>(), std::make_index_sequence<R_size>());
	}
	else
	{
		return arrays::join(arrays::join(lhs, rhs), args...);
	}
}
} // namespace nwd::utility::arrays
