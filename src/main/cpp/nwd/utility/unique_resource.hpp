#pragma once

#include <memory>
#include <utility>
#include <type_traits>

namespace nwd::utility
{
template<typename Resource_type, typename Deleter_type>
struct Unique_resource
{
	~Unique_resource() noexcept
	{
		if (is_present_)
		{
			deleter_(resource_);
		}
	}
	
	constexpr Unique_resource() noexcept
	requires(std::is_default_constructible_v<Deleter_type> and std::is_default_constructible_v<Resource_type>)
		:
		deleter_(),
		resource_(),
		is_present_(false)
	{
	}
	
	template<typename Resource, typename Deleter = Deleter_type>
	constexpr Unique_resource(Resource&& resource, Deleter&& deleter = Deleter()) noexcept(
		(
			std::is_nothrow_constructible_v<Resource_type, Resource> or std::is_nothrow_constructible_v<Resource_type, Resource&>
		)
		and
		(
			std::is_nothrow_constructible_v<Deleter_type, Deleter> or std::is_nothrow_constructible_v<Deleter_type, Deleter&>
		))
		:
		deleter_(std::forward<Deleter>(deleter)),
		resource_(std::forward<Resource>(resource)),
		is_present_(true)
	{
	}
	
	constexpr Unique_resource(Unique_resource&& other) noexcept(
			std::is_nothrow_move_constructible_v<Resource_type> and std::is_nothrow_move_constructible_v<Deleter_type>
		)
		:
		deleter_(std::move(other.deleter_)),
		resource_(std::move(other.resource_)),
		is_present_(other.is_present_)
	{
		other.is_present_ = false;
	}
	
	constexpr Unique_resource& operator=(Unique_resource&& other) noexcept(
			std::is_nothrow_move_assignable_v<Resource_type> and std::is_nothrow_move_assignable_v<Deleter_type>
		)
	{
		std::destroy_at(this);
		std::construct_at(this, std::move(other));
		return *this;
	}
	
	constexpr explicit operator bool() const noexcept
	{
		return is_present_;
	}
	
	constexpr void release() noexcept
	{
		is_present_ = false;
	}
	
	constexpr void reset() noexcept
	{
		if (is_present_)
		{
			deleter_(resource_);
		}
		
		is_present_ = false;
	}
	
	template<typename Resource>
	constexpr void reset(Resource&& resource) noexcept
	{
		if (is_present_)
		{
			deleter_(resource_);
		}
		
		resource_ = std::forward<Resource>(resource);
		is_present_ = true;
	}
	
	constexpr const Resource_type& get() const noexcept
	{
		if (not is_present_)
		{
			std::unreachable();
		}
		
		return resource_;
	}
	
	constexpr const Deleter_type& get_deleter() const noexcept
	{
		return deleter_;
	}
	
private:
	[[no_unique_address]] Deleter_type deleter_;
	[[no_unique_address]] Resource_type resource_;
	[[no_unique_address]] bool is_present_;
};
} // namespace nwd::utility
