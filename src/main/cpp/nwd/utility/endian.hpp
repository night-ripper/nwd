#pragma once

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <bit>

namespace nwd::utility::endian
{
namespace privates
{
template<typename Type, std::endian Endianness>
struct Base
{
	constexpr Base() noexcept = default;
	
	constexpr Base(Type value) noexcept
	requires(Endianness == std::endian::native)
		:
		value_(value)
	{
	}
	
	constexpr Base(Type value) noexcept
	requires(Endianness != std::endian::native)
		:
		value_(std::byteswap(value))
	{
	}
	
	constexpr explicit Base(const std::byte* source) noexcept
	{
		std::memcpy(&value_, source, sizeof(Type));
	}
	
	constexpr Type value() const noexcept
	{
		return Base(value_).value_;
	}
	
	constexpr operator Type() const noexcept
	{
		return value();
	}
	
	std::byte* store(std::byte* target) const noexcept
	{
		std::memcpy(target, &value_, sizeof(Type));
		return target + sizeof(Type);
	}
	
	std::byte* data() noexcept {return reinterpret_cast<std::byte*>(&value_);}
	const std::byte* data() const noexcept {return reinterpret_cast<const std::byte*>(&value_);}
	
private:
	Type value_;
};
} // namespace privates

template<typename Type>
requires(std::is_integral_v<Type>)
using little_t = privates::Base<Type, std::endian::little>;

template<typename Type>
requires(std::is_integral_v<Type>)
using big_t = privates::Base<Type, std::endian::big>;

using little_int16_t = little_t<std::int16_t>;
using little_int32_t = little_t<std::int32_t>;
using little_int64_t = little_t<std::int64_t>;

using little_uint16_t = little_t<std::uint16_t>;
using little_uint32_t = little_t<std::uint32_t>;
using little_uint64_t = little_t<std::uint64_t>;

using big_int16_t = big_t<std::int16_t>;
using big_int32_t = big_t<std::int32_t>;
using big_int64_t = big_t<std::int64_t>;

using big_uint16_t = big_t<std::uint16_t>;
using big_uint32_t = big_t<std::uint32_t>;
using big_uint64_t = big_t<std::uint64_t>;
} // namespace nwd::utility::endian
