#pragma once

#include <cstddef>
#include <cstdint>

#include <istream>
#include <ostream>
#include <ranges>

namespace nwd::utility
{
template<std::int16_t Length = 64>
struct Buffered_reader
{
	using difference_type = std::ptrdiff_t;
	using value_type = std::string_view;
	
	Buffered_reader() = default;
	
	Buffered_reader(std::istream& is) noexcept
		:
		is_(&is)
	{
		if (is_->eof())
		{
			is_ = nullptr;
		}
	}
	
	bool operator==(std::default_sentinel_t) const noexcept
	{
		return is_ == nullptr;
	}
	
	value_type operator*()
	{
		length_ = is_->read(data_, sizeof(data_)).gcount();
		
		if (is_->eof())
		{
			is_ = nullptr;
		}
		
		return std::string_view(data_, length_);
	}
	
	Buffered_reader& operator++()
	{
		return *this;
	}
	
	Buffered_reader operator++(int)
	{
		return *this;
	}
	
private:
	std::istream* is_ = nullptr;
	std::int16_t length_ = 0;
	alignas(8) char data_[Length] {};
};

template<std::int16_t Length = 64>
inline std::ranges::subrange<Buffered_reader<Length>, std::default_sentinel_t> buffered_read(std::istream& is) noexcept
{
	return std::ranges::subrange(Buffered_reader<Length>(is), std::default_sentinel);
}

template<std::int16_t Length>
inline auto read_array(std::istream& is) noexcept
{
	struct Result
	{
		Result(std::istream& is)
		{
			length_ = static_cast<std::int16_t>(is.read(data_, Length).gcount());
		}
		
		std::string_view view() const noexcept
		{
			return std::string_view(data_, std::size_t(length_));
		}
		
		operator std::string_view() const noexcept
		{
			return view();
		}
		
	private:
		std::int16_t length_ = 0;
		char data_[Length] {};
	}
	result(is);
	
	return result;
}

template<std::int16_t Length = 64>
inline std::ostream& buffered_write(std::ostream& os, std::ranges::forward_range auto&& range)
{
	char data[Length];
	
	auto beg = std::ranges::begin(range);
	auto end = std::ranges::end(range);
	
	while (beg != end)
	{
		int i = 0;
		
		for (; i != sizeof(data); ++i)
		{
			if (beg != end)
			{
				data[i] = *beg;
				++beg;
			}
			else
			{
				break;
			}
		}
		
		os.write(data, i);
	}
	
	return os;
}

inline std::ostream&& buffered_write(std::ostream&& os, std::ranges::forward_range auto&& range)
{
	return std::move(buffered_write(os, range));
}
} // namespace nwd::utility
