#pragma once

#include <cstddef>
#include <cstdint>
#include <cstring>

#include <algorithm>
#include <array>
#include <bit>
#include <span>
#include <type_traits>

namespace nwd::utility
{
template<std::ptrdiff_t Length>
struct Byte_array : std::array<std::byte, Length>
{
	constexpr static std::ptrdiff_t length = Length;
	
	Byte_array() = default;
	
	constexpr Byte_array(std::array<std::byte, Length> bytes) noexcept
		:
		std::array<std::byte, Length>(bytes)
	{
	}
	
	constexpr Byte_array(const std::byte* bytes) noexcept
	{
		std::memcpy(this->data(), bytes, length);
	}
	
	friend constexpr auto operator<=>(const Byte_array& lhs, const Byte_array& rhs) noexcept = default;
	
	constexpr static Byte_array of(std::byte value) noexcept
	{
		Byte_array result;
		
		for (std::byte& byte : result)
		{
			byte = value;
		}
		
		return result;
	}
	
	template<typename Type>
	requires(std::is_integral_v<Type>)
	explicit operator Type() const noexcept
	{
		Type result = 0;
		auto byte_length = std::min(std::ssize(*this), std::ptrdiff_t(sizeof(Type)));
		std::memcpy(&result, this->data() + std::ssize(*this) - byte_length, std::size_t(byte_length));
		
		if constexpr (std::endian::native == std::endian::little)
		{
			result = std::byteswap(result);
		}
		
		return result;
	}
	
	//! Big endian increment
	constexpr Byte_array& operator++() noexcept
	{
		for (std::ptrdiff_t i = std::ssize(*this) - 1; i != -1; --i)
		{
			auto value = std::to_integer<unsigned char>(this->data()[i]);
			++value;
			this->data()[i] = std::byte(value);
			
			if (value != 0)
			{
				break;
			}
		}
		
		return *this;
	}
	
	//! Big endian decrement
	constexpr Byte_array& operator--() noexcept
	{
		for (std::ptrdiff_t i = std::ssize(*this) - 1; i != -1; --i)
		{
			auto value = std::to_integer<unsigned char>(this->data()[i]);
			--value;
			this->data()[i] = std::byte(value);
			
			if (value != static_cast<unsigned char>(-1))
			{
				break;
			}
		}
		
		return *this;
	}
	
	constexpr Byte_array& operator|=(const Byte_array& rhs) noexcept
	{
		for (std::ptrdiff_t i = 0; i != std::ssize(*this); ++i)
		{
			this->data()[i] |= rhs.data()[i];
		}
		
		return *this;
	}
	
	friend constexpr Byte_array operator|(Byte_array lhs, const Byte_array& rhs) noexcept
	{
		return lhs |= rhs;
	}
	
	constexpr Byte_array& operator&=(const Byte_array& rhs) noexcept
	{
		for (std::ptrdiff_t i = 0; i != std::ssize(*this); ++i)
		{
			this->data()[i] &= rhs.data()[i];
		}
		
		return *this;
	}
	
	friend constexpr Byte_array operator&(Byte_array lhs, const Byte_array& rhs) noexcept
	{
		return lhs &= rhs;
	}
	
	constexpr Byte_array& operator^=(const Byte_array& rhs) noexcept
	{
		for (std::ptrdiff_t i = 0; i != std::ssize(*this); ++i)
		{
			this->data()[i] ^= rhs.data()[i];
		}
		
		return *this;
	}
	
	friend constexpr Byte_array operator^(Byte_array lhs, const Byte_array& rhs) noexcept
	{
		return lhs ^= rhs;
	}
	
	friend constexpr Byte_array operator~(Byte_array value) noexcept
	{
		for (std::byte& byte : value)
		{
			byte = ~byte;
		}
		
		return value;
	}
	
	constexpr Byte_array& operator+=(const Byte_array& rhs) noexcept
	{
		bool carry = 0;
		
		for (std::ptrdiff_t i = std::ssize(*this) - 1; i != -1; --i)
		{
			short result = short(this->data()[i]) + short(rhs.data()[i]);
			result += carry;
			this->data()[i] = std::byte(result & short(0xff));
			carry = (result > short(0xff));
		}
		
		return *this;
	}
	
	constexpr friend Byte_array operator+(Byte_array lhs, const Byte_array& rhs) noexcept
	{
		return lhs += rhs;
	}
};
} // namespace nwd::utility

template<std::size_t Size>
struct std::tuple_size<nwd::utility::Byte_array<Size>> : std::tuple_size<std::array<std::byte, Size>>
{
};

template<std::size_t Index, std::size_t Size>
struct std::tuple_element<Index, nwd::utility::Byte_array<Size>> : std::tuple_element<Index, std::array<std::byte, Size>>
{
};
