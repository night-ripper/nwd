#include <iostream>
#include <fstream>
#include <memory>

#include <random>
#include <vector>
#include <set>
#include <algorithm>

#include <chrono>
#include <random>
#include <thread>

#include "rendering/context.hpp"
#include "rendering/textures.hpp"
#include "rendering/png.hpp"

#include "world.hpp"

#include "geometry/triangle3d.hpp"
#include "geometry/triangle-strip3d.hpp"
#include "geometry/shape3d.hpp"

using namespace geometry;
using namespace rendering;

struct Lol : bttf::World::Object
{
	Lol()
		:
		Object(this, [](void*, float k) -> void
		{
			std::cout << "lol: " << k << "\n";
		})
	{
	}
};

struct Unlol : bttf::World::Object
{
	Unlol() : Object(this) {}
};

int main(int, const char**)
{
	/*
	/// Need to set locale before GLFW init for proper character encoding
	{
		const char* locale_lc_all = "en_US.utf8";
		
		if (auto* locale = std::getenv("LC_ALL"))
		{
			locale_lc_all = locale;
		}
		
		std::setlocale(LC_ALL, locale_lc_all);
	}
	
	opengl::Context context;
	*/
	
	bttf::World map;
	map.make_object<Lol>();
	map.make_object<Unlol>();
	map.make_object<Lol>();
	map.make_object<Unlol>();
	map.make_object<Lol>();
	{
		auto one = map.make_object<Lol>();
		auto two = map.make_object<Unlol>();
		map.time_pass(1);
		std::cout << one.use_count() << "\n";
		one->disable();
		std::cout << one.use_count() << "\n";
		two->disable();
		map.time_pass(1);
		std::cout << "\n";
	}
	map.make_object<Lol>();
	map.make_object<Unlol>();
	map.make_object<Lol>();
	map.time_pass(1);
	
	std::cout << sizeof(Line_segment3D) << "\n";
	std::cout << sizeof(Triangle3D) << "\n";
	std::cout << sizeof(Shape3D) << "\n";
	
	std::cout << overlap(Line_segment3D::begin_end({0,0,0}, {1,1,1}), Cylinder3D({0,0,10}, 1, 1)) << "\n";
	
	std::cout << overlap(Line_segment3D::begin_end({0,0,0}, {1,1,0}), Cylinder3D({1,0,0}, 1, 1)) << "\n";
	std::cout << overlap(Line_segment3D::begin_end({0,0,0}, {1,1,0}), Cylinder3D({2,0,0}, 1, 1)) << "\n";
	std::cout << overlap(Line_segment3D::begin_end({0,0,0}, {1,1,0}), Cylinder3D({10,0,0}, 1, 1)) << "\n";
	
	std::cout << overlap(Line_segment3D::begin_end({0,0,0}, {0,0,1}), Cylinder3D({0,0,.5}, 1, 1)) << "\n";
	std::cout << overlap(Line_segment3D::begin_end({0,0,0}, {0,0,1}), Cylinder3D({0,0,1.5}, 1, 1)) << "\n";
	std::cout << overlap(Line_segment3D::begin_end({0,0,0}, {0,0,1}), Cylinder3D({0,0,3}, 1, 1)) << "\n";
	
	std::cout << overlap(Line_segment3D::begin_end({-1,-1,1}, {0,1,1}), Cylinder3D({1,1,1}, 1, 1)) << "\n";
	std::cout << overlap(Line_segment3D::begin_end({-1,-1,-1}, {0,1,1}), Cylinder3D({0,0,0}, 1, 1)) << "\n";
	std::cout << overlap(Line_segment3D::begin_end({-1,-1,-1}, {-2,-2,-2}), Cylinder3D({0,0,0}, 10, 0.1)) << "\n";
	std::cout << overlap(Line_segment3D::begin_end({0,0,-10}, {10,10,0}), Cylinder3D({0,0,0}, 9, 1)) << "\n"; // 0
	std::cout << overlap(Line_segment3D::begin_end({0,0,-10}, {10,10,0}), Cylinder3D({0,0,0}, 9.8, 1)) << "\n"; // 1
	
	std::cout << overlap(Line_segment3D::begin_end({-1,-1,-1}, {-1,-1,1}), Bounding_box3D({-2,-2,-2}, {2,2,2})) << "\n"; // 1
	std::cout << overlap(Line_segment3D::begin_end({-1,-1,-1}, {-1,-1,1}), Bounding_box3D({0,-2,-2}, {2,2,2})) << "\n"; // 0
	std::cout << overlap(Line_segment3D::begin_end({-100,0,0}, {1,100,0}), Bounding_box3D({0,-2,-2}, {2,2,2})) << "\n"; // 0
	std::cout << overlap(Line_segment3D::begin_end({-4,0,0}, {1,2,0}), Bounding_box3D({0,-2,-2}, {2,2,2})) << "\n"; // 1
	std::cout << overlap(Line_segment3D::begin_end({-4,0,0}, {-3,0.1,0}), Bounding_box3D({0,-2,-2}, {2,2,2})) << "\n"; // 0
	std::cout << overlap(Line_segment3D::begin_end({-4,0,0}, {0,-2,-2}), Bounding_box3D({0,-2,-2}, {2,2,2})) << "\n"; // 1
	
	std::cout << "##########" << "\n";
	
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0})
	) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Triangle3D::from_points({-2,-2,0}, {2,-2,0}, {0,2,0})
	) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Triangle3D::from_points({-102,-102,0}, {-98,-102,0}, {-100,-98,0})
	) << "\n"; // 0
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Triangle3D::from_points({-1.5,-1.5,0}, {0.5,-1.5,0}, {-0.5,0.5,0})
	) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Triangle3D::from_points({0,-2,0}, {0.1,-2,0}, {1,5,0})
	) << "\n"; // 1
	std::cout <<
		overlap(
		Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Triangle3D::from_points({0,-2,0}, {0.1,-2,0}, {1,-1.01,0})
	) << "\n"; // 0
	std::cout <<
		overlap(
		Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Triangle3D::from_points({-0.5,-3,0}, {-0.5,-2,0}, {10,2,0})
	) << "\n"; // 0
	
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Line_segment3D::begin_end({0,1,0}, {0,2,0})
	) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Line_segment3D::begin_end({0,1,-1}, {0,2,0})
	) << "\n"; // 0
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Line_segment3D::begin_end({0,1,-1}, {0,2,-1})
	) << "\n"; // 0
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Line_segment3D::begin_end({0,1.1,0}, {0,2,0})
	) << "\n"; // 0
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Line_segment3D::begin_end({0,1,0.1}, {0,2,0.1})
	) << "\n"; // 0
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Line_segment3D::begin_end({0,1,0.1}, {0,2,1})
	) << "\n"; // 0
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}),
		Line_segment3D::begin_end({0,0,1}, {0,0,-1})
	) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0.01}, {1,-1,0.02}, {0,1,0}),
		Line_segment3D::begin_end({0.1,0.1,1}, {0,0,-1})
	) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0.01}, {1,-1,0.02}, {0,1,0}),
		Line_segment3D::begin_end({0.1,0.1,1}, {0,0,0.05})
	) << "\n"; // 0
	
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({0,0,0}, 1)) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({0,0,0.5}, 0.4)) << "\n"; // 0
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({0,0,0.5}, 0.5)) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({0,0,0.5}, 0.6)) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({0,0,0.5}, 0.65)) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({0,0,1}, 0.99)) << "\n"; // 0
	
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({0,-1,0.5}, 0.51)) << "\n"; // 1
	std::cout << overlap(Triangle3D::from_points({-1,-1,0}, {1,-1,0}, {0,1,0}), Sphere3D({5,5,5}, 100)) << "\n"; // 1
	
	Vector3D strip[] = {
		{100,100,0},
		{102,100,0},
		{100,101.5,0},
		{103.1,103.2,0},
	};
	
	Triangle_strip3D s({0.5,0.5,0}, std::begin(strip), std::end(strip));
	
	for (auto v : s.vertices())
	{
		std::cout << v << "\n";
	}
	
	for (auto t : s.triangles())
	{
		std::cout << t.normal() << "\n";
	}
}
