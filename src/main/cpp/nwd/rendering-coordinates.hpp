#pragma once

#include "geometry/vector3d.hpp"
#include "geometry/quaternion.hpp"
#include "rendering/type_enum.hpp"
#include "glue/glsl.hpp"
#include "utility/normalize.hpp"

#include <cstddef>
#include <cstdint>

#include <functional>
#include <limits>
#include <memory_resource>

namespace bttf
{
struct Vertex_data
{
	struct Vertex_coordinates
	{
		using element_type = typename geometry::coord_t::underlying_type;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 3;
		
		std::array<element_type, elements_per_vertex> vertex_coordinates;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		
		Vertex_coordinates() noexcept = default;
		
		Vertex_coordinates(geometry::coord_t x, geometry::coord_t y, geometry::coord_t z) noexcept
			:
			vertex_coordinates {x.value(), y.value(), z.value()}
		{
		}
		
		Vertex_coordinates(geometry::Vector3D vector) noexcept
			:
			Vertex_coordinates {vector.x(), vector.y(), vector.z()}
		{
		}
	};
	
	struct Texture_coordinates
	{
		using element_type = std::int16_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 2;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> texture_coordinates;
		
		Texture_coordinates() noexcept = default;
		
		Texture_coordinates(element_type x, element_type y) noexcept
			:
			texture_coordinates {x, y}
		{
		}
	};
	
	struct Texture_indices
	{
		using element_type = std::int16_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 2;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		
		element_type binding = -1;
		element_type array_index = -1;
		
		Texture_indices() noexcept = default;
		
		Texture_indices(element_type binding, element_type array_index) noexcept
			:
			binding {binding},
			array_index {array_index}
		{
		}
		
		static Texture_indices color_only() {return {std::numeric_limits<element_type>::max(), 0};}
	};
	
	struct Relative_position
	{
		using element_type = std::int16_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 3;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> relative_position {};
		
		Relative_position() noexcept = default;
		
		Relative_position(element_type x, element_type y, element_type z) noexcept
			:
			relative_position {x, y, z}
		{
		}
	};
	
	struct Scale
	{
		using element_type = float;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 3;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = false;
		
		std::array<element_type, elements_per_vertex> scale {1, 1, 1};
		
		Scale() noexcept = default;
		
		Scale(element_type x, element_type y, element_type z) noexcept
			:
			scale {x, y, z}
		{
		}
	};
	
	struct Quaternion
	{
		using element_type = std::int16_t;
		
		constexpr static element_type max = std::numeric_limits<element_type>::max();
		
		constexpr static std::ptrdiff_t elements_per_vertex = 4;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> quaternion {0, 0, 0, max};
		
		Quaternion() noexcept = default;
		
		Quaternion(geometry::Quaternion q) noexcept
			:
			quaternion {element_type(q[0] * max), element_type(q[1] * max), element_type(q[2] * max), element_type(q[3] * max)}
		{
		}
		
		Quaternion(element_type x, element_type y, element_type z, element_type w) noexcept
			:
			quaternion {x, y, z, w}
		{
		}
	};
	
	struct Normal_coordinates
	{
		using element_type = std::int16_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 3;
		
		std::array<element_type, elements_per_vertex> normal_coordinates {};
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		Normal_coordinates() noexcept = default;
		
		Normal_coordinates(element_type x, element_type y, element_type z) noexcept
			:
			normal_coordinates {x, y, z}
		{
		}
	};
	
	struct Color
	{
		using element_type = std::int8_t;
		
		constexpr static element_type max = std::numeric_limits<element_type>::max();
		
		constexpr static std::ptrdiff_t elements_per_vertex = 4;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> color {max, max, max, max};
		
		Color() noexcept = default;
		
		Color(element_type r, element_type g, element_type b, element_type a) noexcept
			:
			color {r, g, b, a}
		{
		}
	};
	
	struct Ambient_color
	{
		using element_type = std::int8_t;
		
		constexpr static std::ptrdiff_t elements_per_vertex = 3;
		
		inline static const auto type = rendering::opengl::enum_of<element_type>;
		constexpr static bool normalized = true;
		
		std::array<element_type, elements_per_vertex> ambient_color
		{
			utility::nrm(0.05), utility::nrm(0.05), utility::nrm(0.05),
		};
		
		Ambient_color() noexcept = default;
		
		Ambient_color(element_type r, element_type g, element_type b) noexcept
			:
			ambient_color {r, g, b}
		{
		}
	};
	
	Vertex_coordinates vertex_coordinates;
	Texture_coordinates texture_coordinates;
	Texture_indices texture_indices;
	
	Relative_position relative_position;
	Scale scale;
	Quaternion quaternion;
	
	Normal_coordinates normal_coordinates;
	
	Color color;
	Ambient_color ambient_color;
};

struct Element_index
{
	/// Must be unsigned
	using index_type = std::uint32_t;
	
	static_assert(std::is_unsigned_v<index_type>);
	
	constexpr static index_type restart = std::numeric_limits<index_type>::max();
	
	Element_index() noexcept = default;
	
	template<typename Type>
	requires(std::is_integral_v<Type>)
	Element_index(Type value) noexcept
		:
		value_(value)
	{
	}
	
	operator index_type() const noexcept
	{
		return value_;
	}
	
private:
	index_type value_;
};

struct alignas(16) Light_coordinates
{
	geometry::Vector3D position;
	float distance;
	glsl::vec3 color;
	float cos_inner_cutoff = -1;
	glsl::vec3 unit_direction = {0, 0, 0};
	float cos_outer_cutoff = cos_inner_cutoff;
};

static_assert(std::is_standard_layout_v<Vertex_data>);
static_assert(std::is_standard_layout_v<Element_index>);
static_assert(std::is_standard_layout_v<Light_coordinates>);

/// Make sure that vector::clear() does nothing
static_assert(std::is_trivially_destructible_v<Vertex_data>);
static_assert(std::is_trivially_destructible_v<Element_index>);
static_assert(std::is_trivially_destructible_v<Light_coordinates>);
} // namespace bttf
