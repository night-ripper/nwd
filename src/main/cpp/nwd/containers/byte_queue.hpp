#pragma once

#include <cstring>

#include <iterator>

#include <nwd/containers/vector_queue.hpp>

namespace nwd::containers
{
struct Byte_queue : Vector_queue<std::byte>
{
	void sputn(std::span<const std::byte> source)
	{
		auto new_capacity = this->capacity();
		
		for (auto size = std::ssize(*this); new_capacity < size + std::ssize(source);)
		{
			new_capacity = this->default_capacity_growth(new_capacity);
		}
		
		if (new_capacity != this->capacity())
		{
			this->reserve(new_capacity);
		}
		
		if (this->end_ < 0)
		{
			std::unreachable();
		}
		
		auto moved_size = std::min(std::ssize(source), this->capacity() - this->end_);
		std::memcpy(this->data() + this->end_, source.data(), static_cast<std::size_t>(moved_size));
		this->end_ += moved_size;
		
		if (this->end_ == this->capacity())
		{
			this->end_ = 0;
			std::memcpy(this->data(), source.data() + moved_size, std::size(source) - static_cast<std::size_t>(moved_size));
			this->end_ += std::size(source) - static_cast<std::size_t>(moved_size);
			
			if (this->end_ == this->begin_)
			{
				this->end_ = -1;
			}
		}
	}
	
	std::span<std::byte> sgetn(std::span<std::byte> target) noexcept
	{
		auto size = std::size_t(0);
		
		for (auto span : this->spans())
		{
			auto moved_size = std::min(std::size(span), std::size(target));
			std::memcpy(target.data() + size, span.data(), moved_size);
			size += moved_size;
		}
		
		if (size != 0)
		{
			if (this->end_ == -1)
			{
				this->end_ = this->begin_;
			}
			
			this->begin_ += size;
			this->begin_ %= this->capacity();
		}
		
		return std::span(target.data(), size);
	}
};

static_assert(requires(Byte_queue::iterator it) {{++it} -> std::same_as<Byte_queue::iterator&>;});
static_assert(requires(Byte_queue::iterator it) {{it++} -> std::same_as<Byte_queue::iterator>;});
static_assert(std::random_access_iterator<Byte_queue::iterator>);
static_assert(std::weakly_incrementable<Byte_queue::iterator>);
} // namespace nwd::containers
