#include <iostream>

#include <nwd/containers/heap_repository.hpp>

#include <vector>
#include <ranges>
#include <fstream>

#include <memory>

using namespace nwd::containers;

int main()
{
	Heap_repository<std::int64_t, std::int32_t> rp;
	
	rp.push(1);
	std::cout << rp.top() << "\n";
	rp.push(2);
	std::cout << rp.top() << "\n";
	rp.push(3);
	std::cout << rp.top() << "\n";
	rp.push(4);
	std::cout << rp.top() << "\n";
	rp.push(5);
	std::cout << rp.top() << "\n";
	rp.push(3);
	std::cout << rp.top() << "\n";
	
	for (int i = 0; i != 20; ++i)
	{
		rp.push(i);
	}
		
	while (not rp.empty())
	{
		std::cout << rp.top() << "\n";
		rp.pop();
	}
	
	std::cout << sizeof(Repository<int, std::int32_t>) << "\n";
	std::cout << sizeof(rp) << "\n";
	
	rp.to_dot(std::ofstream("gr.dot"));
}
