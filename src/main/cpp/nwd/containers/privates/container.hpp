#pragma once

#include <memory_resource>

namespace nwd::containers::privates
{
struct Container
{
	using allocator_type = std::pmr::polymorphic_allocator<>;
	
	Container() = default;
	
	Container(allocator_type allocator) noexcept
		:
		allocator_(allocator)
	{
	}
	
	Container(const Container& other)
		:
		allocator_(std::allocator_traits<allocator_type>::select_on_container_copy_construction(other.allocator_))
	{
	}
	
	allocator_type get_allocator() const noexcept {return allocator_;}
	
protected:
	[[no_unique_address]] allocator_type allocator_;
};
} // namespace nwd::containers::privates
