#pragma once

#include <nwd/containers/aa/privates/tree.hpp>

namespace nwd::containers::aa
{
template<typename Key_type, typename Compare = std::less<Key_type>, typename Size_type = std::ptrdiff_t>
struct Multiset : privates::Common_interface<Key_type, void, Compare, Size_type, false>
{
private:
	using Base = privates::Common_interface<Key_type, void, Compare, Size_type, false>;
};
} // namespace nwd::containers::aa
