#pragma once

#include <nwd/containers/aa/privates/tree.hpp>

namespace nwd::containers::aa
{
template<typename Key_type, typename Mapped_type, typename Compare = std::less<Key_type>, typename Size_type = std::ptrdiff_t>
struct Multimap : privates::Common_interface<Key_type, Mapped_type, Compare, Size_type, false>
{
private:
	using Base = privates::Common_interface<Key_type, Mapped_type, Compare, Size_type, false>;
	
public:
	using mapped_type = Mapped_type;
	using typename Base::iterator;
};
} // namespace nwd::containers::aa
