#pragma once

#include <ostream>

#include <nwd/containers/repository.hpp>
#include <nwd/containers/nodes/aa.hpp>
#include <nwd/containers/privates/associative.hpp>

namespace nwd::containers::aa::privates
{
template<typename Traits_type, typename, typename>
requires(not std::is_reference_v<typename Traits_type::value_type>)
struct Tree;

template<typename, typename, typename, typename, bool>
struct Common_interface;

template<typename Node_type, typename Value_type, typename Size_type, typename Reference_type, typename Pointer_type>
struct Const_iterator;

template<typename Node_type, typename Value_type, typename Size_type, typename Reference_type, typename Pointer_type>
struct Iterator : protected nodes::aa::Iterator<Iterator<Node_type, Value_type, Size_type, Reference_type, Pointer_type>,
	Node_type*, Value_type, Size_type, Reference_type, Pointer_type>
{
private:
	using Base = nodes::aa::Iterator<Iterator, Node_type*, Value_type, Size_type, Reference_type, Pointer_type>;
	friend Base;
	
	template<typename Arg_traits_type, typename, typename>
	requires(not std::is_reference_v<typename Arg_traits_type::value_type>)
	friend struct Tree;
	
	template<typename, typename, typename, typename, bool>
	friend struct Common_interface;
	
	template<typename, typename, typename, typename, typename>
	friend struct Const_iterator;
	
	constexpr explicit Iterator(Base other) noexcept
		:
		Base(other)
	{
	}
	
public:
	using typename Base::difference_type;
	using typename Base::value_type;
	using Base::operator*;
	using Base::operator->;
	using Base::operator++;
	using Base::operator--;
	
	constexpr Iterator() noexcept = default;
	
	friend bool operator==(Iterator, Iterator) = default;
};

template<typename Node_type, typename Value_type, typename Size_type, typename Reference_type, typename Pointer_type>
struct Const_iterator : protected nodes::aa::Iterator<Const_iterator<Node_type, Value_type, Size_type, Reference_type, Pointer_type>,
	const Node_type*, const Value_type, Size_type, Reference_type, Pointer_type>
{
private:
	using Base = nodes::aa::Iterator<Const_iterator, const Node_type*, const Value_type, Size_type, Reference_type, Pointer_type>;
	friend Base;
	
	template<typename Arg_traits_type, typename, typename>
	requires(not std::is_reference_v<typename Arg_traits_type::value_type>)
	friend struct Tree;
	
	template<typename, typename, typename, typename, bool>
	friend struct Common_interface;
	
	constexpr explicit Const_iterator(Base other) noexcept
		:
		Base(other)
	{
	}
	
public:
	using typename Base::difference_type;
	using typename Base::value_type;
	using Base::operator*;
	using Base::operator->;
	using Base::operator++;
	using Base::operator--;
	
	friend bool operator==(Const_iterator, Const_iterator) = default;
	
	constexpr Const_iterator() noexcept = default;
	
	constexpr Const_iterator(auto other) noexcept
		:
		Base(other.nodes_, other.index_, other.is_end_)
	{
	}
};

template<typename Traits_type, typename Key_compare = std::less<>, typename Size_type = std::ptrdiff_t>
requires(not std::is_reference_v<typename Traits_type::value_type>)
struct Tree : protected Repository<nodes::aa::Node<typename Traits_type::value_type, Size_type>, Size_type>
{
	using value_type = typename Traits_type::value_type;
	
private:
	using Base = Repository<nodes::aa::Node<value_type, Size_type>, Size_type>;
	
public:
	using typename Base::allocator_type;
	using key_type = typename Traits_type::key_type;
	
	using typename Base::size_type;
	using typename Base::difference_type;
	
	using node_type = nodes::aa::Node<value_type, size_type>;
	
	using key_compare = Key_compare;
	using value_compare = typename Traits_type::template value_compare<key_compare>;
	
public:
	using Base::data;
	
	key_compare& key_comp() noexcept
	{
		return compare_;
	}
	
	const key_compare& key_comp() const noexcept
	{
		return compare_;
	}
	
	value_compare value_comp() const noexcept
	{
		return value_compare(compare_);
	}
	
private:
	struct Key_node_compare : key_compare
	{
		bool operator()(const key_type& lhs, const node_type& rhs) const
		noexcept(noexcept(static_cast<const key_compare&>(*this)(lhs, Traits_type::get_key(nodes::aa::value_of(rhs)))))
		{
			return static_cast<const key_compare&>(*this)(lhs, Traits_type::get_key(nodes::aa::value_of(rhs)));
		}
		
		bool operator()(const node_type& lhs, const key_type& rhs) const
		noexcept(noexcept(static_cast<const key_compare&>(*this)(Traits_type::get_key(nodes::aa::value_of(lhs)), rhs)))
		{
			return static_cast<const key_compare&>(*this)(Traits_type::get_key(nodes::aa::value_of(lhs)), rhs);
		}
	};
	
	template<typename Iterator_type>
	static Iterator_type static_begin(auto& self) noexcept
	{
		return Iterator_type(typename Iterator_type::Base(self.data(), self.front_, self.empty()));
	}
	
	template<typename Iterator_type>
	static Iterator_type static_end(auto& self) noexcept
	{
		return Iterator_type(typename Iterator_type::Base(self.data(), self.back_, true));
	}
	
public:
	std::ranges::subrange<typename Base::const_iterator, std::default_sentinel_t> nodes() const noexcept
	{
		return std::ranges::subrange<typename Base::const_iterator, std::default_sentinel_t>(this->Base::begin(), this->Base::end());
	}
	
	using pointer = typename Traits_type::pointer;
	using const_pointer = typename Traits_type::const_pointer;
	using reference = typename Traits_type::reference;
	using const_reference = typename Traits_type::const_reference;
	
	using iterator = Iterator<node_type, value_type, size_type, reference, pointer>;
	using const_iterator = Const_iterator<node_type, value_type, size_type, const_reference, const_pointer>;
	
	iterator begin() noexcept
	{
		return static_begin<iterator>(*this);
	}
	
	const_iterator begin() const noexcept
	{
		return static_begin<const_iterator>(*this);
	}
	
	iterator end() noexcept
	{
		return static_end<iterator>(*this);
	}
	
	const_iterator end() const noexcept
	{
		return static_end<const_iterator>(*this);
	}
	
	iterator get(size_type position) noexcept
	{
		return iterator(typename iterator::Base(this->data(), position, false));
	}
	
	const_iterator get(size_type position) const noexcept
	{
		return const_iterator(typename const_iterator::Base(this->data(), position, false));
	}
	
	std::tuple<Size_type, Size_type, std::int8_t> find_position(const auto& key) const noexcept
	{
		return nodes::aa::find(this->data(), root_, key, Key_node_compare(key_comp()));
	}
	
private:
	static size_type static_find_simple(auto& self, const auto& key) noexcept
	{
		size_type result = -1;
		
		if (not self.empty())
		{
			result = std::get<0>(self.find_position(key));
		}
		
		return result;
	}
	
	template<typename Iterator_type>
	static Iterator_type static_find(auto& self, const auto& key) noexcept
	{
		Iterator_type result = self.end();
		
		if (size_type position = static_find_simple(self, key); position != -1)
		{
			result = self.get(position);
		}
		
		return result;
	}
	
protected:
	std::ptrdiff_t validate_node(size_type index) const
	{
		auto result = std::ptrdiff_t(1);
		
		auto this_span = this->index_span();
		auto this_level = nodes::aa::get_level((*this)[index]);
		
		if (nodes::aa::get_level((*this)[index]) > 0)
		{
			if (nodes::aa::get_descendant((*this)[index], 0) == -1)
			{
				throw std::logic_error("");
			}
			
			if (nodes::aa::get_descendant((*this)[index], 1) == -1)
			{
				throw std::logic_error("");
			}
		}
		
		if (auto ldes = nodes::aa::get_descendant((*this)[index], 0); ldes != -1)
		{
			if (nodes::aa::get_descendant((*this)[index], 1) == -1)
			{
				throw std::logic_error("");
			}
			
			if (not utility::bit_indexing::contains(this_span.data() + this_span.size(), ldes, this->capacity_))
			{
				throw std::logic_error("");
			}
			
			if (nodes::aa::get_parent((*this)[ldes]) != index)
			{
				throw std::logic_error("");
			}
			
			if (value_compare()(nodes::aa::value_of((*this)[index]), nodes::aa::value_of((*this)[ldes])))
			{
				throw std::logic_error("");
			}
			
			if (this_level != nodes::aa::get_level((*this)[ldes]) + 1)
			{
				throw std::logic_error("");
			}
			
			result += validate_node(ldes);
		}
		
		if (auto rdes = nodes::aa::get_descendant((*this)[index], 1); rdes != -1)
		{
			if (not utility::bit_indexing::contains(this_span.data() + this_span.size(), rdes, this->capacity_))
			{
				throw std::logic_error("");
			}
			
			if (nodes::aa::get_parent((*this)[rdes]) != index)
			{
				throw std::logic_error("");
			}
			
			if (value_compare()(nodes::aa::value_of((*this)[rdes]), nodes::aa::value_of((*this)[index])))
			{
				throw std::logic_error("");
			}
			
			auto rlevel = nodes::aa::get_level((*this)[rdes]);
			
			if (this_level != rlevel and this_level != rlevel + 1)
			{
				throw std::logic_error("");
			}
			
			if (auto rrdes = nodes::aa::get_descendant((*this)[rdes], 1); rrdes != -1)
			{
				auto rrlevel = nodes::aa::get_level((*this)[rrdes]);
				
				if (this_level != rrlevel  + 1 and this_level != rrlevel + 2)
				{
					throw std::logic_error("");
				}
			}
			
			result += validate_node(rdes);
		}
		
		return result;
	}
	
public:
	iterator find(const auto& key) noexcept
	{
		return static_find<iterator>(*this, key);
	}
	
	const_iterator find(const auto& key) const noexcept
	{
		return static_find<const_iterator>(*this, key);
	}
	
	template<bool Single_key = true, typename... Args>
	std::pair<size_type, bool> try_emplace(const auto& key, Args&&... args)
	{
		if (this->empty())
		{
			root_ = this->Base::emplace(std::forward<Args>(args)...);
			front_ = root_;
			back_ = root_;
			return std::pair(root_, true);
		}
		
		auto [position, parent, parent_index] = find_position(key);
		
		if (position != -1)
		{
			if constexpr (Single_key)
			{
				return std::pair(position, false);
			}
			else
			{
				parent_index = 1;
				
				if ((position = nodes::aa::get_descendant(this->data()[parent], 1)) != -1)
				{
					parent = position;
					parent_index = 0;
					
					while ((position = nodes::aa::get_descendant(this->data()[parent], 0)) != -1)
					{
						parent = position;
					}
				}
				
			}
		}
		
		position = this->Base::emplace(std::forward<Args>(args)...);
		
		if (nodes::aa::insert_rebalance(this->data(), parent, parent_index, position))
		{
			root_ = nodes::aa::skew(this->data(), root_);
			root_ = nodes::aa::split(this->data(), root_);
			nodes::aa::set_parent((*this)[root_], size_type(-1));
		}
		
		if (nodes::aa::get_descendant(this->data()[front_], 0) == position
			or nodes::aa::get_descendant(this->data()[position], 1) == front_)
		{
			front_ = position;
		}
		
		if (nodes::aa::get_parent(this->data()[position]) == back_)
		{
			back_ = position;
		}
		
		return std::pair(position, true);
	}
	
	void erase(size_type position) noexcept
	{
		auto parent = nodes::aa::get_parent(this->data()[position]);
		auto rdes = nodes::aa::get_descendant(this->data()[position], 1);
		this->Base::erase(position);
		
		if (auto new_root = nodes::aa::erase_rebalance(this->data(), position); new_root != -1)
		{
			root_ = new_root;
		}
		else if (this->empty())
		{
			root_ = -1;
		}
		
		if (position == front_)
		{
			if (rdes != -1)
			{
				front_ = rdes;
			}
			else
			{
				front_ = parent;
			}
		}
		
		if (position == back_)
		{
			back_ = parent;
		}
	}
	
	static size_type static_erase(auto& self, const auto& key) noexcept
	{
		if (size_type position = static_find_simple(self, key); position != -1)
		{
			self.Tree::erase(position);
			return 1;
		}
		
		return 0;
	}
	
	size_type size() const noexcept
	{
		return this->Base::size_;
	}
	
	void clear() noexcept
	{
		this->Base::clear();
		
		root_ = -1;
		front_ = -1;
		back_ = -1;
	}
	
public:
	size_type root_ = -1;
	size_type front_ = -1;
	size_type back_ = -1;
	[[no_unique_address]] Key_compare compare_;
};

template<typename Key_type, typename Mapped_type, typename Compare, typename Size_type, bool Single_key>
void validate(const Common_interface<Key_type, Mapped_type, Compare, Size_type, Single_key>& self);

template<typename Key_type, typename Mapped_type, typename Compare, typename Size_type, bool Single_key>
struct Common_interface : Tree<containers::privates::Associative_entry_traits<Key_type, Mapped_type>, Compare, Size_type>
{
private:
	using traits_type = containers::privates::Associative_entry_traits<Key_type, Mapped_type>;
	using Base = Tree<traits_type, Compare, Size_type>;
	
public:
	using typename Base::allocator_type;
	using typename Base::iterator;
	using typename Base::const_iterator;
	
	using typename Base::value_type;
	using typename Base::size_type;
	using key_type = Key_type;
	
	using Base::Base;
	
	using Base::clear;
	
	using Base::empty;
	using Base::size;
	
	using Base::key_comp;
	using Base::value_comp;
	
	using Base::begin;
	using Base::end;
	
	using Base::operator[];
	
private:
	bool private_contains(const auto& key) const
	{
		return this->Base::find(key) != this->end();
	}
	
	iterator private_insert(auto&& value)
	{
		auto [position, inserted] = this->Base::template try_emplace<Single_key>(
			traits_type::get_key(value), std::forward<decltype(value)>(value)
		);
		
		return this->Base::get(position);
	}
	
	size_type private_erase(const auto& key) requires(Single_key)
	{
		return this->Base::static_erase(*this, key);
	}
	
public:
	iterator find(const key_type& key)
	{
		return this->Base::find(key);
	}
	
	const_iterator find(const key_type& key) const
	{
		return this->Base::find(key);
	}
	
	iterator find(const auto& key)
	requires requires {typename Compare::is_transparent;}
	{
		return this->Base::find(key);
	}
	
	const_iterator find(const auto& key) const
	requires requires {typename Compare::is_transparent;}
	{
		return this->Base::find(key);
	}
	
	bool contains(const auto& key) const
	requires requires {typename Compare::is_transparent;}
	{
		return private_contains(key);
	}
	
	bool contains(const key_type& key) const
	{
		return private_contains(key);
	}
	
	template<typename... Args>
	iterator emplace(Args&&... args)
	{
		return private_insert(value_type(std::forward<Args>(args)...));
	}
	
	iterator insert(const value_type& value)
	{
		return private_insert(value);
	}
	
	iterator insert(value_type&& value)
	{
		return private_insert(std::move(value));
	}
	
	iterator erase(const_iterator position) noexcept
	{
		auto index = position.index_;
		++position;
		this->Base::erase(index);
		
		if (position.is_end_)
		{
			return this->Base::end();
		}
		else
		{
			return iterator(typename iterator::Base(this->Base::data(), position.index_, false));
		}
	}
	
	size_type erase(const key_type& key)
	{
		return private_erase(key);
	}
	
	size_type erase(const auto& key)
	requires requires {typename Compare::is_transparent;}
	{
		return private_erase(key);
	}
	
	friend void validate<Key_type, Mapped_type, Compare, Size_type, Single_key>(
		const Common_interface<Key_type, Mapped_type, Compare, Size_type, Single_key>& self
	);
};

template<typename Key_type, typename Mapped_type, typename Compare, typename Size_type, bool Single_key>
void validate(const Common_interface<Key_type, Mapped_type, Compare, Size_type, Single_key>& self)
{
	if (self.empty() != (self.size() == 0))
	{
		throw std::logic_error("");
	}
	
	if (self.empty() != (self.begin() == self.end()))
	{
		throw std::logic_error("");
	}
	
	if (self.empty() != (self.root_ == -1))
	{
		throw std::logic_error("");
	}
	
	if (self.empty() != (self.front_ == -1))
	{
		throw std::logic_error("");
	}
	
	if (self.empty() != (self.back_ == -1))
	{
		throw std::logic_error("");
	}
	
	auto this_span = self.index_span();
	auto repo_size = std::ptrdiff_t(0);
	
	if (not self.empty())
	{
		if (not utility::bit_indexing::contains(this_span.data() + this_span.size(), self.root_, self.capacity_))
		{
			throw std::logic_error("");
		}
		
		if (self.size() != self.validate_node(self.root_))
		{
			throw std::logic_error("");
		}
	}
	
	for (auto& entry : self)
	{
		++repo_size;
		
		if (self.value_comp()(entry, nodes::aa::value_of(self[self.front_])))
		{
			throw std::logic_error("");
		}
		
		if (self.value_comp()(nodes::aa::value_of(self[self.back_]), entry))
		{
			throw std::logic_error("");
		}
	}
	
	if (self.size() != repo_size)
	{
		throw std::logic_error("");
	}
}

template<typename Key_type, typename Mapped_type, typename Compare, typename Size_type, bool Single_key>
inline std::ostream& to_dot(std::ostream& os, const Common_interface<Key_type, Mapped_type, Compare, Size_type, Single_key>& value)
{
	os << "digraph {\n";
	
	constexpr auto print_node = [](std::ostream& os, std::ptrdiff_t index, const auto& node) -> void
	{
		os
		<< "node" << index << " [shape=record,label=\"{"
		<< "position: " << index << "\\n"
		<< "parent: " << nodes::aa::get_parent(node) << "\\n"
		<< "level: " << nodes::aa::get_level(node) << "\\n"
		<< "key: " << containers::privates::Associative_entry_traits<Key_type, Mapped_type>::get_key(nodes::aa::value_of(node)) << "\\n"
		<< "|{"
		<< "<ldes>" << nodes::aa::get_descendant(node, 0)
		<< "|"
		<< "<rdes>" << nodes::aa::get_descendant(node, 1)
		<< "}}"
		<< "\"];"
		<< "\n";
	};
	
	if (not value.empty())
	{
		print_node(os, value.root_, value[value.root_]);
	}
	
	for (auto& node : value.nodes())
	{
		auto index = &node - value.data();
		
		if (index != value.root_)
		{
			print_node(os, index, value[index]);
		}
	}
	
	for (auto& node : value.nodes())
	{
		auto index = &node - value.data();
		
		if (node.parent_ != -1)
		{
			os << "node" << index << " -> " << "node" << nodes::aa::get_parent(node) << " [style=dotted]" << ";\n";
		}
	}
	
	for (auto& node : value.nodes())
	{
		auto index = &node - value.data();
		
		for (std::pair<bool, char> array[] {{0, 'l'}, {1, 'r'}}; auto [desc, letter] : array)
		{
			if (nodes::aa::get_descendant(node, desc) != -1)
			{
				os << "node" << index << ":" << letter << "des" << " -> " << "node" << node.descendants_[desc] << ";\n";
			}
		}
	}
	
	os << "}";
	
	return os;
}

template<typename Key_type, typename Mapped_type, typename Compare, typename Size_type, bool Single_key>
inline std::ostream&& to_dot(std::ostream&& os, const Common_interface<Key_type, Mapped_type, Compare, Size_type, Single_key>& value)
{
	return std::move(to_dot(os, value));
}
} // namespace nwd::containers::aa::privates
