#pragma once

#include <cstddef>
#include <cstdint>

#include <algorithm>
#include <array>
#include <bit>
#include <compare>
#include <concepts>
#include <tuple>
#include <utility>
#include <span>
#include <iterator>

#include <nwd/utility/storage.hpp>
#include <nwd/containers/privates/vector_storage.hpp>
#include <nwd/containers/aa/node.hpp>
#include <nwd/containers/disorder/primes.hpp>
#include <nwd/utility/iterators.hpp>

namespace nwd::containers::disorder
{
template<typename Value_type, typename Size_type>
struct Node
{
	std::uint8_t level_ = 0;
	Size_type parent_ = -1;
	Size_type descendants_[2] = {-1, -1};
	utility::Aligned_storage<Value_type> value_;
	
	constexpr friend std::int8_t aa_getl(const Node& node) {return node.level_;}
	constexpr friend void aa_setl(const Node& node, std::int8_t value) {node.level_ = value;}
	
	constexpr friend Size_type aa_getp(const Node& node) {return node.parent_;}
	constexpr friend void aa_setp(const Node& node, Size_type value) {node.parent_ = value;}
	
	constexpr friend Size_type aa_getd(const Node& node, bool index) {return node.descendants_[index];}
	constexpr friend void aa_setd(const Node& node, bool index, Size_type value) {node.descendants_[index] = value;}
};

template<typename Node_type, typename Hasher, typename Key_compare, typename Size_type = std::ptrdiff_t>
struct Disorder : protected privates::Container, privates::Vector_storage_data<Size_type>
{
private:
	using Base = privates::Vector_storage_data<Size_type>;
	
public:
	using typename Base::size_type;
	using typename privates::Container::allocator_type;
	
	using hasher = Hasher;
	using key_compare = Key_compare;
	
	using node_type = Node_type;
	
public:
	~Disorder()
	{
		this = std::move(*this);
	}
	
	explicit Disorder(const hasher& hash = hasher(), const key_compare& key_comp = key_compare(), const allocator_type& allocator = allocator_type()) noexcept
		:
		Base(allocator),
		hasher_(hash),
		key_compare_(key_comp)
	{
	}
	
	Disorder(Disorder&& other) noexcept
		:
		Disorder()
	{
		*this = std::move(other);
	}
	
	Disorder& operator=(Disorder&& other) noexcept
	{
		// TODO
		return *this;
	}
	
	constexpr static std::ptrdiff_t num_numbers(std::ptrdiff_t capacity) noexcept
	{
		return ((capacity - 1) / std::numeric_limits<std::size_t>::digits + 1) * 2;
	}
	
	constexpr static std::ptrdiff_t nodes_offset(std::ptrdiff_t capacity) noexcept
	{
		return privates::trailer_offset(sizeof(std::size_t) * num_numbers(capacity), alignof(node_type));
	}
	
	constexpr static std::ptrdiff_t size_for(std::ptrdiff_t capacity) noexcept
	{
		return nodes_offset(capacity) + sizeof(node_type) * capacity;
	}
	
	constexpr static std::ptrdiff_t alignment = std::max(alignof(std::size_t), alignof(node_type));
	
	constexpr static std::span<node_type> nodes_of(Base storage) noexcept
	{
		return std::span(reinterpret_cast<node_type*>(storage.data_ + nodes_offset(storage.capacity_)), storage.capacity_);
	}
	
	constexpr static std::span<std::size_t> heap_of(Base storage) noexcept
	{
		return std::span(reinterpret_cast<std::size_t*>(storage.data_), num_numbers(storage.capacity_));
	}
	
	constexpr static std::span<std::size_t> headers_of(Base storage) noexcept
	{
		return std::span(heap_of(storage).data(), num_numbers(storage.capacity_));
	}
	
	static void heap_decrease_top(Base storage) noexcept
	{
		
	}
	
	static void insert(Base storage, hasher& hasher, key_compare& key_comapre, auto&& key, auto&&... args)
	{
		const std::size_t orig_hash = hasher(key) % std::size_t(storage.capacity_);
		std::size_t hash = orig_hash;
		
		auto headers = headers_of(storage);
		
		if (headers[orig_hash / std::numeric_limits<std::size_t>::digits]
			& (1uz << (orig_hash % std::numeric_limits<std::size_t>::digits)))
		{
			hash /= std::numeric_limits<std::size_t>::digits;
			std::size_t inner_index = std::countl_zero(headers[hash]);
			
			if (inner_index == std::numeric_limits<std::size_t>::digits
				or hash * std::numeric_limits<std::size_t>::digits + inner_index == storage.capacity_)
			{
				hash = heap_of(storage)[0];
				inner_index = std::countl_zero(headers[hash]);
			}
			
			hash *= std::numeric_limits<std::size_t>::digits;
			hash += inner_index;
		}
		
		size_type descendant = -1;
		size_type parent = -1;
		bool parent_index = 0;
		
		if (hash != orig_hash)
		{
			std::tie(descendant, parent, parent_index) = aa::aa_find(nodes_of(storage).data(), orig_hash, key, key_comapre);
			
			if (descendant != -1)
			{
				// Value found
				return;
			}
		}
		
		std::construct_at(nodes_of(storage)[hash]->value_.data(), std::piecewise_construct,
			std::forward_as_tuple(std::forward<decltype(key)>(key)),
			std::forward_as_tuple(std::forward<decltype(args)>(args)...)
		);
		
		if (parent != -1)
		{
			aa_setd(nodes_of(storage)[parent], parent_index, hash);
			aa_setp(nodes_of(storage)[hash], parent);
			
			// TODO ???
			nodes_of(storage)[orig_hash] = aa::aa_insert_rebalance(nodes_of(storage).data(), parent, parent_index, hash)
		}
	}
	
	void reserve(size_type capacity)
	{
		if (capacity > this->capacity_)
		{
			auto prime_index = 0;
			
			while (capacity < primes[prime_index])
			{
				++prime_index;
			}
			
			auto new_storage = Base(static_cast<std::byte*>(this->get_allocator().allocate_bytes(size_for(capacity), alignment)), capacity);
			
			std::ranges::uninitialized_value_construct(heap_of(new_storage));
			std::ranges::uninitialized_value_construct(headers_of(new_storage));
			
			privates::construct_and_destroy_n(std::get<0>(this->data()), num_numbers(this->capacity_),
				reinterpret_cast<std::size_t*>(new_storage.data_)
			);
			
			auto* nodes = std::get<1>(this->data());
			auto* new_nodes = nodes_of(storage, capacity);
			
			// TODO rehash
			for (std::ptrdiff_t i = 0; i != this->capacity_; ++i)
			{
				std::construct_at(new_nodes + i);
				
				if (nodes[i].rn_value_present())
				{
					new_nodes[i].rn_construct_value(std::move(*nodes[i].rn_data()));
					nodes[i].rn_destroy_value();
				}
			}
		}
	}
	
	void private_try_emplace()
	{
		if (not this->data_)
		{
			auto new_capacity = primes[0];
		}
	}
	
	size_type size() const noexcept
	{
		return size_;
	}
	
	bool empty() const noexcept
	{
		return size_ == 0;
	}
	
public:
	size_type size_ = 0;
	[[no_unique_address]] hasher hasher_;
	[[no_unique_address]] key_compare key_compare_;
};
} // namespace nwd::containers::disorder
