#pragma once

#include <algorithm>
#include <bit>
#include <functional>
#include <memory>
#include <memory_resource>
#include <span>
#include <tuple>

#include <nwd/containers/privates/vector_storage.hpp>

namespace nwd::containers::hash::privates
{
template<typename Value_type>
struct Hash_base_node
{
	constexpr static std::size_t max_size = 64;
	
	std::size_t header_ = 0;
	std::aligned_storage_t<sizeof(Value_type), alignof(Value_type)> values_[max_size];
	
	static_assert(std::numeric_limits<decltype(header_)>::digits >= max_size);
	
	template<typename Type>
	static Type& static_at(auto& self, std::ptrdiff_t index) noexcept
	{
		return *reinterpret_cast<Type*>(&self.values_[index]);
	}
	
	Value_type& operator[](std::ptrdiff_t index) noexcept {return static_at<Value_type>(*this, index);}
	const Value_type& operator[](std::ptrdiff_t index) const noexcept {return static_at<const Value_type>(*this, index);}
	
	bool is_present(std::ptrdiff_t index) const noexcept
	{
		return header_ & (std::size_t(1) << index);
	}
	
	void destroy_at(std::ptrdiff_t index) noexcept
	{
		std::destroy_at(&(*this)[index]);
		header_ ^= (std::size_t(1) << index);
	}
	
	Value_type* construct_at(std::ptrdiff_t index, auto&&... args)
	noexcept(std::is_nothrow_constructible_v<Value_type, decltype(args)...>)
	{
		Value_type* result = std::construct_at(&(*this)[index], std::forward<decltype(args)>(args)...);
		header_ |= (std::size_t(1) << index);
		return result;
	}
	
	struct Index
	{
		/// We want to be able to represent overflowed value as well as -1
		constexpr static std::size_t bit_width = std::bit_width(max_size + 1) + 1;
		
		constexpr friend bool operator==(Index, Index) noexcept = default;
		
		std::ptrdiff_t outer_ : std::numeric_limits<std::size_t>::digits - bit_width;
		std::ptrdiff_t inner_ : bit_width;
	};
	
	static_assert(sizeof(Index) == sizeof(std::size_t));
};

template<typename Key_type, typename Value_type, typename Hasher, typename Key_equal, const Key_type&(Get_key)(const Value_type&)>
struct Hash_base : privates::Vector_storage<Value_type>
{
	using key_type = Key_type;
	using value_type = Value_type;
	
	using size_type	= std::ptrdiff_t;
	using difference_type = std::ptrdiff_t;
	
	using hasher = Hasher;
	using key_equal = Key_equal;
	
	using reference = value_type&;
	using const_reference = const value_type&;
	
	using allocator_type = std::pmr::polymorphic_allocator<>;
	
	using node_type = Hash_base_node<value_type>;
	using Index = typename Hash_base_node<value_type>::Index;
	
public:
	constexpr static auto get_key = Get_key;
	
private:
	constexpr static std::size_t bit_width = std::bit_width(std::size_t(std::numeric_limits<std::size_t>::digits));
	
	constexpr static std::ptrdiff_t size_capacity_multiplier(std::size_t size) noexcept
	{
		return size * 4 / 3;
	}
	
public:
	constexpr static std::size_t max_size = std::numeric_limits<std::ptrdiff_t>::max() >> (bit_width + 1);
	
protected:
	node_type* table_ = nullptr;
	struct {
		bool multiplier_ : 1 = 0;
		std::size_t shift_ : bit_width = 0;
		std::ptrdiff_t size_ : (std::numeric_limits<std::size_t>::digits - bit_width - 1) = 0;
	} capacity_;
	std::pmr::memory_resource* memory_resource_ = std::pmr::get_default_resource();
	[[no_unique_address]] hasher hash_;
	[[no_unique_address]] key_equal equal_;
	
	static auto data_members(auto& self) noexcept
	{
		return std::tie(self.table_, self.capacity_, self.memory_resource_, self.hash_, self.equal_);
	};
	
public:
	~Hash_base()
	{
		if constexpr (not std::is_trivially_destructible_v<Value_type>)
		{
			clear();
		}
		
		if (auto capacity = allocated_capacity())
		{
			std::destroy_n(table_, capacity);
			get_allocator().deallocate_object(table_, capacity);
		}
	}
	
	void swap(Hash_base& other) noexcept(std::is_nothrow_swappable_v<value_type>)
	{
		auto lt(data_members(*this)), rt(data_members(other));
		std::swap(lt, rt);
	}
	
	friend void swap(Hash_base& lhs, Hash_base& rhs) noexcept(noexcept(lhs.swap(rhs)))
	{
		lhs.swap(rhs);
	}
	
	Hash_base() noexcept = default;
	
	Hash_base(const Hash_base& other)
	noexcept(std::is_nothrow_copy_constructible_v<value_type>)
	requires(std::is_copy_constructible_v<value_type>)
		:
		capacity_ {
			.multiplier_ {other.capacity_.size_ != 0},
			.shift_ {std::bit_width((size_capacity_multiplier(other.capacity_.size_) - 1) / node_type::max_size)},
			.size_ {other.capacity_.size_},
		},
		memory_resource_ {other.memory_resource_},
		hash_ {other.hash_},
		equal_ {other.equal_}
	{
		if (const auto capacity = allocated_capacity())
		{
			table_ = get_allocator().template allocate_object<node_type>(capacity);
			std::uninitialized_default_construct_n(table_, capacity);
			
			other.visit([&](const value_type& value)
			noexcept(std::is_nothrow_copy_constructible_v<value_type>) -> void
			{
				auto [outer, inner] = find_empty(std::span(table_, capacity), hash_(get_key(value)));
				construct_at(table_, {outer, inner}, value);
				
				table_[outer].header_ ^= (std::size_t(1) << inner);
			});
		}
	}
	
	Hash_base(Hash_base&& other) noexcept(noexcept(swap(other)))
	requires(std::is_move_constructible_v<value_type>)
		:
		Hash_base()
	{
		swap(other);
	}
	
	Hash_base& operator=(Hash_base other) noexcept(noexcept(swap(other)))
	{
		swap(other);
		return *this;
	}
	
	Hash_base(allocator_type allocator) noexcept
		:
		memory_resource_ {allocator.resource()}
	{
	}
	
	Hash_base(const hasher& hash, const key_equal& equal, allocator_type allocator) noexcept
		:
		memory_resource_ {allocator.resource()},
		hash_ {hash},
		equal_ {equal}
	{
	}
	
	allocator_type get_allocator() const noexcept {return {memory_resource_};}
	
	struct const_iterator
	{
		using value_type = Value_type;
		using difference_type = std::ptrdiff_t;
		using pointer = const value_type*;
		using reference = const value_type&;
		using iterator_category = std::input_iterator_tag;
		
	public:
		node_type* table_ = nullptr;
		Index index_ = {};
		
		const value_type& operator*() const
		{
			return table_[index_.outer_][index_.inner_];
		}
		
		constexpr explicit operator bool() const noexcept {return table_;}
		
		constexpr friend bool operator==(const_iterator, const_iterator) noexcept = default;
	};
	
	using iterator = const_iterator;
	
protected:
	static auto& value_at(auto* table, Index index) noexcept
	{
		return table[index.outer_][index.inner_];
	}
	
private:
	static bool is_present(const node_type* table, Index index) noexcept
	{
		return table[index.outer_].is_present(index.inner_);
	}
	
	static void destroy_at(node_type* table, Index index) noexcept
	{
		table[index.outer_].destroy_at(index.inner_);
	}
	
	static value_type* construct_at(node_type* table, Index index, auto&&... args) noexcept
	{
		return table[index.outer_].construct_at(index.inner_, std::forward<decltype(args)>(args)...);
	}
	
private:
	static Index next_index(Index index, std::size_t capacity) noexcept
	{
		++index.inner_;
		
		const bool crossed = (index.inner_== node_type::max_size);
		index.inner_ -= crossed * node_type::max_size;
		
		index.outer_ += crossed;
		index.outer_ &= (capacity - 1);
		
		return index;
	}
	
	static Index prev_index(Index index, std::size_t capacity) noexcept
	{
		--index.inner_;
		
		const bool crossed = (index.inner_== -1);
		index.inner_ += crossed * node_type::max_size;
		
		index.outer_ -= crossed;
		index.outer_ &= (capacity - 1);
		
		return index;
	}
	
	static std::size_t global_index(Index index) noexcept
	{
		std::size_t result = index.outer_;
		result *= node_type::max_size;
		result += index.inner_;
		return result;
	}
	
	static std::size_t modulus(std::size_t hash_value, std::size_t capacity) noexcept
	{
		/// Displace a continuous sequence of hash values in a interleaving manner
		hash_value %= capacity * node_type::max_size / 2 - 1;
		hash_value *= 2;
		
		return hash_value;
	}
	
	static Index hash_index(std::size_t capacity, std::size_t hash_value) noexcept
	{
		hash_value = modulus(hash_value, capacity);
		
		std::ptrdiff_t outer_index = hash_value / node_type::max_size;
		std::ptrdiff_t inner_index = hash_value - outer_index * node_type::max_size;
		
		return {outer_index, inner_index};
	}
	
	static Index open_address(std::size_t capacity, std::size_t hash_value, auto&& predicate) noexcept
	{
		auto index = hash_index(capacity, hash_value);
		
		while (predicate(index))
		{
			index = next_index(index, capacity);
		}
		
		return index;
	}
	
	static Index find_empty(std::span<const node_type> span, std::size_t hash_value) noexcept
	{
		return open_address(span.size(), hash_value, [&](Index index) -> bool
		{
			return is_present(span.data(), index);
		});
	}
	
	static std::tuple<Index, bool> find_nonequal(std::span<const node_type> span,
		const auto& key, std::size_t hash_value, key_equal equal) noexcept
	{
		bool nonequal_value = true;
		
		auto result = open_address(span.size(), hash_value, [&](Index index) noexcept -> bool
		{
			if (is_present(span.data(), index))
			{
				const auto& colliding_value = value_at(span.data(), index);
				
				if (equal(key, get_key(colliding_value)))
				{
					nonequal_value = false;
					return false;
				}
				
				return true;
			}
			
			return false;
		});
		
		return {result, nonequal_value};
	}
	
	template<typename Node_type>
	static void visit_private(auto&& visitor, std::span<Node_type> span)
	{
		for (auto& node : span)
		{
			for (auto header = node.header_; std::popcount(header) != 0;)
			{
				const auto index_inner = std::countr_zero(header);
				
				visitor(static_cast<Node_type&>(node), index_inner);
				
				header ^= (std::size_t(1) << index_inner);
			}
		}
	}
	
	std::tuple<Index, bool> find_nonequal(const auto& key, std::size_t allocated_capacity) const noexcept
	{
		return find_nonequal(std::span(table_, allocated_capacity), key, hash_(key), equal_);
	}
	
protected:
	std::tuple<Index, bool> find_to_insert(const key_type& key) noexcept
	{
		if (const auto old_capacity = capacity(); old_capacity <= size_capacity_multiplier(size()))
		{
			const auto old_allocated_capacity = allocated_capacity();
			
			/// To ensure a sequence: 0, 1, 2, 3, ...
			capacity_.shift_ += capacity_.multiplier_;
			capacity_.multiplier_ = 1;
			
			const auto new_allocated_capacity = allocated_capacity();
			
			auto* new_table = get_allocator().template allocate_object<node_type>(new_allocated_capacity);
			std::uninitialized_default_construct_n(new_table, new_allocated_capacity);
			
			for (size_type i = 0; i != old_allocated_capacity; ++i)
			{
				for (auto& node = table_[i]; std::popcount(node.header_) != 0;)
				{
					const auto index_inner = std::countr_zero(node.header_);
					const std::size_t hash_value = hash_(get_key(node[index_inner]));
					
					auto new_index = find_empty(std::span(new_table, new_allocated_capacity), hash_value);
					
					construct_at(new_table, new_index, std::move(node[index_inner]));
					
					node.destroy_at(index_inner);
				}
			}
			
			if (table_)
			{
				std::destroy_n(table_, old_allocated_capacity);
				get_allocator().deallocate_object(table_, old_allocated_capacity);
			}
			
			table_ = new_table;
		}
		
		const std::size_t hash_value = hash_(key);
		
		return find_nonequal(std::span(table_, allocated_capacity()), key, hash_value, equal_);
	}
	
protected:
	template<typename Visitor>
	Visitor visit_all(Visitor visitor) const
	noexcept(std::is_nothrow_invocable_v<Visitor, const value_type&>)
	{
		visit_private<const node_type>([&](const node_type& node, auto index_inner) -> void
		{
			visitor(node[index_inner]);
		}, std::span(table_, allocated_capacity()));
		
		return visitor;
	}
	
	/// @param args... Agruments for value_type
	value_type& emplace(Index index, auto&&... args)
	noexcept(std::is_nothrow_constructible_v<value_type, decltype(args)...>)
	{
		auto& result = *construct_at(table_, index, std::forward<decltype(args)>(args)...);
		++capacity_.size_;
		return result;
	}
	
	template<typename... Args>
	constexpr static bool is_emplace_noexcept = noexcept(std::declval<Hash_base>().emplace(std::declval<Index>(), std::declval<Args>()...));
	
	const_iterator find(const auto& key) const noexcept
	{
		if (auto al_capacity = allocated_capacity(); al_capacity != 0)
		{
			if (auto [index, empty] = find_nonequal(key, al_capacity); not empty)
			{
				return {table_, index};
			}
		}
		
		return {};
	}
	
	bool contains(const key_type& key) const noexcept
	{
		return bool(find(key));
	}
	
	void erase(const_iterator pos) noexcept
	{
		const auto capacity = allocated_capacity();
		
		auto index = pos.index_;
		auto global_position = global_index(pos.index_);
		
		for (auto next = pos.index_; next = next_index(next, capacity), is_present(table_, next);)
		{
			using std::swap;
			
			auto& value = value_at(table_, next);
			
			if (auto position = modulus(hash_(get_key(value)), capacity);
				position <= global_position)
			{
				swap(value_at(table_, index), value);
				
				index = next;
				global_position = global_index(next);
			}
		}
		
		destroy_at(table_, index);
		--capacity_.size_;
	}
	
	auto extract(const_iterator pos, auto&& getter)
	noexcept(std::is_nothrow_move_constructible_v<std::invoke_result_t<decltype(getter), value_type&>>)
	{
		auto result = std::move(getter(value_at(table_, pos.index_)));
		erase(pos);
		return result;
	}
	
	bool erase(const auto& key) noexcept
	{
		auto position = find(key);
		
		if (position)
		{
			erase(position);
		}
		
		return bool(position);
	}
	
	size_type size() const noexcept
	{
		return capacity_.size_;
	}
	
	[[nodiscard]] bool empty() const noexcept
	{
		return size() == 0;
	}
	
	size_type allocated_capacity() const noexcept
	{
		return size_type(capacity_.multiplier_) << capacity_.shift_;
	}
	
	size_type capacity() const noexcept
	{
		return allocated_capacity() * node_type::max_size;
	}
	
	void clear() noexcept
	{
		if constexpr (not std::is_trivially_destructible_v<Value_type>)
		{
			visit_private<node_type>([](node_type& node, auto index_inner) -> void
			{
				node.destroy_at(index_inner);
			}, std::span(table_, allocated_capacity()));
		}
		else
		{
			for (auto& node : std::span(table_, allocated_capacity()))
			{
				node.header_ = 0;
			}
		}
		
		capacity_.size_ = 0;
	}
};
} // namespace nwd::containers::hash::privates
