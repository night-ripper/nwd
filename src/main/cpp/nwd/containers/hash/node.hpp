#pragma once

#include <tuple>

#include <nwd/containers/repository.hpp>

namespace nwd::containers::hash
{
template<typename Value_type, typename Size_type>
struct Node
{
protected:
	std::uint8_t level_ : 7 = 0;
	bool value_present_ : 1 = false;
	Size_type parent_ = -1;
	Size_type descendants_[2] = {-1, -1};
	utility::Aligned_storage<Value_type> value_;
	
public:
	Value_type* rn_data() noexcept {return value_.data();}
	const Value_type* rn_data() const noexcept {return value_.data();}
	
	template<bool Index>
	constexpr friend Size_type rn_geti(const Node& node) noexcept
	{
		return node.descendants_[Index];
	}
	
	template<bool Index>
	constexpr friend void rn_seti(Node& node, Size_type value) noexcept
	{
		node.descendants_[Index] = value;
	}
	
	void rn_destroy_value()
	{
		std::destroy_at(value_.data());
		value_present_ = false;
	}
	
	template<typename... Args>
	void rn_construct_value(Args&&... args)
	{
		std::construct_at(value_.data(), std::forward<Args>(args)...);
		value_present_ = true;
	}
	
	bool rn_value_present() const noexcept
	{
		return value_present_;
	}
};
} // namespace nwd::containers::hash
