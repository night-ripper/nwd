#pragma once

#include <utility>

#include <nwd/containers/hash/hash_base.hpp>

namespace nwd::containers::hash
{
namespace privates
{
template<typename Key_type, typename Mapped_type>
struct Hash_map_value_type
{
	Key_type key_;
	Mapped_type mapped_;
	
	Hash_map_value_type()
	noexcept(
		std::is_nothrow_default_constructible_v<Key_type> and
		std::is_nothrow_default_constructible_v<Mapped_type>)
	= default;
	
	Hash_map_value_type(auto&& key, auto&&... args)
	noexcept(
		std::is_nothrow_constructible_v<Key_type, decltype(key)> and
		std::is_nothrow_constructible_v<Mapped_type, decltype(args)...>)
		:
		key_(std::forward<decltype(key)>(key)),
		mapped_(std::forward<decltype(args)>(args)...)
	{
	}
	
	constexpr static const Key_type& get_key(const Hash_map_value_type<Key_type, Mapped_type>& value) noexcept
	{
		return value.key_;
	}
};
} // namespace privates

template<typename Key_type, typename Mapped_type, typename Hasher = std::hash<Key_type>, typename Key_equal = std::equal_to<Key_type>>
struct Hash_map : protected privates::Hash_base<
	Key_type, privates::Hash_map_value_type<Key_type, Mapped_type>, Hasher, Key_equal,
	&privates::Hash_map_value_type<Key_type, Mapped_type>::get_key
>
{
public:
	using key_type = Key_type;
	using mapped_type = Mapped_type;
	using value_type = privates::Hash_map_value_type<Key_type, Mapped_type>;
	
private:
	using Base = privates::Hash_base<Key_type, value_type, Hasher, Key_equal,
		&privates::Hash_map_value_type<Key_type, Mapped_type>::get_key
	>;
	
public:
	using size_type	= typename Base::size_type;
	using difference_type = typename Base::difference_type;
	
	using hasher = typename Base::hasher;
	using key_equal = typename Base::key_equal;
	using allocator_type = typename Base::allocator_type;
	
	using reference = value_type&;
	using const_reference = const value_type&;
	
	using iterator = typename Base::iterator;
	using const_iterator = typename Base::const_iterator;
	
	using node_type = typename Base::node_type;
	
public:
	using Base::size;
	using Base::capacity;
	using Base::allocated_capacity;
	using Base::data;
	
	using Base::find;
	using Base::contains;
	
	using Base::clear;
	using Base::erase;
	
	mapped_type extract(const_iterator pos)
	noexcept(std::is_nothrow_move_constructible_v<mapped_type>)
	{
		return Base::extract(pos, [](value_type& value) noexcept -> mapped_type& {return value.mapped_;});
	}
	
	std::pair<const_iterator, bool> try_emplace(auto&& key, auto&&... args)
	noexcept(Base::template is_emplace_noexcept<decltype(key), decltype(args)...>)
	{
		auto [index, empty] = Base::find_to_insert(key);
		
		if (empty)
		{
			Base::emplace(index, std::forward<decltype(key)>(key), std::forward<decltype(args)>(args)...);
		}
		
		return {{Base::table_, index}, empty};
	}
	
	std::pair<const_iterator, bool> insert(auto&& key, auto&& mapped)
	noexcept(Base::template is_emplace_noexcept<decltype(key), decltype(mapped)>)
	{
		return Base::emplace(std::forward<decltype(key)>(key), std::forward<decltype(mapped)>(mapped));
	}
	
	std::pair<const_iterator, bool> insert_or_assign(auto&& key, auto&& mapped)
	noexcept(Base::template is_emplace_noexcept<decltype(key), decltype(mapped)>
		and std::is_nothrow_move_assignable_v<value_type>)
	{
		auto [index, empty] = Base::find_to_insert(key);
		
		if (empty)
		{
			Base::emplace(index, std::forward<decltype(key)>(key), std::forward<decltype(mapped)>(mapped));
		}
		else
		{
			Base::value_at(Base::table_, index) = std::forward<decltype(mapped)>(mapped);
		}
		
		return {{Base::table_, index}, empty};
	}
	
	mapped_type& operator[](auto&& key)
	noexcept(Base::template is_emplace_noexcept<decltype(key)>)
	{
		auto [index, empty] = Base::find_to_insert(key);
		
		return (empty ?
			Base::emplace(index, std::forward<decltype(key)>(key)) :
			Base::value_at(Base::table_, index)
		).mapped_;
	}
};
} // namespace nwd::containers::hash
