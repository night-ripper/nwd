#include <iostream>

#include <fstream>

#include <nwd/containers/aa/privates/tree.hpp>
#include <nwd/containers/aa/map.hpp>

using namespace nwd::containers::aa::privates;

int main()
{
	nwd::containers::aa::map<int, int> s;
	
// 	for (int i = 0; i != 100000; ++i)
// 	for (int i = 0; i != 1000; ++i)
	{
// 		s.emplace(i);
	}
	
	static_assert(std::ranges::forward_range<nwd::containers::Repository<int>>);
	static_assert(std::forward_iterator<nwd::containers::Repository<int>::iterator>);
	static_assert(std::bidirectional_iterator<Tree<int>::iterator>);
	
	
	for (int i = 0; i < 5; ++i)
	{
		s.try_emplace(i, i);
	}
	
	to_dot(std::ofstream("gr-aa.dot"), s) << "\n";
	
	auto it = s.begin();
	
	while (not s.empty())
	{
		to_dot(std::ofstream("gr-aa.dot"), s) << "\n";
		std::cout << s.size() << "\n";
		std::cout << "\t" << it->first << "\n";
		s.erase(it++);
	}
	
	std::cout << s.empty() << "\n";
	
	for (int i = 0; i < 5; ++i)
	{
		s.try_emplace(i, i);
	}
	
	to_dot(std::ofstream("gr-aa.dot"), s) << "\n";
}
