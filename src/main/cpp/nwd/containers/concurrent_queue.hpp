#pragma once

#include <cstddef>
#include <cstdint>

#include <atomic>
#include <chrono>
#include <memory>
#include <optional>
#include <thread>
#include <type_traits>

#include <nwd/utility/storage.hpp>

namespace nwd::containers
{
namespace concurrent_queues::privates
{
enum struct Status
{
	empty, constructing, present, destructing,
};

struct Base
{
	using size_type = std::int32_t;
	
	bool empty() const noexcept
	{
		return num_present_.load(std::memory_order::relaxed) <= 0;
	}
	
protected:
	bool full(std::ptrdiff_t capacity) const noexcept
	{
		return num_nonempty_.load(std::memory_order::relaxed) >= capacity;
	}
	
protected:
	/// Entries that are being constructed / destructed or are present
	std::atomic<size_type> num_nonempty_ = 0;
	
	/// Entries that are present, not being constructed / destructed
	std::atomic<size_type> num_present_ = 0;
};

/// Ad-hoc heuristic to achieve that different threads will be starting at
/// different indices resulting in less contention
inline std::size_t start_index(std::ptrdiff_t capacity) noexcept
{
	thread_local std::size_t start_index = 0;
	
	const std::size_t addition = capacity / 8 + 1;
	
	start_index += addition;
	start_index -= capacity * (std::ptrdiff_t(start_index) >= capacity);
	
	return start_index;
}
} // concurrent_queues::privates

template<typename Value_type, std::ptrdiff_t Capacity = 128>
struct Concurrent_queue : concurrent_queues::privates::Base
{
	using value_type = Value_type;
	constexpr static std::ptrdiff_t capacity = Capacity;
	
	using size_type = std::int32_t;
	
	~Concurrent_queue()
	{
		if (num_nonempty_.load(std::memory_order::acquire) > 0)
		{
			for (size_type index = 0; index != capacity; ++index)
			{
				switch (status_at(index))
				{
				case Status::present:
					std::destroy_at(&value_at(index));
					[[fallthrough]];
				case Status::empty:
					continue;
				default:
					/// NOTE not sure whether throw / print / ignore
					continue;
 					// throw std::runtime_error("Destroying concurrent queue with invalid objects");
				}
			}
		}
	}
	
	Concurrent_queue() noexcept = default;
	
	Concurrent_queue(const Concurrent_queue&) = delete;
	Concurrent_queue& operator=(const Concurrent_queue&) = delete;
	
	bool full() const noexcept
	{
		return full(capacity);
	}
	
	bool try_emplace(auto&&... args)
	{
		const auto try_construct = [&](size_type index) -> bool
		{
			auto expected = Status::empty;
			
			if (status_at(index).load(std::memory_order::relaxed) == Status::empty
				and status_at(index).compare_exchange_weak(expected, Status::constructing,
				std::memory_order::acquire, std::memory_order::relaxed))
			{
				try
				{
					std::construct_at(&value_at(index), std::forward<decltype(args)>(args)...);
				}
				catch (...)
				{
					status_at(index).store(Status::empty, std::memory_order::release);
					num_nonempty_.fetch_sub(1, std::memory_order::relaxed);
					throw;
				}
				status_at(index).store(Status::present, std::memory_order::release);
				num_present_.fetch_add(1, std::memory_order::relaxed);
				return true;
			}
			
			return false;
		};
		
		if (num_nonempty_.fetch_add(1, std::memory_order::relaxed) < capacity)
		{
			const size_type start_index = concurrent_queues::privates::start_index(capacity);
			
			for (size_type index = start_index; index != capacity; ++index)
			{
				if (try_construct(index))
				{
					return true;
				}
			}
			
			for (size_type index = 0; index != start_index; ++index)
			{
				if (try_construct(index))
				{
					return true;
				}
			}
		}
		
		num_nonempty_.fetch_sub(1, std::memory_order::relaxed);
		return false;
	}
	
	void emplace_timeout(std::chrono::microseconds timeout, auto&&... args)
	{
		while (not try_emplace(std::forward<decltype(args)>(args)...))
		{
			std::this_thread::sleep_for(timeout);
		}
	}
	
	void emplace(auto&&... args)
	{
		emplace_timeout(std::chrono::microseconds(100), std::forward<decltype(args)>(args)...);
	}
	
	std::optional<value_type> try_pop() noexcept
	{
		std::optional<value_type> result;
		
		const auto try_move = [&](size_type index) -> bool
		{
			auto expected = Status::present;
			
			if (status_at(index).load(std::memory_order::relaxed) == Status::present
				and status_at(index).compare_exchange_weak(expected, Status::destructing,
				std::memory_order::acquire, std::memory_order::relaxed))
			{
				result.emplace(std::move(value_at(index)));
				std::destroy_at(&value_at(index));
				status_at(index).store(Status::empty, std::memory_order::release);
				num_nonempty_.fetch_sub(1, std::memory_order::relaxed);
				return true;
			}
			
			return false;
		};
		
		if (num_present_.fetch_sub(1, std::memory_order::relaxed) > 0)
		{
			const size_type start_index = concurrent_queues::privates::start_index(capacity);
			
			for (size_type index = start_index; index != capacity; ++index)
			{
				if (try_move(index))
				{
					return result;
				}
			}
			
			for (size_type index = 0; index != start_index; ++index)
			{
				if (try_move(index))
				{
					return result;
				}
			}
		}
		
		num_present_.fetch_add(1, std::memory_order::relaxed);
		return result;
	}
	
private:
	using Status = concurrent_queues::privates::Status;
	
	struct Value
	{
		std::atomic<Status> status_ = Status::empty;
		utility::Aligned_storage<value_type> value_;
	};
	
	std::atomic<Status>& status_at(size_type index)
	{
		return values_[index].status_;
	}
	
	value_type& value_at(size_type index)
	{
		return *values_[index].value_.data();
	}
	
private:
	Value values_[capacity];
};

/// copy-pasted from concurrent queue
template<typename Value_type>
struct Concurrent_queue_header : concurrent_queues::privates::Base
{
	using value_type = Value_type;
	
	using size_type = std::int32_t;
	
	~Concurrent_queue_header()
	{
		if (num_nonempty_.load(std::memory_order::acquire) > 0)
		{
			for (size_type index = 0; index != capacity(); ++index)
			{
				switch (status_at(index))
				{
				case Status::present:
					std::destroy_at(&value_at(index));
					[[fallthrough]];
				case Status::empty:
					continue;
				default:
					/// NOTE not sure whether throw / print / ignore
					continue;
 					// throw std::runtime_error("Destroying concurrent queue with invalid objects");
				}
			}
		}
	}
	
	Concurrent_queue_header(std::ptrdiff_t capacity) noexcept
		:
		capacity_(capacity)
	{
		std::uninitialized_value_construct_n(values_, capacity_);
	}
	
	Concurrent_queue_header(const Concurrent_queue_header&) = delete;
	Concurrent_queue_header& operator=(const Concurrent_queue_header&) = delete;
	
	std::ptrdiff_t capacity() const noexcept {return capacity_;}
	
	bool full() const noexcept
	{
		return full(capacity());
	}
	
	bool try_emplace(auto&&... args)
	{
		const auto try_construct = [&](size_type index) -> bool
		{
			auto expected = Status::empty;
			
			if (status_at(index).load(std::memory_order::relaxed) == Status::empty
				and status_at(index).compare_exchange_weak(expected, Status::constructing,
				std::memory_order::acquire, std::memory_order::relaxed))
			{
				try
				{
					std::construct_at(&value_at(index), std::forward<decltype(args)>(args)...);
				}
				catch (...)
				{
					status_at(index).store(Status::empty, std::memory_order::release);
					num_nonempty_.fetch_sub(1, std::memory_order::relaxed);
					throw;
				}
				status_at(index).store(Status::present, std::memory_order::release);
				num_present_.fetch_add(1, std::memory_order::relaxed);
				return true;
			}
			
			return false;
		};
		
		if (num_nonempty_.fetch_add(1, std::memory_order::relaxed) < capacity())
		{
			const size_type start_index = concurrent_queues::privates::start_index(capacity());
			
			for (size_type index = start_index; index != capacity(); ++index)
			{
				if (try_construct(index))
				{
					return true;
				}
			}
			
			for (size_type index = 0; index != start_index; ++index)
			{
				if (try_construct(index))
				{
					return true;
				}
			}
		}
		
		num_nonempty_.fetch_sub(1, std::memory_order::relaxed);
		return false;
	}
	
	void emplace_timeout(std::chrono::microseconds timeout, auto&&... args)
	{
		while (not try_emplace(std::forward<decltype(args)>(args)...))
		{
			std::this_thread::sleep_for(timeout);
		}
	}
	
	void emplace(auto&&... args)
	{
		emplace_timeout(std::chrono::microseconds(100), std::forward<decltype(args)>(args)...);
	}
	
	std::optional<value_type> try_pop() noexcept
	{
		std::optional<value_type> result;
		
		const auto try_move = [&](size_type index) -> bool
		{
			auto expected = Status::present;
			
			if (status_at(index).load(std::memory_order::relaxed) == Status::present
				and status_at(index).compare_exchange_weak(expected, Status::destructing,
				std::memory_order::acquire, std::memory_order::relaxed))
			{
				result.emplace(std::move(value_at(index)));
				std::destroy_at(&value_at(index));
				status_at(index).store(Status::empty, std::memory_order::release);
				num_nonempty_.fetch_sub(1, std::memory_order::relaxed);
				return true;
			}
			
			return false;
		};
		
		if (num_present_.fetch_sub(1, std::memory_order::relaxed) > 0)
		{
			const size_type start_index = concurrent_queues::privates::start_index(capacity());
			
			for (size_type index = start_index; index != capacity(); ++index)
			{
				if (try_move(index))
				{
					return result;
				}
			}
			
			for (size_type index = 0; index != start_index; ++index)
			{
				if (try_move(index))
				{
					return result;
				}
			}
		}
		
		num_present_.fetch_add(1, std::memory_order::relaxed);
		return result;
	}
	
	constexpr static std::ptrdiff_t size_with(std::ptrdiff_t capacity) noexcept
	{
		return sizeof(Concurrent_queue_header) + capacity * sizeof(Value);
	}
	
private:
	using Status = concurrent_queues::privates::Status;
	
	struct Value
	{
		std::atomic<Status> status_ = Status::empty;
		utility::Aligned_storage<value_type> value_;
	};
	
	std::atomic<Status>& status_at(size_type index)
	{
		return values_[index].status_;
	}
	
	value_type& value_at(size_type index)
	{
		return *values_[index].value_.data();
	}
	
private:
	std::ptrdiff_t capacity_;
	Value values_[0];
};

template<typename Value_type>
std::shared_ptr<Concurrent_queue_header<Value_type>> shared_queue(std::ptrdiff_t capacity)
{
	/// TODO make_shared<unsigned char[]>
	
	auto shared_storage = std::shared_ptr<unsigned char[]>(new unsigned char[
		Concurrent_queue_header<Value_type>::size_with(capacity)
	]);
	
	auto* queue = reinterpret_cast<Concurrent_queue_header<Value_type>*>(shared_storage.get());
	
	return std::shared_ptr<Concurrent_queue_header<Value_type>>(std::move(shared_storage), std::construct_at(queue, capacity));
}

template<typename Value_type, std::ptrdiff_t Capacity = 128>
struct Naive_queue
{
	using value_type = Value_type;
	constexpr static std::ptrdiff_t capacity = Capacity;
	
	using size_type = std::int32_t;
	
private:
	static size_type next(size_type value) noexcept
	{
		return (value + 1) % Capacity;
	}
	
public:
	Naive_queue() noexcept = default;
	
	bool try_emplace(auto&&... args) noexcept
	{
		auto lock = std::lock_guard(mtx_);
		
		if (next(end_) == begin_)
		{
			return false;
		}
		
		std::construct_at(values_.data() + end_, std::forward<decltype(args)>(args)...);
		end_ = next(end_);
		
		return true;
	}
	
	std::optional<value_type> try_pop() noexcept
	{
		std::optional<value_type> result;
		
		auto lock = std::lock_guard(mtx_);
		
		if (begin_ == end_)
		{
			return result;
		}
		
		result.emplace(std::move(values_[begin_]));
		std::destroy_at(values_.data() + begin_);
		begin_ = next(begin_);
		
		return result;
	}
	
public:
	size_type begin_ = 0;
	size_type end_ = 0;
	std::mutex mtx_;
	utility::Aligned_storage<value_type, capacity> values_;
};
} // nwd::containers
