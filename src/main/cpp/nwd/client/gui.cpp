#include <fstream>
#include <filesystem>
#include <vector>
#include <iostream>

#include <epoxy/gl.h>

#include <nwd/graphics/glfw.hpp>
#include <nwd/graphics/glfw_constants.hpp>
#include <nwd/graphics/image.hpp>
#include <nwd/graphics/png.hpp>
#include <nwd/graphics/opengl/buffers.hpp>
#include <nwd/graphics/opengl/program.hpp>
#include <nwd/graphics/opengl/normalize.hpp>
#include <nwd/graphics/opengl/textures.hpp>
#include <nwd/graphics/opengl/type_enum.hpp>

#include <nwd/utility/utility.hpp>
#include <nwd/utility/fileio.hpp>

#include <nwd/containers/aa/set.hpp>

namespace nwd::graphics::opengl
{
struct OpenGL_error : std::runtime_error
{
	using std::runtime_error::runtime_error;
};

[[gnu::cold]]
[[maybe_unused]]
static void opengl_debug_callback(
	[[maybe_unused]] GLenum source,
	[[maybe_unused]] GLenum type,
	[[maybe_unused]] GLuint id,
	[[maybe_unused]] GLenum severity,
	[[maybe_unused]] GLsizei length,
	[[maybe_unused]] const char* message,
	[[maybe_unused]] const void* user_parameter)
{
	const char* source_string = "(unknown)";
	
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		source_string = "api";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		source_string = "window system";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		source_string = "shader compiler";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		source_string = "third party";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		source_string = "application";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		source_string = "other";
		break;
	}
	
	const char* type_string = "(unknown)";
	
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		type_string = "error";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		type_string = "deprecated behaviour";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		type_string = "undefined behaviour";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		type_string = "portability";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		type_string = "performance";
		break;
	case GL_DEBUG_TYPE_MARKER:
		type_string = "marker";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		type_string = "push group";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		type_string = "pop group";
		break;
	case GL_DEBUG_TYPE_OTHER:
		type_string = "other";
		break;
	}
	
	const char* severity_string = "(unknown)";
	
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		severity_string = "high";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		severity_string = "medium";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		severity_string = "low";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		severity_string = "notification";
		break;
	}
	
	std::string message_middle = std::string("(source: ") + source_string + ", " +
		"type: " + type_string + ", " + "severity: " + severity_string + "): "
	;
	
	if (severity == GL_DEBUG_SEVERITY_HIGH)
	{
		throw OpenGL_error(message_middle + std::string(message, static_cast<std::size_t>(length)));
	}
	else
	{
		std::clog << "OpenGL warning ";
		std::clog << message_middle;
		std::clog.write(message, length);
		std::clog << "\n";
	}
}
} // namespace nwd::graphics::opengl

using namespace nwd;

struct Framebuffer_context
{
	struct Vertex_data
	{
		std::array<std::int8_t, 2> vertex_position_;
		std::array<std::int8_t, 2> texture_coordinates_;
	};
	
	constexpr static decltype(Vertex_data::vertex_position_)::value_type v_min = graphics::opengl::nrm_min();
	constexpr static decltype(Vertex_data::vertex_position_)::value_type v_max = graphics::opengl::nrm_max();
	constexpr static decltype(Vertex_data::texture_coordinates_)::value_type t_min = graphics::opengl::nrm_min();
	constexpr static decltype(Vertex_data::texture_coordinates_)::value_type t_max = graphics::opengl::nrm_max();
	
	constexpr static auto framebuffer_data = std::to_array<Vertex_data>(
	{
		{{v_min, v_min}, {0, 0}},
		{{v_max, v_min}, {t_max, 0}},
		{{v_min, v_max}, {0, t_max}},
		
		{{v_max, v_min}, {t_max, 0}},
		{{v_max, v_max}, {t_max, t_max}},
		{{v_min, v_max}, {0, t_max}},
	});
	
	Framebuffer_context()
	{
		vao_.format_bind_data_buffer(0,
			std::tuple_size_v<decltype(Vertex_data::vertex_position_)>,
			graphics::opengl::enum_of<decltype(Vertex_data::vertex_position_)::value_type>,
			true,
			offsetof(Vertex_data, vertex_position_),
			vbo_, 0, sizeof(Vertex_data)
		);
		vao_.format_bind_data_buffer(1,
			std::tuple_size_v<decltype(Vertex_data::texture_coordinates_)>,
			graphics::opengl::enum_of<decltype(Vertex_data::texture_coordinates_)::value_type>,
			true,
			offsetof(Vertex_data, texture_coordinates_),
			vbo_, 0, sizeof(Vertex_data)
		);
		vbo_.data(std::as_bytes(std::span(framebuffer_data)));
		program_.uniform<float>(0, 1.f);
		program_.uniform<float>(1, 1.2f);
		
		rescale(128, 128);
	}
	
	void rescale(int width, int height)
	{
		width_ = width;
		height_ = height;
		
		bindless_.reset();
		texture_ = graphics::opengl::Texture_2D(width, height);
		texture_.storage(graphics::opengl::Texture::Format::rgb_f_16);
		texture_.parameter(graphics::opengl::Texture::Magnifying::nearest);
		texture_.parameter(graphics::opengl::Texture::Minifying::nearest);
		bindless_ = graphics::opengl::Texture_handle::Resident(texture_);
		
		renderbuffer_ = graphics::opengl::Renderbuffer(width, height);
		renderbuffer_.storage(graphics::opengl::Texture::Format::depth_16);
		
		framebuffer_.attach(texture_, graphics::opengl::Framebuffer::Attachment::color);
		framebuffer_.attach(renderbuffer_, graphics::opengl::Framebuffer::Attachment::depth);
		
		program_.uniform_handle(2, bindless_.get());
	}
	
	int width_;
	int height_;
	graphics::opengl::Vertex_array vao_ = graphics::opengl::Vertex_array::create();
	graphics::opengl::Buffer vbo_ = graphics::opengl::Buffer::create();
	graphics::opengl::Renderbuffer renderbuffer_;
	graphics::opengl::Texture_2D texture_;
	graphics::opengl::Texture_handle::Resident bindless_;
	graphics::opengl::Framebuffer framebuffer_ = graphics::opengl::Framebuffer::create();
	graphics::opengl::Program program_ = graphics::opengl::Program {
		graphics::opengl::Vertex_shader(std::ifstream("src/main/glsl/framebuffer.vert.glsl")),
		graphics::opengl::Fragment_shader(std::ifstream("src/main/glsl/framebuffer.frag.glsl")),
	};
};

int main()
{
	bool opengl_debug_profile = true;
	bool opengl_no_error = false;
	
	auto glfw = graphics::glfw::Library();
	
	glfw.window_hint(graphics::glfw::Context::Hint::opengl_major_version(4));
	glfw.window_hint(graphics::glfw::Context::Hint::opengl_minor_version(5));
	glfw.window_hint(graphics::glfw::Context::Hint::opengl_forward_compatibility);
	
	if (opengl_debug_profile)
	{
		glfw.window_hint(graphics::glfw::Context::Hint::opengl_debug_profile);
	}
	
	if (opengl_no_error)
	{
		glfw.window_hint(graphics::glfw::Context::Hint::opengl_no_error_profile);
	}
	
	auto context = glfw.create_context(800, 600, "New World Disorder");
	
	{
		auto icons = std::vector<graphics::Image_rgba_u_8>();
		
		for (const auto& dir_entry : std::filesystem::directory_iterator("src/main/resources/icons"))
		{
			if (dir_entry.path().extension() == ".png")
			{
				icons.push_back(graphics::png::read_top_down(utility::fileio::read_binary(dir_entry.path())));
			}
		}
		
		context.set_icon(icons);
	}
	
	context.set_cursor(graphics::png::read_top_down(utility::fileio::read_binary("src/main/resources/cursors/mouse.png")));
	
	context.make_current();
	
	if (opengl_debug_profile)
	{
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(&graphics::opengl::opengl_debug_callback, nullptr);
	}
	
	glEnable(GL_CULL_FACE);
	
	glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
	
	glDepthFunc(GL_LEQUAL);
	
	struct Vertex_data
	{
		std::array<std::int16_t, 3> vertex_position_;
		std::array<std::uint16_t, 2> texture_coordinates_;
		std::array<std::uint64_t, 1> handle_;
	};
	
	auto vao = graphics::opengl::Vertex_array::create();
	auto vbo = graphics::opengl::Buffer::create();
	vao.format_bind_data_buffer(0,
		std::tuple_size_v<decltype(Vertex_data::vertex_position_)>,
		graphics::opengl::enum_of<decltype(Vertex_data::vertex_position_)::value_type>,
		true,
		offsetof(Vertex_data, vertex_position_),
		vbo, 0, sizeof(Vertex_data)
	);
	vao.format_bind_data_buffer(1,
		std::tuple_size_v<decltype(Vertex_data::texture_coordinates_)>,
		graphics::opengl::enum_of<decltype(Vertex_data::texture_coordinates_)::value_type>,
		true,
		offsetof(Vertex_data, texture_coordinates_),
		vbo, 0, sizeof(Vertex_data)
	);
	vao.format_l_bind_data_buffer(2,
		std::tuple_size_v<decltype(Vertex_data::handle_)>,
		// graphics::opengl::enum_of<Vertex_data::Handle::value_type>,
		GL_DOUBLE,
		offsetof(Vertex_data, handle_),
		vbo, 0, sizeof(Vertex_data)
	);
	
	auto imag = graphics::png::read_bottom_up(utility::fileio::read_binary("src/main/resources/icons/main-logo-256.png"));
	auto texture = graphics::opengl::Texture_2D(256, 256);
	texture.storage(graphics::opengl::Texture::Format::rgb_u_3_3_2);
	texture.upload(graphics::opengl::Texture::Format::rgba_u_8, imag.data().get());
	auto bindless = graphics::opengl::Texture_handle::Resident(graphics::opengl::Texture_handle(texture));
	
	auto vertices = std::to_array<Vertex_data>(
	{
		{{graphics::opengl::nrm_min(), graphics::opengl::nrm_max(), 0}, {0, graphics::opengl::nrm_max()}, {bindless.get()}},
		{{graphics::opengl::nrm_min(), graphics::opengl::nrm_min(), 0}, {0, 0}, {bindless.get()}},
		{{graphics::opengl::nrm_max(), graphics::opengl::nrm_min(), 0}, {graphics::opengl::nrm_max(), 0}, {bindless.get()}},
		
		{{graphics::opengl::nrm_min(), graphics::opengl::nrm_max(), 0}, {0, graphics::opengl::nrm_max()}, {bindless.get()}},
		{{graphics::opengl::nrm_max(), graphics::opengl::nrm_min(), 0}, {graphics::opengl::nrm_max(), 0}, {bindless.get()}},
		{{graphics::opengl::nrm_max(), graphics::opengl::nrm_max(), 0}, {graphics::opengl::nrm_max(), graphics::opengl::nrm_max()}, {bindless.get()}},
		
		{{graphics::opengl::nrm(-0.5f), graphics::opengl::nrm(0.5f), 0}, {0, graphics::opengl::nrm_max()}, {bindless.get()}},
		{{graphics::opengl::nrm(-0.5f), graphics::opengl::nrm(-0.5f), 0}, {0, 0}, {bindless.get()}},
		{{graphics::opengl::nrm(0.5f), graphics::opengl::nrm(-0.5f), 0}, {graphics::opengl::nrm_max(), 0}, {bindless.get()}},
		
		{{graphics::opengl::nrm(-0.5f), graphics::opengl::nrm(0.5f), 0}, {0, graphics::opengl::nrm_max()}, {bindless.get()}},
		{{graphics::opengl::nrm(0.5f), graphics::opengl::nrm(-0.5f), 0}, {graphics::opengl::nrm_max(), 0}, {bindless.get()}},
		{{graphics::opengl::nrm(0.5f), graphics::opengl::nrm(0.5f), 0}, {graphics::opengl::nrm_max(), graphics::opengl::nrm_max()}, {bindless.get()}},
	});
	
	vbo.data(std::as_bytes(std::span(vertices)));
	
	auto program = graphics::opengl::Program {
		graphics::opengl::Vertex_shader(std::ifstream("src/main/glsl/bindless.vert.glsl")),
		graphics::opengl::Fragment_shader(std::ifstream("src/main/glsl/bindless.frag.glsl")),
	};
	
	auto framebuffer = Framebuffer_context();
	
	context.key_handler([&](graphics::glfw::Keyboard_key key, [[maybe_unused]] std::int32_t scancode,
		graphics::glfw::Action action, [[maybe_unused]] std::int32_t modifier_keys) -> void
	{
		if (action == graphics::glfw::action::press)
		{
			if (key == graphics::glfw::keys::key_o and action == graphics::glfw::action::press)
			{
				framebuffer.rescale(framebuffer.width_ * 2, framebuffer.height_ * 2);
			}
			else if (key == graphics::glfw::keys::key_p)
			{
				framebuffer.rescale(framebuffer.width_ / 2, framebuffer.height_ / 2);
			}
			
			std::cout << framebuffer.width_ << " : " << framebuffer.height_ << "\n";
		}
	});
	
	while (not context.should_close())
	{
		auto viewport = std::array<GLint, 4>();
		
		{
			auto framebuffer_bound = framebuffer.framebuffer_.bind(graphics::opengl::Framebuffer::Target::read_draw);
			vao.bind();
			program.use();
			
			glGetIntegerv(GL_VIEWPORT, std::data(viewport));
			glViewport(0, 0, framebuffer.width_, framebuffer.height_);
			
			glDepthMask(true);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);
			glDisable(GL_BLEND);
			
			glDrawArrays(GL_TRIANGLES, 0, std::ssize(vertices));
		}
		
		////////////////////////////////////////////////////////////////////////
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		glDepthMask(false);
		
		// TODO draw transparent objects
		
		////////////////////////////////////////////////////////////////////////
		
		framebuffer.vao_.bind();
		framebuffer.program_.use();
		
		glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
		glClear(GL_COLOR_BUFFER_BIT);
		
		glDisable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDrawArrays(GL_TRIANGLES, 0, std::ssize(framebuffer.framebuffer_data));
		
		////////////////////////////////////////////////////////////////////////
		
		// TODO draw gui elements
		
		////////////////////////////////////////////////////////////////////////
		
		context.swap_buffers();
		context.poll_events();
	}
}
