/// g++ -std=c++2a $(pkg-config --cflags --libs Qt5Gui Qt5Widgets) src/main/cpp/application/qt.cpp

#include <QApplication>
#include <QPushButton>
#include <QStyle>
#include <QScreen>
#include <QComboBox>
#include <QGroupBox>
#include <QGridLayout>
#include <QRadioButton>
#include <QLabel>
#include <QStyleFactory>
#include <QDialogButtonBox>
#include <QCheckBox>

#include <optional>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <memory>

#include <epoxy/gl.h>

#include <nwd/audio/openal.hpp>
#include <nwd/graphics/glfw.hpp>

nwd::graphics::glfw::Library* glfw_library;

static QString QString_from_view(std::string_view view)
{
	return QString::fromUtf8(view.data(), view.size());
}

struct Graphics_group : QGroupBox
{
	struct Screen_options : QGroupBox
	{
		Screen_options()
		{
			setTitle("Screen options");
			setLayout(&layout_);
			
			layout_.setAlignment(Qt::AlignTop);
			
			layout_.addWidget(&fullscreen_);
			layout_.addWidget(&windowed_);
			layout_.addWidget(&windowed_fullscreen_);
			
			fullscreen_.setChecked(true);
		}
		
		QVBoxLayout layout_;
		
		QRadioButton fullscreen_ {"Fullscreen"};
		QRadioButton windowed_ {"Windowed"};
		QRadioButton windowed_fullscreen_ {"Windowed fullscreen"};
	};
	
	struct Monitors : QGroupBox
	{
		Monitors()
		{
			setTitle("Monitors");
			setLayout(&layout_);
			
			selection_.setParent(this);
			
			layout_.setAlignment(Qt::AlignTop);
			layout_.addWidget(&selection_);
			
			for (nwd::graphics::glfw::Monitor monitor : glfw_library->monitors())
			{
				selection_.addItem(QString_from_view(monitor.name()));
			}
		}
		
		QVBoxLayout layout_;
		
		QComboBox selection_;
	};
	
	Graphics_group()
	{
		setTitle("Graphics");
		setLayout(&layout_);
		
		layout_.setAlignment(Qt::AlignTop);
		
		{
			std::cout << glGetString(GL_VERSION) << "\n";
			opengl_version_.setText(QString("OpenGL version: %1")
				.arg(reinterpret_cast<const char*>(glGetString(GL_VERSION))))
			;
			renderer_.setText(QString("OpenGL renderer: %1")
				.arg(reinterpret_cast<const char*>(glGetString(GL_RENDERER))))
			;
			auto version_compiled = nwd::graphics::glfw::Library::version_compiled;
			glfw_version_compiled_.setText(QString("GLFW version (compiled): %1.%2.%3")
				.arg(version_compiled.major_).arg(version_compiled.minor_).arg(version_compiled.revision_))
			;
			
			auto version_runtime = nwd::graphics::glfw::Library::version_runtime();
			glfw_version_runtime_.setText(QString("GLFW version (runtime): %1.%2.%3")
				.arg(version_runtime.major_).arg(version_runtime.minor_).arg(version_runtime.revision_))
			;
		}
		
		layout_.addWidget(&opengl_version_);
		layout_.addWidget(&renderer_);
		layout_.addWidget(&glfw_version_compiled_);
		layout_.addWidget(&glfw_version_runtime_);
		layout_.addWidget(&screen_options_);
		layout_.addWidget(&monitors_);
	}
	
	QVBoxLayout layout_;
	
	QLabel opengl_version_;
	QLabel renderer_;
	QLabel glfw_version_compiled_;
	QLabel glfw_version_runtime_;
	Screen_options screen_options_;
	Monitors monitors_;
};

struct Audio_group : QGroupBox
{
	constexpr static std::string_view no_sound_device = "(No sound device)";
	
	Audio_group()
	{
		setTitle("Sound device");
		setLayout(&layout_);
		
		selection_.setParent(this);
		
		layout_.setAlignment(Qt::AlignTop);
		
		layout_.addWidget(&selection_);
		layout_.addWidget(&vendor_);
		layout_.addWidget(&version_);
		layout_.addWidget(&renderer_);
		
		QComboBox::connect(&selection_, &QComboBox::currentTextChanged, [&](const QString& qstring)
		{
			QString vendor_string("Vendor: %1");
			QString version_string("Version: %1");
			QString renderer_string("Renderer: %1");
			
			if (qstring == no_sound_device.data())
			{
				std::cout << "nyes" << "\n";
				context_.reset();
				device_.reset();
				
				vendor_string = vendor_string.arg("");
				version_string = version_string.arg("");
				renderer_string = renderer_string.arg("");
			}
			else
			{
				std::cout << "new" << "\n";
				context_.reset();
				device_.emplace(qstring.toUtf8());
				context_.emplace(*device_).make_current();
				
				vendor_string = vendor_string.arg(QString_from_view(context_->vendor()));
				version_string = version_string.arg(QString_from_view(context_->version()));
				renderer_string = renderer_string.arg(QString_from_view(context_->renderer()));
			}
			
			vendor_.setText(vendor_string);
			version_.setText(version_string);
			renderer_.setText(renderer_string);
		});
		
		for (std::string_view name : nwd::audio::openal::Device::names())
		{
			selection_.addItem(QString(name.data()));
		}
		
		selection_.addItem(no_sound_device.data());
	}
	
	QVBoxLayout layout_;
	
	QComboBox selection_;
	QLabel vendor_;
	QLabel version_;
	QLabel renderer_;
	QLabel extensions_;

	std::optional<nwd::audio::openal::Device> device_;
	std::optional<nwd::audio::openal::Context> context_;
};

struct Window : QWidget
{
	struct Main_grid : QWidget
	{
		Main_grid()
		{
			setLayout(&layout_);
			
			layout_.addWidget(&graphics_group_, 0, 0);
			layout_.addWidget(&audio_group_, 0, 1);
		}
		
		QGridLayout layout_;
		
		Graphics_group graphics_group_;
		Audio_group audio_group_;
	};
	
	struct Buttons : QWidget
	{
		Buttons()
		{
			setLayout(&layout_);
			
			buttons_.setParent(this);
			
			button_defaults_ = buttons_.addButton("Defaults", QDialogButtonBox::ResetRole);
			buttons_.setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Apply | QDialogButtonBox::Cancel);
			layout_.addWidget(button_defaults_);
			layout_.addWidget(&buttons_);
		}
		
		QHBoxLayout layout_;
		
		QPushButton* button_defaults_ = nullptr;
		QDialogButtonBox buttons_;
	};
	
	Window()
	{
		setLayout(&layout_);
		
		layout_.addWidget(&main_grid_);
		
		// Expand or not...
		layout_.setAlignment(&main_grid_, Qt::AlignTop);
		// layout_.setStretchFactor(&main_grid_, 1);
		
		layout_.addWidget(&buttons_);
		layout_.setAlignment(&buttons_, Qt::AlignBottom);
	}
	
	QVBoxLayout layout_;
	
	Main_grid main_grid_;
	Buttons buttons_;
};

int main(int argc, char** argv)
{
	auto glfw_library = nwd::graphics::glfw::Library();
	::glfw_library = &glfw_library;
	
	glfw_library.window_hint(nwd::graphics::glfw::Context::Hint::opengl_debug_profile);
	glfw_library.window_hint(nwd::graphics::glfw::Context::Hint::opengl_forward_compatibility);
	glfw_library.window_hint(nwd::graphics::glfw::Context::Hint::opengl_core_profile);
	glfw_library.window_hint(nwd::graphics::glfw::Context::Hint::opengl_major_version(4));
	glfw_library.window_hint(nwd::graphics::glfw::Context::Hint::opengl_minor_version(5));
	glfw_library.window_hint(nwd::graphics::glfw::Context::Hint::visible(false));
	
	auto opengl_context = glfw_library.create_context(1000, 500, "hmm?");
	opengl_context.make_current();
	GLint num_ext = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &num_ext);
	
	std::cout << "num ext " << num_ext << "\n";
	
	for (GLint i = 0; i != num_ext; ++i)
	{
		std::cout << reinterpret_cast<const char*>(glGetStringi(GL_EXTENSIONS, i)) << "\n";
	}
	
	std::cout << epoxy_has_gl_extension("GL_ARB_bindless_texture") << "\n";
	
	std::cout << reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS)) << "\n";
	
	{
		QCoreApplication::setApplicationName("Launcher");
		auto application = QApplication(argc, argv);
		
		for (auto& qs : QStyleFactory::keys())
		{
			std::cout << "available style: " << qs.toStdString() << "\n";
		}
#ifdef _WIN32
		// The spec says to call this before starting the application but then it
		// doesn't find the windowsvista style
		std::cout << QApplication::setStyle("windowsvista") << "\n";
#endif
		auto window = std::make_unique<Window>();
		
		window->show();
		// Center window after displaying it
		window->move(window->pos() + (QGuiApplication::primaryScreen()->geometry().center() - window->geometry().center()));
		
		QObject::connect(window->buttons_.buttons_.button(QDialogButtonBox::Ok), &QPushButton::clicked, [&]() -> void
		{
			window->close();
		});
		QObject::connect(window->buttons_.buttons_.button(QDialogButtonBox::Cancel), &QPushButton::clicked, [&]() -> void
		{
			window->close();
		});
		
		return application.exec();
	}
	
// 	execv("vulkan", argv);
}
