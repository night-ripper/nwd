#pragma once

#include <array>
#include <tuple>

namespace nwd::geometry
{
struct coord_t;
struct angle_t;

struct Vector3D;
struct Line_segment3D;
struct Triangle3D;
struct Sphere3D;
struct Cylinder3D;

struct Bounding_box3D;

angle_t angle(Vector3D vector1, Vector3D vector2)  noexcept;

angle_t angle(Vector3D vector, Line_segment3D line) noexcept;
angle_t angle(Line_segment3D line, Vector3D vector) noexcept;

bool overlap(Vector3D lhs, Vector3D rhs) noexcept;
bool overlap(Vector3D lhs, Line_segment3D rhs) noexcept;
bool overlap(Vector3D lhs, Triangle3D rhs) noexcept;
bool overlap(Vector3D lhs, Sphere3D rhs) noexcept;
bool overlap(Vector3D lhs, Cylinder3D rhs) noexcept;

bool overlap(Line_segment3D lhs, Vector3D rhs) noexcept;
bool overlap(Line_segment3D lhs, Line_segment3D rhs) noexcept;
bool overlap(Line_segment3D lhs, Triangle3D rhs) noexcept;
bool overlap(Line_segment3D lhs, Sphere3D rhs) noexcept;
bool overlap(Line_segment3D lhs, Cylinder3D rhs) noexcept;
bool overlap(Line_segment3D lhs, Bounding_box3D rhs) noexcept;

bool overlap(Triangle3D lhs, Vector3D rhs) noexcept;
bool overlap(Triangle3D lhs, Line_segment3D rhs) noexcept;
bool overlap(Triangle3D lhs, Triangle3D rhs) noexcept;
bool overlap(Triangle3D lhs, Sphere3D rhs) noexcept;
bool overlap(Triangle3D lhs, Cylinder3D rhs) noexcept;
bool overlap(Triangle3D lhs, Bounding_box3D rhs) noexcept;

bool overlap(Sphere3D lhs, Vector3D rhs) noexcept;
bool overlap(Sphere3D lhs, Line_segment3D rhs) noexcept;
bool overlap(Sphere3D lhs, Triangle3D rhs) noexcept;
bool overlap(Sphere3D lhs, Sphere3D rhs) noexcept;
bool overlap(Sphere3D lhs, Cylinder3D rhs) noexcept;
bool overlap(Sphere3D lhs, Bounding_box3D rhs) noexcept;

bool overlap(Cylinder3D lhs, Vector3D rhs) noexcept;
bool overlap(Cylinder3D lhs, Line_segment3D rhs) noexcept;
bool overlap(Cylinder3D lhs, Triangle3D rhs) noexcept;
bool overlap(Cylinder3D lhs, Sphere3D rhs) noexcept;
bool overlap(Cylinder3D lhs, Cylinder3D rhs) noexcept;
bool overlap(Cylinder3D lhs, Bounding_box3D rhs) noexcept;

bool overlap(Bounding_box3D lhs, Line_segment3D rhs) noexcept;
bool overlap(Bounding_box3D lhs, Triangle3D rhs) noexcept;
bool overlap(Bounding_box3D lhs, Sphere3D rhs) noexcept;
bool overlap(Bounding_box3D lhs, Cylinder3D rhs) noexcept;

bool contains(Sphere3D outer, Vector3D inner) noexcept;
bool contains(Sphere3D outer, Line_segment3D inner) noexcept;
bool contains(Sphere3D outer, Triangle3D inner) noexcept;
bool contains(Sphere3D outer, Sphere3D inner) noexcept;
bool contains(Sphere3D outer, Cylinder3D inner) noexcept;

bool contains(Cylinder3D outer, Vector3D inner) noexcept;
bool contains(Cylinder3D outer, Line_segment3D inner) noexcept;
bool contains(Cylinder3D outer, Triangle3D inner) noexcept;
bool contains(Cylinder3D outer, Sphere3D inner) noexcept;
bool contains(Cylinder3D outer, Cylinder3D inner) noexcept;

bool contains(Bounding_box3D outer, Line_segment3D inner) noexcept;
bool contains(Bounding_box3D outer, Triangle3D inner) noexcept;
bool contains(Bounding_box3D outer, Sphere3D inner) noexcept;
bool contains(Bounding_box3D outer, Cylinder3D inner) noexcept;

Bounding_box3D box_of(Line_segment3D line) noexcept;
Bounding_box3D box_of(Triangle3D triangle) noexcept;
Bounding_box3D box_of(Sphere3D sphere) noexcept;
Bounding_box3D box_of(Cylinder3D cylinder) noexcept;
} // namespace nwd::geometry
