#pragma once

#include <iterator>
#include <type_traits>
#include <ranges>

#include <nwd/geometry/line_segment3d.hpp>
#include <nwd/utility/iterators.hpp>

namespace nwd::geometry
{
template<typename Vertex_iterator>
requires(std::forward_iterator<Vertex_iterator> and std::is_convertible_v<typename std::iterator_traits<Vertex_iterator>::value_type, Vector3D>)
struct Line_iterator3D
{
	using difference_type = typename std::iterator_traits<Vertex_iterator>::difference_type;
	using value_type = Line_segment3D;
	using pointer = void;
	using reference = void;
	using iterator_category = std::forward_iterator_tag;
	
	Vertex_iterator begin_;
	Vertex_iterator end_;
	Vertex_iterator it_;
	
	Line_iterator3D() = default;
	
	constexpr Line_iterator3D(Vertex_iterator begin, Vertex_iterator end) noexcept
		:
		begin_(begin),
		end_(end)
	{
		switch (auto distance = std::distance(begin_, end_))
		{
		case 0: [[fallthrough]];
		case 1:
			break;
		case 2:
			it_ = begin_;
			break;
		default:
			it_ = std::prev(end);
			break;
		}
	}
	
	constexpr bool operator==(std::default_sentinel_t) const noexcept
	{
		return it_ == end_;
	}
	
	constexpr friend bool operator==(Line_iterator3D lhs, Line_iterator3D rhs) noexcept
	{
		return lhs.it_ == rhs.it_;
	}
	
	value_type operator*() const noexcept
	{
		if (it_ == std::prev(end_))
		{
			return Line_segment3D::begin_end(*it_, *(begin_));
		}
		else
		{
			return Line_segment3D::begin_end(*it_, *std::next(it_));
		}
	}
	
	constexpr Line_iterator3D& operator+=(difference_type diff) noexcept
	{
		it_ += diff;
		
		if (it_ == std::prev(end_))
		{
			it_ = {};
		}
		else if (it_ == end_)
		{
			it_ = begin_;
		}
		
		return *this;
	}
	
	constexpr friend Line_iterator3D operator+(Line_iterator3D it, difference_type diff) noexcept
	{
		return it += diff;
	}
	
	constexpr Line_iterator3D& operator++() noexcept
	{
		++it_;
		return *this;
	}
	
	constexpr Line_iterator3D operator++(int) noexcept
	{
		return utility::operators::post_increment(*this);
	}
};

////////////////////////////////////////////////////////////////////////////////

template<typename Vertex_iterator>
requires(std::forward_iterator<Vertex_iterator> and std::is_convertible_v<typename std::iterator_traits<Vertex_iterator>::value_type, Vector3D>)
inline constexpr std::ranges::subrange<Line_iterator3D<Vertex_iterator>, std::default_sentinel_t> line_range(Vertex_iterator begin, Vertex_iterator end)
{
	return std::ranges::subrange(Line_iterator3D<Vertex_iterator>(begin, end), std::default_sentinel);
}

template<typename Vertex_range_type, typename Vertex_iterator = decltype(std::begin(std::declval<Vertex_range_type>()))>
requires(std::is_convertible_v<decltype(*std::begin(std::declval<Vertex_range_type>())), Vector3D>)
inline constexpr std::ranges::subrange<Line_iterator3D<Vertex_iterator>, std::default_sentinel_t> line_range(const Vertex_range_type& range)
{
	return line_range(std::begin(range), std::end(range));
}
} // namespace nwd::geometry
