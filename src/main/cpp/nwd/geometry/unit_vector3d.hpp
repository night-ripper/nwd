#pragma once

#include <cmath>
#include <cstdint>

#include <array>
#include <ostream>

#include <nwd/geometry/units.hpp>
#include <nwd/serialization/fwd.hpp>

namespace nwd::geometry
{
struct Unit_vector3D
{
	constexpr static int coord_digits = 64 / 3;
	constexpr static std::uint64_t coord_bitfield = 0b1'1111'1111'1111'1111'1111;
	constexpr static float multiplier = 1.L / (coord_bitfield >> 1);
	
	constexpr Unit_vector3D() noexcept = default;
	
	constexpr Unit_vector3D(const Unit_vector3D&) = default;
	constexpr Unit_vector3D& operator=(const Unit_vector3D&) = default;
	
	constexpr Unit_vector3D(float x, float y, float z) noexcept
		:
		bitfield_()
	{
		bitfield_ |= (std::uint64_t(x * float(coord_bitfield)) << (0 * coord_digits));
		bitfield_ |= (std::uint64_t(y * float(coord_bitfield)) << (1 * coord_digits));
		bitfield_ |= (std::uint64_t(z * float(coord_bitfield)) << (2 * coord_digits));
	}
	
	constexpr float operator[](std::int8_t index) const noexcept
	{
		auto result = std::uint32_t((bitfield_ & (coord_bitfield << (coord_digits * index))) >> (coord_digits * index));
		bool sign = (result & 1);
		result >>= 1;
		result = std::bit_cast<std::uint32_t>(float(result) * multiplier);
		result |= (std::uint32_t(sign) << 31);
		return std::bit_cast<float>(result);
	}
	
	constexpr float x() const noexcept {return (*this)[0];}
	constexpr float y() const noexcept {return (*this)[1];}
	constexpr float z() const noexcept {return (*this)[2];}
	
// private:
	std::uint64_t bitfield_;
};
} // namespace nwd::geometry
