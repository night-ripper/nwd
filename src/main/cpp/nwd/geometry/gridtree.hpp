#pragma once

#include <cstring>
#include <cstddef>
#include <cstdint>

#include <atomic>
#include <bitset>
#include <ostream>
#include <type_traits>
#include <utility>
#include <stdexcept>

#include <nwd/geometry/bounding_box3d.hpp>

#include <nwd/containers/repository.hpp>
#include <nwd/containers/nodes/linked.hpp>

#include <nwd/utility/invoker.hpp>
#include <nwd/utility/iterators.hpp>

namespace nwd::geometry
{
namespace gridtrees
{
template<std::ptrdiff_t Bitset_size>
struct Path : std::bitset<Bitset_size>
{
	using std::bitset<Bitset_size>::bitset;
	
	friend auto operator<=>(Path lhs, Path rhs) noexcept
	{
		return std::memcmp(&lhs, &rhs, sizeof(Path));
	}
	
	std::int16_t array_index() const noexcept
	{
		return std::int16_t(((*this) & Path(0b111)).to_ulong());
	}
	
	friend std::ostream& operator<<(std::ostream& os, Path self)
	{
		int count = 0;
		
		while (self != Path(1))
		{
			if (count == 4)
			{
				os << ',';
				count = 0;
			}
			os << self.array_index();
			self >>= 3;
			++count;
		}
		
		return os;
	}
};

template<typename Size_type>
struct Handle
{
	Size_type node_index_;
	Size_type object_index_;
};
} // namespace gridtrees

template<
	typename Object_type,
	typename Size_type = std::int32_t,
	std::int16_t Bitset_size = 64,
	std::int16_t Box_divisor = 16,
	std::int16_t Box_larger_multiplier = 9>
struct Gridtree
{
	using object_type = Object_type;
	using value_type = std::pair<object_type, const Bounding_box3D>;
	
	constexpr static std::int16_t bitset_size = Bitset_size;
	constexpr static std::int16_t box_divisor = Box_divisor;
	constexpr static std::int16_t box_larger_multiplier = Box_larger_multiplier;
	constexpr static std::int16_t box_smaller_multiplier = box_divisor - Box_larger_multiplier;
	
	using path_type = gridtrees::Path<bitset_size>;
	using size_type = Size_type;
	
	using Handle = gridtrees::Handle<size_type>;
	
private:
	constexpr static std::int16_t array_size = 8;
	
	struct node_type
	{
		path_type path_;
		size_type first_ = -1;
		size_type last_ = -1;
		size_type parent_;
		size_type indices_[array_size] {-1, -1, -1, -1, -1, -1, -1, -1};
		std::int16_t depth_;
		Bounding_box3D box_;
		
		bool empty() const noexcept
		{
			return first_ == -1;
		}
		
		node_type(path_type path, std::int16_t depth, size_type parent, Bounding_box3D box) noexcept
			:
			path_(path),
			parent_(parent),
			depth_(depth),
			box_(box)
		{
		}
	};
	
	//! Cast signed value to unsigned
	//! TODO use utility::integer_cast after testing it
	template<typename Type>
	requires(std::is_signed_v<Type>)
	constexpr static std::make_unsigned_t<Type> cast_s_u(Type val) noexcept
	{
		return static_cast<std::make_unsigned_t<Type>>(val) + std::numeric_limits<Type>::max() + 1;
	}

	static std::make_unsigned_t<coord_t::underlying_type> box_fraction(Bounding_box3D box) noexcept
	{
		return (cast_s_u(box.max()[0].value()) - cast_s_u(box.min()[0].value())) / box_divisor;
	}
	
	static Bounding_box3D reduce_box(Bounding_box3D box, std::int16_t array_index) noexcept
	{
		Vector3D minv = box.min();
		Vector3D maxv = box.max();
		
		auto fraction = box_fraction(box);
		
		for (std::size_t ax = 0; ax != 3; ++ax)
		{
			if (array_index & (1 << ax))
			{
				minv[ax].value() = coord_t::underlying_type(box.min()[ax].value() + std::ptrdiff_t(box_smaller_multiplier * fraction));
			}
			else
			{
				maxv[ax].value() = coord_t::underlying_type(box.min()[ax].value() + std::ptrdiff_t(box_larger_multiplier * fraction));
			}
		}
		
		return Bounding_box3D(minv, maxv);
	}
	
	static std::uint8_t reduce_box_candidate_set(Bounding_box3D outer, Bounding_box3D box) noexcept
	{
		constexpr std::uint8_t masks[]
		{
			0b0101'0101,
			0b0011'0011,
			0b0000'1111,
		};
		
		auto outer_fraction = box_fraction(outer);
		
		auto result = std::uint8_t(-1);
		
		for (std::size_t ax = 0; ax < 3; ++ax)
		{
			if (box_larger_multiplier * outer_fraction + cast_s_u(outer.min()[ax].value())
				< cast_s_u(box.max()[ax].value()))
			{
				result &= ~masks[ax];
			}
			
			if (cast_s_u(box.min()[ax].value())
				< box_smaller_multiplier * outer_fraction + cast_s_u(outer.min()[ax].value()))
			{
				result &= masks[ax];
			}
		}
		
		return result;
	}
	
	static Bounding_box3D box_of_path(path_type self) noexcept
	{
		auto result = Bounding_box3D::full();
		
		while (self != path_type(1))
		{
			result = reduce_box(result, self.array_index());
			self >>= 3;
		}
		
		return result;
	}
	
	using Node_repository_type = containers::Repository<node_type, size_type>;
	using Object_repository_type = containers::Repository<containers::nodes::linked::Node<value_type, size_type>, size_type>;
	
	struct Object_iterator
	{
		using reference = const value_type&;
		using difference_type = std::ptrdiff_t;
		
		const typename Object_repository_type::value_type* objects_;
		size_type index_;
		
		bool operator==(std::default_sentinel_t) const noexcept
		{
			return index_ == -1;
		}
		
		friend bool operator==(Object_iterator lhs, Object_iterator rhs) noexcept = default;
		
		reference operator*() const
		{
			return containers::nodes::linked::value_of(objects_[index_]);
		}
		
		Object_iterator& operator++()
		{
			index_ = containers::nodes::linked::get_next(objects_[index_]);
			return *this;
		}
		
		Object_iterator operator++(int) {return utility::operators::post_increment(*this);}
	};
	
	std::ranges::subrange<Object_iterator, std::default_sentinel_t> objects(size_type node_index) const
	{
		return std::ranges::subrange(Object_iterator(objects_.data(), nodes_[node_index].first_), std::default_sentinel);
	}
	
	void visit_node(size_type index, auto& filter, auto& visitor) const
	{
		if (filter(std::as_const(nodes_[index].box_)))
		{
			for (auto& pair : objects(index))
			{
				if (filter(pair.second))
				{
					visitor(pair.first);
				}
			}
			
			for (auto node_index : nodes_[index].indices_)
			{
				if (node_index != -1)
				{
					visit_node(node_index, filter, visitor);
				}
			}
		}
	}
	
	struct Path_computation
	{
		Bounding_box3D outer_;
		Bounding_box3D box_;
		path_type result_ = 0;
		std::int16_t depth_ = 0;
		
		Path_computation(Bounding_box3D outer, Bounding_box3D box) noexcept
			:
			outer_(outer),
			box_(box)
		{
		}
		
		Path_computation(Bounding_box3D box) noexcept
			:
			Path_computation(Bounding_box3D::full(), box)
		{
		}
		
		Path_computation(const node_type& node, Bounding_box3D box) noexcept
			:
			Path_computation(node.box_, box)
		{
			result_ = node.path_;
			depth_ = std::int16_t(bitset_size - std::countl_zero(result_.to_ulong()) - 1);
			result_.reset(std::size_t(depth_));
		}
		
		path_type get_result() const noexcept
		{
			auto result = result_;
			result.set(std::size_t(depth_));
			return result;
		}
		
		bool is_leaf() const noexcept
		{
			return depth_ >= bitset_size - 3;
		}
		
		void move_down(std::int16_t array_index) noexcept
		{
			outer_ = reduce_box(outer_, array_index);
			result_ |= (path_type(std::size_t(array_index)) << std::size_t(depth_));
			depth_ += 3;
		}
		
		std::int16_t operator()()
		{
			if (is_leaf())
			{
				return -1;
			}
			
			auto candidate_set = reduce_box_candidate_set(outer_, box_);
			
			if (candidate_set == 0)
			{
				return -1;
			}
			
			auto array_index = std::int16_t(std::countr_zero(candidate_set));
			
			move_down(array_index);
			
			return array_index;
		}
	};
	
	size_type insert_new_path_from(size_type node_index, Bounding_box3D box)
	{
		auto cmp = Path_computation(nodes_[node_index], box);
		
		std::uint8_t candidate_set = reduce_box_candidate_set(cmp.outer_, box);
		auto array_index = std::int16_t(std::countr_zero(candidate_set));
		
		while (not cmp.is_leaf() and candidate_set != 0)
		{
			cmp.move_down(std::int16_t(std::countr_zero(candidate_set)));
			candidate_set = reduce_box_candidate_set(cmp.outer_, box);
		}
		
		if (array_index < 8)
		{
			auto descendant = nodes_[node_index].indices_[array_index];
			
			if (descendant == -1)
			{
				descendant = nodes_.emplace(cmp.get_result(), cmp.depth_, node_index, cmp.outer_);
				nodes_[node_index].indices_[array_index] = descendant;
			}
			
			node_index = descendant;
		}
		
		return node_index;
	}
	
	size_type find_to_insert_from(size_type node_index, Bounding_box3D box)
	{
		auto cmp = Path_computation(nodes_[node_index], box);
		
		std::uint8_t candidate_set = reduce_box_candidate_set(cmp.outer_, box);
		
		if (not cmp.is_leaf() and candidate_set != 0)
		{
			auto array_index = std::int16_t(std::countr_zero(candidate_set));
			
			if (not std::has_single_bit(candidate_set))
			{
				for (std::int16_t i = 0; i != 8; ++i)
				{
					if ((candidate_set & (1 << i)) and nodes_[node_index].indices_[i] != -1)
					{
						array_index = i;
						break;
					}
				}
			}
			
			auto descendant = nodes_[node_index].indices_[array_index];
			
			if (descendant == -1)
			{
				return insert_new_path_from(node_index, box);
			}
			else if (contains(nodes_[descendant].box_, box))
			{
				return find_to_insert_from(descendant, box);
			}
			else
			{
				auto descendant_path = nodes_[descendant].path_;
				descendant_path >>= std::size_t(nodes_[node_index].depth_);
				descendant_path >>= 3;
				cmp.move_down(array_index);
				
				while (reduce_box_candidate_set(cmp.outer_, box) & (1 << descendant_path.array_index()))
				{
					cmp.move_down(descendant_path.array_index());
					descendant_path >>= 3;
				}
				
				auto new_node_index = nodes_.emplace(cmp.get_result(), cmp.depth_, node_index, cmp.outer_);
				nodes_[descendant].parent_ = new_node_index;
				nodes_[node_index].indices_[array_index] = new_node_index;
				nodes_[new_node_index].indices_[descendant_path.array_index()] = descendant;
				
				return insert_new_path_from(new_node_index, box);
			}
		}
		
		return node_index;
	}
	
	size_type find_to_insert(Bounding_box3D box)
	{
		auto cmp = Path_computation(box);
		auto root_path = nodes_[root_].path_;
		
		while (reduce_box_candidate_set(cmp.outer_, box) & (1 << root_path.array_index()))
		{
			cmp.move_down(root_path.array_index());
			root_path >>= 3;
		}
		
		if (root_path != path_type(1))
		{
			auto old_root = root_;
			auto path = cmp.get_result();
			Bounding_box3D outer = cmp.outer_;
			root_ = nodes_.emplace(path, cmp.depth_, -1, outer);
			size_type node_index = root_;
			
			nodes_[node_index].indices_[root_path.array_index()] = old_root;
			nodes_[old_root].parent_ = node_index;
		}
		
		return insert_new_path_from(root_, box);
	}
	
	//! If there is a single value in the @param array different from -1, returns
	//! its position, otherwise returns -1.
	std::int16_t find_single_index(auto&& array)
	{
		std::int16_t descendant_index = -1;
		
		for (std::int16_t i = 0; i != std::ssize(array); ++i)
		{
			if (array[i] != -1)
			{
				if (descendant_index != -1)
				{
					descendant_index = -1;
					break;
				}
				
				descendant_index = i;
			}
		}
		
		return descendant_index;
	}
	
	void erase_upwards(size_type node_index)
	{
		while (nodes_[node_index].empty()
			and std::ranges::all_of(nodes_[node_index].indices_, [](auto value) -> bool
				{return value == -1;}))
		{
			auto parent_index = nodes_[node_index].parent_;
			
			if (parent_index != -1)
			{
				for (auto& idx : nodes_[parent_index].indices_)
				{
					if (idx == node_index)
					{
						idx = -1;
						break;
					}
				}
			}
			
			nodes_.erase(node_index);
			
			if (nodes_.empty())
			{
				root_ = -1;
				return;
			}
			
			if (parent_index == -1)
			{
				break;
			}
			
			node_index = parent_index;
		}
		
		if (nodes_[node_index].empty())
		{
			std::int16_t descendant_array_index = find_single_index(nodes_[node_index].indices_);
			auto parent_index = nodes_[node_index].parent_;
			
			if (parent_index != -1 and descendant_array_index != -1)
			{
				size_type descendant_index = nodes_[node_index].indices_[descendant_array_index];
				
				auto parent_index = nodes_[node_index].parent_;
				
				for (auto& idx : nodes_[parent_index].indices_)
				{
					if (idx == node_index)
					{
						idx = descendant_index;
						break;
					}
				}
				
				nodes_[descendant_index].parent_ = parent_index;
				
				nodes_.erase(node_index);
				
				node_index = parent_index;
			}
		}
		
		if (node_index == root_)
		{
			while (nodes_[root_].empty())
			{
				std::int16_t descendant_index = find_single_index(nodes_[root_].indices_);
				
				if (descendant_index == -1)
				{
					break;
				}
				
				auto new_root = nodes_[root_].indices_[descendant_index];
				nodes_.erase(root_);
				root_ = new_root;
			}
			
			nodes_[root_].parent_ = -1;
		}
	}
	
	void append_object(size_type node_index, size_type object_index)
	{
		auto& node = nodes_[node_index];
		containers::nodes::linked::link_back(objects_.data(), object_index, node.last_);
		node.last_ = object_index;
		
		if (node.first_ == -1)
		{
			node.first_ = object_index;
		}
	}
	
	void unlink_object(Handle handle)
	{
		auto [prev, next] = containers::nodes::linked::unlink(objects_.data(), handle.object_index_);
		
		auto& first = nodes_[handle.node_index_].first_;
		auto& last = nodes_[handle.node_index_].last_;
		
		if (first == handle.object_index_)
		{
			first = next;
		}
		
		if (last == handle.object_index_)
		{
			last = prev;
		}
	}
	
	size_type walk_up(size_type node_index, Bounding_box3D box)
	{
		while (node_index != -1 and not contains(nodes_[node_index].box_, box))
		{
			node_index = nodes_[node_index].parent_;
		}
		
		return node_index;
	}
	
	Handle private_insert(auto&& object, Bounding_box3D box)
	{
		size_type node_index;
		
		if (nodes_.empty())
		{
			auto cmp = Path_computation(box);
			while (cmp() != -1);
			node_index = nodes_.emplace(cmp.get_result(), cmp.depth_, -1, cmp.outer_);
			root_ = 0;
		}
		else if (contains(nodes_[root_].box_, box))
		{
			node_index = find_to_insert_from(root_, box);
		}
		else
		{
			node_index = find_to_insert(box);
		}
		
		auto object_index = objects_.emplace(std::forward<decltype(object)>(object), box);
		append_object(node_index, object_index);
		
		return {node_index, object_index};
	}
	
	Handle private_insert(Handle near, auto&& object, Bounding_box3D box)
	{
		auto node_index = walk_up(near.node_index_, box);
		
		if (node_index != -1)
		{
			node_index = find_to_insert_from(node_index, box);
		}
		else
		{
			node_index = find_to_insert(box);
		}
		
		auto object_index = objects_.emplace(std::forward<decltype(object)>(object), box);
		append_object(node_index, object_index);
		
		return {node_index, object_index};
	}
	
public:
	object_type& operator[](Handle handle) noexcept
	{
		return std::get<0>(containers::nodes::linked::value_of(objects_[handle.object_index_]));
	}
	
	const object_type& operator[](Handle handle) const noexcept
	{
		return std::get<0>(containers::nodes::linked::value_of(objects_[handle.object_index_]));
	}
	
	Bounding_box3D box_of(Handle handle) const noexcept
	{
		return std::get<1>(containers::nodes::linked::value_of(objects_[handle.object_index_]));
	}
	
	Handle insert(const object_type& object, Bounding_box3D box)
	{
		return private_insert(object, box);
	}
	
	Handle insert(object_type&& object, Bounding_box3D box)
	{
		return private_insert(std::move(object), box);
	}
	
	Handle insert(Handle near, const object_type& object, Bounding_box3D box)
	{
		return private_insert(near, object, box);
	}
	
	Handle insert(Handle near, object_type&& object, Bounding_box3D box)
	{
		return private_insert(near, std::move(object), box);
	}
	
	Handle transform(Handle handle, Bounding_box3D new_box)
	{
		auto node_index = walk_up(handle.node_index_, new_box);
		
		if (node_index != handle.node_index_)
		{
			if (node_index != -1)
			{
				node_index = find_to_insert_from(node_index, new_box);
			}
			else
			{
				node_index = find_to_insert(new_box);
			}
			
			unlink_object(handle);
			append_object(node_index, handle.object_index_);
			auto* old_box = &std::get<1>(containers::nodes::linked::value_of(objects_[handle.object_index_]));
			std::destroy_at(old_box);
			std::construct_at(old_box, std::move(new_box));
			erase_upwards(handle.node_index_);
		}
		
		handle.node_index_ = node_index;
		
		return handle;
	}
	
	void erase(Handle handle)
	{
		unlink_object(handle);
		objects_.erase(handle.object_index_);
		erase_upwards(handle.node_index_);
	}
	
	template<typename Filter, typename Visitor>
	requires(std::is_invocable_r_v<bool, Filter, Bounding_box3D>
		and std::is_invocable_v<Visitor, const object_type&>)
	void visit(Filter&& filter, Visitor&& visitor)
	{
		auto guard = Visit_guard(*this);
		
		if (root_ != -1)
		{
			return visit_node(root_, filter, visitor);
		}
	}
	
	template<typename Filter, typename Visitor>
	requires(std::is_invocable_r_v<bool, Filter, Bounding_box3D>
		and std::is_invocable_v<Visitor, const object_type&>)
	void visit_iterative(Filter&& filter, Visitor&& visitor) const
	{
		auto guard = Visit_guard(*this);
		
		auto node_index = root_;
		size_type descendant_index = -1;
		
		while (node_index != -1)
		{
			if (descendant_index != -1 or filter(std::as_const(nodes_[node_index].box_)))
			{
				if (descendant_index == -1)
				{
					for (auto& pair : objects(node_index))
					{
						if (filter(pair.second))
						{
							visitor(pair.first);
						}
					}
				}
				
				std::int16_t array_index = 0;
				
				if (descendant_index != -1)
				{
					while (descendant_index != nodes_[node_index].indices_[array_index])
					{
						++array_index;
					}
					
					++array_index;
				}
				
				while (array_index != array_size)
				{
					auto index = nodes_[node_index].indices_[array_index];
					
					if (index != -1)
					{
						descendant_index = -1;
						node_index = index;
						goto main_loop;
					}
					
					++array_index;
				}
			}
			
			descendant_index = node_index;
			node_index = nodes_[node_index].parent_;
			
			main_loop:;
		}
	}
	
	struct Query
	{
		utility::Invoker<bool(const Bounding_box3D&) const noexcept> filter_;
		Object_iterator it_ = Object_iterator(nullptr, -1);
		size_type node_index_ = -1;
		size_type descendant_index_ = -1;
		typename Node_repository_type::value_type* nodes_ = nullptr;
		
		using difference_type = std::ptrdiff_t;
		using reference = typename Object_iterator::reference;
		using value_type = std::remove_reference_t<reference>;
		using pointer = std::add_pointer_t<value_type>;
		using iterator_category = std::forward_iterator_tag;
		
		Query() = default;
		
		Query(Gridtree& self) noexcept
			:
			it_(self.objects_.data(), -1),
			node_index_(-1),
			nodes_(self.nodes_.data())
		{
		}
		
		Query(Gridtree& self, utility::Invoker<bool(const Bounding_box3D&) const noexcept> filter)
			:
			filter_(filter),
			it_(self.objects_.data(), -1),
			node_index_(self.root_),
			descendant_index_(-1),
			nodes_(self.nodes_.data())
		{
			walk<false>();
		}
		
		bool operator==(std::default_sentinel_t) const noexcept
		{
			return node_index_ == -1;
		}
		
		friend bool operator==(const Query& lhs, const Query& rhs) noexcept
		{
			return lhs.it_ == rhs.it_
				and lhs.node_index_ == rhs.node_index_
				and lhs.nodes_ == rhs.nodes_
			;
		}
		
		reference operator*() const noexcept
		{
			return *it_;
		}
		
		template<bool Jump>
		Query& walk()
		{
			if constexpr (Jump)
			{
				goto inner_label;
			}
			
			while (node_index_ != -1)
			{
				if (descendant_index_ != -1 or filter_(nodes_[node_index_].box_))
				{
					if (descendant_index_ == -1)
					{
						it_.index_ = nodes_[node_index_].first_;
						
						while (it_ != std::default_sentinel)
						{
							if (filter_((*it_).second))
							{
								return *this;
							}
							
							[[maybe_unused]]
							inner_label:;
							++it_;
						}
					}
					
					std::int16_t array_index = 0;
					
					if (descendant_index_ != -1)
					{
						while (descendant_index_ != nodes_[node_index_].indices_[array_index])
						{
							++array_index;
						}
						
						++array_index;
					}
					
					while (array_index != array_size)
					{
						auto index = nodes_[node_index_].indices_[array_index];
						
						if (index != -1)
						{
							descendant_index_ = -1;
							node_index_ = index;
							goto main_loop;
						}
						
						++array_index;
					}
				}
				
				descendant_index_ = node_index_;
				node_index_ = nodes_[node_index_].parent_;
				
				main_loop:;
			}
			
			return *this;
		}
		
		Query& operator++()
		{
			return walk<true>();
		}
		
		Query operator++(int)
		{
			return utility::operators::post_increment(*this);
		}
	};
	
	std::ranges::subrange<Query, std::default_sentinel_t> query(
		utility::Invoker<bool(const Bounding_box3D&) const noexcept> filter)
	{
		return std::ranges::subrange(Query(*this, filter), std::default_sentinel);
	}
	
	Query query_end() noexcept
	{
		return Query(*this);
	}
	
	std::ostream& to_dot(std::ostream& os) const
	{
		const auto& gridtree = *this;
		
		os << "digraph nodes {" << "\n";
		
		const auto print_node = [&](auto i) -> void
		{
			const auto& node = gridtree.nodes_[i];
			os
			<< "node" << i
			<< "[shape=record,label=\"{"
			<< "position: " << i << "\\n"
			<< "parent: " << node.parent_ << "\\n"
			<< "depth: " << node.depth_ << "\\n"
	// 		<< "objects: " << node.objects_.size() << "\\n"
			<< "path: " << node.path_ << "\\n"
			<< "|{box min|" << node.box_.min()[0] << "|" << node.box_.min()[1] << "|" << node.box_.min()[2] << "}"
			<< "|{box max|" << node.box_.max()[0] << "|" << node.box_.max()[1] << "|" << node.box_.max()[2] << "}"
			<< "|{indices"
			;
			
			for (std::int16_t j = 0; j != std::ssize(node.indices_); ++j)
			{
				os << "|<d" << j << ">" << node.indices_[j];
			}
			
			os << "}}\"];\n";
		};
		
		if (gridtree.root_ != -1)
		{
			print_node(gridtree.root_);
		}
		
		for (const auto& node : gridtree.nodes_)
		{
			size_type i = &node - gridtree.nodes_.data();
			print_node(i);
		}
		
		for (const auto& node : gridtree.nodes_)
		{
			size_type i = &node - gridtree.nodes_.data();
			
			if (auto parent = node.parent_; parent != -1)
			{
				os << "node" << i << " -> node" << parent << " [style=dotted];\n";
			}
			
			for (std::int16_t j = 0; j != std::ssize(node.indices_); ++j)
			{
				if (node.indices_[j] != -1)
				{
					os << "node" << i << ":d" << j << " -> node" << node.indices_[j] << ";\n";
				}
			}
		}
		
		os << "}" << "\n";
		
		return os;
	}
	
	std::ostream&& to_dot(std::ostream&& os)
	{
		return std::move(to_dot(os));
	}
	
private:
	size_type root_ = -1;
	Node_repository_type nodes_;
	Object_repository_type objects_;
};
} // namespace nwd::geometry::gridtrees
