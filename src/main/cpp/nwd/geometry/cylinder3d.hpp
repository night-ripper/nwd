#pragma once

#include <ostream>

#include <nwd/geometry/vector3d.hpp>

namespace nwd::geometry
{
struct Cylinder3D
{
	Cylinder3D() = default;
	
	Cylinder3D(Vector3D center, coord_t height, float radius) noexcept
		:
		center_(center), height_(height), radius_(radius)
	{
	}
	
	static Cylinder3D bottom_up(Vector3D bottom, coord_t height, float radius) noexcept
	{
		height /= 2;
		bottom[2] += height;
		return {bottom, height, radius};
	}
	
	Vector3D center() const noexcept {return center_;}
	coord_t height() const noexcept {return height_;}
	float radius() const noexcept {return radius_;}
	
	Cylinder3D& move(Vector3D vector) noexcept
	{
		center_ += vector;
		return *this;
	}
	
public:
	Vector3D center_;
	coord_t height_;
	float radius_;
};

inline std::ostream& operator<<(std::ostream& os, Cylinder3D cylinder)
{
	return os << "Cylinder3D {center: " << cylinder.center() << ", height: " <<
		float(cylinder.height()) << ", radius: " << float(cylinder.radius()) << "}"
	;
}
} // namespace nwd::geometry

namespace nwd::serialization
{
template<> inline constexpr std::ptrdiff_t serialized_size<geometry::Cylinder3D>
= serialized_size<geometry::Vector3D> + serialized_size<geometry::coord_t> + serialized_size<float>;

template<> inline constexpr auto& private_serialize<geometry::Cylinder3D> =
*[](std::byte* target, geometry::Cylinder3D value) -> void
{
	serialize_shift(target, value.center_);
	serialize_shift(target, value.height_);
	serialize_shift(target, value.radius_);
};

template<> inline constexpr auto& private_deserialize<geometry::Cylinder3D> =
*[](const std::byte* source, geometry::Cylinder3D& result) -> void
{
	deserialize_shift(source, result.center_);
	deserialize_shift(source, result.height_);
	deserialize_shift(source, result.radius_);
};
} // namespace nwd::serialization
