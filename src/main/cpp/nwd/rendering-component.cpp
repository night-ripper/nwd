#include "rendering-component.hpp"

#include <cstddef>

#include <fstream>
#include <iostream>

#include <epoxy/gl.h>

using namespace rendering::opengl;

namespace bttf
{
static Rendering_component* current_comp = nullptr;

[[gnu::cold]]
[[maybe_unused]]
static void opengl_debug_callback(
	[[maybe_unused]] GLenum source,
	[[maybe_unused]] GLenum type,
	[[maybe_unused]] GLuint id,
	[[maybe_unused]] GLenum severity,
	[[maybe_unused]] GLsizei length,
	[[maybe_unused]] const char* message,
	[[maybe_unused]] const void* user_parameter)
{
	const char* source_string = "(unknown)";
	
	switch (source)
	{
	case GL_DEBUG_SOURCE_API: source_string = "api"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM: source_string = "window system"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: source_string = "shader compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY: source_string = "third party"; break;
	case GL_DEBUG_SOURCE_APPLICATION: source_string = "application"; break;
	case GL_DEBUG_SOURCE_OTHER: source_string = "other"; break;
	}
	
	const char* type_string = "(unknown)";
	
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR: type_string = "error"; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_string = "deprecated behaviour"; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: type_string = "undefined behaviour"; break;
	case GL_DEBUG_TYPE_PORTABILITY: type_string = "portability"; break;
	case GL_DEBUG_TYPE_PERFORMANCE: type_string = "performance"; break;
	case GL_DEBUG_TYPE_MARKER: type_string = "marker"; break;
	case GL_DEBUG_TYPE_PUSH_GROUP: type_string = "push group"; break;
	case GL_DEBUG_TYPE_POP_GROUP: type_string = "pop group"; break;
	case GL_DEBUG_TYPE_OTHER: type_string = "other"; break;
	}
	
	const char* severity_string = "(unknown)";
	
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH: severity_string = "high"; break;
	case GL_DEBUG_SEVERITY_MEDIUM: severity_string = "medium"; break;
	case GL_DEBUG_SEVERITY_LOW: severity_string = "low"; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: severity_string = "notification"; break;
	}
	
	std::string message_middle = std::string("(source: ") + source_string + ", " +
		"type: " + type_string + ", " + "severity: " + severity_string + "): "
	;
	
	if (severity == GL_DEBUG_SEVERITY_HIGH)
	{
		throw std::runtime_error("OpenGL error: " + message_middle + std::string(message, length));
	}
	else
	{
		std::clog << "OpenGL warning ";
		std::clog << message_middle;
		std::clog.write(message, length);
		std::clog << "\n";
	}
}

static Program anime_program;

Rendering_component* Rendering_component::current()
{
	return current_comp;
}

Rendering_component::~Rendering_component()
{
	current_comp = nullptr;
	anime_program = {};
}

void Rendering_component::set_gamma()
{
	framebuffer_program_.uniform<float>(0, gamma_);
}

void Rendering_component::set_exposure()
{
	framebuffer_program_.uniform<float>(1, exposure_);
}

Rendering_component::Rendering_component()
	:
	framebuffer_width_ {640},
	framebuffer_height_ {360},
	gamma_ {1},
	exposure_ {1.2},
	vao_ {Vertex_array::create()},
	buf_data_ {Buffer::create()},
	buf_indices_ {Buffer::create()},
	ssbo_lights_ {0},
	program_ {
		Vertex_shader {
			std::ifstream("src/rendering/shaders/main.vert"),
			std::ifstream("src/rendering/shaders/shared.glsl"),
		},
		Fragment_shader {
			std::ifstream("src/rendering/shaders/main.frag"),
			// std::ifstream("src/rendering/shaders/colors.glsl"),
			std::ifstream("src/rendering/shaders/shared.glsl"),
		},
	},
	framebuffer_ {Framebuffer::create()},
	framebuffer_vao_ {Vertex_array::create()},
	framebuffer_program_ {
		Vertex_shader {
			std::ifstream("src/rendering/shaders/framebuffer.vert"),
		},
		Fragment_shader {
			std::ifstream("src/rendering/shaders/framebuffer.frag"),
		},
	},
	framebuffer_buf_ {Buffer::create()}
{
	current_comp = this;
	
	anime_program = {
		Vertex_shader {
			std::ifstream("src/rendering/shaders/post.vert"),
		},
		Fragment_shader {
			std::ifstream("src/rendering/shaders/post.frag"),
		},
	};
	
	//// Enables
	{
		glClearColor(0., 0., 0., 0.);
		
#ifndef NDEBUG
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(&opengl_debug_callback, nullptr);
#endif
		glEnable(GL_CULL_FACE);
 		glCullFace(GL_BACK);
		
		glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
		
		// glEnable(GL_MULTISAMPLE);
	}
	////
	
	rescale(framebuffer_width_, framebuffer_height_);
	
	{
		std::uint32_t attribute = 0;
		
		glEnableVertexArrayAttrib(*framebuffer_vao_, attribute);
		glVertexArrayAttribBinding(*framebuffer_vao_, attribute, attribute);
		glVertexArrayAttribFormat(*framebuffer_vao_, attribute,
			Post_data::Vertex_coordinates::elements_per_vertex,
			Post_data::Vertex_coordinates::type,
			Post_data::Vertex_coordinates::normalized,
			offsetof(Post_data, vertex_coordinates)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*framebuffer_vao_, attribute);
		glVertexArrayAttribBinding(*framebuffer_vao_, attribute, attribute);
		glVertexArrayAttribFormat(*framebuffer_vao_, attribute,
			Post_data::Texture_coordinates::elements_per_vertex,
			Post_data::Texture_coordinates::type,
			Post_data::Texture_coordinates::normalized,
			offsetof(Post_data, texture_coordinates)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*framebuffer_vao_, attribute);
		glVertexArrayAttribBinding(*framebuffer_vao_, attribute, attribute);
		glVertexArrayAttribIFormat(*framebuffer_vao_, attribute,
			Post_data::Texture_indices::elements_per_vertex,
			Post_data::Texture_indices::type,
			offsetof(Post_data, texture_indices)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*framebuffer_vao_, attribute);
		glVertexArrayAttribBinding(*framebuffer_vao_, attribute, attribute);
		glVertexArrayAttribFormat(*framebuffer_vao_, attribute,
			Post_data::Color::elements_per_vertex,
			Post_data::Color::type,
			Post_data::Color::normalized,
			offsetof(Post_data, color)
		);
		++attribute;
		
		std::vector<GLuint> buffers(attribute, *framebuffer_buf_);
		std::vector<GLintptr> offsets(attribute, 0);
		std::vector<GLsizei> strides(attribute, sizeof(Post_data));
		
		glVertexArrayVertexBuffers(*framebuffer_vao_, 0, attribute,
			std::data(buffers), std::data(offsets), std::data(strides)
		);
	}
	
	/// Format attributes
	
	{
		std::uint32_t attribute = 0;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		/// Note the big "I" because we are sending the underlying integer
		/// fixed-point coordinates
		glVertexArrayAttribIFormat(*vao_, attribute,
			Vertex_data::Vertex_coordinates::elements_per_vertex,
			Vertex_data::Vertex_coordinates::type,
			offsetof(Vertex_data, vertex_coordinates)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribFormat(*vao_, attribute,
			Vertex_data::Texture_coordinates::elements_per_vertex,
			Vertex_data::Texture_coordinates::type,
			Vertex_data::Texture_coordinates::normalized,
			offsetof(Vertex_data, texture_coordinates)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribIFormat(*vao_, attribute,
			Vertex_data::Texture_indices::elements_per_vertex,
			Vertex_data::Texture_indices::type,
			offsetof(Vertex_data, texture_indices)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribFormat(*vao_, attribute,
			Vertex_data::Relative_position::elements_per_vertex,
			Vertex_data::Relative_position::type,
			Vertex_data::Relative_position::normalized,
			offsetof(Vertex_data, relative_position)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribFormat(*vao_, attribute,
			Vertex_data::Scale::elements_per_vertex,
			Vertex_data::Scale::type,
			Vertex_data::Scale::normalized,
			offsetof(Vertex_data, scale)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribFormat(*vao_, attribute,
			Vertex_data::Quaternion::elements_per_vertex,
			Vertex_data::Quaternion::type,
			Vertex_data::Quaternion::normalized,
			offsetof(Vertex_data, quaternion)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribFormat(*vao_, attribute,
			Vertex_data::Normal_coordinates::elements_per_vertex,
			Vertex_data::Normal_coordinates::type,
			Vertex_data::Normal_coordinates::normalized,
			offsetof(Vertex_data, normal_coordinates)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribFormat(*vao_, attribute,
			Vertex_data::Color::elements_per_vertex,
			Vertex_data::Color::type,
			Vertex_data::Color::normalized,
			offsetof(Vertex_data, color)
		);
		++attribute;
		
		glEnableVertexArrayAttrib(*vao_, attribute);
		glVertexArrayAttribBinding(*vao_, attribute, attribute);
		glVertexArrayAttribFormat(*vao_, attribute,
			Vertex_data::Ambient_color::elements_per_vertex,
			Vertex_data::Ambient_color::type,
			Vertex_data::Ambient_color::normalized,
			offsetof(Vertex_data, ambient_color)
		);
		++attribute;
		
		std::vector<GLuint> buffers(attribute, *buf_data_);
		std::vector<GLintptr> offsets(attribute, 0);
		std::vector<GLsizei> strides(attribute, sizeof(Vertex_data));
		
		glVertexArrayVertexBuffers(*vao_, 0, attribute,
			std::data(buffers), std::data(offsets), std::data(strides)
		);
		glVertexArrayElementBuffer(*vao_, *buf_indices_);
		
		program_.uniform<float>(0, geometry::coord_t::epsilon());
		program_.uniform<>(3, std::int32_t(Vertex_data::Texture_indices::color_only().binding));
		
		ssbo_lights_.bind();
		
		set_gamma();
		set_exposure();
	}
}

void Rendering_component::rescale(float width, float height)
{
	framebuffer_width_ = width;
	framebuffer_height_ = height;
	
	texture_ = {0, Texture::Format::rgb_f_16, int(framebuffer_width_), int(framebuffer_height_)};
	texture_.storage();
	texture_.parameter(Texture::Magnifying::nearest);
	texture_.parameter(Texture::Minifying::nearest);
	framebuffer_.attach(texture_, Framebuffer::Attachment::color);
	
	renderbuffer_ = {Texture::Format::depth_24, int(framebuffer_width_), int(framebuffer_height_)};
	renderbuffer_.storage();
	
	framebuffer_.attach(renderbuffer_, Framebuffer::Attachment::depth);
}

void Rendering_component::draw(geometry::Vector3D camera_position, const glsl::mat4& camera_matrix)
{
	GLint viewport[4];
	
	{
		vao_.bind();
		
		program_.use();
		
		program_.uniform(1,
			camera_position.x().value(),
			camera_position.y().value(),
			camera_position.z().value()
		);
		program_.uniform<4, 4>(2, static_cast<const float*>(camera_matrix));
		
		framebuffer_.bind(Framebuffer::Target::read_draw);
		
		glGetIntegerv(GL_VIEWPORT, viewport);
		glViewport(0, 0, framebuffer_width_, framebuffer_height_);
		
		glDepthMask(true);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		
		glNamedBufferData(*buf_data_,
			std::ssize(vdata_) * sizeof(*std::data(vdata_)),
			std::data(vdata_), GL_DYNAMIC_DRAW
		);
		
		glNamedBufferData(*buf_indices_,
			std::ssize(vindices_) * sizeof(*std::data(vindices_)),
			std::data(vindices_), GL_DYNAMIC_DRAW
		);
		
		glNamedBufferData(*ssbo_lights_,
			std::ssize(vlights_) * sizeof(*std::data(vlights_)),
			std::data(vlights_), GL_DYNAMIC_DRAW
		);
		
		glDrawRangeElements(GL_TRIANGLE_STRIP, 0, last_index_, std::ssize(vindices_),
			enum_of<Element_index::index_type>, nullptr
		);
		
		////////////////////////////////////////////////////////////////////////
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		glDepthMask(false);
		
		glNamedBufferData(*buf_data_,
			std::ssize(vdata_transparent_) * sizeof(*std::data(vdata_transparent_)),
			std::data(vdata_transparent_), GL_DYNAMIC_DRAW
		);
		
		glNamedBufferData(*buf_indices_,
			std::ssize(vindices_transparent_) * sizeof(*std::data(vindices_transparent_)),
			std::data(vindices_transparent_), GL_DYNAMIC_DRAW
		);
		
		glDrawRangeElements(GL_TRIANGLE_STRIP, 0, last_index_transparent_, std::ssize(vindices_transparent_),
			enum_of<Element_index::index_type>, nullptr
		);
		
		vdata_.clear();
		vdata_transparent_.clear();
		vindices_.clear();
		vindices_transparent_.clear();
		vlights_.clear();
		last_index_ = 0;
		last_index_transparent_ = 0;
		
		framebuffer_.unbind();
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	{
		framebuffer_vao_.bind();
		framebuffer_program_.use();
		
		glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
		glClear(GL_COLOR_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		using utility::nrm;
		
		constexpr Post_data data[] = {
			{{nrm(-1.f), nrm(-1.f)}, {nrm(0.f), nrm(0.f)}, {{}, {}}, {}},
			{{nrm(1.f), nrm(-1.f)}, {nrm(1.f), nrm(0.f)}, {{}, {}}, {}},
			{{nrm(-1.f), nrm( 1.f)}, {nrm(0.f), nrm(1.f)}, {{}, {}}, {}},
			
			{{nrm(1.f), nrm(-1.f)}, {nrm(1.f), nrm(0.f)}, {{}, {}}, {}},
			{{nrm(1.f), nrm( 1.f)}, {nrm(1.f), nrm(1.f)}, {{}, {}}, {}},
			{{nrm(-1.f), nrm( 1.f)}, {nrm(0.f), nrm(1.f)}, {{}, {}}, {}},
		};
		
		glNamedBufferData(*framebuffer_buf_, sizeof(data), data, GL_DYNAMIC_DRAW);
		
		glDrawArrays(GL_TRIANGLES, 0, 6);
		
		////////////////////////////////////////////////////////////////////////
		
		anime_program.use();
		
		glNamedBufferData(*framebuffer_buf_, std::ssize(post_triangles_) * sizeof(Post_data),
			std::data(post_triangles_), GL_DYNAMIC_DRAW
		);
		
		glDrawArrays(GL_TRIANGLES, 0, std::ssize(post_triangles_));
		
		post_triangles_.clear();
	}
}
} // namespace bttf
