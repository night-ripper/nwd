-std=c++2b

-Isrc/main/cpp
-I/usr/include/freetype2
-I/usr/include/libpng16
-I/usr/include/harfbuzz
-I/usr/include/glib-2.0
-I/usr/lib64/glib-2.0/include
-I/usr/include/sysprof-4

-Wall
-Wextra
-Wconversion

-D_GLIBCXX_DEBUG
-D_GLIBCXX_ASSERTIONS

-D__TBB_ALLOW_MUTABLE_FUNCTORS
